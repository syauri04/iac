-- MariaDB dump 10.17  Distrib 10.4.12-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: iac_dbdev
-- ------------------------------------------------------
-- Server version	10.4.12-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `id_banner` int(15) NOT NULL AUTO_INCREMENT,
  `type` enum('Slide','Iklan') NOT NULL,
  `image` text NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `url_mobile` text DEFAULT NULL,
  `url_web` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` int(15) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(15) DEFAULT NULL,
  PRIMARY KEY (`id_banner`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` VALUES (1,'Slide','15945540071594554007.jpg','Cara Upload Event',NULL,'https://mobilretroklasik.com/indonesia-automotive-community/cara-upload-kegiatan-komunitas-di-aplikasi-iac-indonesia-automotive-community.html','https://mobilretroklasik.com/indonesia-automotive-community/cara-upload-kegiatan-komunitas-di-aplikasi-iac-indonesia-automotive-community.html','2020-02-27 05:55:11',1,'2020-07-12 11:40:07',1);
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_boarding`
--

DROP TABLE IF EXISTS `com_boarding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_boarding` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `summary` text DEFAULT NULL,
  `order_by` char(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_boarding`
--

LOCK TABLES `com_boarding` WRITE;
/*!40000 ALTER TABLE `com_boarding` DISABLE KEYS */;
INSERT INTO `com_boarding` VALUES (1,'15869497331586949733.jpeg','Monetize your merchandise','3','2020-03-29 09:19:20','2020-04-15 03:22:13'),(2,'15869497151586949715.jpg','Manage Your Community','2','2020-03-29 09:19:44','2020-04-15 03:21:55'),(3,'15869496711586949671.jpg','Blast Your Event','1','2020-03-29 09:25:26','2020-04-15 03:21:11');
/*!40000 ALTER TABLE `com_boarding` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_event`
--

DROP TABLE IF EXISTS `com_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `community_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` enum('Ongoing','Done') NOT NULL,
  `like` char(10) DEFAULT NULL,
  `dislike` char(10) DEFAULT NULL,
  `tertarik` char(10) DEFAULT NULL,
  `siap_hadir` char(10) DEFAULT NULL,
  `view` char(10) DEFAULT '0',
  `user_create` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_event`
--

LOCK TABLES `com_event` WRITE;
/*!40000 ALTER TABLE `com_event` DISABLE KEYS */;
INSERT INTO `com_event` VALUES (21,18,'7eab248f8c1bdbbcc54fd9c0922654b7.jpg','Jakarta Breakfast Meet Up','2020-06-27','2020-06-27','06:00:00','12:00:00','Ranch Market Pondok Indah, Jakarta','Halo sahabat GROUP OTOMOTIF 1990\n\nDengan adanya rencana kami untuk mengadakan breakfast meet up temu kangen GROUP OTOMOTIF 1990 yg rencana diadakan tgl 27 Juni 2020 di Ranch Market Pondok Indah Jakarta, Kami sangat terkejut dgn respon teman\" semua, kami sudah mendata lebih dari 100 mobil yg bakal hadir di acara tersebut (yang awalnya kami kira hanya bbrp mobil saja). \n\nAdapun kekhawatiran kami dr team GO90\'s akan ada penumpukan peserta yg akan hadir begitu banyak dan menimbulkan \"Cluster baru\" di pandemi ini dan kami sedang mencari solusi terbaik untuk kelanjutan acara tersebut.\n\nMengacu Permenkes no 9 thn 2020 tentang pelarang keras adanya acara atau perkumpulan yg melibatkan kerumunan banyak orang akan ada tindakan hukum dr pemerintah krn adanya Pandemi yg sedang berlangsung.\n\nSetelah kami berkonsultasi dengan rekan2 dan para senior pelaku otomotif, Kami selaku team GO90\'s dgn rasa sangat menyesal akan meninjau dan mengundur ulang acara tersebut hingga waktu & tempat yang akan ditentukan di kemudian hari.\n\nKami akan mengumumkan kembali waktu dan tanggalnya.\nDengan segala kerendahan hati kami mohon maaf atas keputusan ini 🙏\n\nSemoga dengan pengumuman ini rekan2 semua agar dapat memaklumimya.\n\nTerima kasih atas atensi dan masukan yang luar biasa sekali dari rekan2 semua termasuk senior2 otomotif di Indonesia atas acara ini.\n\nBersilaturahmi memang diwajibkan, tetapi kami sangat mementingkan kesehatan dan kenyamanan rekan2 semua 🙏\n\nRegards,\nGO90\'s Team','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-06-26 16:31:37',NULL),(22,18,'0c41389e0dc0f49675483a49213c4baf.jpg','Mid Year Meet 2020','2020-06-28','2020-06-28','09:00:00','12:00:00','Resto Cerita Rasa,  Jalan Ampera Raya 9, Cilandak','By Corolla Retro Club','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-06-26 16:39:16',NULL),(23,18,'5b337265611b18c8ac640944f7ebdd13.jpg','Festival Modifikasi 2','2020-07-11','2020-07-12','08:00:00','17:00:00','Online','Undangan terbuka, \nTanpa mengurangi rasa hormat dan persaudaraan, kami (GARIS enterprise) ingin mengundang teman dan sahabat yang mempunyai dan menyukai mobil dengan tampilan modifikasi untuk mengikuti \"Festival Modifikasi 2\". Terbuka untuk semua tipe, semua merek & semua aliran .\n11 - 12 Juli 2020, ONLINE. Detail WA : (+62) 818838099','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-06-26 16:43:37',NULL),(25,19,'a2742004b355340a7995cf1bfc799c23.jpg','Jamda 3 YRKI','2021-04-02','2021-04-04','08:00:00','19:00:00','Yogyakarta','INFO RESMI JAMDA 3\nYRKI PENGPROV DIY\n\nHellow Kinggers\n\nSalam Raja.........!!\n\nMenindaklanjuti hasil Kesepakatan Pengurus YRKI DIY & Panitia JAMDA 3 YRKI DIY serta Pihak EO Supertrack bahwa utk pelaksanaan kegiatan JAMBORE DAERAH 3 YRKI DIY \"DI UNDUR\" pelaksanaannya yaitu pada tanggal 2, 3 & 4 April 2021 dikarenakan adanya Wabah Nasional Covid 19.\n\nDemikian pemberitauan ini kami sampaikan, mohon kiranya agar segenap Kingers Se_Nusantara dapat memahami dan memakluminya.\n\nSalam,\nPENGPROV YRKI DIY\nPANITIA JAMDA 3 YRKI DIY\nSUPERTRACK CLOTHING','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-06-26 16:59:30',NULL),(27,18,'456ea61db56c7abaae007ddda70f7945.jpg','SOTR W124 MBCI','2020-06-28','2020-06-28','08:00:00','02:00:00','Pabrik Badjoe Bandung','PSBB lewat juga nih,kuy akh manteman kita kumpul lagi,ngupi2 bareng sambil silahturahmi di SOTR MBCI Bandung Chapter,jangan lupa bawa masker dan hand sanitizer ya gaeess..kita tetap terapkan standar protokol kesehatan masa pandemi.\n\nHeill MBCI....','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-06-27 12:25:50',NULL),(28,19,'3c5f414358ecd31d5069b6672669ef5d.jpg','Touring Wisata RX King','2020-07-11','2020-07-11','07:00:00','17:56:00','Pagar Alam','Ayo sedulur rx king yg mau ikut touring wisata jadwal berubah dari yg ditentukan sebelum nya dan sekarang sudah deal di tanggal 11 juli 2020','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-06-28 06:56:36',NULL),(32,18,'0358e166b18ddd3268f5c166b530ea17.jpg','ABL Goes To Bengkulu','2020-10-28','2020-10-30','06:00:00','05:00:00','Sumatera Selatan','Diselenggarakan oleh  Ampera Bus Lover\'s Reborn (Komunitas Penggemar Bus) Sumatera Selatan.\nBiaya: Rp350.000,- \nFasilitas: Makan Prasmanan, wisata dan hotel\nCP: 085377716859 (Alhadi)','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-06-29 01:05:34',NULL),(37,19,'d12e7b4516bb9b00d90637d7244d8710.jpg','Ngopi Bareng Hawa Dingin','2020-07-04','2020-07-04','08:00:00','17:00:00','Tawang Mangu','JOIN GAN....\n\nDiselenggarakan oleh SEDOYO HEREX dan TEAM MORAT MARET\n\n- Wajib masker\n- Safety Ridding \n- CB GL MP Tiger C-Series \n\nNB:\n1. Ra usah banter-bantaran pokok taat peraturan.\n2. Oleh bucin, asal yang e diopeni dewe-dewe','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-06-29 04:31:57',NULL),(38,19,'0ff53239ca98807d500a5053e1307a77.jpg','Temu Kangen CB Berbek','2020-07-04','2020-07-04','08:00:00','17:00:00','Tawangmangu','Gas Road to Tawangmangu\nWajib memakai masker\nSafety Ridding\nTeman sejalan (golek konco gak golek bondo)\n\nDiselenggarakan oleh Nganjuk CB Club','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-06-29 19:34:37',NULL),(39,19,'2b4969b5dc980d50655cb6366ee68b90.jpg','Kopdar Honda Wave','2020-07-04','2020-07-04','21:00:00','11:55:00','Jl. Panglima Polim Raya, Melawai, Kebayoran Baru, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta. \nPatokan ( Seberang Ace Hardware Panglima Polim ).','Buat kalian pengguna Wave atau motor bebek Honda yang ada di Wilayah Jabodetabek, yuk merapat kita nongkrong dan ngopi bareng ramaikan dengan canda tawamu di kopdaran kami.\n#sharing\n#ngopi\n#KenalanMemberBaru\n#forum\n#BahasApaYangPerluDibahas\n#BahasAnniv6Hwi\n\nMasih bingung mau dateng ke kopdaran ?\natau bingung mau bareng siapa🚶ngga usah bingung, Kalian bisa menghubungi kontak di bawah ini buat jalan bareng ke tempat kopdar biar lebih mesra.\n\nWilayah Citayam, Jawa Barat :\n➡Domisili (Citayam, Sawangan, Parung, Masjid Kubah Mas, Rawa Denok dan sekitarnya) 📞⤵\nAnwar Chairul (mamangs) : 082112454785\nM.Iqbal amrullah : \nWilayah Depok, Jawa Barat :085691978041\n\n➡Domisili (Margonda, Kelapa Dua, Cimanggis dan sekitarnya) 📞⤵\nMaulana Yusuf : 089670332416\n\nWilayah Jakarta Selatan :\n➡Domisili (Pondok Labu, Cinere, Ragunan, Cilandak, Fatmawati dan jagakarsa ) 📞⤵\nghemplonk :  085710952373\nIrfan Rahmadi Wiyanto. : 089504234489\n\nWilayah Jakarta Timur :\n➡Domisili (Ciracas, Cilangkap, Kranggan, Cibubur, Kalisari, Cijantung dan sekitarnya)📞⤵\nRickson. : 083871561351/081807453219\naray. : 085886227047\n\nWilayah Bekasi, Jawa Barat :\n➡Domisili (Jati asih, Pondok gede, TMII, BKT, Kalimalang, Rawamangun dan sekitarnya) 📞⤵\nAgung Manullang : 089630773717 (cari jodoh)\nAngga. : 089692667055\n\nWilayah Jakarta Selatan :\n➡Domisili (Kalibata, Ps.minggu, Mampang, Pancoran, Pejaten, Kemang) 📞⤵\nChairul Fajrin (bagol ajin) : 089622861224\njhon : 087779530965\n\nWilayah Jakarta Selatan :\n➡Domisili (Manggarai, Matraman, Kp.melayu, Setiabudi, Menteng dan sekitarnya) 📞⤵\nAditya Ramadhany Putra (gege) : 087888441913\n\nDiselenggarakan oleh Honda Wave Indonesia','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-06-30 07:04:45',NULL),(40,19,'ce25168620296b5b30ffda97d79c30fc.jpg','Ngopi Bareng Dulur Herex','2020-07-04','2020-07-04','18:00:00','23:55:00','Taman Seribu Lampu, Cepu','- Wajib Masker\n- Safety Ridding \n- CB GL MP Tiger C-series\n\nCP:\nJaka (Admin Bikers Selonjor Tuban) 085707692512\n\nAmel (Team Resah Racing Bojonegoro) 0853182836','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-06-30 16:13:34',NULL),(41,18,'fda64c4ecefcf2d4ab36cd771b515ad5.jpg','Kopdar Regional Banten','2020-07-05','2020-07-05','11:00:00','17:00:00','Kawasan Milenium Cikupa','Diselenggarakan oleh:  Corolla Twin Cam Owners\nClub BANTEN','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-01 00:30:30',NULL),(42,18,'2a0877cd436386b1c757f6f5bc6dee78.jpg','Kopdar Corolla Twincam','2020-07-05','2020-07-05','10:00:00','17:00:00','Gedung Juang 45, Jalan Sultan Hasanudin, Tambun Bekasi','Kopdar Wajib Bulanan Sub Reg Bekasi Raya\n\nDiselenggarakan oleh Toyota Twin Cam Owners Club','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-01 04:25:28',NULL),(43,19,'2aa89faf319b9c3c9c22377bd32dd44a.jpg','Kopdar Gabungan Granderist','2020-07-18','2020-07-18','20:00:00','11:55:00','Taman Kota Metro Lampung','Silaturahmi dan ngopi bareng para Granderist / pecinta Grand plat BE ke 8','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-01 05:02:36',NULL),(44,19,'a5b7a7d990aafc3c0e08789b85574198.jpg','Kopdar Awal Bulan Sedulur 120ers','2020-07-03','2020-07-03','19:00:00','23:55:00','Kediaman Mas Yudi Sumowono','Pembukaan kopdar awal bulan, sedulur 120 ers Ungaran.\nSemoga sedulur semua sehat, berkah dan berkenan hadir dan tidak ada halangan yg berarti.\nSalam \"Sempak Teles\" \nGak ada kalian gak gayeng ...\n\nDiselenggarakan oleh SUZUKI SATRIA 2TAK UNGARAN (SATRIA 120ERS JATENG DIY','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-01 05:42:13',NULL),(45,19,'d89e537747c9691117bc6bf39efefa85.jpg','Kopdar BESFIC East Jakarta','2020-07-04','2020-07-04','20:00:00','23:55:00','Jl.Raden Inten II - Jakarta Timur (Ruko Domino\'s Pizza - Seberang MCD Buaran Duren Sawit)','SALAM BIKERS 👊🏻😎\n\n Info Kopdar BESFIC East Jakarta\n\n Assalamu\'alaikum wr. wb.\n\nDiinformasikan kepada seluruh Member, Prospek, & Calon Anggota BESFIC East Jakarta untuk hadir.\n\nUntuk yang baru mau gabung, sekarang sudah bisa yak!!! \n\nSyarat & Ketentuan Kopdar \n- Motor Honda CB150R All Series \n- Full menggunakan pakaian Safety (Helm SNI, Jaket, Sarung Tangan, Sepatu). \n- Gunakan masker dan tetap disiplin menerapkan protokol kesehatan. \n\nJangan Lupa Juga Follow & Like \nIG : @besficeastjakarta \nFB : BESFIC East Jakarta \nCP HUMAS I : 085771435308 \nCP HUMAS II : 088224796760 \n\nDemikian informasi yang bisa kami sampaikan, atas perhatiannya kami ucapkan terimakasih. \n\nWassalamu\'alaikum wr.wb  \n\n#besficeastjakarta #asosiasistreetfirejakarta #asosiasistreetfireindonesia #cb150r #hondacb150r #cb150rjakarta #cb150rjakartatimur #clubcb150rjakarta #cb150rcommunityjakarta https://instagr.am/p/CCDfw2XH3uw/','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-01 05:47:49',NULL),(46,18,'3bb26cd77dfb39783c64556ebc673d0b.jpg','Halal Bihalal CTOC','2020-07-05','2020-07-05','09:30:00','16:00:00','Cantik Darma Park PMPP TNI - Jamblang Sentul','Kopdar Keluarga Besar CTOC (Corolla Twincam Owners Club) Regional Jakarta\n\nInfo Tikum: Rest Area Cibubur Jam 08.30','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-01 06:52:24',NULL),(47,18,'f61d090cf4c17415baec0b7b6b217666.jpg','Silaturahmi CCTV & Driver Truck','2020-07-12','2020-07-12','10:00:00','17:00:00','Pantai Widuri, Jalan Tos Sudarso,  Widuri, Pemalang','Kopdar Ketiga CCTV Perbatasansantuy CCTV Pemalang.\nHTM: 15K (Free Stiker & Snack)\nNyodrek bareng\nFoto bareng\nWajib Masker','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-01 15:57:46',NULL),(48,19,'64447e79a8bb4d5414d57af4252f0d18.jpg','Tourgab','2020-07-04','2020-07-05','18:00:00','17:00:00','Santolo Beach','Gass 😍\nAyo yg mau ikut gabung atau info lebih lanjut Hub saya. \nsilahkan klick Link di bawah ini, selanjut ny akan langsung terhubung personal chat dengan saya \n\nwa.me/62895703147978\n\n\nTikum : Jalan Terusan Cibaduyut No 84\nHari Sabtu tgl 4\nPukul : 18.00 Wib','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-01 19:30:57',NULL),(49,18,'5163d6852e5d46f50796f745e3fe7a14.jpg','Kopdar KDGI AGRAYA','2020-07-05','2020-07-05','09:00:00','17:00:00','Pantai Gemah Tulungagung','Assalamualaikum warahmatullohi wabarokatuh\nSalam sejahtera untuk semua\n\nDemi memeriahkan &  menyukseskan acara, dimohon kehadiran semua keluarga KDGI AGRAYA pada acara tersebut. \nTerima kasih\n\nNB:\nSetiap Mobil dimohon membawa tikar, camilan, makanan minuman, payung Seperti pada kopdar-kopdar sebelumnya.  JANGAN LUPA MEMBAWA MASKER & TETAP TERTIB PROTOKOL KESEHATAN. \n\nSalam Dari Kita Untuk Kita\nKDGI AG RAYA\n\n#KDGIAGRAYA\n#KomunitasDatsun\n#KOPDAR\n#Juli2020','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-02 18:59:27',NULL),(50,19,'2ef8bb92d1cf5bde62c2c9fe7e94bd34.jpg','Kopdar WAVE ALL STAR Jakarta','2020-07-04','2020-07-04','21:00:00','11:55:00','Depan harvest senopati - jaksel','🌟assallammualaikum🌟\n\n⭐INFO KOPDAR⭐\n\nWave All Star Indonesia\n_*Zone Jakarta*_\n\n\n•Agenda :\n-Pembahasan internal\n-ngopii santuyy\n- dll\n\n•Bagi member lama di harapkan kehadiran nya, (WAJIB memakai masker)  dan bagi yang mau gabung silahkan jangan sungkan. Karena kami disini memandang solidaritas kekeluargaan bukanlah memandang seberapa bagus nya motor yang di bawa.\n•dihar\n•Di harapkan untuk selalu safety ridding dengan membawa : \n\n-helm\n-jaket\n-sepatu\n-perlengkapan surat\" kendaraan.\n\n⚠Dilarang membawa senjata tajam/api, obat\"an terlarang sejenis narkoba dll,& minuman keras/oplosan.\napabila melanggar kami akan memberikan sangsi kepada para pelaku tsb.⚠\n\nJangan lupa untuk wajib membawa uang kas sebesar 10.000 rupiah\nBagi yang mau bergabung silahkan hubungi cp di bawah ini :\n\nPUSAT :\nAri : 089530210693\n\nUTARA :\nRahmatt : 0857-1016-3441\n\nBARAT :\nAnggahsun : 08987907796\n\nTIMUR :\nBagas : 08159554020\n\nSELATAN : \nAlfi : 085840054928\n\n#zonejkt48\n#karenawavekitabersatu\n#salambodygetar\n#teambarbar\n#jakartasolidfamily\n#nggaadawavetetapkeluarga\n#zoneantibucin','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-02 20:54:48',NULL),(53,19,'0e862cdbebe803bc9ac40f8511c91cac.jpg','PARAT #6','2020-08-01','2020-08-01','07:00:00','15:00:00','Sahabat Tani Fair, Riung Gunung, Pangalengan, Bandung','Diselenggarakan oleh Pangalengan Adventure Trail\n\nIDR 200K\nJersey Eksklusif, ID Card, Scrut, Makan & Live Musik\n\nDOORPRIZE\n1 unit mobil\n1 unit sepeda motor\nBerikut seabreg hadiah hiburan\n\nDimeriahkan oleh:\nMC KONDANG: Wan Van Haw & Jaja Soulmate\nALAY BINTANG PANTURA\n\nCONTACT PERSON:\nYayan Doyok 0852 2075 9548\nAbah Gares 0813 2294 4252\nDede Goler 0823 2102 7301\nCIWONX BATARA 0812 9273 4421\n\nJALUR 90% PARAWAN 10% RANDA TILAS TAPI RAOS','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-04 08:38:18',NULL),(54,19,'1b5b510eabb9bfd97674c0b7a8802bc9.jpg','Gass Amal','2020-07-26','2020-07-26','06:00:00','18:00:00','Lapangan Sepak Bola Ds. Negara Saka, Kec. Negeri Katon, Kab Pesawaran, Lampung','Donasi 70K Non Asuransi, untuk Pembangunan Mesjid Nurul Hidayah Desa Negara Saka, ke BRI A.n. Muhyidin.\n\nFASILITAS: Makan Siang, Sticker, Jalur & Evakuasi\n\nNOTE:\n- Setiap peserta dari luar kota membawa suket kesehatan\n- Peserta diwajibkan memakai masker dan membawa handssanitizer\n- Peserta wajib mematuhi protokol kesehatan, mencuci tangan & jaga jarak dengan peserta/kelompok lain\n\nCONTACT PERSON:\n- Papah Rooni 0882 8625 8737\n- Rohim Peta 0853 8228 3242\n- Am1x Peta 0812 7904 6222\n- Yidin Tagana 0852 7969 5614','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-04 16:38:20',NULL),(55,19,'6cba7f9b98aa6f2d1d40619d9080deb0.jpg','Baksos Al-Hizrah','2020-07-19','2020-07-19','08:00:00','18:00:00','Parapatan Citeko, Kp. Sinargalih -Desa Citeko Kaler, Plered, Purwakarta','Memperingati Hari Bhayangkara ke-74\n\nDONASI\n- 120K: Baksos, Jersey, Makan, Scrut, Jalur\n- 50K: Baksos, Makan, Scrut, Jalur\n\nON-TRACK EXPLORE:\n- Bukit Singalengis\n- Gunung Merak\n- Bukit Lutung\n- Batu Cula\n\nJALUR:\n- 100% Full Tanah\n- Full Tanjakan\n- Tanjakan Hiburan\n- Firdan Sport\n- SJ075\n- KTJ\n\nCONTACT PERSON:\n- Banet 0819 0944 4464\n- Gumilar 0877 7871 7779\n- Asep Panjul 0878 4743 3932\n- Dadan Firdan Sport 0859 5164 1674','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-04 17:28:53',NULL),(56,19,'7b1e2c81806b679b796a471303f695f6.jpg','Kopdar SSFC KORWIL BALAM','2020-10-23','2020-10-23','20:00:00','11:55:00','Pelataran Indomaret Balam km08','Assalamualaikum Wr. Wb. Selamat sore gaes selamat berbahagia semoga selalu dilimpahkan rejeki & kesehatan teman teman. \n\nAGENDA: Forum, Nongkrong, Ngopi,dan masih banyak lagi dah pokoknya\n\nUntuk anggota inti & partisipan SSFC Korwil balam mari kita ramaikan kopdar malam Sabtu ... & jangan lupa safety ride ya. Karna keselamatan yang utama.\n\nSYARAT DAN KETENTUAN:\n- Mempunyai Satria FU / Satria FU Injeksi ?\n- STNK & Sim C\n- Spion Terpasang & Plat Nopol Terpasang.\n- Lampu Rem belakang menyala Merah normal. \n- Memakai Celana Panjang/Jeans.\n- Memakai Jaket & Sepatu\n- Helm boleh HalfFace / Fullface.\n- perlengkapan Safety riding & gear lain nya.. \n\nCome to Join...!!! Dan untuk informasi yang Terima kasih, see you kopdaran, bingung cara nya gabung lebih jelasnya bisa chat member kita nih.\n\nCONTACT PERSONA:\n- Eko Putra Handoko 081271211068 \n- Sidiq 082284408924 \n- Kiki Prasetyo 082272003063','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-05 01:18:11',NULL),(57,19,'e533514e5b52d8c59141d7db28e91a17.jpg','Trail Adventure','2020-07-19','2020-07-19','06:00:00','18:00:00','Ponpes Al Majidi, Selodakon Tanggul Jember','Bansos Jaguar Pademi Corona 19\n\nHADIAH UTAMA:\n- 1 Unit Motor Second\n- Sepeda Gunung\n- Puluhan Doorprize\n- Tanjakan Berhadiah Uang Tunai\n\nPENDAFTARAN 60K\nFasilitas: ID Card, Stiker, Kaos, Makan Siang + Snack\n\nMC. AHONG\n\nCONTACT PERSON:\n- Ali Maxxi 081 235 210 23\n- Faruk Afero 085 232 757 524\n- Imam 085 213 470 148\n- Arifin Dalbong 082 237 892 221\n- Saiful Doank 082 140 755 281\n- Pak Jenggot 085 105 260 686\n\n\nSEKRETARIAT JAGUAR:\nJl. DR. Subandi No.02 Tanggul Kulon\nTanggul - Jember\nHP 085 232 757 524','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-05 01:53:43',NULL),(58,19,'17b1c842cca261d692e1370cf9eb1e6c.jpg','KNPI Trail Adventure','2020-08-09','2020-08-09','06:00:00','18:00:00','Lapang Cibatu - Purwakarta','Naratas Purwakarta Istimewa\nRegist Rp200.000, Include: Jersey - ID Card - Scrut - Lunchbox - Undian\n\nGRANDPRIZE:\n- 1 Unit Double Cabin\n- 1 Paket Umroh\n- Uang Tunai 10 0 Juta\n- 2 Motor Matic\n- Produck Gordon\n- Dan Hiburan Lainnya\n\nMC:\n- Uncle Boenk\n- Bayi Stones\n- Ayu Kuya\n- Kusye d\'Bodor (Kang Odih Tukang Ojeg Pengkolan)\n\nINFORMATION:\n- Iwan Ciwonk 0870 0522 7888\n- Ajay 0813 1648 5454\n- Pendi Akbar 0838 1677 6065\n- Gugun JR 0821 3005 2427\n- Yayan Doyok 0852 2076 9548\n- Andi Jabar 0813 2007 7888\n- Arix J: 0813 1993 3789','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-05 05:24:51',NULL),(59,19,'08d5ea81bd988f930d9a500ab821540e.jpg','GRASSTRACK (Uji Coba)','2020-07-11','2020-07-12','06:00:00','18:00:00','Sirkuit Annisa Sukanagara Cianjur','Memperingati Hari Jadi Sukanagara Ke 60\n\nKELAS:\n- Bebek Standar Pemula\n- FFA Pemula\n- Bebek Modifikasi Open\n- FFA Open\n- Bebek Standar 15thn\n- Matic Open\n- Matic Pemula\n- Mini Moto s/d 10thn\n- Mini Moto s/d 13thn\n- Bebek Standar Pemula B Lokal Cianjur\n- FFA Pemula B Lokal Cianjur\n- FFA Pemula C Lokal Cianjur\n- Matic Wanita\n- Adventure Non Pembalap 30+\n- Vespa Standar\n- Vespa FFA\n\nJUARA UMUM OPEN (Domba)\n\nCONTACT PERSON: Hendra (0817 705 607)','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-05 05:43:29',NULL),(60,18,'ba9bc6b610559867479b082387afbc80.jpg','Kopdar KCBest','2020-07-12','2020-07-12','07:00:00','05:00:00','Pantai Muara Bungin, Bekasi','Assalamu\'alaikum.....\n\nSalam sejahtera untuk semuanya...\nApa kabar Om dan Tante pengguna Karimun, udah pada kangen kan dengan Kopdar, touring, becanda dan silaturahmi dengan keluarga Besar KCBest...?\n\nAyo ajak keluarga di acara KOPDAR Perdana setelah dihentikanya masa PSBB sekaligus \"Mini touring KC BEST 2020\"\n\nDi lokasi kita akan melakukan kegiatan pemotongan tumpeng sukuran HUT KCBest yg ke\" 5\", Baksos penyerahan pakaian layak pakai dan masker, melakukan bersih bersih pantai, menandatangani dukungan untuk pantai cabang bungin sebagai objek wisata kebanggaan Bekasi, dan bakar ikan bersama.\n\nTikum di masing2 wilayahnya\nYuk, jangan sia-siakan kesempatan kali ini pasti seru lho.\nBagi Member baru & Calon member di wajibkan hadir, untuk perkenalan/penempelan sticker.\n\nAYOOOO..... IKUTTTTTT KOPDAR PERDANA INI......!!!!\n\nCc. Ketua dan All pengurus','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-05 06:01:50',NULL),(61,21,'a9e4b8ef163badad4bca6e106db37ed1.jpg','Ngumpul Kuy','2020-07-10','2020-07-10','17:00:00','23:55:00','Rest Area KM7 BSD','kopdar','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-07 16:23:59',NULL),(62,19,'25bf774487ce9ecd25ee28a38cd93522.jpg','Halal bi Halal','2020-07-12','2020-07-12','08:00:00','17:00:00','Basecamp Papatong Cibeunying, Cipeundeuy Subang','Diselenggarakan oleh Subang Ngahiji (SUJI) Trail Adventure Community Korwil Barat\n\nThe Power Of Udunan\nDari Kita Untuk Kita\nUdunan 50K (makan + buff)\nJalur by: Papatong Trail Adventure\n\nManjangkeun Silaturahmi, Mageuhkeun Duduluran','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-07 16:55:50',NULL),(63,19,'510abf7b8d20d621cd38889df6b7eea0.jpg','Halal Bihalal Ubar Covid','2020-07-25','2020-07-25','08:00:00','17:00:00','Desa wisata cibuntu, kec. Pesawahan\nKab. Kuningan - Jawa Barat','Diselenggarakan oleh TRACK\nRegistrasi Seikhlasnya\nThe Power of Papatoengan\n\nAcara:\n- Halal Hihalal\n- Gasss Tivis-tivis Ajah\n- Santunan Anak Yatim\n- Pembagian Sembako\n- Baksos Masjid Desa Cibuntu','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-07 17:18:36',NULL),(64,19,'063996e7699bea5d69da2048bef37b19.jpg','FUN RACE ENDURO','2020-08-15','2020-08-16','08:00:00','17:00:00','Sirkuit Samolo Kalapanunggal Sukabumi','KALAPANUGGAL FUN RACE ENDURO\nSpesial Hari Kemerdekaan Republik Indonesia ke 75\n\nKelas Race:\n- FFA Open\n- Adventure Open\n- Bebek open\n- Matic Open\n- Supermoto Open\n- Built Up Special Engine\n- FFA Lokal Kec. Kalapanunggal\nClassic Open\nFFA Lokal 4 Kecamatan (Kalapanunggal, Kabandungan, Bojonggenteng, Cikidang)\n\nMC Dudih Anune\n\nIDR 100K\nNo. Rekening:\n-BCA 3520341233 Imam Noeril\n- BRI 409801018288530 R Aldi M\n- MANDIRI 900003178905 Kekes Safriatik\n\nSEKRETARIAT PENDAFTARAN\nKMR - ZMS - ABO SPEED - REHAL MOTOR - POS HONDA KALAPANUNGGAL\n\nContact Person:\nAldo MT 0857 9847 4469\nZahal 0815 6322 3772\nAngga 0857 2302 0103','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-07 17:44:07',NULL),(65,19,'d1d6752b011d61d39e45262c98f72f95.jpg','Safari Ngopi','2020-07-11','2020-07-11','17:00:00','22:00:00','Stasiun Safari Godong','Kopdar Safari Ngopi Gratis\nAll Bikers\n\n- Oleh gowo yang e neng dijogo\n- Wajib bawa masker\n\nHiburan: Kroncong\n\nNo Blayer No Arogan\nNgraketke Seduluran','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-08 06:44:55',NULL),(66,19,'958a74d467902f7e392e153fa0ca3da3.jpg','Gass Keun Boy!','2020-07-26','2020-07-26','08:00:00','17:00:00','Desa Sindang Cikijing','Sodaqoh 50 ribu \n- Baksos Masjid Al-hikmah\n- Pengobatan Gratis\n\nTanjakan Gembong, Tanjakan Abah Wilanta\n\nCP 08567233428','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-08 06:59:00',NULL),(67,18,'98b3f3d4b2e3abbdcdb2e016b596b414.jpg','Ngumpul di Bandung','2020-07-12','2020-07-12','14:00:00','21:00:00','SIRKUIT BRIGIF 15 KUJANG - Cimahi','(dari bandung geser dikit ke cimahi)\nHalooo om² wargi Bandung & Cimahi....\n\nBuat om² yang lg tdk punya acara,\nKita Ngumpul bernostalgia Minggu ini yuk!...\n\nHari: Minggu\nTempat: SIRKUIT BRIGIF 15 KUJANG - Cimahi\nTanggal: 12 Juli 2020\nJam: 14:00 - Selesai\nAgenda: Ngobrol santai + nonton latihan Drag Race mobil \n\nBawa mobil max 1999 biar bisa mejeng dan foto bareng\n\nNote: \n• Selalu Gunakan masker & terapkan physical distancing\n• Bayar tiket masuk parkir Rp.20,000/mobil\n• Buat yang mau ikutan drag bayar Rp.150,000/mobil \n• Lintasan drag dilengkapi staging light \n• Bayar on the spot ke pengelola tempat\n\nLOKASI:\nEXIT TOL BAROS/CIMAHI & SEARCH SIRKUIT BRIGIF 15 KUJANG CIMAHI\n> akses ke venue dekat exit tol -/+ cuman 10 menit\n   dan tdk ada macet + gampang dicari.\n\npic: 082311968968','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-08 07:08:35',NULL),(68,19,'148757367ef57e2816e9cc313e6f1035.jpg','Ubar Sidat','2020-08-08','2020-08-08','08:00:00','17:00:00','Terminal Sindang Barang Cianjur','Buat Sidat New Normal \n\nIDR 150K\nJersey Printing + Makan + ID Card & Scrut \n\nHiburan: Rock Dangdut \nMC: Van Haus\n\nDOOR PRIZE: 3 unit motor & Hadiah Lainnya\n\nCONTACT PERSON;\n- Agus Males 0858 2092 2910\n- Akbar NS 0856 5991 7379\n- Abah Unang 0857 2322 1988\n- Dandan 0855 2399 3590\n- Van Haus 0812 2436 127','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-08 07:19:27',NULL),(69,19,'8f2435f1347f07f11a20787b2d7c87d9.jpg','IERC','2020-11-20','2020-11-22','08:00:00','17:00:00','Kelud Mountain - Trawas East Java','REGISTERATION:\n- Team Class: 9.000.000\n- Individual Class: 3.500.000\n\nInfo: 0811 333037','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-08 07:35:27',NULL),(70,19,'dd4bb49f22223a5dd210da025b1f38ed.jpg','Nge-gas Bareng','2020-07-19','2020-07-19','08:00:00','17:00:00','Lapangan Bojongtongo Muncangkohok, Pantai Cipatujah, Tasikmalaya','Fastabiqul Khoirot \nBerlomba-lomba dalam kebaikan\nNge-gas bareng BSS & Ranggagading \nDalam rangka pembangunan Masjid\n\nKontribusi Rp75.000\nNo hadiah - No Asuransi - No Jersey \n\nHiburan: Organ Tunggal\n\nContact Person:\n- 0822 4017 1667 Wa Apik\n- 0813 2357  5599 Wa Guru Mv\n- 0853 1467 2660 Wa Bagja \n- 0853 5225 8825 Wa Bayi\n- 0813 2384 9234 Wa Jendra','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-08 16:38:08',NULL),(71,18,'e13625ecc770579ee857303791081a25.jpg','Kopdar Santuy','2020-07-19','2020-07-19','08:00:00','17:00:00','D Gunungan Taman Dayu Pandaan Malang','Kopdar Santuy\nMember CR-V G3 Brotherhood Jatim dan semua pencinta CR-V Gen 3\n\nTikum Paskul Kahuripan\n\nDresscode: Seragam CR-V G3 dan bagi yang belum punya kaos warna hitam\n\nDoor Prize Menarik...','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-08 16:50:47',NULL),(72,19,'52e71251b08503f918cea5e225daf10e.jpg','Ubar & Syukuran','2020-07-19','2020-07-19','08:00:00','17:00:00','Lapangan Bola Desa Jayi, Kec. Sukahaji, Kab. Majalengka','Ubar & Syukuran BATRIX (Barudak Trail Extreme)\nTeu Kudu Bosen\nSadulur Sajalur Salumpur\n\nPENDAFTARAN: Rp100.000 (kaos-makan-scrutt)\n\nHadiah utama\n- 2 unit motor bebek jepang 2nd\n- Seabreg hadiah undian lainnya\n\nJANGAN LUPA PAKAI MASKER, CUCI TANGAN YANG BERSIH, PATUHI PROTOKOL KESEHATAN\n\nCONTACT PERSON:\n\nBomen       0823 2141 2884\nMax Erah    0852 2372 7196\nMang Bilik  0813 8421 8794\nMang Ipin   0895 3707 6765\nBendot      0853 1527 5005','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-08 17:14:00',NULL),(73,19,'190a40d3602a2b38910c58322c8aa883.jpg','Sapat Adventure','2020-09-12','2020-09-12','08:00:00','17:00:00','Lap. Koperpu, Ds. Batok, Kec. Tenjo, Bogor - Jawa Barat','Sapat Adventure\nUbar Jalur Janda Rasa Parawan\nSampe Ka Sapat 2\nGar-gara Corona\n\nRp125.000\n- Door Prize\n- 2 unit motor new\n- Ratusan hadiah lainnya\n\nMC Wan Van Haw\nDJ Kiesy + Dangdut\nSexy Dancer, Lady Wash\nUmbrella Girls\n\nCONTACT PERSON:\n- Jumri    0813 2018 6383\n- Ameng Sapat 0813 9838 2467\n- Helminazz Amx 0815 8585 4310\n- Ade Rush 0812 8123 2727','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-08 21:02:57',NULL),(74,19,'e92e707607043ef1679d9296ede1ae91.jpg','Ready To Braaap','2020-07-19','2020-07-19','08:00:00','17:00:00','Pasa Baru Batang Palupuah, Jl. Bukitinggi - Medan KM.10, Kab. Agam, Sumbar','You Never Braaap Alone\n\nBelok Balai Sixth Edition\n\nExplore Bukit Tinggi Kota Wisata','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-08 21:24:46',NULL),(75,19,'a01eb1e99374bf8e57559a850c459c92.jpg','Kibrat','2020-08-09','2020-08-09','08:00:00','17:00:00','Wanaraya, Kikimbarat','Kibrat (Kikimbarat Adventure) Jawara 2\n\nREGISTRASI: 200K (Jersey, Id Card, Makan)\n\nDORPRACE:\n- 2 unit motor jepang\n- TV\n- Kulkas\n- Dan masih banyak lagi lainnya\n- Group pendaftar terbanyak Dapat Kambing\n\nKONTAK PERSON:\n0852 6882 3051 - Cak No\n0813 8821 7832 - Rohmad\n0812 7339 7500 - Eko dan Rianto\n0822 1838 4845 - Andi Prasetyo\n0852 6936 1995 - Dick Yoarianto Sanjaya','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-08 21:44:01',NULL),(76,19,'14bca824844874d15e58d42481d4f224.jpg','Social Charity','2020-08-02','2020-08-02','08:00:00','17:00:00','Lap. Bola Tanjung Jaya, Desa Tanjung, Cipunagara, Subang','Social Charity And Anniversary 2rd\n\n\"Tebar Kepedulian Ciptakan Kebersamaan\"\n\nDonasi 125.000 (T-Shirt, Makan, Jalur & Hiburan)\n\n100% Keuntungan hasil penjualan T-Shirt kami donasikan untuk anak\" yatim\n\nJuru Haok: Yayu \"Opixeka\" & Jhon Kissbet\n\nNOTE: PESERTA WAJIB MENGIKUTI PROTOKOL KESEHATAN\n\nJALUR SA AYANA, DULUR SA ENYANA','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-08 22:06:15',NULL),(77,19,'0f29d3c0dc34f2d3ba8bd596638c1c4c.jpg','Silaturahmi Tanpa Batas','2020-09-12','2020-09-12','08:00:00','17:00:00','Lap. Sepakbola Cikaret, Ds. Cikupa, Karangnunggal, Kab. Tasikmalaya','Silaturahmi tanpa batas\nCalik wae moal paham\n\nPESONA ALAM KARANGNUNGGAL\nONE DAY TRAIL ADVENTURE\n\nREGIST 200 RB (Jersey, ID, Makan, Asuransi)\n\nGRANDPRIZE:\n- 1 unit mobil second\n- 5 unit sepeda motor new\n- Dan hadiah menarik lainnya\n\nCONTACT PERSON:\n0823 2102 7307 - Dede Goler\n0852 2075 9548 - Yayan Doyok\n0813 2375 5599 - Abah Guru\n0821 1914 1141 - Z14ang\'s Lcr','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-08 22:34:47',NULL),(78,19,'4fd0cb6fee731aace502e01cf29760c0.jpg','Ngetrail New Normal','2020-07-26','2020-07-26','08:00:00','17:00:00','Jawar Gajah Sambit Ponorogo','Ngetrail New Normal\nBaksos Mushola Miftahul Jannah\n\nSuport by: Komunitas Trail Ponorogo','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-10 04:33:38',NULL),(79,19,'708e3fef7addedcf0a3d7c1e78638ef7.jpg','Adventure Merdeka','2020-08-09','2020-08-09','08:00:00','17:00:00','Lapang Sepak Bola Desa Jayamukti, Kec. Pancatengah, Kab. Tasikmalaya','Adventure Merdeka\nDalam rangka menyambut HUT RI ke-75\nBersama TAJAM (Trail Adventure Jayamukti)\nPendaftaran Rp150.000\nFree T-Shirt + Snack, Hiburan\nTrack X-trame\nButuh Skil dan Keberanian\nJalur 70% masih perawan\n\nDOORFRIZE...!!!\n4 unit motor & hadiah lainnya\n\nCONTACT PERSON\n- 0852 8111 1860 (H. Irin)\n- 0852 2262 6049 (Tambeng)\n- 0853 2181 9767 (Cibong)\n\nMC: ABAH GURU','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-10 04:49:06',NULL),(80,19,'f94fbdd4fffdd850ed6b70d65cac674f.jpg','INFO KOPDAR HONDA WAVE','2020-07-11','2020-07-11','20:30:00','11:55:00','Samping SPBU panjer taman kota , Kebumen Jawa Tengah','Buat kalian pengendara wave atau motor bebek honda yang ada di kebumen ,yuk ramaikan kopdaran kami 🙋\nAgenda : •sharing •forum •uang kas rutin 5k •pendataan member •ngopi bareng •rasan rasan\n\nBingung ke kopdaran bisa hubungi:\nWilayah gombong sekitarnya\nNofal :+62 858-7988-9764 \nWilayah bumkot sekitarnya :\nSahrul :+6285648528890\nWilayah prembun sekitarnya \nDjanuar :+62895433144350\nJangan lupa safety riding dalam berkendara,gunakan helm , sepatu ,celana panjang , jaket , lengkapi kelengkapan kendaraan anda (spion kankir,plat d/b),stnk ,sim buat berjaga jaga dari hal yang tidak diinginkan,tetep pakai masker stay save🙏🏻\n#PasukanManganKaroNdog\n#PasukanTuru\n#JanganAdaKataSuhuDiantaraKita\n#staysave','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-10 07:34:07',NULL),(81,18,'b6f4160ec55ed2578b6d57933f3866eb.jpg','KOPDARGAB','2020-07-19','2020-07-19','08:00:00','17:00:00','Food Garden City - Cakung Jaktim','KOPDARGAB CRV GEN3 BROTHERHOOD\nJAKARTA RAYA\n\nKepada Yth:\nALL MEMBER CRV GEN3 BROTHERHOOD\n\nBesar harapan kami rekan rekan dapat menghadiri acara yang dimaksud.\n\nNOTE: Wajib mematuhi protokol kesehatan (Masker ) Handsanitizer + Social Distancing)\n\nDAPATKAN:\n- Doorprize 1 unit TV 32 Inc\n- Free Merchandise (T\'Shirt & Aticker G3B Jakarta Raya)\n- Free Luch, Snack & Parking)\n- Free Masker G3B Jakarta Raya & masih banyak Doorprize lainnya\n\nLIVE MUSIC PERFORMANCE: ORGAN TUNGGAL','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-11 14:44:30',NULL),(83,19,'0efefd8c8879926967ae9ebe22913bb0.jpg','Riding Sore Ceria','2020-07-19','2020-07-19','17:00:00','11:55:00','Kolam Renang Aryana Karawaci (Belakang Griya Karawaci)','Pengendara Bebek\n1\'st Anniversary\n\nGaskeun','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-13 03:53:31',NULL),(84,19,'cf1fe7c796b048c97a1264833d44c5d8.jpg','Bubuka Jalur','2020-07-19','2020-07-19','08:00:00','17:00:00','Reg Pasir Angin','Mu ngundang orang kota ah siapa tau ada yg mau coba jalur cianjur\n\nSambil mu bikin jalur baru\n\nHayu kita ngaliwet sambil ngejalur. Tanpa biaya\n\nBubuka Jalur Sabubukna\n\nSarat daekan jeng te ngedul\nBonus (ngeunah sare)\n\nGOROBAS Go Trabas\n\nContac admin 0857 7628 1211','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-13 04:03:41',NULL),(85,19,'ab4abccbaf435472b8d406255fcc14a6.jpg','Kopdar Honda Astrea','2020-07-19','2020-07-19','09:30:00','03:00:00','Taman Kilisuci Pare Kediri (Aula Sebelah Barat)','Monggo Dulur yg bisa merapat.\nDan sampaikan/share ke dulur semua\n\nMonggo Honda Astrea ne di gaskaaan\nUndangan Kopdar Buat semua pemilik / penggemar Honda Astrea\n\n!!! Jangan lupa bawa Masker... Tetep protokol kesehatan !!!','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-13 15:02:28',NULL),(86,19,'623d1c67f9d7d634a1cd136635f700b6.jpg','KOPGAB JABAR SSFC','2020-08-29','2020-08-29','08:00:00','17:00:00','villa telkom rainbow hills ( tempat kopgab tahun kemarin).','Diselenggarakan oleh: Suzuki Satria F150 Club (SSFC) KORWIL CILEUNGSI\n\nAsalamualaikum wr.wb.\nSalam sejahtera untuk kita semua.\n\nSaya mewakili ssfc pengda bogor sekaligus tuan rumah KOPGAB JABAR ke 10 mau menginformasikan, terkait KOPGAB JABAR yang sempat tertunda karena adanya Covid-19.\nDikarenakan sebagian wilayah bogor masih menerapkan psbb dan sering adanya patroli/jam malam. Maka dari itu demi kenyamanan dan keamanan kita semua, kami selaku pengurus dan jajaran panitia KOPGAB JABAR ke 10 sepakat untuk menyelenggarakan KOPGAB JABAR ke 10 \n\nContack W.A\nAzis(ketua) +628989325936\nZenal(waketu) +6281318440406\nJuli(humas) +6289516867032\nKey(Div.Event) +6281903197251\n\nDemikian informasi yang dapat kami sampaikan, kiranya dapat dipahami dan dimaklumi.\nTerimakasih.\n\nWasalamualaikum wr.wb.\n\n#SSFC_INDONESIA\n#SSFC_JABAR\n#SSFC_PengdaBogor\n#SSFC_BRAVO\n#SSFC_HITS','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-13 15:26:45',NULL),(87,18,'f28fb03144cca3966f017c308b1d12a6.jpg','Fun Futsal Starleters','2020-08-09','2020-08-09','10:00:00','17:00:00','Water Joy, Harvest City, Cileungsi','hayu ngopi ngopi futsal renang gokart ada smwa di mari om om ramaikan\n\nDiselenggarakan oleh Komunitas Toyota Starlet Family','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-13 15:57:42',NULL),(88,18,'c8b93146e1250b6b4a3c850748d32d7e.jpg','AAOC Goes to Yogyakarta','2020-08-29','2020-08-30','08:00:00','11:00:00','Jogja','Salam pekok (pencinta kopi kentel)\nSeduluran saklawase AAOC 678\nOjo lali Yo dulurrr ramaikan acara KOPDARGAB\n\nRuly Maulana [AAOC 045] WA : 0821 1930 6710\n\nDiselenggarakan oleh: Agya Ayla Owners Community','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-14 15:15:33',NULL),(89,19,'e949f8de3d109e02dab6c3c4417f0ec4.jpg','Sunmori Bikers','2020-08-02','2020-08-02','04:00:00','17:00:00','Start: JB Cisinga - Finish: Pantai Pangandaran','Sunmori Bikers\nKumpul Riung Bikers\n\nCONTACT PERSON:\n\n0812 9754 0604 - Neneng RA\n0859 7730 9099 - Amay Kuya\n0812 9755 6535 - Ririn\n\nKEEP SAFETY RIDING\n\nDiselenggarakan oleh komunitas: Modifikasi beat thailook/babylook\nhttps://www.facebook.com/groups/1098205537234709/permalink/1431413387247254/','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-14 15:39:45',NULL),(90,18,'34f3533afb946d604136497a8ccfd9dd.jpg','Futsal Ceria','2020-08-08','2020-08-08','20:00:00','11:55:00','Lap. Galaxy Futsal Tajur Bogor','Futsal Ceria\nKijang Kapsul Community Indonesia\nJABODETABEK\n\nMempererat Tali Silaturahmi\n\nGLORY KKCI\n\nDiselenggarakan Oleh Komunitas Kijang Kapsul Community Indonesia (KKCI) Jabodetabek','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-14 16:19:43',NULL),(91,19,'f4d6ca7ed5f4ce6b381b5ef5f5c36363.jpg','Touring Wisata','2020-07-18','2020-07-18','08:00:00','17:00:00','Danau Ranau','Logo yang sebagian belum dimasukkan oleh team support\n\nBagi yang berkenan log club nya mau naik dalam kegiaran ini dikenakan biaya 30.000 per logo silakan japrik team support Sdr Riky KBC 0812 7821 0846\n\nDiselenggarakan oleh Komunitas RX King Se Indonesia','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-14 16:48:36',NULL),(92,19,'6f55875964d02eccb1011b864729306c.jpg','Track Pantai','2020-07-24','2020-07-26','08:00:00','17:00:00','Welcome Back To Track Pantai Ujunggenteng Sukabum','Welcome Back To Track Pantai Ujunggenteng Sukabumi\n\nEra New Normal (HUT Karang Taruna Desa Ujunggenteng Ke 5)\n\nKELAS LOMBA:\n- Supra Std\n- Supra Modif\n- Bebek Std (Non Mesin Berdiri)\n- Bebek Modif (Non Mesin Berdiri)\n- Matic Std\n- Matic Ffa\n- Matic Ffa (Khusus Wanita)\n- FFA Open (Khusus 4 tak)\n- OMR F1ZR (Spek Bebas)\n- OMR RX King (Spek Bebas)\n- OMR Kawasaki Ninja (Spek Bebas)\n- SUPERMOTOT 4 Tak FFA (Spek Bebas)\n- Bebek Modif (Mesin berdiri spek bebas)\n- FFA Open (4 tak & 2 tak)\n- Bebek 4 tak 130cc (Regulasi)\n- Matic 155cc (Regulasi)\n- Matic 200cc (Regulasi)\n- SUPERMOTO 4 Tak 200cc (Regulasi)\n- Sport 155cc Frame Std (Regulasi)\n\nKepada semua calon peserta diwajibkan hadir pada hari pertama Jumat 24 Juli 2020, dikarenakan ada bereaping peserta dan pendaftaran mulai dibuka\n\nCONTACT PERSON:\n0852 1084 7236 Welly\n0857 8123 4504 Maya\n0858 6285 7211 Kamal\n\nDiselenggarakan oleh Komunitas Penggemar Motor Trail Indonesia','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-14 17:19:34',NULL),(93,19,'1585ab0b7de067d1b72de20fa07d26ff.jpg','Touring Pespah Syariah','2020-08-15','2020-08-15','08:00:00','17:00:00','Tanjung Kodok','Salam mesin kanan\n\nDalam Rangka memperingati HUT RI ke 75 KPK akan menggelar treng teng teng Kemerdekaan di tanjung kodok.\n\nSiyapno pespahmu, siyapno kaos KPK mu,siyapno oli sampingmu, sebentar lagi kita akan ber treng teng teng...\n\nJadwal bisa berubah menyesuaikan protokol kesehatan pemerintah \nWis rek ngunu ae disek, engkuk tak tambahi maneh lek onok perkembangan..','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-14 17:38:19',NULL),(94,18,'c9411f3ab137030669f28e51841904b9.jpg','Berkendara di Masa Pandemi','2020-07-20','2020-07-20','15:00:00','18:00:00','Online','Berkendara aman selamatkan jiwa\n\nDapatkan hotline pendaftaran melalui call centre kami di +62 812 4553 4553\n\nCovid-19, survival tips\n\nCapacity off 1000\n\nFee Registration & Digital Certificate\n\nMODERATOR/DIGITRAC:\n- Rini Mulyawati \n- Robby Prakoso','Ongoing',NULL,NULL,NULL,NULL,'0',30,'2020-07-14 19:13:41',NULL),(95,18,'cb2b486c090882922936fa74f7683ea0.jpg','Kopdar Resmi CTI','2020-07-25','2020-07-25','14:00:00','18:00:00','Sea Horse Coffee','Bogor Is Back\nCorolla Twincam Indonesia Bogor Chapter\nKopdar Resmi\n\nKEGIATAN: \n- Mempererat tali silaturahmi\n- Pembahasan Famgat 2020 dan rolling\n\nNOTED:\n- Wajib menggunakan masker\n- Membawa Handsanitizer\n- Tetap Jaga Jarak\n\nDRESS CODE: Seragam CTI (Bila belum punya bebas)\n\nCP: 0813 2479 6974 (Farhan)','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-15 04:34:37',NULL),(96,18,'c0b2f55c0ac13e4ae65f4c59c004cd1e.jpg','INFO KOPDAR','2020-07-18','2020-07-18','20:00:00','11:55:00','ANC BOGOR GARAGE','Assalamualaikum wr.wb,\nUntuk mengisi Agenda event bulanan kita, kami Mengundang Teman2 Anggota, crew dan camem ANC Bogor untuk menghadiri kopdar resmi dengan Tema: SADULUR NGARIUNG ANC BOGOR\n\n- ALL NEW COROLLA BOGOR\n- ALL NEW COROLLA INDONESIA\n- COROLLA AINK KUMAHA AINK\n- GAK ADA LO GA RAME\n- NEW NORMAL\n\nHormat Kami\n\nPengurus\n\n#allnewcorollaindonesia #allnewcorollabogor #corollaainkkumahaaink #bogorkotahujan','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-15 04:49:44',NULL),(97,19,'719f43d25223a929c7db0b46c1a062f9.jpg','Silaturahmi Pengendara Bebek Jadul','2020-07-17','2020-07-17','20:00:00','11:55:00','Depan Polsek Kemayoran','Pengendara Bebek Tua\nPRESENT\nSILATURAHMI SESAMA PENGENDARA BEBEK JADUL\n\nCOME ON JOIN US!','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-15 05:00:08',NULL),(98,19,'62aece396db5f7e72fac7dfb09af092e.jpg','Touring Bareng','2020-07-18','2020-07-19','08:00:00','17:00:00','Sekretariat Ikatan Vespa Sukabumi, Taman Bahagia No.14, Sukabumi','Halal Bihalal Ikatan Vespa Sukabumi\n\nDAFTAR ACARA (Kegiatan Selama di Lokasi)\n- Rolling Thunder\n- Game Kekompakan Tiap Divisi\n- Api Unggun\n- Doa Bersama\n- Camping Ground (Jika Punya Tenda)','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-15 14:26:54',NULL),(99,19,'3d4528de94cb19660124068da3fcaa7c.jpg','Anniversary Scooters Independent Crush','2020-07-25','2020-07-25','08:00:00','23:55:00','Villa Cimacan Cianjur','- Camping Fun\n- Live Acoustic\n- Senam Pagi\n- Potong Tumpeng\n- BBQ\n\nHIMBAUAN: Wajib menggunakan masker/slayer, dilarang beratribut sampah, tetap keep safty ridding\n\nCONTAK PERSON:\n0838 0195 0052 70 Rizki/Marmut\n0896 3506 5723 Dimas/Bimbim','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-17 17:16:11',NULL),(100,19,'6ddff4ca7c68a7091e6c926149d368bd.jpg','Meet Up & Sunmori','2020-07-19','2020-07-19','08:00:00','23:55:00','Penatapan Berastagi, Titik Kumpul: Mesjid Raya','Wave 110.125\nCultivate The Thailand Wave Modification\nWASI GONE MEDAN & WAVE FAMILY SUMATERA UTARA\n\nNB: \n- Wajib Masker\n- Wajib Sefty Diri \n- Sefty Unit\n\nCONTACT PERSON:\n8813 7189 9388 M. Wahyu Ramadhani (Ketua Wasi Zone Medan)\n8831 7915 6894 Ardiansyah (Penguruwavefamilysumut)','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-17 17:34:45',NULL),(101,19,'96812b9893473762a997f61d9aca9e7f.jpg','KOPDAR GABUNGAN','2020-08-15','2020-08-17','08:00:00','17:55:00','Danau Ranau OKU Selatan.','Salam New Normal...\n\nHadiri & ramaikan acara KOPDAR GABUNGAN MAXXIO REGIONAL SUMATERA dan perayaan ulang tahun ke-1 MAXXIO CHAPTER OGAN ILIR.\n\nBagi rekan² yang akan hadir silahkan reservasi akomodasi ke panitia.\n\nKetersediaan penginapan dengan kelas dan harga sbb:\n\nI. RANAU INDAH:\n- Kelas Ekonomi, Rp 300.000/malam (kapasitas 2 orang), Ketersediaan 7 kamar FULL BOOKING\n- Kelas Superior, Rp 450.000/malam (kapasitas 4 orang), Ketersediaan 6 kamar.\n- Kelas VIP, Rp 550.000/malam (kapasitas 4 orang), Ketersediaan 12 kamar.\n- Kelas VVIP Rp 850.000/malam (kapasitas 6 orang), Ketersediaan 2 kamar.\n\nFASILITAS: Sarapan pagi, Kolam renang, Waterboom, Khusus kelas VVIP tersedia pantry\n\n--------------------------------\n\nII. MESS OKUS\n- Kelas Satu, Rp 350.000/malam (kapasitas 5 orang), Ketersediaan 5 kamar.\n- Kelas Dua, Rp 250.000/malam (kapasitas 4 orang), Ketersediaan 15 kamar.\n\nTidak ada fasilitas sarapan pagi.\n\nReservasi:\n082181009013 - Om Udin (Ketua Pelaksana)\n085269693870 - Adi Boncou (Koordinator)\n\n\nCATATAN:\n1. Acara tanggal 16 Agustus FREE makan siang.\n2. Pelunasan kamar paling lambat 1 Agustus 2020\n3. Wajib mematuhi protokol kesehatan Covid-19.','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-17 17:52:05',NULL),(102,18,'6c2e464bc40d7245ace3a367717afac8.jpg','Wani Gabung Wani Kopdar','2020-07-26','2020-07-26','08:00:00','17:55:00','Pantai Nguyahan, Saptosari, Gunung Kidul, Yogyakarta','Diselenggarakan oleh Komunitas Chery Joglo Semar','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-17 18:07:10',NULL),(103,18,'264759adc3bf0cd18ed7a54c1a673c93.jpg','Temu Kangen Baraya Kijang Jilid 2','2020-08-23','2020-08-23','09:00:00','17:56:00','Lokasi Gunung Puntang, Cimaung, Banjaran Sari, Bandung.','IDR 100.000 Per unit untuk tiket masuk, souvenir, dll\n\nUntuk pembayaran bisa langsung transfer ke Norek BRI 4118 0101 0226 538. atas nama Yulisumiati\n\nBatas Tempo Pembayaran 10 Agustus 2020\n\nKarena Kijang Kita Sodara, Silaturahmi Tanpa Batas\n\nCONTAC PERSON:\n0812 2196 5282    Kosasih\n0858 9156 9207    Yudi\n0859 2624 4705    Supri\n0881 0237 0379 3  Dasep\n0821 1998 5781    Carwita Yuli','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-17 18:34:40',NULL),(104,19,'3cabb476e3f7792befc3780e57b67711.jpg','Riding Santuy Astrea Peduli','2020-07-19','2020-07-19','08:00:00','17:55:00','Embung Krapyak, Pantok Wetan, Banjaroyo, Kalibawang, Kulon Progo','IURAN RP10.000 (Konsumsi, parkir, sticker dan santunan\n\nPendaftaran/iuran paling lambar haru Jumat 17 Juli 2020, pukul 15.00\n\nAssalamulaikum Wr. Wb.\nPiye kabare Lur?\nGabut? Yuk gas bareng MAC\n\nTIKUM: Pertokoan Grand Maris, Jl. Jend Sarwo Edhie Wibowo, Seneng Satu, Banyurojo, Kec. Mertoyudan, Magelang Jawa Tengah\n\nRULES: Safety Riding, Ikuti Protokol Kesehatan (Wajib Masker), Sopan, No Drug, No Narkoba, No Ugal2-an, Taati Aturan MAC\n\nCONTAT PERSON:\n- 0812 1358 951\n- 0818 8285 0625\n- 0895 4149 3305 9\n\nSEDULURAN SAK DAWANE DALAN\n\nWASSALAMUALAIKUM Wr. Wb.','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-17 19:02:59',NULL),(105,19,'51ffc654475614aa012209ed71f53daa.jpg','Super GrassTrack','2020-07-25','2020-07-26','08:00:00','17:00:00','Sirkuit Satria Fortuna, Ds. Tanjung Waras, Kec. Bukit Kemuning, Kab. Lapung Utara','Sirkuit Satria Fortuna, Ds. Tanjung Waras, Kec. Bukit Kemuning, Kab. Lapung Utara\n\nKELAS PERLOMBAAN:\n- Bebek STD 4L 130cc Pemula\n- Bebek STD 2L 116cc - 4L 130cc Pemula\n- Bebek Modif 2L 116cc / 4L 130cc Junior\n- FFA 250cc Junior\n- Bebek Modif 4L 130cc Senior\n- Sport Trail s/d 250cc Senior\n- OMR Bebek STD 2L 116 cc Open\n- Adventure Non Pembalap & Eks Pembalap\n- Campuran s/d 155cc Lampung Utara & Waykanan\n- Campuran s/d 155cc 3 Kabupaten (Lampung Utara, Lampung Barat, Waykanan)\n- Bebebk STD 2L 116cc / 4L 130cc Lampung Utara\n- Bebek Modif 4L 130cc Lampung Utara & Waykanan\n- FFA 250cc U16\n\nJUARA UMUM:\n- Senior  Rp5.000.000\n- Junior  Rp3.000.000\n- Pemula  Rp2.000.000\n\nCONTACT PERSON:\n0852 6939 8049 Budi bkl\n0812 7652 1500 Arip\n0852 6994 4900 Harianda','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-19 15:31:08',NULL),(106,18,'a07041946f4243385fafc937ae00908b.jpg','KopDats Gokart Bareng','2020-07-26','2020-07-26','12:00:00','17:00:00','Rooftop Blu Plaza, Bekasi','Stay Social Distancing\n\nDiselenggarakan oleh Komunitas DATSUN Jakarta','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-19 15:34:32',NULL),(107,19,'07f1dfe8eed0cab6d66711cdc3253a7a.jpg','Ubar TS Jabar #5','2020-09-05','2020-09-05','08:00:00','17:00:00','Villa Palm Garden Cipanas, Cianjur','MORE INFO: Dewan Robert 0856 2440 5900','Ongoing',NULL,NULL,NULL,NULL,'0',23,'2020-07-19 15:43:33',NULL);
/*!40000 ALTER TABLE `com_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_event_like`
--

DROP TABLE IF EXISTS `com_event_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_event_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `likeordis` char(1) NOT NULL DEFAULT '0' COMMENT '1 = like, 2 = dislike',
  `likes` char(1) NOT NULL DEFAULT '0',
  `dislikes` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_event_like`
--

LOCK TABLES `com_event_like` WRITE;
/*!40000 ALTER TABLE `com_event_like` DISABLE KEYS */;
/*!40000 ALTER TABLE `com_event_like` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_event_tertarik`
--

DROP TABLE IF EXISTS `com_event_tertarik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_event_tertarik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `tertarikorhadir` char(1) NOT NULL DEFAULT '0' COMMENT '1 = tertarik, 2 = siap hadir',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_event_tertarik`
--

LOCK TABLES `com_event_tertarik` WRITE;
/*!40000 ALTER TABLE `com_event_tertarik` DISABLE KEYS */;
INSERT INTO `com_event_tertarik` VALUES (1,20,3,'2'),(2,30,22,'2'),(3,30,32,'2'),(4,30,40,'2'),(5,30,41,'2'),(6,23,53,'2'),(7,30,54,'2'),(8,30,69,'2');
/*!40000 ALTER TABLE `com_event_tertarik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_forum`
--

DROP TABLE IF EXISTS `com_forum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `community_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `user_create` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_forum`
--

LOCK TABLES `com_forum` WRITE;
/*!40000 ALTER TABLE `com_forum` DISABLE KEYS */;
INSERT INTO `com_forum` VALUES (17,19,'Trik Dasar, Banyak yang Tak Tahu Cara Naik Motor Trail','Sepeda motor tipe trail yang mampu menjelajah jalur on dan offroad memang memiliki ground clearance lebih tinggi. Ini tak lepas dari penggunaan pelek berukuran 21 inci bagian depan dan pelek 18 inci pada bagian belakang, serta ban pacul yang menambah postur motor menjadi lebih tinggi.\n\nDemikian juga bagian shock breaker disetting berbeda dengan tipe skuter matik, bebek ataupun sport, sehingga dudukanya pun terasa jangkung. Karenanya motor trail mampu melahap medan dengan kontur jalan bebatuan atau lumpur.\n\nHal ini pula yang kerap membuat para calon konsumen urung membeli motor trail karena bentuknya cukup tinggi.\n\nNamun begitu Instruktur Safety Riding Wahana, Siswanto menyatakan, semua orang memiliki kesempatan untuk mengendarai motor, baik trail atau cross, maupun tipe sport.\n\nKarena itu, Siswanto memberikan sedikit tips cara menunggangi sepeda motor trail, khususnya bagi mereka yang postur badannya kurang tinggi.\n\nPosisi Motor Diam atau Belum Menyala\n\n“Kalau memang tidak sampai naik untuk motor cross, maka yang dilakukan adalah posisi standar tetap miring, kemudian kaki (bagian kiri) menginjak step, lalu badan berdiri (seperti naik tangga), kemudian kaki kanan melangkahi jok dan mendarat di step kanan,” ungkap Siswanto saat ditemui Liputan6.com, beberapa waktu lalu.\n\nJika sudah melakukan itu, lanjutnya, bagian pantat sedikit digeser, lalu naikkan posisi standar samping.\n\nSelanjutnya, kata Siswanto, untuk setiap akan berhenti, maka bagian pantat harus selalu digeser. Jika berhenti, ada baiknya menggunakan kaki kiri.\n\n“Pasti nyampe, karena paling nyaman itu kaki kiri, robohnya lebih susah. Untuk naik motor trail, jika merasa akan terpeleset, kaki harus mau turun, ini untuk menyeimbangkan,” ungkapnya.\n\nPosisi Motor Berjalan\n\nJika telah mengetahui trik naik motor tinggi, kini giliran saat motor trail dengan posisi mesin siap menyala.\n\nYang patut diperhatikan ketika bermanuver motor trail jelas berbeda dengan motor sport. Di mana posisi badan ikut miring atau kerap disebut cornering atau belok rebah.\n\nUntuk motor trail, posisi pantat ikut digeser ke arah berlawanan baik kanan atau kiri. Tak lupa, bagian kaki turun sesuai arah belokan.\n\n“Kalau tanjakan, gerakan badan, kalau naik badan agak depan, kalau turun tarik ke belakang. Geser ke kanan kiri pantatnya, kita harus mau turunin kaki kalau mau kepeleset,” ungkap.\n\nSelain itu, kata Siswanto, saat menggunakan trail, ada baiknya meminimalkan penggunaan rem depan.\n\nSumber: https://www.liputan6.com/otomotif/read/2909490/trik-dasar-banyak-yang-tak-tahu-cara-naik-motor-trail#:~:text=Posisi%20Motor%20Berjalan&text=Untuk%20motor%20trail%2C%20posisi%20pantat,kalau%20turun%20tarik%20ke%20belakang.',30,'2020-07-07 18:20:07',NULL),(18,18,'Solusi Otomotif: Putaran Mesin Great Corolla Tinggi Ketika Iddle','Assalamu\'alaikum Pak Mustafa, saya punya Greco tahun 1994. Pertanyaan saya:\n1. Kenapa stationer mesin bertahan pada di atas 1.000 (tinggi) dan tidak bisa diturunkan.\n2. Dulu mobil saya itu sepertinya tipe matic, tapi sejak saya beli sudah diubah jadi manual. Apakah pengaruhnya? Terima kasih\n\n-Agus Sarbito, Karanganyar-\n\nGreco adalah panggilan kesayangan bagi penggemar Toyota Great Corolla tahun 1994 yang sempat sangat berjaya. Sampai saat ini Greco masih cukup banyak populasinya dan terbukti mesin dan sasisnya sangat andal. Saya akan membantu menjawab pertanyaannya agar Greco lebih nyaman untuk dikendarai.\n\nPertanyaan 1\n\nKenapa stasioner mesin bertahan di atas 1.000 rpm (tinggi dan tidak bisa diturunkan ke normal (800-900 rpm).\n\nSebelum memeriksa hal-hal yang utama, coba Pak Agus melakukan pemeriksaan secara visual apakah selang saluran udara masuk (air intake) dalam kondisi bagus dan tidak ada retak ataupun bocor pada karet-karetnya. Apabila terjadi kebocoran udara masuk, hal ini akan dibaca ECU untuk menambah jumlah injeksi bahan bakar dan akibatnya putaran mesin menjadi tinggi.\n\nPutaran 1.000 rpm atau lebih apakah AC dalam kondisi mati? Saya asumsikan AC mati. Sehingga rpm tinggi ini disebabkan idle speed control (ISC) pada throttle gasnya macet dan posisi terbuka, sehingga jumlah udara yang masuk ke intake manifold besar. ECU membaca kondisi ini dengan mengatur menambah injeksi lebih besar dari posisi stasioner (iddle) dan pada akhirnya membuat rpm tinggi.\n\nAdapun sistim ISC pada Toyota Greco menggunakan dua cara kerja, yaitu:\n\n1. Rotari selenoid open (RSO) dan rotari selenoid close (RSC) yang diatur oleh electronic ECU, dan \n\n2. Menggunakan sistem wax atau lilin yang digabungkan di bawah throttle. Saat mesin dalam temperatur dingin, saluran udara akan terbuka, sehingga rpm saat mesin dingin menjadi tinggi (fast idle). Saat temperatur kerja mesin sudah normal, wax atau lilin diatur sistem akan mencair dan menutup saluran udara agar rpm mesin berubah ke idle (800rpm).\n\nTips:\n\na. Untuk pemeriksaan elektronic sebaiknya dilakukan di bengkel resmi atau umum untuk memastikan apakah ISC rotari selenoidnya bekerja normal atau macet. Nah, kalau Pak Agus mau mencoba sendiri bisa diperiksa ISC selenoidnya menggunakan voltmeter apakah tegangan listrik dari ECU (5-12 volt) pada socket throttle ISC sudah ada. Kalau ada berarti ECU bagus.\n\nApabila soket ISC dilepas dan rpm mesin masih tinggi, kemungkinan ISC rotari selenoidnya rusak (macet). Coba dibersihkan atau ganti baru.\n\nb. Untuk pemeriksaan wax cukup dibersihkan saluran air pendingin yang masuk ke ISC. Bila sudah umur akan terjadi karat di dalam saluran ISC, sehingga membuat air pendinginan mampet. Coba dibersihkan atau ganti baru.\n\nPertanyaan 2\n\nUntuk Toyota Greco tahun 1994, sistem elektroniknya hanya ada di mesin dan menggunakan EFI (electronic fuel injection). Untuk transmisi matiknya menggunakan full hidrolik jadi apabila diubah ke transmisi manual tidak berpengaruh.\n\nSemoga Toyota Great Corolla Pak Agus Sarbito semakin nyaman dan awet untuk dirawat.\n\nWassalam \nH Agus Mustafa\n\nhttps://www.liputan6.com/otomotif/read/2478612/solusi-otomotif-putaran-mesin-great-corolla-tinggi-ketika-iddle',30,'2020-07-07 18:26:46',NULL),(19,19,'Mau Pelihara Vespa Klasik, Simak Tips dan Triknya','Motor Vespa klasik memiliki penggemarnya tersendiri. Tak hanya orang tua, anak muda pun ada yang terpesona dengan Vespa klasik.\n\nPerlu diketahui juga motor ini miliki beragam model, seperti Vespa PX, Vespa Excel, Vespa Super dan lain-lain.\n\nKarena sudah berumur tua, sudah pasti perawatan yang dilakukan lebih banyak dan pemilik harus telaten. Namun bukan berarti perawatannya sulit. Pradipta Fadhli, salah seorang pengguna Vespa Super sejak 2012 berikan tips mudah anti repot dalam merawat motor jenis ini.\n\n\"Merawat Vespa dapat terbilang mudah. Pengguna harus peka terhadap bunyi-bunyi yang muncul dari Vespanya, seperti suara yang timbul dari magnet kipas mesin, pertanda bahwa mungkin ada permasalah dari kopling atau lainnya\", ujarnya.\n\nOli Samping\nJangan lupa juga untuk mencukupi oli samping yang miliki peranan penting dari mesin dua tak. Apabila tak demikian maka mesin mudah panas dan piston yang sedang bekerja dapat tersangkut, sehingga membuat kinerja mesin Vespa tak optimal. Tetapi juga tak boleh terlalu banyak, oli samping yang terlalu banyak dapat di lihat dari busi yang basah.\n\nBagian lain yang perlu diperhatikan adalah CDI. Mempunyai peranan paling penting dalam sebuah motor. CDI yang sudah rusak dapat diketahui dari knalpot Vespa yang sering nembak atau meledak. Keunggulan dari CDI sendiri, kekuatannya yang dapat bertahan hingga bertahun-tahun lamanya, serta mudah untuk dipasang kembali.\n\nPlatina\nSedangkan untuk Vespa yang masih menggunakan platina disarankan untuk selalu mengecek kembali kendaraannya sebelum bepergian. Jika memang waktunya ganti, dapat dilihat dari platina yang sering tergeser.\n\nMeluruskan kembali platina yang tergeser ke posisinya disarankan untuk tidak memukulnya karena dapat mengakibatkan baut platina menjadi \'slek\' atau posisinya yang salah secara permanen. Kekurangan dari platina itu sendiri ada di letak yang berada di dalam magnet kipas. Membuat susah untuk mengeluarkan dan memasangkannya kembali.\n\nSesekali Vespa juga harus dibawa bepergian jauh untuk mengetahui kelebihan dan kekurangan motor tersebut. Disarankan untuk membawa suku cadang yang penting, seperti tali kopling, tali gas, dan busi.\n\nSumber: Otosia.com\n\nhttps://www.liputan6.com/otomotif/read/4151845/mau-pelihara-vespa-klasik-simak-tips-dan-triknya',30,'2020-07-07 18:33:29',NULL),(20,18,'Mau Beli Mercedes-Benz W124 Alias Mercy Boxer? Ini Tipsnya','Mercedes-Benz W124 atau dikenal dengan panggilan Mercy Boxer, merupakan salah satu lini produk Mercedes-Benz yang melegenda di Indonesia.\n\nDi Indonesia sendiri merupakan mobil tua bangka (motuba) yang hingga kini masih banyak dicari meskipun usianya sudah mencapai 3 dekade.\n\nJika Anda ingin meminangnya, berikut tips yang perlu Anda perhatikan.\n\n1. Interior\n\n\"Yang spesial dari mobil ini adalah interiornya, karena harga jual dan kondisi dari mobil ini sangat dipengaruhi oleh kondisi interior,\" buka Heri, manajer operasional bengkel DTM Station saat ditemui GridOto.com (6/5).\n\nSpare part fast moving dan slow moving mobil ini memang cenderung murah untuk mobil Eropa, tapi part interiornya lah yang hampir sulit untuk dicari jika rusak.\n\nKalaupun ada sebatas barang copotan dari mobil lain.\n\nHeri juga menjelaskan, kondisi mobil keseluruhan bisa dikatakan baik bila pada bagian interior terawat tanpa ada kerusakan berarti karena pemilik mobil pasti merawatnya dengan baik.\n\n2. Transmisi\n\nPada Mercedes-Benz W124 memiliki varian transmisi manual dan otomatis.\n\n\"Varian transmisi otomatis memiliki value dan harga jual yang lebih tinggi karena varian ini terdapat banyak opsi yang tidak dimiliki varian manual,\" jelas Heri.\n\nOpsi yang ada antara lain jok kursi elektrik, pengaturan teleskopik stir elektrik, dan krey belakang elektrik.\n\nNamun, secara perawatan memang varian transmisi otomatis lebih mahal, mengingat umurnya sudah lebih dari 3 dekade dengan jarak tempuh yang cukup tinggi.\n\n3. Mesin\n\nPada model pre-facelift, sistem pembakaran yang digunakan sudah menggunakan injeksi namun masih mekanikal dengan fuel distributor.\n\nPerawatannya lebih murah karena fuel distributor jika rusak bisa diservis, tidak harus diganti.\n\nNamun, Anda perlu sering mengecek kondisi komponen tersebut karena sifatnya mekanikal yang settingannya bisa berubah sewaktu-waktu.\n\nLain halnya dengan model facelift dengan mesin baru dan sistem pembakarannya yang sudah full injeksi menggunakan ECU dan harness.\n\nPerawatan lebih trouble free tapi jika harness sudah rusak perlu diganti keseluruhan dengan biaya yang cukup tinggi.\n\n4. Varian\n\nTerutama pada model facelift, terdapat varian tertinggi yaitu Masterpiece, Sportline, dan Masterpiece Sportline.\n\nTiga varian tersebut memang yang paling banyak dicari, disamping keberadaannya yang cukup langka.\n\n\"Namun, ini yang perlu menjadi kewaspadaan Anda saat membeli karena banyak pedagang bandel karena kurang pemahaman soal varian,\" ujar Heri.\n\nMisalkan ada yang menjual Mercedes-Benz W124 seharga model E320 Masterpiece, tapi ternyata setelah dicek hanya emblem tambahan dan bukan model Masterpiece.\n\n\"Paling mudah membedakannya adalah hadirnya trim kayu pada setir dan konsol dashboard serta panel tengah, juga hadirnya krey belakang dan kiri-kanan penumpang belakang,\" tutup Heri.\n\nhttps://www.gridoto.com/read/221030734/mau-beli-mercedes-benz-w124-alias-mercy-boxer-ini-tipsnya?page=3',30,'2020-07-07 18:40:21',NULL),(21,19,'Kolektor Bagi-bagi Tips Memilih Motor Yamaha RX-King, Kuncinya di Komstir','Bagi pecinta motor, Yamaha RX-King punya karakter tersendiri.\n\nMesin dengan akselerasi cepat, imej motor jalanan, serta desain yang klasik, membuat motor ini punya penggemar fanatik.\n\nTertarik meminang motor dengan julukan \"Raja Jalanan\" ini? \n\nPerhatikan beberapa trik ini, disadur Motorplus-online.com dari kolektor RX-King kelas kakap!\n\nPenggemar dan kolektor RX-King ini adalah H.Hedi Achmad Sugandi, atau lebih sering dipanggil H. Gandi.\n\nKoleksinya bermacam-macam, dari varian langka sampai yang sudah dimodifikasi.\n\nMau mengikut jejak Gandi? Tentu harus tahu cara melihat motor incaran.\n\nJika benar-benar ‘pemain’ baru, lebih baik ajak teman yang sudah mengerti.\n\nBahkan yang sudah mengerti saja, kadang belum paham secara keseluruhan.\n\nBiasanya dilihat hanya sepintas saja. Kaki-kaki, mesin, kelengkapan dan lainnya.\n\nSupaya bisa lebih detil, Gandi punya triknya nih.\n\nMenurutnya, mudah saja melihat motor RX-King tersebut sudah ‘capek’ atau belum.\n\n“Lihat di komstirnya. Kalau sudah ada cekungan, berarti itu motor yang capek\"\n\n\"Bisa ada cekungan, karena biasanya motor kalau parkir, setangnya dibelokkan dan ‘bersandar’ di komstir itu\" \n\n\"Selain itu, kalau lagi putar balik, juga biasanya dipentokin,” ungkap Gandi.\n\nNamanya motor berjalan, pasti pernah parkir.\n\n“Betul memang, tapi cari yang cekungannya tidak terlalu dalam,” wantinya lagi.\n\nNah, tambah lagi ilmu untuk deteksi motor nih.\n\nJika tertarik untuk ‘bermain’ RX-King, ada dua pilihan jalur.\n\nPertama membeli sendiri dan proses membangun, dan kedua serahkan ke bengkel.\n\nKalau memutuskan membeli sendiri, disarankan pilih yang tahun muda.\n\nYakni mulai keluaran 2000-an.\n\nIni disebabkan berbagai part masih banyak tersedia di toko, karena part number dari komponen juga masih ada.\n\nSangat berbeda dibanding tahun tua, yang sudah tidak ada lagi.\n\nAtau mau menempuh jalur via bengkel.\n\nMenurut Gandi, untuk proses restorasi yang bagus, angkanya berawal sekitar Rp 15 juta.\n\nJika full restorasi, membengkak jadi sekitar Rp 40-50 juta.\n\nAkan semakin mahal, jika lebih banyak menggunakan barang orisinal, terutama yang tahun tua. \n\nHarga yang pantas, untuk menebus sang Raja dengan kondisi prima.\n\nhttps://www.motorplus-online.com/read/251645923/kolektor-bagi-bagi-tips-memilih-motor-yamaha-rx-king-kuncinya-di-komstir?page=all',30,'2020-07-07 18:47:09',NULL),(22,18,'TIPS & TRIK BIKIN DATSUN LEBIH SPORTY & ELEGAN','Apapun mobilnya, pasti ada pemiliknya yang gatal ingin mendadani atau memodifikasi agar tampilan mobil lebih ciamik. Tapi ingat, jangan asal modif sebab modifikasi itu memiliki etika dan estetika dalam pengaplikasiannya. “Termasuk dalam memodifikasi Datsun,” sahut Gatut Satrio, dedengkot Datsun Go Club Indonesia (DGCI).\n\nSeperti diketahui Datsun bermain di kelas mobil LCGC atau Low Cost Green Car. Dan mobil LCGC itu punya pembeda dengan mobil lainnya yaitu mobil yang irit atau efisien bahan bakar. “Dari ketentuan tersebut saja, cara memodifikasi mobil kita pun harus tidak menghilangkan sisi efisiensi mobil Datsun tersebut,” lanjut Gatut yang sehari-hari berkutat di media modifikasi.\n\nDatsun memiliki dua (2) varian model, yaitu Datsun GO+ yang diperuntukkan bagi keluarga dikarenakan kapasitas kabin dengan 5+2 seat atau tiga (3) row seat dan Datsun GO hatchback yang sepertinya cocok bagi anak muda.\n\nEKSTERIOR\n\nDari sisi eksterior, Datsun GO+ dan Datsun GO keluaran pabrikan bentuknya sudah mumpuni. “Jadi tak banyak yang mengutak-atik bodinya,” lanjut Gatut. Banyak para pengguna yang sudah mendandani tampilan di sektor lampu depan (Head Lamp) seperti menambahkan reflektor, neon flexi, angle eye, dan HID.\n\nDRL (Day time Running Light) pun sering disematkan, baik di bagian gril atau di daerah bumper untuk menambah kesan manis dan modern. Sedang bagian Fog Lamp, biasanya para pemaka Datsun menyematkan untuk menambah kesan elegan, baik produk keluaran OEM ataupun aftermarket.\n\nKAKI-KAKI\n\nUntuk sektor kaki – kaki, biasanya terjadi penggantian per. Baik menggantinya dengan produk lowering kit pabrikan OEM, aftermarket ataupun custom. “Tapi perlu diperhatikan bahwa penggantian per harus tanpa menghilangkan sisi kenyamanan, karena suspensi salah satu faktor kenyamanan pengendara dan penumpang mobil tersebut.”\n\nSementara untuk velg, karena velg bawaan Datsun hanya velg kaleng ukuran ring 13 inci, biasanya para pengguna menggantinya dengan velg keluaran OEM, aftermarket ataupun menggunakan velg Nissan March karena PCD dan ukuran Center Bore-nya pas dan sesuai dengan Datsun.\n\n“Tapi perlu dicermati, saat penggantian velg kita harus memahami karakter mobil dan harus disesuaikan dengan karakter dan alur modifikasi yang akan kita lakukan. Ada velg Racing, Elegan, dan juga Rally Look. PCD yang Datsun miliki adalah 4 x 100, sehingga disarankan penggantian velg di ukuran tersebut. Sebab velg mengambil peranan sebagai penentu efisiensi atau tidak,” tegas Gatut.\n\nINTERIOR\n\nKita bergerak ke bagian interior. Banyak yang sudah melapisi joknya dengan cover sythetic leather ataupun juga melapisinya secara permanen dengan bahan synthetic leather. Sering juga ditemui ada yang menggunakan warna–warna cerah, warna netral, bahkan sering kita dapati joknya menggunakan pelapis dengan aksen–aksen tertentu, seperti menyematkan ornamen busa tambahan dan juga kancing. Sehingga terkesan elegan dan mewah.\n\nKarena Datsun tidak menyematkan peredam pada bodi dalam, banyak pengguna menambahkan peredam dengan bahan aspal lembaran untuk bagian bodi bawah (floor) dan samping. Sedang untuk atap biasanya menggunakan peredam dengan bentuk busa yang ditempekan pada langit–langit kabin. Dengan begitu kabin lebih nyaman kedap suara.\n\nSelamat memodifikasi!\n\nhttps://www.otomotifmagz.com/tips-trik-bikin-datsun-lebih-sporty-elegan/',30,'2020-07-07 18:52:17',NULL),(23,19,'Cara Bikin Honda Astrea Bertenaga Untuk Harian','Honda Astrea merupakan motor yang cukup fenomenal. Motor ini terbukti tangguh dan irit. Pada akhir tahun 2000an motor ini masih sering dijumpai di jalan raya, namun kini sebagian besar masyarakat sudah beralih ke motor bebek dengan sistem injeksi dan juga scooter matic. \n\nNah bagi brader and sister yang masih punya motor Honda Legenda sebaiknya dirawat dengan baik, dan jangan takut ketinggalan jaman dengan motor jaman sekarang. Honda Legenda juga bisa dijadikan motor drag atau harian yang bertenaga. \n\nBerikut kabaroto.com mendapatkan cara mengoprek motor Honda Astrea Legenda dari Ahass Bintang Motor agar lebih bertenaga.\n\nHonda astrea memgadopsi mesin 100cc, dan untuk mendongkrak tenaga motor ini susah susah gampang, karena jarak keempat baut silinder terlalu dekat dengan liner atau boring. karena terlalu dekat, susah untuk dibore up gede-gedean, itu karena pilihan diameter piston pengganti jadi terbatas.\n\nNah cara sederhana yang bisa dilakukan adalah Honda Legenda ini bisa mengadopso piston Honda Sonic yang diameter pistonnya 58 - 60mm. Pertama yang harus dilakukan adalah bongkar seluruh mesin termasuk bagian crankcase yang juga harus dibuka.\n\nKemudian yang harus dilakukan adalah membubut mulut crankcase yang posisinya menghadap pantat blok silinder sebanyak 3mm. Kenapa bagian ini harus dibubut, agar boring bisa masuk dengan leluasa.\n\nSelanjutnya adalah silinder blok, sebelum memasang atau menentukan liner piston baru, anda harus menjelaskan ke bengkel bubut untuk memapas bagian blok yang punya jarak renggang dengan baut blok sebanyak 2 mm.\n\nJika langkah-langkah tersebut diatas sudah dilakukan, selanjutnya bubut bagian blok yang berjarak lebih rapat. samakan diameter agar berbentuk bulat sepenuhnya, bengkel bubut sudah paham dengan hal ini jadi anda tidak perlu repot menjelaskan lagi. Setelah itu liner baru dengan diameter 60,5 mm, lalu tinggal pasang piston. mau ukuran 58 atau 60 mm, tinggal tentukan sesuai kebutuhan.\n\nsetelah semua proses diatas anda lakukan, kembali pasang, minta teknisi anda untuk menseting karburator dan sesuaikan angin dengan bahan bakar agar lebih sempurna. Jika sudah rapi, anda jangan menggunakan bahan bakar seperti premium, gunakan bahan bakar yang kadar oktannya lebih tinggi agar tidak ada suara menggelitik yang keluar karena knocking.\n\nhttps://kabaroto.com/post/read/cara-bikin-honda-astrea-bertenaga-untuk-harian',23,'2020-07-13 15:08:57',NULL),(24,19,'Tips Perawatan Suzuki Satria F150, Biar Si Ayam Jago Tetap Bertaji!','Setiap orang pasti pengen motor kesayanganya selalu sehat dan siap digunakan bepergian kapan pun, tak terkecuali bagi pemilik Suzuki Satria F150.\n\nSuzuki Satria F150 memang usianya boleh dibilang udah cukup tua nih, untuk tahun paling awal bahkan sudah lebih dari 10 tahun.\n\nNah, berikut ini ada tips untuk merawat Suzuki Satria F150 kamu.\n\n\n1. Jangan lupa perawatan berkala\n\nSalah satu pengguna Satria F150 yang umurnya lebih dari 10 tahun adalah Tiarno Panjita, yang sudah nunggang si ayam jago sejak 2006.\n\nTips pertama agar motor awet ala Panji, adalah melakukan perawatan berkala seperti tune-up.\n\n\"Perawatan seperti ganti oli, kampas kopling, sama part fast moving lainnya, yang berat-berat terhitung jarang,\" terang Panji, dikutip dari Otomania.com.\n\nSelama Panji menggunakan Satria F150, penggantian paling berat di mesin adalah piston.\n\nSekadar informasi, Satria F150 yang digunakan Panji adalah yang CBU Thailand yang konon punya silinder head yang lebih keras materialnya.\n\n\"Untuk perawatan biasa di bengkel resmi, namun sekarang sering juga ke bengkel spesialis,\" tambah Panji.\n\n\n2. Mau modifikasi? Gunakan part yang ringan\n\nSatria F150 milik Panji masih terlihat standar karena yang diganti alias modifikasi hanya warna bodi dan kaki-kakinya saja.\n\n\"Cukup pakai striping Raider 150R Thailand serta cowling mesin bawah agar mempertahankan kesan ayam jago Thailand,\" jelas Panji.\n\nSelain itu Satria F150 miliknya menggunakan rear set dan ban soft compound, agar riding position dan handlingnya semakin mantap untuk bermanuver.\n\nPanji juga meremajakan piringan cakram menggunakan cakram milik Satria F150 FI yang sudah model wave/bergelombang.\n\n\"Bagi saya Satria FU150 itu sudah keren tidak usah ekstrim ubahannya biar makin menawan,\" kata Panji.\n\nSecara spesifikasi Satria F150 sudah mumpuni dari awal, mulai dari performa mesin, ditambah dengan bobot motor yang ringan.\n\n\"Makanya segini saja sudah enak mau dipakai riding, touring, sampai masuk sirkuit,\" jelas Panji.\n\nKarena ubahannya minim, perawatan motor juga lebih mudah dan kondisi motor mudah dipantau jika terjadi masalah.\n\n\n3. Rutin mencuci motor\n\nBicara soal memantau kondisi motor, trik dari Panji adalah rutin mencuci motor dan kalau bisa dilakukan sendiri.\n\n\"Selain kondisi motor tetap bersih, sewaktu kita mencuci kita bisa mengecek kondisi motor seperti kerenggangan rantai atau baut-baut yang hilang,\" ungkap Panji.\n\nApalagi jika Satria F150 dipakai harian seperti miliknya, jika rutin dicuci akan mengurangi kemungkinan masalah seperti karat dan cat pudar.\n\nWalau sudah 12 tahun kondisi Satria F150 yang diberi nama Saskia ini masih tetap mulus.\n\n\n4. Sering-sering dipakai\n\nSelama 12 tahun dipakai Satria F150 milik Panji sudah \"disiksa\" di berbagai medan dari aspal sampai tanah.\n\nSalah satu poin krusial agar motor tetap awet justru malah sering dipakai riding.\n\n\"Kalau motor jarang dipakai, bagian mesin seperti gearbox malah jadi rusak karena lubrikasi oli tidak jalan,\" terang Panji.\n\nAlasan lain kenapa sering dipakai membuat Satria F150 masih enak dipakai, karena pesona motornya sendiri.\n\n\"Karena motornya enak dipakai makanya sering dipakai riding bikin kita makin senang merawat Satria,\" tutup Panji.\n\nItulah empat trik dari pengguna Satria F150 agar motornya tetap awet meski usianya diatas 10 tahun.\n\nhttps://www.gridoto.com/read/221765953/tips-perawatan-suzuki-satria-f150-biar-si-ayam-jago-tetap-bertaji?page=all',23,'2020-07-13 15:28:05',NULL),(25,18,'Cara Merawat Toyota Starlet Agar Tetap Handal Dibawa Naik Gunung','Mobil lawas Toyota Starlet masih memiliki penggemar fanatik. \"Starlet itu mobil lawas yang jarang rewel, apalagi kalau perawatannya bagus, mobilnya bandel, diajak kemanapun, menempuh medan apapun tak pernah bermasalah,\" ujar Kepala Mekanik Bengkel Saman Speed Yogya, Sugiyanto alias Kubil kepada Tempo Jumat 15 Maret 2019.\n\nKubil yang juga aktif di Indonesian Startlet Community (ISC) Yogya itu menuturkan Starlet tak kalah performanya dengan mobil-mobil favorit keluaran terbaru seperti Toyota Avanza saat diajak menanjak atau melewati medan berat.\n\n\"Saya memakai Starlet 1989, pergi ke daerah dataran tinggi lancar tak pernah masalah,\" ujar Kubil yang sudah pernah membawa Starlet tuanya ke area wisata Dieng, Banjarnegara sampai Hutan Pinus Mangunan Yogya itu.\n\nKubil menuturkan selama menangani Starlet sejak 17 tahun, persoalan umum yang sering dikeluhkan pengguna Starlet yakni terjadinya overheat mesin. Hal ini disebabkan biasanya karena pengguna kurang memperhatikan radiator, selang-selang, motor fan yang memicu overheat cepat pada mobil lawas itu.\n\nPadahal, ujar Kubil, Starlet sebenarnya termasuk mobil yang awet dan tak rentan overheat. Sebab selang sirkulasi air pada mesinnya juga tak begitu banyak. Sistem pendinganannya juga masih belum menggunakan blok berbahan alumunium tapi masih besi sehingga lebih rentan kropos atau korosi.\n\nKubil menuturkan jika persoalan overheat ini terus dibiarkan lama lama membuat performa mesin Starlet jadi menurun dan tak stabil. Baham bakar juga bisa lebih boros karena pembakaran tak sempurna. \"Bisa bisa turun setengah mesin kalau sudah kena, seperti (silinder) head melengkung, harus cek semua,\" ujarnya.\n\nKubil menuturkan mobil yang pertama diluncurkan di Indonesia sejak tahun 1985 itu idealnya memang dicek rutin kondisinya tiap jarak tempuh 5 ribu kilometer atau tiap tiga sampai empat bulan sekali.\n\n\"Terutama Starlet kotak yang keluaran sebelum tahun 1990, karena pengapiannya masih memakai platina harus lebih rutin, karena bahan platina kerjanya lebih berat dan berpotensi terjadi kerak,\" ujar ayah dua anak itu.\n\nBerbeda halnya dengan Starlet keluaran lebih baru atau di atas tahun 1995 yang sudah menerapkan sistem CDI atau elektrik yang lebih simpel perawatannya.\n\nKubil menuturkan untuk seri seri Starlet lawasan di bawah tahun 1990, agar mesin tetap tokcer hal yang perlu diperhatikan memang lebih baragam. Misalnya pada saluran distribusi bahan bakar, filter bensin harus dicek guna memastikan sirkulasi bahan bakar lancar. Sebab jika saluran dan tangki sampai kaburator sampai tersumbat akan menggangu kinerja mesin.\n\n\"Starlet seri lama (di bawah tahun 1990) saluran bahan bakar dan tangki rentan keropos,\" ujarnya.\n\nhttps://www.gooto.com/read/1186978/cara-merawat-toyota-starlet-agar-tetap-handal-dibawa-naik-gunung',23,'2020-07-13 15:58:39',NULL),(26,18,'Tips Membuat Toyota Agya Lebih Responsif','Mobil murah ramah lingkungan dari Toyota memang terbukti tak murahan. Terbukti sejak diluncurkan pada akhir tahun lalu, Astra Toyota Agya tampak sudah banyak seliweran di jalanan kota-kota besar di Indonesia.\n\nTapi bagi sebagian orang, mobil jenis lCGC ini masih perlu dioprek lagi agar lebih powerfull. Diantaranya adalah dengan menambahkan fitur, aksesoris hingga mendongkrak performa sesuai dengan yang diinginkan.\n\nUntuk membuat Astra Toyota Agya lebih responsive, Anda bisa mengaplikasi grounding wire kit. Hasil yang akan didapatkan adalah mobil yang akan terasa lebih bertenaga pada putaran bawah serta tengah. Selain itu, distribusi arus kelistrikan juga akan terasa lebih stabil.\n\nAnda bisa mencari kabel ground aftermarket yang banyak dijual di pasaran. Untuk pemasangannya, Anda hanya membutuhkan kunci 10 mm dan tang. Caranya adalah dengan memasang kepala kabel grounding cabang 5 pada kepala busi kutub negatif\n\nSetelah itu, masing-masing kabel dihubungkan pada bagian kepala silinder. Hal tersebut dimaksudkan agar proses pengapian busi lebih stabil dan menghasilkan pembakaran yang lebih optimal.\n\nKabel lainnya dihubungkan ke bagian alternator dengan maksud agar pengisian aki lebih maksimal. Sedangkan kabel ke tiga dihubungkan ke bagian throttle body dengan tujuan agar buka tutup gas lebih responsif.\n\nDua kabel terakhir dihubungkan ke ground body yang terletak di bagian kanan dan kiri sektor mesin. Untuk peletakannya sendiri bisa pada bagian rumah sokbreker kanan ataupun dekat relay fuse.\n\nDengan mengaplikasikan cara diatas, mobil LCGC Astra Toyota Agya dijamin lebih responsive dan lebih powerful.\n\nSumber: http://www.toyota-astrido.com/index.php/home/tips_c/read/75.',23,'2020-07-14 15:19:06',NULL),(27,19,'Apa itu Thailook ?','Apa itu Thailook ?\n\nTHAILOOK (MoThai) adalah modifikasi motor ala THAILAND,\nDi Indonesia pun banyak anak remaja yang memodifikasi motor dengan style THAILAND (THAILOOK)\nAda yang bilang kalo modif  THAILOOK itu kejam,eettsss… bukan berarti kekerasan ,maksud dari kejam adalah harga dari akseso nya yang lumayan menguras kocek para modifikator, tentunya  harga tidak menjadi penghalang bagi udah me-modifikasi motor nya dengan konsep THAILOOK  karena sudah hoby.\nKarena tingginya minat untuk memodifikasi THAILOOK / MOTHAI semakin tinggi juga minat para oknum-oknum tidak bertanggung jawab untuk menjual part / akseso palsu dari akseso thailand tsb, ya semoga aja kita tidak menjadi korban ya ,maka dari itu kita harus jeli dalam memilih akseso yang kita beli ,\nmungkin yang pemula atau masih awan dengan modifikasi thailook\nsilahkan lihat – lihat pada blog Pontianak Automodifikasi ini dan jangan lupa untuk like atau menyebarkan (share) blog ini ke teman atau status anda agar diketahui. \n\nhttps://thailookstyleblog.wordpress.com/2016/05/16/apa-itu-thailook/',23,'2020-07-14 15:42:12',NULL),(28,18,'Tips Irit BBM Toyota Kijang Kapsul','Meski sejujurnya hal ini agak sulit dilakukan, namun ternyata tips irit BBM ini dapat diterapkan pada Toyota Kijang Kapsul baik yang masih mengusung sistem pasokan bahan bakar karburator maupun yang sudah EFI.\n\nSudah menjadi rahasia umum bila Toyota Kijang tidak irit dalam hal konsumsi BBM. Sebab teknologi mesinnya masih sederhana nan kuno, sehingga kerugian mekanisnya masih tinggi. Apalagi Toyota Kijang Kapsul yang masih menganut sistem pengabutan bahan bakar karburator. Namun, bukan berarti tips irit BBM Toyota Kijang Kapsul tidak dapat dilakukan, ada beberapa hal yang harus Anda perhatikan jika ingin tips irit BBM ini berhasil.\n\nYang pertama adalah Anda harus kenal terlebih dahulu mobil Kijang Kapsul yang Anda bawa. Toyota Kijang kapsul dengan sistem pasokan bahan bakar injeksi alias Electronic Fuel Injection (EFI) pasti akan punya kemampuan konsumsi BBM yang lebih ekonomis dari versi karburator. Sebab injeksi elektronik mampu mengatur debit bahan bakar sesuai dengan kebutuhan mesin dengan diatur oleh sistem elektronik.\n\nTips irit BBM Toyota Kijang Kapsul ini dapat dimulai dari cara Anda mengemudi mobil. Untuk Toyota Kijang Kapsul yang mesinnya masih mengusung 7K dengan karburator, ssahakan pindah gigi pada putaran mesin maksimum 2.800 rpm, karena torsi maksimum sebesar 139 Nm keluar di rentang putaran mesin itu. Sementara untuk Toyota Kijang yang sudah mengadopsi sistem injeksi berkode 7K-E maksimum gear up di 3.200 rpm.\n\nTak kalah penting juga setup karburator. Untuk Toyota Kijang yang masih pakai karburator jangan lupa mengecek settingan karburator agar selalu optimal. Spuyer yang dipakai juga harus pas dengan kebutuhan. Sementara untuk Toyota Kijang yang sudah menganut injeksi Anda hanya perlu membiasakan diri eco driving agar konsumsi BBM Toyota Kijang Kapsul Anda selalu ekonomis.\n\nhttps://hargatoyota.com/konsultan-mobil/tips-irit-bbm-toyota-kijang-kapsul-aid1281',23,'2020-07-14 16:27:36',NULL),(29,19,'Cara Gampang Jaga Performa Yamaha RX-King Tetap Awet','Yamaha RX-King sudah menjadi salah satu motor yang legendaris di Indonesia. Usianya rata-rata sudah lebih dari 10 tahun. Meski demikian, masih banyak yang terjaga kondisi mesinnya. Banyak yang menyebutkan bahwa memelihara motor 2-tak seperti RX-King itu mudah. Pernyataan tersebut terbukti dari banyaknya RX-King yang hingga kini masih banyak terlihat di jalan raya.\n\nSuryadi, anggota King Club Djakarta (KCDj), mengatakan, RX-King tak hanya mudah perawatannya, tapi juga konstruksi mesinnya bisa dibilang cukup sederhana.\n\n“ Perawatan mesinnya sangat mudah, yang penting jangan sampai telat oli sampingnya,” ujar pria yang akrab disapa Bacung tersebut, saat dihubungi Kompas.com, belum lama ini. Oli samping pada motor 2-tak memiliki fungsi yang sangat penting. Fungsinya sama seperti pelumas mesin lainnya. Tapi, kalau oli samping hanya melumasi bagian silinder dan kepala silinder saja.\n\n“Jika oli sampingnya telat, nanti bisa merusak bagian piston, dinding silinder, dan pasti merembet ke bagian lainnya juga,” kata Bacung, yang kesehariannya bekerja sebagai mekanik di bengkel Surya Motor, di bilangan Kebon Jeruk, Jakarta Barat. Jika bagian piston dan sekitarnya sudah rusak, bisa dipastikan akan berpengaruh juga terhadap performanya.\n\nhttps://otomotif.kompas.com/read/2020/02/29/094200515/cara-gampang-jaga-performa-yamaha-rx-king-tetap-awet',23,'2020-07-14 16:50:18',NULL),(30,19,'Trik Dasar, Banyak yang Tak Tahu Cara Naik Motor Trail','Sepeda motor tipe trail yang mampu menjelajah jalur on dan offroad memang memiliki ground clearance lebih tinggi. Ini tak lepas dari penggunaan pelek berukuran 21 inci bagian depan dan pelek 18 inci pada bagian belakang, serta ban pacul yang menambah postur motor menjadi lebih tinggi.\n\nDemikian juga bagian shock breaker disetting berbeda dengan tipe skuter matik, bebek ataupun sport, sehingga dudukanya pun terasa jangkung. Karenanya motor trail mampu melahap medan dengan kontur jalan bebatuan atau lumpur.\nHal ini pula yang kerap membuat para calon konsumen urung membeli motor trail karena bentuknya cukup tinggi.\n\nNamun begitu Instruktur Safety Riding Wahana, Siswanto menyatakan, semua orang memiliki kesempatan untuk mengendarai motor, baik trail atau cross, maupun tipe sport.\n\nKarena itu, Siswanto memberikan sedikit tips cara menunggangi sepeda motor trail, khususnya bagi mereka yang postur badannya kurang tinggi.\n\n\nPosisi Motor Diam atau Belum Menyala\n\n“Kalau memang tidak sampai naik untuk motor cross, maka yang dilakukan adalah posisi standar tetap miring, kemudian kaki (bagian kiri) menginjak step, lalu badan berdiri (seperti naik tangga), kemudian kaki kanan melangkahi jok dan mendarat di step kanan,” ungkap Siswanto saat ditemui Liputan6.com, beberapa waktu lalu.\n\nJika sudah melakukan itu, lanjutnya, bagian pantat sedikit digeser, lalu naikkan posisi standar samping.\n\nSelanjutnya, kata Siswanto, untuk setiap akan berhenti, maka bagian pantat harus selalu digeser. Jika berhenti, ada baiknya menggunakan kaki kiri.\n\n“Pasti nyampe, karena paling nyaman itu kaki kiri, robohnya lebih susah. Untuk naik motor trail, jika merasa akan terpeleset, kaki harus mau turun, ini untuk menyeimbangkan,” ungkapnya.\n\n\nPosisi Motor Berjalan\n\nJika telah mengetahui trik naik motor tinggi, kini giliran saat motor trail dengan posisi mesin siap menyala.\n\nYang patut diperhatikan ketika bermanuver motor trail jelas berbeda dengan motor sport. Di mana posisi badan ikut miring atau kerap disebut cornering atau belok rebah.\n\nUntuk motor trail, posisi pantat ikut digeser ke arah berlawanan baik kanan atau kiri. Tak lupa, bagian kaki turun sesuai arah belokan.\n\n“Kalau tanjakan, gerakan badan, kalau naik badan agak depan, kalau turun tarik ke belakang. Geser ke kanan kiri pantatnya, kita harus mau turunin kaki kalau mau kepeleset,” ungkap.\n\nSelain itu, kata Siswanto, saat menggunakan trail, ada baiknya meminimalkan penggunaan rem depan.\n\nhttps://www.liputan6.com/otomotif/read/2909490/trik-dasar-banyak-yang-tak-tahu-cara-naik-motor-trail',23,'2020-07-14 17:22:43',NULL),(31,19,'Mau Pelihara Vespa Klasik, Simak Tips dan Triknya','Motor Vespa klasik memiliki penggemarnya tersendiri. Tak hanya orang tua, anak muda pun ada yang terpesona dengan Vespa klasik.\n\nPerlu diketahui juga motor ini miliki beragam model, seperti Vespa PX, Vespa Excel, Vespa Super dan lain-lain.\n\nKarena sudah berumur tua, sudah pasti perawatan yang dilakukan lebih banyak dan pemilik harus telaten. Namun bukan berarti perawatannya sulit. Pradipta Fadhli, salah seorang pengguna Vespa Super sejak 2012 berikan tips mudah anti repot dalam merawat motor jenis ini.\n\n\"Merawat Vespa dapat terbilang mudah. Pengguna harus peka terhadap bunyi-bunyi yang muncul dari Vespanya, seperti suara yang timbul dari magnet kipas mesin, pertanda bahwa mungkin ada permasalah dari kopling atau lainnya\", ujarnya.\n\nOli Samping\nJangan lupa juga untuk mencukupi oli samping yang miliki peranan penting dari mesin dua tak. Apabila tak demikian maka mesin mudah panas dan piston yang sedang bekerja dapat tersangkut, sehingga membuat kinerja mesin Vespa tak optimal. Tetapi juga tak boleh terlalu banyak, oli samping yang terlalu banyak dapat di lihat dari busi yang basah.\n\nBagian lain yang perlu diperhatikan adalah CDI. Mempunyai peranan paling penting dalam sebuah motor. CDI yang sudah rusak dapat diketahui dari knalpot Vespa yang sering nembak atau meledak. Keunggulan dari CDI sendiri, kekuatannya yang dapat bertahan hingga bertahun-tahun lamanya, serta mudah untuk dipasang kembali.\n\nPlatina\nSedangkan untuk Vespa yang masih menggunakan platina disarankan untuk selalu mengecek kembali kendaraannya sebelum bepergian. Jika memang waktunya ganti, dapat dilihat dari platina yang sering tergeser.\n\nMeluruskan kembali platina yang tergeser ke posisinya disarankan untuk tidak memukulnya karena dapat mengakibatkan baut platina menjadi \'slek\' atau posisinya yang salah secara permanen. Kekurangan dari platina itu sendiri ada di letak yang berada di dalam magnet kipas. Membuat susah untuk mengeluarkan dan memasangkannya kembali.\n\nSesekali Vespa juga harus dibawa bepergian jauh untuk mengetahui kelebihan dan kekurangan motor tersebut. Disarankan untuk membawa suku cadang yang penting, seperti tali kopling, tali gas, dan busi.\n\nSumber: Otosia.com',23,'2020-07-14 17:39:16',NULL),(32,18,'Waw Magic! Fitur Ini Bikin All New Toyota Altis Bisa Ngerem Sendiri','PT Toyota Astra Motor (TAM) telah resmi meluncurkan All New Toyota Corolla Altis pada Kamis 12 September 2019 lalu. Tak hanya satu, TAM menghadirkan tiga varian All New Corolla Altis yakni tipe 1.8G dan 1.8V berbahan bakar besin, serta satu tipe lainnya yang termasuk golongan Hybrid Electronic Vehicle (HEV).\n\nYoshihiro Nakata selaku President Director PT TAM mengatakan kehadiran All New Corolla Altis varian HEV merupakan bagian dari bentuk dukungan TAM kepada pemerintah untuk mengembangkan kendaraan elektrik di Indonesia. All New Toyota Corolla Altis dikembangkan dalam platform Toyota New Global Architecture (TNGA).\n\nSebagai salah sedan legendaris di Indonesia, All New Corolla Altis generasi terbaru sudah dibekali dengan fitur kekinian yang biasa ditemui di mobil-mobil keluaran terbaru saat ini. Dari sisi keamanan, All New Corolla Altis dilengkapi fitur blind spot monitor dan tujuh airbags. Selain itu, salah satu teknologi menarik lainnya yakni hadirnya Toyota Safety Sense (TSS) untuk varian HEV. Teknologi ini mencakup sejumlah fitur yang berfungsi menghadirkan perlindungan optimal pada pengguna kendaraan untuk meminimalisir potensi terjadinya kecelakaan.\n\nLalu, apa saja fitur keselamatan canggih yang disematkan melalui TSS? Berikut penjelasan lengkapnya.\n\nPre-Collision System (PCS)\nFungsi fitur ini adalah memperingatkan pengemudi jika ada potensi terjadinya benturan dengan objek atau kendaraan yang berada di depan. Peringatan dalam bentuk bunyi akan aktif dengan memanfaatkan sensor yang berada di depan mobil. Fitur Pre Collision System akan aktif pada kecepatan 10-180 km/jam. Jika potensi tabrakan semakin besar, maka fitur ini akan mengaktifkan rem secara otomatis.\n\nDynamic Radar Cruise Control (DRCC)\nHampir sama dengan fungsi Pre Collision System, DRCC berfungsi untuk memantau keberadaan kendaraan lain di depan dan menjaga jarak aman. Sensor yang digunakan berupa kamera yang tersemat di balik kaca depan mobil. Sebagai catatan, fitur ini baru aktif jika mobil sudah dikendarai di atas kecepatan 50 km/jam. Kecanggihan lain dari fitur ini yakni mampu secara otomatis mengurangi dan menjaga jarak dengan kendaraan di depan jika ada kendaraan lain yang tiba-tiba memotong jalur ketika mobil sedang berjalan. Fitur DRCC juga memungkinkan untuk melakukan pengereman secara otomatis jika diperlukan.\n\nLane Departure Alert dengan Steering Assist (LDA)\nFitur ini berfungsi untuk membaca marka jalan dengan memanfaatkan kamera depan. Tujuannya untuk memastikan mobil berjalan lurus sesuai marka jalan dan tidak miring. Jika mobil dirasa melenceng dari jalur sebenarnya, kemudi akan otomatis mengendalikan mobil kembali ke jalur yang lurus. Fitur LDA pada All New Corolla Altis bisa membaca marka jalan berwarna kuning maupun putih.\n\nAutomatic High Beam (AHB)\nLampu akan otomatis menyesuaikan dengan kondisi sekitar dengan memanfaatkan fitur ini. Jika ada mobil di depan, maka sorot lampu akan turun secara otomatis sehingga tidak membuat silau pengendara lain dari arah berlawanan.\n\nAdapun All New Corolla Altis menggunakan motor listrik P610 HEV transaxle bertenaga 72 PS, setara dengan mesin 2ZR-FXE yang dikombinasikan dengan Hybrid 4 silinder. Dari sisi driving performance, penggunaan platform TNGA membuat handling mobil menjadi lebih stabil dan kokoh. Selain itu, kapasitas bagasinya juga lebih besar karena menggunakan suspensi belakang double wishbone.\n\nUntuk fitur hiburan, All New Corolla Altis dilengkapi head unit display delapan inci yang terintegrasi dengan sistem monitor hybrid, miracast, Apple car play, bluetooth, wifi, maupun Smart Device Link (SDL).\n\nhttps://momobil.id/news/fitur-ini-bikin-all-new-toyota-altis-bisa-ngerem-sendiri/',23,'2020-07-15 04:51:03',NULL),(33,19,'Menyiasati Roda Depan Goyang pada Bebek \"Jadul\"','Kecintaan Muhammad Faizal pada sepeda motor bebek (cub) lama (jadul), ternyata membawanya pada penemuan, yang bisa menjadi solusi salah satu permasalahan yang sering dialami model ini.\n\n\"Saya suka dengan motor jenis ini, jadi bagaimanapun saya akan berusaha untuk membuatnya tetap nyaman. Hingga akhirnya saya menemukan permasalahan ini dan juga menemukan solusi,\" ujar Rizal yang juga pemilik bengkel modifikasi Wander Machine, Senin (11/5/2015).\n\nRizal menjelaskan, penyakit yang selalu dialami bebek jadul ini ada di sokbreker depan. Suspensi bawaan pabrik yang tidak teleskopik membuat lebih rentan terhadap gangguan. Seperti roda mudah goyang, posisi roda menjadi tidak center, kendali menjadi tidak sempurna, dan saat melewati jalan rusak kontrol jadi terganggu.\n\n\"Jenis sokbrekernya fork parkit. Menggunakan tuas as yang berfungsi sebagai perantara antara roda dan suspensi. Jadi tidak seperti sokbreker pada umumnya, dan titik lemahnya adalah parkit yang berada di posisi belakang yang menghubungkan fork dengan tuas. Plus laher pabrikan yang terbuat dari bahan pvc membuat mudah terkikis dan menjadi \'oblak\' ,\"ujar Rizal.\n\n\nMengatasi hal tersebut akhirnya Rizal menemukan \"laher bambu\" sebagai solusi permasalahan tersebut. Penggunaan laher bambu beserta pen yang dibuat RIzal, menjadikan sokbreker dan ban depan jadi lebih nyaman tanpa ada gangguan. \"Kita biasa menyebut laher bambu, berfungsi untuk meringankan gesekan, lebih lentur, dan tahan lama,\" beber Rizal.\n\nRencananya Rizal akan memproduksi paket komponen berikut dengan parkit set. Namun untuk saat ini hanya menyediakan ongkos pemasangan. \"BIsa datang ke rumah dengan membawa sokbreker beserta fork, untuk pemasangan laher bambu berikut jasa pemasangan  Rp 250.000,\" ujar Rizal.\n\nBerminat? Sambangi Wander Machine di kawasan Cipinang Muara, Jakarta, atau hubungi 081266762971.\n\nhttps://otomania.gridoto.com/read/241168513/menyiasati-roda-depan-goyang-pada-bebek-jadul',23,'2020-07-15 05:01:07',NULL),(34,19,'Ini yang Harus Diperhatikan Sebelum Membeli Vespa Tua','Vespa kini makin digemari oleh kalangan muda hingga orang tua. Selain karena desain klasiknya, motor buatan Italia tersebut dicari sebab sudah tidak dapat diragukan lagi akan kekuatannya.\n\nNah, bagi yang berminat beli Vespa, pemuda bernama Pradipta Fadhli yang telah lebih dari lima tahun menjadi pengguna vespa tua membagikan tips terkait hal itu.\n\n\nCek Surat-surat Kendaraan\n\nMencocokan surat-surat kendaraan Vespa tersebut. Mulai dari nomor mesin dan nomor rangka. Ketidakcocokan surat dan nomor rangka dapat disimpulkan bahwa mesin atau rangka Vespa tua tersebut telah diganti.\n\nSelain itu, nomor rangka bisa saja tidak jelas terlihat karena terkikisnya nomer rangka akibat body Vespa yang sudah berumur.\n\n\nPeriksa Kondisi Mesin\n\nMemeriksa perangkat mesin hingga pengapian dalam kondisi baik atau tidak. Hisapan yang menyemburkan bensin terlalu banyak memungkinkan bahwa hisapan pengapian mesin Vespa tua tersebut sudah tidak layak.\n\nKinerja mesin Vespa tua yang kurang bertenaga dapat disebabkan oleh hisapan yang terlalu menyembur atau boring yang sudah bocor. Boring yang bocor dapat diatasi dengan menambahkan packing agar menjadi rapat kembali.\n\n\nCek Kondisi Kaki-kaki\n\nMemeriksa bagian kaki kaki Vespa. Untuk mengetahui bahwa kaki- kaki Vespa yang layak dapat dilakukan dengan melepaskan kendali. Vespa yang goyang dapat disimpulkan bahwa fork sudah miring.\n\nMenanggulangi fork yang miring dapat dilakukan dengan mengepres fork Vespa tersebut agar lurus \nkembali.\n\n\nJangan Lupa Cek Dek Vespa\n\nDek Vespa yang masih layak dapat dilihat dari bagian tengah Vespa. Apabila cat sudah mengelupas dan dek sudah bengkok, dapat disimpulkan bahwa dek sudah tidak layak.\n\nBahaya apabila dek dipaksakan untuk berkendara, karena akibatnya Vespa dapat terbelah dua. Penanggulangan dapat dilakukan dengan cara mengganti dek atau mengelasnya. Dek Vespa tua pun dapat Otolovers temukan di bengkel cat.\n\n\nKondisi Pengaruhi Harga\n\nTerakhir sebelum membeli Vespa tua, pastikan telah menyiapkan uang untuk membelinya. Kondisi Vespa tua yang bagus atau tidak akan mempengaruhi budget yang dikeluarkan.\n\nhttps://www.otosia.com/berita/ini-yang-harus-diperhatikan-sebelum-membeli-vespa-tua.html',30,'2020-07-15 14:30:52',NULL);
/*!40000 ALTER TABLE `com_forum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_forum_comment`
--

DROP TABLE IF EXISTS `com_forum_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_forum_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_id` int(11) NOT NULL,
  `comment` longtext NOT NULL,
  `user_create` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_forum_comment`
--

LOCK TABLES `com_forum_comment` WRITE;
/*!40000 ALTER TABLE `com_forum_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `com_forum_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_jenismodel`
--

DROP TABLE IF EXISTS `com_jenismodel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_jenismodel` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `jenis_model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_jenismodel`
--

LOCK TABLES `com_jenismodel` WRITE;
/*!40000 ALTER TABLE `com_jenismodel` DISABLE KEYS */;
INSERT INTO `com_jenismodel` VALUES (1,'Sedan','2020-03-29 08:41:47',NULL),(2,'Station Wagon','2020-03-29 08:42:49',NULL),(3,'Pick Up','2020-03-29 08:43:12',NULL),(4,'Motor','2020-03-30 00:07:29',NULL),(5,'SUV','2020-05-08 18:49:32',NULL);
/*!40000 ALTER TABLE `com_jenismodel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_merchandise`
--

DROP TABLE IF EXISTS `com_merchandise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_merchandise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `community_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `harga` double NOT NULL,
  `weight` text DEFAULT NULL,
  `description` longtext NOT NULL,
  `status` enum('Komunitas','Umum') NOT NULL,
  `user_create` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `stat_banned` enum('active','banned') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_merchandise`
--

LOCK TABLES `com_merchandise` WRITE;
/*!40000 ALTER TABLE `com_merchandise` DISABLE KEYS */;
INSERT INTO `com_merchandise` VALUES (18,23,'Kopi Bubuk Aseli Cap Gunung Pesagi Lampung Netto 250 gr','440c7c1c470dd16d1a0ff95ec063f080.jpg',100000,'1','Rahasia para leluhur sebelum, saat dan setelah bekerja keras menyeruput secangkir kaleng kopi tubruk asli terbukti membuat mereka panjang umur, anti sakit, rata2 wafat diatas usia 90an dengan gigi dan fisik yg terawat baik. Terungkap.\n\nHanya dengan minum 1.5 sendok makan setara 12 gram, Kopi Robusta Asli Gunung Pesagi 4 cangkir sehari, anda telah mewarisi tradisi mereka yg sdh lama ditinggalkan karena gaya hidup modern yg lebih berpotensi menimbulkan berbagai jenis penyakit.\n\nKopi Robusta Asli Gunung Pesagi mengandung 2x cafein dan 2x chlorgenic acid antihepatitis, antioxidant, antidiabetes, antihypertensi, lever protector dll.\n\nTinggalkan gaya hidup modern, beralihlah ke resep para leluhur sebagai pencegahan penyakit jantung, penyumbatan pembuluh darah, stroke, alzeimer, diabetes dll.','Umum',33,'2020-06-28 22:21:55','2020-06-28 22:29:08',NULL,'active'),(19,24,'bxkg','4c03db0650b13ebda6effe35af02dac0.jpg',24576764,'0.55','hxkgs','Komunitas',25,'2020-07-02 18:57:12',NULL,NULL,'active'),(20,27,'jams','26dd315f7afa15cc5acd8d0462c2f3c8.jpg',100000,'0.5','test','Umum',20,'2020-07-02 23:24:43','2020-07-02 23:24:52',NULL,'active'),(23,19,'R25 Kwaci','69c1674f7a0eda6b47b978391a75d2ba.jpg',110000000,'1','R25 kwaci no paper posisi Bandung, minta 110jt tanki ada bocor, langsung kontak aja klo yg minat WA +62 818-0722-7934','Umum',30,'2020-07-11 00:38:06',NULL,NULL,'active'),(24,19,'BMW','5cda4f437d1f37217e59dd69f9c84953.jpg',220000000,'1','Punya temen nih, full paper pajak nya off (2017) lokasi di Bandung, yg bukan ori kaca lampu depan sama karburator \nHarga 220jt (tdk ada pm\" an🤭)\nYg minat langsung hub WA +62 838-1742-4672\nGrab fast ,ga bakalan kecewa di jamin👍👍','Umum',30,'2020-07-11 00:40:10',NULL,NULL,'active'),(25,18,'Colt Pickup 1973','921ae31739dfc58e066a374733538a96.jpg',25000000,'1','Mitsubishi colt pickup 1973 built up japan\nSurat lengkap stnk bpkb buku kir\nPajak kir telat semua\nBisa bantu perpanjangan\nBody interior rapih\nBukan dipake muatan\nMesin kaki2 halus\nPerawatan murah\nDaripada beli combi pickup, mihil \nSehat siap gas\nJual cepat open 25 jt\nNego depan unit\nWa 081323443317\nHp 085324528813\nTasikmalaya','Umum',30,'2020-07-11 00:43:05',NULL,NULL,'active'),(26,19,'Mesin Triumph','36eaf81545765bdec20655f56b03bea1.jpg',53000000,'1','Yuk yang jauh mendekat yang dekat merapat \nDi jual mesin Triumph Thruxton karbu th2004 865cc,cari bembeli yang serius aja,untuk harga 53jt bisa negoo..kondisi persis seperti di foto,untuk cek vidio hidupnya  langsung wa saja ke no.0895702028537','Umum',30,'2020-07-11 00:46:20','2020-07-11 01:03:50',NULL,'active'),(27,19,'Mesin Binter','67c262e1e3fad9803f43d9c76e68d2d8.jpg',7500000,'1','Bismillah..\nDi jual Mesin Binter Ss komplit STNK BPKB akur, rangka dapet 2, tangki ada, shock depan belakang, velg roda ada 3..\nbuka harga 7.5\nLokasi Bandung kota\nWa/Telp : 081229702435','Umum',30,'2020-07-11 00:48:23','2020-07-11 01:03:32',NULL,'active'),(28,19,'NSU','9876455a168b86d6698a9609fbf3ac6f.jpg',65000000,'1','Bismillah\nFs ..NSU supermax  250cc 🇩🇪\nMesin sehat siap di pake harian dan jalan jauh\n\nTuker tambah dgn motor tua jg boleh asal cocok ada kembalian ..\n\nNo pape (NP)\nAda stnk nsu untuk pegagan aja ..\n\nBody2 orisinil\nStyle ala Scrambler\nBan masih baru dan gondrong\n\nMotor cukup enak di pake dan nyaman...\n\nBuka Harga 65 jt ..boleh nego sewajar ny..\nMangga di cek n test ride..& nego di dpn unit..\n\nInfo lanjut via wa..\n👇👇👇👇👇👇☎️📲📲☎️\n\n 081381515038','Umum',30,'2020-07-11 00:50:56',NULL,NULL,'active'),(29,19,'BSA','cd862956a83ffd7ed0b4e72c01570c58.jpg',60000000,'1','-#KRISNA USED BIG BIKE#-\n\nBsa c 11G 250 cc tahun 1956 \n�Mesin sehat siap touring \n�Sudah cdi\n�kaca lampu lucas ori \n�spidometer smith ori \n�Kaki kaki ori\n�Peleng depan blakang tk japan (model talang )\n�np((no paper))\n�tas yg ada di motor tidak termasuk\n�Unit di Denpasar-Bali.\n�harga 60jt nego\n�������������\nWa 085858846711\nTerimakasih \nSalam sehat \nSalam olahraga �','Umum',30,'2020-07-11 00:53:00',NULL,NULL,'active'),(30,19,'Yamaha DT100','288b6bf4089e97e957436b7956d8e384.jpg',11000000,'1','Yamaha DT 100. TH 1980.\nSurat Lengkap Plat BE. Mati Pajak Lamaa ( Stnk Bag Depan Hilang )\n11 jt Neggo👍\n081392419100','Umum',30,'2020-07-11 00:55:55',NULL,NULL,'active'),(31,19,'Zundapp','f3f0774d1aa9b8a0e82a96eb8a7c76db.jpg',15000000,'1','Zundapp\nno papper\n15jt ajah nego\n\nmesin lama ga hidup 1th\ncuma buat pajangan barber\n\n082282288315','Umum',30,'2020-07-11 00:58:00',NULL,NULL,'active'),(32,18,'FJ40','f3718c2da4b445a1f84bacc5a52f156b.jpg',175000000,'1','Bismillah ..fs f40 tahun 73\n4x4 oke tv ori gearbox ori\nMesin ori 1f (  Bensin) cukup irit  BBM \nPower steering\nNon AC\nSTNK BPKB komplit D (Bandung)\nKondisi mobil sehat siap harian\n\nHarga open 175jt ..serius mangga nego aja ..klo cocok silakan ....\n\nInfo boleh via wa \n081381515038','Umum',30,'2020-07-11 01:00:16',NULL,NULL,'active'),(33,19,'Binter','8c2631fbc1b2cfc74e60e250ee0b720f.jpg',12000000,'1','1982 Kawasaki Sepeda Motor · Jarak yang sudah ditempuh 1.000.000 kilometer\n\nBinter mercy kz 200\n12jt nego\nLiat aja motor nya di rumah\nNomor wa : 083168952149','Umum',30,'2020-07-11 01:02:59','2020-07-11 01:03:10',NULL,'active'),(34,19,'Super Cub','b7a5698f1cb9a23272118ed0dfa1b64b.jpg',2000000,'1','Scup 81 engkel surat lengkap mati,mesih halus,lokasi kalijudan sby timur\nHarga 2000 nego\nWa 085648467700','Umum',30,'2020-07-11 01:05:36',NULL,NULL,'active'),(35,19,'Yamaha RX King','69e44eda85cc7b8da909c3de7bd8d13f.jpg',16000000,'1','Yamaha RX King \n135 \nHitam \nSurat lengkap Pajak Hidup plat B Jakarta \n1987 \nBlok Y1 os 100 belum OB \nKondisi enak lancar \nOli samping pisah \nMinat 081283220978\nBuka harga 16.000.000 nego \nLokasi Rawamangun','Umum',30,'2020-07-11 01:07:55',NULL,NULL,'active'),(36,19,'2 unit Trail KE 125','31a7ae98f9cb113262aef0e82efa8031.jpg',16000000,'1','2 unit Trail KE 125. Harga 16 jt👍\nBiru .Surat Lengkap Plat L. Pajak Mati .\nHijau. Surat Bpkb  Stnk Hilang.\n081392419100 WA','Umum',30,'2020-07-11 01:10:37',NULL,NULL,'active'),(37,19,'Noken as BSA','7482207f05fd7d80fa75b476488c0aba.jpg',800000,'1','Siapa tau ada yg membutuhkan / buat cadangan\nNoken As / Chamshaft buat BSA c15 / B40 /B44\n\nJual Cepat sajah 800rb\n\nWA 08563959509\n\nJangan lupa follow IG nya @fronzy_store7\n\nLokasi di bandung','Umum',30,'2020-07-11 01:12:51',NULL,NULL,'active'),(38,19,'Royal Enfield Classic 500','5f1f5446ef491e872c361c93a94f4e43.jpg',89000000,'1','Royal Enfield Classic 500 \n2014 \nSurat lengkap \nPajak hidup plat B Jaktim \nKm 7.6xx \nMinat ? 081283220978 \nLokasi Rawamangun \nHarga 89.000.000 nego','Umum',30,'2020-07-11 01:14:48',NULL,NULL,'active'),(39,18,'Opel Kapitan','537e65584c6dd7c3e800a50ed8adca32.jpg',120000000,'1','🗣️ For Sale / Dijual\n🚗 Opel Kapitan\n⚫ Tahun 1961\n⚫ Surat Lengkap\n⚫ Mesin Sehat siap pake\n⚫ Ac Ada , Ban luar 4 baru\n⚫ Full restorasi\n⚫ Pernak pernik Original\n⚫ Tinggal pakai aaja\n⚫ Yang serius bisa move ya ke wa\n⚫ Unit ada di solo\n🌑 SERIOUSLY BUYER!!!\n💰 Open Price 120.000.000 Jt NEGO\n☎️ Cp/Wa 082138379921\n#opelkapiten #opelcar #mobiltua #mobiltuaindonesia #opelvintage #opelforsale #dijualmobilclassic #forsale #mobilclassic  #mobillangka #mungcahrosok #jualmobil #mobilantikindonesia','Umum',30,'2020-07-11 01:17:21',NULL,NULL,'active'),(40,18,'Manual Book','9afb9147e59153a865c80f15651b47c1.jpg',350000,'1','Jaguar XJ6 2.8, 3.4, 4.2 (\'68 -\'78)\nDaimler Sovereign 2.8, 3.4, 4.2\n\n192 Hal. Bhs.Inggris, Jadul\nRp.350.000,00 sdh ongkir (Jawa)\nKontak / WA   081 3332 33336','Umum',30,'2020-07-11 02:35:40',NULL,NULL,'active'),(41,18,'Distributor/Delco Mitsubishi','59e8922bc2fa7f244424c4388afc40cb.jpg',900000,'1','Distributor/Delco Mitsubishi MD009 102\nRp900.000\nWa. 081387339163','Komunitas',30,'2020-07-11 02:38:36',NULL,NULL,'active'),(42,19,'Astrea Grand Bulus 1992','441afce35dfbbe3f431c0bff076b3d9b.jpg',8500000,'1','Dijual Astrea Grand Bulus 1992\nRestored by Guus Motorwork.\n• Asli tahun 1992\n• Surat komplit, pajak baru, kaleng baru\n• Semua parts baru dari depan sampai belakang (list parts dikasih setelah wa karena kalau tulis disini kepanjangan)\n• Kelistrikan On\n• Jangan tanya minus, siap pakai\n\nRp 8.500.000,- NEGO\nLokasi Tembalang, Semarang\n📞 WA 0822-4194-8961\n\nSemarang Free Ongkir diantar sampai depan rumah anda\nSiap kirim ke seluruh indonesia\n! Siapkan uang anda sebelum menawar, No bid n run !','Komunitas',30,'2020-07-11 02:46:47',NULL,NULL,'active'),(43,19,'Suzuki GT 380','1ada3b4b91b45a3b3dc10845f3c92258.jpg',60000000,'1','Suzuki GT 380 cc. Full Restorasi👍Warna Hitam.\nSurat Lengkap. Plat AD..Pajak Hidup\nUnit...Siappp..Gas🏍☁️☁️\n60 jt ❤\n081392419100 WA','Komunitas',30,'2020-07-11 02:48:48',NULL,NULL,'active'),(44,18,'Morris Minor 1952','4ad43142fda0ae7b3f30dd535a452834.jpg',65000000,'1','Morris Minor 1952, body orisinil, nopaper, mesin pakai kijang, lampu sein semaphore masi hidup, siap jalan-jalan. Bengen di Barter motor Tua BSA/ Ariel, siap nambah jika perlu. WA 081548797451. 65 jt nego.  Purwokerto','Komunitas',30,'2020-07-11 02:50:30',NULL,NULL,'active'),(45,19,'Softail nightrain 2007','2772e62efb9c1e3ff75efc1e14f81845.jpg',225000000,'1','-#KRISNA USED BIG BIKE#-\n\nDijual....\nSoftail nightrain 2007\n\n�Renegade style\n�6-speed\n�Knalpot vance hines\n�Side bag\n�Engine guard \n�Mesin kering\n�No paper(surat surat tidak ada)\n�Unit ada di Denpasar-Bali\n�Siap pakai bukan siap dibawa ke bengkel gan..�\n�Harga 225.000.000,- masih nego\n�������������\nLangsung saja di getarkan \nTelp/WA : 085858846711\nSalam sehat\nSalam olahraga �','Umum',30,'2020-07-11 02:53:53',NULL,NULL,'active'),(46,19,'ariel red hunter 1954','7e954a05a75913e5eb27c2bae65e8578.jpg',85000000,'1','-# KRISNA USED BIG BIKE #-\n\nDIJUAL!!\nariel red hunter 1954 350cc\ngood speck =\n\n�frame body original utuh.\n�swing arm original.\n�triple T original.\n�tangki spakbord dpan blkng �original.\n�pengapian CDI (magneto masih terpasang).\n�ban dpan belakang baru tebal.\n�bok aki dan tangki oli original.\n�dek sel cover primary sbelah kiri masih original almunium.\n�foot step,tuas rem,tuas persneling original.\n�standart tengah masih ada ORI.\n�mesin gerbok enak.\n�pompa oli lancar .\n�Surat ada STNK lawas kuno tempelan merk Ariel\n�Harga 85.000.000,-nego.\n�Unit ada di Denpasar-Bali\nDi satroni saja langsung braderr.. \n�������������\nTelp/WA : 085858846711\nSalam sehat\nSalam olahraga','Umum',30,'2020-07-11 02:56:33',NULL,NULL,'active'),(47,19,'helm klasik','1b04aac9d262d17cad1a215e4a5cd41b.jpg',250000,'1','Dijual helm klasik ori lawasan.\nWa 081389459645\nJombang Jatim','Komunitas',30,'2020-07-11 02:59:45',NULL,NULL,'active'),(48,19,'1978 Piaggio Vespa','d0e9324b32be6929c980d472b2b4d846.jpg',17000000,'1','1978 Piaggio Vespa · Jarak yang sudah ditempuh 6.000 kilometer\n\nVespa sprint 78 ( coklat susu )\nMesin sehat\nPajak Off\nSurat komplit\nKelistrikan On\nBody lempeng\n\nMinat hub \nWa : 08986630890','Umum',30,'2020-07-11 03:02:16',NULL,NULL,'active'),(49,19,'YDT 100','c38d4e817cda5d5daae87878ba1303dd.jpg',8500000,'1','Bahan Ydt 💯\nSurat BPKB saja plat L\nMasih perlu sentuhan\nNo rangka dan no mesin akur\nIDR 8,5 nego\nLokasi Lampung barat, Rekber ON\nInfo detail Wa 082282626978\nTerimakasih 🙏','Komunitas',30,'2020-07-11 03:04:31',NULL,NULL,'active'),(50,19,'Batok l2g repro','58594f443206bc2e61268cd25ef44b83.jpg',150000,'1','Batok l2g repro chrom set dengan kaca depan dan lampu .mau set dengan kupingan cnc juga bisa .bisa di japri aja chat atau wa 088261606186','Umum',30,'2020-07-11 03:06:50',NULL,NULL,'active'),(51,19,'Suzuki Crystal','e0c3c340ca56bf605933e7f07b7dae7b.jpg',3500000,'1','Keadaan\n\n- Ss komplit mati (minus) \n-Warna sesuai surat (real) \n-oli samping pisah\n-lampu on\n-MESIN MANTAP. JANGAN DI RAGUKAN. \n-Warna antik jarang ada. \n-(Minus) mesin tua. Dan spda tua😁\n\nSiap kirim luar kota. \nMasing masing 3.5 jt\n\nWa 081336514381\nLokasi jember','Umum',30,'2020-07-11 03:09:03',NULL,NULL,'active'),(52,19,'Trail honda','a901c2c714cd61606af568179827b3e4.jpg',8000000,'1','Trail honda siap gas\nMesin megapro boring head tiger\nShock depan ori ts\nMonoshcok ts \nRangka trail beijing 200\nTangki ori ts\nKarbu pe\nRem depan blkng cakram\nMotor siap gas\nMinus stnk kw\nJual unit 8 jt\nRoling sasis tanpa mesin 5.5 jt\nLokasi bandung \nNo wa 08888202405\nYg serius aja','Umum',30,'2020-07-11 03:12:01',NULL,NULL,'active'),(53,19,'v80 deluxe','015b8c8f174095f926eaafd5d91ad3a5.jpg',3100000,'1','Dijual v80 deluxe robot tahun 84 surat komplit pajak telat bln 3 kmarin, plat K pwd, body utuh tdk keropos, mesin normal, oli samping pisah, indikator bensin, spedometer jln, harga 3,1 lokasi Ungaran, minat bisa tlp/Wa : 081333553407','Umum',30,'2020-07-11 03:14:59',NULL,NULL,'active'),(54,19,'Japstyle','3bdd3a95a731ecb7b069dbd5e505558c.jpg',5000000,'1','Japstyle clasic basic GL surat komplit plat.B\n5jt nego\nwa 0823 2877 9700 Lok moga pemalang','Umum',30,'2020-07-11 03:17:34',NULL,NULL,'active'),(55,19,'lampu depan astra','fbdd922f44f62607240e39fc7c536557.jpg',420000,'1','Di jual lampu depan astra tahun 68,\nBarang bagus no minus dan udah di crome,\nHarga 420rb nego santuy,,,\nMinat WA/ Tlpn di 082247194444','Komunitas',30,'2020-07-11 03:19:18',NULL,NULL,'active'),(56,19,'honda C70','5aacb5f9beb51a4c2975d8561e4fa5e7.jpg',10000000,'1','Bismillah\n\nDijial honda C70 basic astrea star th 1996. \n\nCat mulus sprti motor baru tdk ada lecet. Bodi utuh masih terbungkus plastik. 😊Pajak hidup AB Bantul, STNK BPKB siap. Mesin halus kering, stater hidup, lampu2 dan sein nyala. Ban baru, part2 baru, dan full acesories. Harga 10,5 jt nego. \n\nYg berkenan bisa WA 083195869000. \n\nLokasi Gunungkidul Yogyakarta. \n\nSiap kirim kemana saja. \n\nFollow  IG : omahklasikjogja','Umum',30,'2020-07-11 03:21:40',NULL,NULL,'active'),(57,19,'v80 dobel starter','41c796af4bcc5fcce19c25d8a8951f4b.jpg',3500000,'1','Dijual v80 dobel starter jringgg .\nSurat lengkap mati\nMesin sehat alussssss olsam pisah\nKelistrikan lampu depan blakang nyala\nLampu senja hidup\nLampu sein depan blakang hidup\nCat rapih terawat\nBel juga hidup\nMotor siap pakek. Kaki2 ruji juga baru.\nMesin kering. \nLokasi Mojokerto harga 3.5 freeongkir se jawa jamin amanah. Wa 085648300593','Umum',30,'2020-07-11 03:23:37',NULL,NULL,'active'),(58,19,'Yamaha L2 Super','81b7f23297cdf4e922914531d1e1cb24.jpg',3500000,'1','Surat lengkap of mesin sehat walaupun motor tua dan bisa mengenang kenangan di th 80an harga 3.5 jt nego wa 0895377447788 serius only','Umum',30,'2020-07-11 03:25:22',NULL,NULL,'active'),(59,19,'Bagasi samping','1236ecafbaf894197afdc5e9d1d66bb4.jpg',400000,'1','Bagasi samping / side box\n✓ Bahan dari serat fiber\n✓ Untuk motor klasik\n✓ Sudah termasuk bracket\n✓ Bracekt untuk motor Honda C70\n✓ tidak termasuk baut\nUkuran \nP : 21 cm L : 9 cm\nT Depan : 18 cm\nT Belakang : 15 cm\n    { Terimakasih }\n\nCatatan penting !!\nBisa hubungi via : \nWA : 085216208385\nLapak : keklasikan\nFAST RESPON','Komunitas',30,'2020-07-11 03:27:03',NULL,NULL,'active'),(60,19,'V80 thn 82','d91a25fa96d118c29e8bb6fa79566a5c.jpg',2500000,'1','V80 thn 82. .asli cdi.\nss komplit pajak off.\nmesin juara, olie pisah.\nklistrikan lampu dpn blkg aj.\nslebor depan baru, jok baru, jari2 baru.\nHarga 2500 free ongkir. .kurang murah nego di wa aja 0895703311700. .','Umum',30,'2020-07-11 03:28:44',NULL,NULL,'active'),(61,19,'astrea 800','ea0b0865089d7ef8329dba6261f13023.jpg',3400000,'1','4sale astrea 800\nss lengkap off ciamis\nmotor siap nongky\nmesin repeh, adem ayem\ntarikan joss\nkelistrikan nyala\nstater jalan\nindikator jalan\n3,4 nego\nwa 083116596242','Umum',30,'2020-07-11 03:30:33',NULL,NULL,'active'),(62,19,'Jaket running MX','336fb76f45a9a221bf3b6509265a24d3.jpg',200000,'1','Jaket running MX nya gan,, UK L dan XL.\n200 rb aja... Obat dinginn.. kualitasnya wah gan.\nBarang limited edition.\nBarang jelek bisa dikembalikann. \n\nTerima grosir dan reseller\n\nYang dekat bisa melipir (kota malang) yang jauh gak usah khawatir, bisa dikirim ke seluruh penjuru bumi.\n\njoin menjadi reseller GRATIS\nWa 081233890682\nhttps://bit.ly/2vITsvD\nIG 2f_trailapparel\nalwistore_malang','Umum',30,'2020-07-11 07:22:28',NULL,NULL,'active'),(63,19,'Sepatu Boot Trail','932269344437a9403000d1ff2341bc06.jpg',1300000,'1','Assalamualaikum wr.wb..\nIjin buka lapak nih siapa tau ada yg tertarik sama \nSepatu boots buat ngetraillnya, produksi sendiri karya anak bangsa ni boss👌\nBahan dari full kulit asli kualitas sudah terjamin bgus, udh pake besi atas bawah jdi udh pasti safety pkonya aman tahan air sama tahan lumpur👌\nHarga 1,3 jt + bisa request nama/warna dan nomber sesuai keinginan, bisa di sesuai kan juga dengan jerseynya👌\nPengiriman ke seluruh Indonesia via paket j&t \n‌MINAT bisa tanya\" dlu langsung inbok/WA:089678620738','Umum',30,'2020-07-11 07:28:06',NULL,NULL,'active'),(64,18,'fiat','5bea0ccb6dc4cec318136021b60974d5.jpg',17000000,'1','Dijual fiat semi bahan, surat komplit pajak off 2013 plat H jateng ,pernak pernik komplit siap naik cat, list\" ada dicopot body utuh sudah dilas semua, mesin bagus tinggal rapiin paking gasket sudah saya belikan 1 set  detail info foto\" sebelum saya poxy dan sesudah silahkan wa 08972152888\nBuka harga 17jt nego  sejadinya','Umum',30,'2020-07-11 07:55:19',NULL,NULL,'active'),(65,18,'KE25 1973,','c767faacb08147ea1a0928c6bbd46855.jpg',140000000,'1','KE25 1973, FP Pajak Hidup, Lokasi Jakarta 0857 7280 5880','Umum',30,'2020-07-11 14:01:38',NULL,NULL,'active'),(66,18,'Volvo + Suzuki','9ea66ec34f659be9078553196bf9ba76.jpg',120000000,'1','- Volvo 264 GL tahun 1984, BPKB & STNK lengkap (Plat F), pajak 0ff 1 tahun\n\n- Suzuki GS 500 E Slingshot, NP, Tahun 1995, Original, Accecories lengkap, gambar detail via japri\n\nNote: boleh beli satuan\n\nInfo lengkap: 081212392014','Umum',30,'2020-07-11 16:48:30',NULL,NULL,'active'),(67,18,'VW GOLF MK 1','6e0a12ca5a23b8b190d2d25939cebec0.jpg',40000000,'1','VW GOLF MK 1 1978 surat mati mesin prima jual murah BU 40 jt nego, hubungi 08129064485','Umum',30,'2020-07-11 16:54:10',NULL,NULL,'active'),(68,19,'Kawasaki estrella 250cc','bcc58f8669babfc0129ccb75ffb62e7c.jpg',50000000,'1','Kawasaki estrella 250cc, warna hijau, tahun 2015\nMasih tangan pertama, jarang dipakai, terawat baik\nNomor polisi AD klaten, motor di jakarta pusat\nBarusan bayar pajak 5 tahunan (ganti plat nomor)\nMotor masih asli bawaan pabrik, belum di custom\nHanya ganti spion bulat & plat nomor depan pindah dibawah lampu\nSpion ori & dudukan plat nomor ori disimpan (dikasih ke pembeli)\nSudah pasang standar tengah & rak bagasi belakang\nBelum lama ganti aki (20/01/2020), kunci kontak masih 2 biji\nManual book masih ada, bonus kampas rem depan\nDijual Rp 50 juta. Nanya2 by WA 081227550807\nMinus: ada jerawat di blok mesin kiri tapi tidak pengaruhi performa','Umum',30,'2020-07-11 17:11:58',NULL,NULL,'active'),(69,18,'Honda Accord','67962f58831b74efbc77dd626db7d772.jpg',45000000,'1','For sale!!!\nHonda Accord GEN1 1979 (ex Dubes)\n-full paper pajak 05-2020 plat 05-2024\n-bodi kenceng \n-cat mulus 100% masih kondisi baru cat\n-tidak keropos lantai UTUH\n-AC sangat dingin\n-mesin masih tangguh sudah teruji\n-tidak rembes\n-kaki2 empuk tidak gemblodak\n-interior rapih bersih\n-crom,list,panel utuh\n-velg bbs klasik R13\n-ban baru 4 titik\n-Audio standard tidak gembret\nSudah AUX DVD\n\nKELEBIHAN:\n-SILAHKAN BROWSING SEBELUM MEMBELI\nunit ini tersedia hanya 1 karena interior all blue colour\n-bekas KEDUTAAN BESAR NEW ZEALAND (Buildup)\n-Build up\n\nKEKURANGAN:\n-pajak telat bulan 5 kemarin 2020\n-selebihnya cek unit\n\nOPEN PRICE: \nRp:45.000.000,-\n\nWa/tlp: 081214588181\n(Owner DADYS STANCE GARAGE)\nLokasi: Jombang jawa timur','Umum',30,'2020-07-11 18:38:26',NULL,NULL,'active'),(70,18,'MORRIS COUPLE VAN','582c8e3523d59847929b4857c37dc1fc.jpg',200000000,'1','FOR SALE/DIJUAL :\nMORRIS COUPLE VAN \nCOMERCIAL 915 cc\nTahun 1957,Warna Merah\nEX DISHUB cuma 2 unit di Indonesia\n(yg banyak beredar type TRAVELLER Seperti Oplet SI DUL) \nSURAT LENGKAP BPKB STNK \nPajak Hidup 01 2021\nStnk sampai 01 2022\nCat Repaint kurang bagus\nMesin original standar\nBody Masih original\nKondisi ban depan belakang Oke \nPosisi di Purwokerto\nJawa Tengah\n200jt nego\nwa 085728509300','Komunitas',30,'2020-07-12 04:39:02',NULL,NULL,'active'),(71,19,'HD Softail Custom Chopper','58f9ca5f51a0e01250d074e6262f7a70.jpg',182000000,'1','Jual HD Softail Custom Chopper\nFrame asli HD basic Softail Evo\nNo-ka No-Sin akur tembus web HD\nMesin kering siap gas antariksa\nKnalpot VnH, ban 18-21 belakang ukuran 200\nVelg Fatdaddy big spoke, Daymaker,\nape hanger, open air filter, dll\nKondisi muluusss istimewaaa\nUnit di Jogja, hrg 182jt nego sopan\nFoto detail dan Video ready\nWA 081225073529','Umum',30,'2020-07-12 04:42:42',NULL,NULL,'active'),(72,18,'FIAT 1300 Station Wagon 1965.','c417ad1ffb44368e6d129dd82dcb954d.jpg',50000000,'1','BU penawaran hanya untuk satu bulan\nFIAT 1300 Station Wagon 1965.\nSecara umum kondisi baik sekali, merupakan mobil harian saya\nMesin 100% original FIAT 1300, masih sangat halus lus lus..\nGearbox upgrade FIAT 125\nKarburator Weber\nFull Paper (NoPol cantik, D 1300 CH)\nCat warna Sable Cream by Sikkens\nFinishing Clear by Spies Hecker\nFull Restorasi tahun 2018\nLantai luar dalam dilapisi Flintkote\nJok bagus rapi\nSpeedometer berfungsi normal, lampu kabin nyala\nVelg 15\" original KIA\nAccu akhir 2019\n\nPR:\nPajak blm bayar sejak September 2018, kaleng habis September 2019.\nDoortrim dan karet pintu blm diganti\nNo power steering, no AC, no audio, just original vintage car\n\nFor serious buyer WA 0811565681','Umum',30,'2020-07-14 01:57:14','2020-07-14 01:57:35',NULL,'active');
/*!40000 ALTER TABLE `com_merchandise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_merk`
--

DROP TABLE IF EXISTS `com_merk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_merk` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `merk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_merk`
--

LOCK TABLES `com_merk` WRITE;
/*!40000 ALTER TABLE `com_merk` DISABLE KEYS */;
INSERT INTO `com_merk` VALUES (1,'Audi','2020-02-24 13:26:24','2020-02-24 13:26:24'),(2,'BMW','2020-02-24 13:27:10',NULL),(3,'Citroen','2020-02-24 13:33:03','2020-02-24 13:36:26'),(4,'mercedes','2020-05-08 18:48:58',NULL);
/*!40000 ALTER TABLE `com_merk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_polling`
--

DROP TABLE IF EXISTS `com_polling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_polling` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('INDUK','CABANG') DEFAULT NULL,
  `community_id` int(11) NOT NULL,
  `topik` varchar(255) NOT NULL,
  `jawaban_a` varchar(255) NOT NULL,
  `jawaban_b` varchar(255) NOT NULL,
  `jawaban_c` varchar(255) DEFAULT NULL,
  `jawaban_d` varchar(255) DEFAULT NULL,
  `jawaban_e` varchar(255) DEFAULT NULL,
  `user_create` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_polling`
--

LOCK TABLES `com_polling` WRITE;
/*!40000 ALTER TABLE `com_polling` DISABLE KEYS */;
INSERT INTO `com_polling` VALUES (11,'CABANG',25,'dimana tempat event selanjutnya?','bandung','jakarta','bogor','NULL','NULL',20,'2020-07-02 23:44:35',NULL),(12,'CABANG',27,'test','a','b','NULL','NULL','NULL',20,'2020-07-02 23:45:12',NULL),(13,'CABANG',24,'hai  aki','hhfg','ghhh','bhhh','bhhh','hhuy',25,'2020-07-02 23:49:49',NULL),(14,'CABANG',28,'test','a','aa','NULL','NULL','NULL',38,'2020-07-04 06:44:08',NULL);
/*!40000 ALTER TABLE `com_polling` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_polling_count`
--

DROP TABLE IF EXISTS `com_polling_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_polling_count` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `polling_id` int(11) DEFAULT NULL,
  `count` char(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_polling_count`
--

LOCK TABLES `com_polling_count` WRITE;
/*!40000 ALTER TABLE `com_polling_count` DISABLE KEYS */;
INSERT INTO `com_polling_count` VALUES (1,1,'b',20),(2,2,'a',20),(3,5,'e',12),(4,7,'b',20),(5,8,'b',18),(6,8,'c',30),(7,8,'a',20),(8,8,'e',25),(9,9,'a',30),(10,10,'c',28),(11,10,'a',33),(12,10,'a',30),(13,12,'a',20),(14,11,'d',34),(15,10,'b',38),(16,14,'a',20),(17,10,'b',37),(18,10,'b',45),(19,11,'b',20);
/*!40000 ALTER TABLE `com_polling_count` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_type`
--

DROP TABLE IF EXISTS `com_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_type`
--

LOCK TABLES `com_type` WRITE;
/*!40000 ALTER TABLE `com_type` DISABLE KEYS */;
INSERT INTO `com_type` VALUES (1,'Corolla','2020-02-24 13:35:21','2020-02-24 13:36:01'),(2,'Corona','2020-02-24 13:36:44',NULL),(3,'crown','2020-05-08 18:49:16',NULL);
/*!40000 ALTER TABLE `com_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_usiakendaraan`
--

DROP TABLE IF EXISTS `com_usiakendaraan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_usiakendaraan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `usia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_usiakendaraan`
--

LOCK TABLES `com_usiakendaraan` WRITE;
/*!40000 ALTER TABLE `com_usiakendaraan` DISABLE KEYS */;
INSERT INTO `com_usiakendaraan` VALUES (1,'10','2020-03-29 08:24:08',NULL),(2,'20','2020-03-29 08:24:24',NULL),(3,'40','2020-03-29 08:24:42',NULL),(4,'0-5','2020-05-08 18:49:56',NULL);
/*!40000 ALTER TABLE `com_usiakendaraan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `com_warna`
--

DROP TABLE IF EXISTS `com_warna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_warna` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `warna` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_warna`
--

LOCK TABLES `com_warna` WRITE;
/*!40000 ALTER TABLE `com_warna` DISABLE KEYS */;
INSERT INTO `com_warna` VALUES (1,'Merah','2020-02-24 13:42:21',NULL),(2,'Kuning','2020-02-24 13:42:34',NULL),(3,'Hijau','2020-02-24 13:42:49','2020-02-24 13:43:21'),(4,'Semua Warna','2020-05-08 18:50:13',NULL);
/*!40000 ALTER TABLE `com_warna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `communites_anggota`
--

DROP TABLE IF EXISTS `communites_anggota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communites_anggota` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `community_id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `level` enum('Ketua','Wakil','Pengurus','Member') COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_ktp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Pending','Verified','Not Verified') COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_approve` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=269 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `communites_anggota`
--

LOCK TABLES `communites_anggota` WRITE;
/*!40000 ALTER TABLE `communites_anggota` DISABLE KEYS */;
INSERT INTO `communites_anggota` VALUES (7,2,20,'Ketua',NULL,'Verified',NULL,'2020-06-07 18:04:16',NULL),(8,2,20,'Wakil',NULL,'Verified',NULL,'2020-06-07 18:04:16',NULL),(9,2,3,'Pengurus',NULL,'Verified',NULL,'2020-06-07 18:04:17',NULL),(13,4,20,'Ketua',NULL,'Verified',NULL,'2020-06-08 00:47:49',NULL),(14,4,20,'Wakil',NULL,'Verified',NULL,'2020-06-08 00:47:49',NULL),(15,4,3,'Pengurus',NULL,'Verified',NULL,'2020-06-08 00:47:49',NULL),(16,5,20,'Ketua',NULL,'Verified',NULL,'2020-06-08 06:38:16',NULL),(17,5,20,'Wakil',NULL,'Verified',NULL,'2020-06-08 06:38:16',NULL),(18,5,23,'Pengurus',NULL,'Verified',NULL,'2020-06-08 06:38:16',NULL),(26,8,20,'Ketua',NULL,'Verified',NULL,'2020-06-13 21:44:44',NULL),(27,8,5,'Wakil',NULL,'Verified',NULL,'2020-06-13 21:44:44',NULL),(28,8,3,'Pengurus',NULL,'Verified',NULL,'2020-06-13 21:44:45',NULL),(29,8,12,'Member','b3359e053cf591eb0e4015466808fb81.jpg','Verified',NULL,'2020-06-13 21:59:27',NULL),(33,10,12,'Ketua',NULL,'Verified',NULL,'2020-06-16 05:28:20',NULL),(34,10,2,'Wakil',NULL,'Verified',NULL,'2020-06-16 05:28:20',NULL),(35,10,3,'Pengurus',NULL,'Verified',NULL,'2020-06-16 05:28:21',NULL),(60,6,20,'Ketua',NULL,'Verified',NULL,'2020-06-16 06:01:43',NULL),(61,6,13,'Wakil',NULL,'Verified',NULL,'2020-06-16 06:01:43',NULL),(62,6,13,'Pengurus',NULL,'Verified',NULL,'2020-06-16 06:01:43',NULL),(63,6,20,'Pengurus',NULL,'Verified',NULL,'2020-06-16 06:01:43',NULL),(64,6,21,'Pengurus',NULL,'Verified',NULL,'2020-06-16 06:01:43',NULL),(65,6,22,'Pengurus',NULL,'Verified',NULL,'2020-06-16 06:01:43',NULL),(135,7,14,'Ketua',NULL,'Verified',NULL,'2020-06-16 06:13:43',NULL),(136,7,13,'Wakil',NULL,'Verified',NULL,'2020-06-16 06:13:43',NULL),(137,7,13,'Pengurus',NULL,'Verified',NULL,'2020-06-16 06:13:43',NULL),(138,7,20,'Pengurus',NULL,'Verified',NULL,'2020-06-16 06:13:43',NULL),(139,7,21,'Pengurus',NULL,'Verified',NULL,'2020-06-16 06:13:43',NULL),(140,7,22,'Pengurus',NULL,'Verified',NULL,'2020-06-16 06:13:43',NULL),(141,1,29,'Ketua',NULL,'Verified',NULL,'2020-06-16 06:15:37',NULL),(142,1,14,'Wakil',NULL,'Verified',NULL,'2020-06-16 06:15:37',NULL),(143,1,2,'Pengurus',NULL,'Verified',NULL,'2020-06-16 06:15:37',NULL),(144,9,28,'Ketua',NULL,'Verified',NULL,'2020-06-16 06:15:50',NULL),(145,9,2,'Wakil',NULL,'Verified',NULL,'2020-06-16 06:15:50',NULL),(146,9,3,'Pengurus',NULL,'Verified',NULL,'2020-06-16 06:15:50',NULL),(150,3,20,'Ketua',NULL,'Verified',NULL,'2020-06-17 21:37:43',NULL),(151,3,9,'Wakil',NULL,'Verified',NULL,'2020-06-17 21:37:43',NULL),(152,3,5,'Pengurus',NULL,'Verified',NULL,'2020-06-17 21:37:43',NULL),(153,11,25,'Ketua',NULL,'Verified',NULL,'2020-06-20 00:00:03',NULL),(154,11,2,'Wakil',NULL,'Verified',NULL,'2020-06-20 00:00:04',NULL),(155,11,13,'Pengurus',NULL,'Verified',NULL,'2020-06-20 00:00:04',NULL),(156,12,23,'Ketua',NULL,'Verified',NULL,'2020-06-21 19:10:28',NULL),(157,12,18,'Wakil',NULL,'Verified',NULL,'2020-06-21 19:10:28',NULL),(158,12,28,'Pengurus',NULL,'Verified',NULL,'2020-06-21 19:10:28',NULL),(159,12,20,'Member','e7929b5379dab7d2adf5a04a00f47902.jpg','Verified',NULL,'2020-06-22 04:59:08',NULL),(160,13,18,'Ketua',NULL,'Verified',NULL,'2020-06-22 15:56:07',NULL),(161,13,23,'Wakil',NULL,'Verified',NULL,'2020-06-22 15:56:07',NULL),(162,13,28,'Pengurus',NULL,'Verified',NULL,'2020-06-22 15:56:07',NULL),(166,14,20,'Ketua',NULL,'Verified',NULL,'2020-06-25 20:01:12',NULL),(167,14,9,'Wakil',NULL,'Verified',NULL,'2020-06-25 20:01:12',NULL),(168,14,3,'Pengurus',NULL,'Verified',NULL,'2020-06-25 20:01:12',NULL),(169,14,31,'Pengurus',NULL,'Verified',NULL,'2020-06-25 20:01:12',NULL),(170,14,25,'Member','7be718de5fee734ae1725419ee87bcee.jpg','Verified',NULL,'2020-06-25 23:38:38',NULL),(171,13,25,'Member','d2ed344084611caff6744a77e7b696c5.jpg','Verified',NULL,'2020-06-25 23:39:03',NULL),(175,13,20,'Member','3a37bab1a3779750386da7495a63924f.jpg','Verified',NULL,'2020-06-25 23:56:44',NULL),(176,16,25,'Ketua',NULL,'Verified',NULL,'2020-06-26 00:02:35',NULL),(177,16,5,'Wakil',NULL,'Verified',NULL,'2020-06-26 00:02:36',NULL),(178,16,14,'Pengurus',NULL,'Verified',NULL,'2020-06-26 00:02:36',NULL),(179,17,25,'Ketua',NULL,'Verified',NULL,'2020-06-26 00:06:31',NULL),(180,17,16,'Wakil',NULL,'Verified',NULL,'2020-06-26 00:06:32',NULL),(181,17,20,'Pengurus',NULL,'Verified',NULL,'2020-06-26 00:06:32',NULL),(182,15,25,'Ketua',NULL,'Verified',NULL,'2020-06-26 00:07:26',NULL),(183,15,2,'Wakil',NULL,'Verified',NULL,'2020-06-26 00:07:26',NULL),(184,15,2,'Pengurus',NULL,'Verified',NULL,'2020-06-26 00:07:26',NULL),(202,22,20,'Ketua',NULL,'Verified',NULL,'2020-06-27 03:49:41',NULL),(203,22,3,'Wakil',NULL,'Verified',NULL,'2020-06-27 03:49:41',NULL),(204,22,9,'Pengurus',NULL,'Verified',NULL,'2020-06-27 03:49:42',NULL),(212,23,33,'Ketua',NULL,'Verified',NULL,'2020-06-28 22:15:11',NULL),(213,23,30,'Wakil',NULL,'Verified',NULL,'2020-06-28 22:15:11',NULL),(214,23,23,'Pengurus',NULL,'Verified',NULL,'2020-06-28 22:15:11',NULL),(218,21,14,'Ketua',NULL,'Verified',NULL,'2020-06-29 06:58:51',NULL),(219,21,28,'Wakil',NULL,'Verified',NULL,'2020-06-29 06:58:51',NULL),(220,21,30,'Pengurus',NULL,'Verified',NULL,'2020-06-29 06:58:51',NULL),(231,20,23,'Ketua',NULL,'Verified',NULL,'2020-07-02 01:12:02',NULL),(232,20,14,'Wakil',NULL,'Verified',NULL,'2020-07-02 01:12:02',NULL),(233,20,30,'Pengurus',NULL,'Verified',NULL,'2020-07-02 01:12:02',NULL),(234,21,37,'Member','3c0617d7f0d54b0fae41949c25b2298a.jpg','Verified',NULL,'2020-07-02 04:42:34',NULL),(238,24,2,'Ketua',NULL,'Verified',NULL,'2020-07-02 18:58:14',NULL),(239,24,16,'Wakil',NULL,'Verified',NULL,'2020-07-02 18:58:14',NULL),(240,24,25,'Pengurus',NULL,'Verified',NULL,'2020-07-02 18:58:14',NULL),(244,26,3,'Ketua',NULL,'Verified',NULL,'2020-07-02 19:01:38',NULL),(245,26,25,'Wakil',NULL,'Verified',NULL,'2020-07-02 19:01:38',NULL),(246,26,2,'Pengurus',NULL,'Verified',NULL,'2020-07-02 19:01:38',NULL),(247,27,32,'Ketua',NULL,'Verified',NULL,'2020-07-02 19:22:49',NULL),(248,27,20,'Wakil',NULL,'Verified',NULL,'2020-07-02 19:22:49',NULL),(249,27,20,'Pengurus',NULL,'Verified',NULL,'2020-07-02 19:22:49',NULL),(250,20,25,'Member','f5cc7e3efac3f64059d351b30a8d2591.jpg','Verified',NULL,'2020-07-02 21:34:19',NULL),(251,21,25,'Member','61d6c1584d2e434107d8274c3542954c.jpg','Verified',NULL,'2020-07-02 21:36:30',NULL),(252,20,37,'Member','6cb9638e68d15cc2c65eb02e5f36313d.jpg','Verified',NULL,'2020-07-02 22:35:34',NULL),(253,20,38,'Member','eb645f4ec45a7872019b7a9fd8225001.jpg','Verified',NULL,'2020-07-04 01:56:53',NULL),(254,28,20,'Ketua',NULL,'Verified',NULL,'2020-07-04 05:27:33',NULL),(255,28,38,'Wakil',NULL,'Verified',NULL,'2020-07-04 05:27:33',NULL),(256,25,20,'Ketua',NULL,'Verified',NULL,'2020-07-04 05:36:59',NULL),(257,25,34,'Wakil',NULL,'Verified',NULL,'2020-07-04 05:36:59',NULL),(258,25,32,'Pengurus',NULL,'Verified',NULL,'2020-07-04 05:36:59',NULL),(259,25,38,'Pengurus',NULL,'Verified',NULL,'2020-07-04 05:36:59',NULL),(260,29,20,'Ketua',NULL,'Verified',NULL,'2020-07-06 22:49:53',NULL),(261,29,38,'Wakil',NULL,'Verified',NULL,'2020-07-06 22:49:53',NULL),(262,19,23,'Ketua',NULL,'Verified',NULL,'2020-07-10 22:11:08',NULL),(263,19,30,'Wakil',NULL,'Verified',NULL,'2020-07-10 22:11:08',NULL),(264,19,30,'Pengurus',NULL,'Verified',NULL,'2020-07-10 22:11:08',NULL),(265,18,30,'Ketua',NULL,'Verified',NULL,'2020-07-11 00:41:45',NULL),(266,18,23,'Wakil',NULL,'Verified',NULL,'2020-07-11 00:41:45',NULL),(267,18,23,'Pengurus',NULL,'Verified',NULL,'2020-07-11 00:41:45',NULL),(268,20,45,'Member','b1409858cf61013d34fac7eca0641c32.jpg','Verified',NULL,'2020-07-16 17:53:39',NULL);
/*!40000 ALTER TABLE `communites_anggota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `communities`
--

DROP TABLE IF EXISTS `communities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` enum('mobil','motor') COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `bank_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `noreq` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `atas_nama` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat_detail` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ketua` int(11) DEFAULT NULL,
  `wakil` int(11) DEFAULT NULL,
  `activate` enum('active','banned') COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_create` int(11) NOT NULL,
  `user_approved` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `communities_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `communities`
--

LOCK TABLES `communities` WRITE;
/*!40000 ALTER TABLE `communities` DISABLE KEYS */;
INSERT INTO `communities` VALUES (18,'IAC Car Division','c1887c989f302a0485ade4d53c91e946.IMG-20200623-WA0018.jpg','mobil',79,'Abc','123456','IAC mobil','Jalan Babadak 69, Bogor',30,23,'active',30,NULL,NULL,'2020-06-26 16:22:27','2020-07-11 00:41:45'),(19,'IAC Motorcycle Division','a7cd5d2ade9e551181c6787bfe6682bc.IMG-20200512-WA0000.jpg','mobil',79,'abc','123456','IAC Motorcycle Division','Jalan Babadak 69, Bogor',23,30,'active',23,NULL,NULL,'2020-06-26 16:57:14','2020-07-10 22:11:08'),(23,'Pencinta Touring dan Kopi','5454a3e2ec9fee73d6049cac6ff1e359.1593410775759.jpg','mobil',78,'BCA','0950629398','Anda RukandaKomunita','Jl.Ciomas Raya No.346 D Ciomas, Bogor.',33,30,'active',33,NULL,NULL,'2020-06-28 22:07:01','2020-06-28 22:15:11'),(24,'Iskla','70524adaef86f84e3d6ca7ae4a7837f4.IMG_20200629_150301.jpg','mobil',54,'BCA','0784648466','BRI','Jl raya  sejend',2,16,'active',2,NULL,NULL,'2020-06-29 09:34:54','2020-07-02 18:58:14'),(25,'mobil tes kuning','d198ecc44d6a3cf06921601535a7a6b0.1593452855478.jpg','mobil',232,'bca','123456789','kuning','jl raya kuning',20,34,'active',20,NULL,NULL,'2020-06-29 09:51:23','2020-07-04 05:36:59'),(26,'Karta bersalip','c929eb88410eca0d2ba58fb1ca5fb554.IMG_20200628_171721.jpg','mobil',24,'jcjxus','00875457','hxnx','h,gzhs',3,25,'active',3,NULL,NULL,'2020-07-02 19:01:11','2020-07-02 19:01:38'),(27,'komunitas test','60668ffb384f46944fedaba62cd421bb.1593746440118.jpg','mobil',78,'test','084545454','test','Ciawi bogor',32,20,'active',32,NULL,NULL,'2020-07-02 19:22:49',NULL),(28,'test Komunitas','4d7ab24e37ea2b2e52a0bb7346a8c705.IMG_20200701_080250.jpg','mobil',28,'bca','123','test','ciawi',20,38,'active',20,NULL,NULL,'2020-07-04 05:27:33',NULL),(29,'Komunitas Test Lagi','e124c037c5c4380166863c15ccbd6f3d.IMG_20200701_080250.jpg','mobil',135,'bca','123','ferdi','jogja',20,38,'active',20,NULL,NULL,'2020-07-06 22:49:53',NULL);
/*!40000 ALTER TABLE `communities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `communities_detail`
--

DROP TABLE IF EXISTS `communities_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communities_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `communities_id` int(11) NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `merk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usia_kendaraan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jml_silinder` enum('2 Silinder','3 Silinder','4 Silinder','6 Silinder') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `warna` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_mesin` enum('Bensin','Diesel') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `communities_detail_communities_id_unique` (`communities_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `communities_detail`
--

LOCK TABLES `communities_detail` WRITE;
/*!40000 ALTER TABLE `communities_detail` DISABLE KEYS */;
INSERT INTO `communities_detail` VALUES (1,1,'Disini untuk membuat komunitas baru',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,2,'Bogor',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,3,'matic bogor',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,4,'zh',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,5,'mobil klasik',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,6,'Disini untuk membuat komunitas baru',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,7,'Disini untuk membuat komunitas baru',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,8,'test',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,9,'jshshh',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,10,'ypclyx',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,11,'hclx',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,12,'komunitas motor',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,13,'Komunitas Mobil',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,14,'test',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,15,'g hcuf',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,16,'yfyf',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,17,'tchf',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,18,'Komunitas Mobil tanpa pembatasan merk dan tahun',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,19,'Komunitas Motor Tanpa Pembatasan',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,20,'Perhimpunan Penggemar Mobil Kuno Indonesia Pengurus Daerah DKI Jakarta',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,21,'Disclaimer:\nPeReCi bukan sebuah klub dan tidak ingin menciptakan sebuah klub. PeReCi HANYA sebuah inisiatif mencoba menyatukan para pemilik, pencinta dan penggemar mobil Perancis, sebagai ajang ngobrol ngalor ngidul serta manasin mobil, mewujudkan eksistensinya di Indonesia. Tidak kurang tidak lebih.\n\nRules:\nTata krama standard, no SARAP. Tinggalkan dan tanggalkan klub dulu, yang penting Viva Le France aja. Exit kapan aja, no hard feeling. Kalo ada reference, tinggal sebut nomor untuk di-invite.',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,22,'test',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,23,'Komunitas pencinta touring dan penikmat kopi',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(24,24,'deskriosi',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(25,25,'komunitas kuning',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(26,26,'gzhshd',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(27,27,'komunitas test',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(28,28,'test',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(29,29,'test',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `communities_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `community_gallery`
--

DROP TABLE IF EXISTS `community_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `communities_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `user_create` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community_gallery`
--

LOCK TABLES `community_gallery` WRITE;
/*!40000 ALTER TABLE `community_gallery` DISABLE KEYS */;
INSERT INTO `community_gallery` VALUES (15,19,'7eb8cf73f73e2a7e38187f6e80048965.jpeg','Touring RX King',23,'2020-06-27 04:31:16'),(16,18,'d7f26d60254ffeb00b2349c6de6520e5.jpeg','Komunitas Retro',30,'2020-06-27 04:33:11'),(17,20,'4061983582ebc91657889761906373cb.jpg','Mobil apa tuh?',30,'2020-06-27 04:34:01'),(18,21,'dad5bf7ff1c7ba5a46e196e23d325576.jpg','Kumpul2 citroen 602',30,'2020-06-28 03:03:03'),(19,20,'2a00e6e74ce98e7fa2ea08b02dbe1bfa.jpg','Tapos, 28 Juli 2020',30,'2020-06-28 03:24:54'),(20,19,'038f00184d36f40ae630c12a2fd0dd7c.jpg','Suzuki Satria F150 Club Lamongan',30,'2020-06-30 22:59:07'),(21,19,'9fd550eb188391b845743653d97f853a.jpg','Wave All Star Indonesia',30,'2020-07-02 16:26:15'),(22,18,'9bb9e1be0116a9c3591f6bee2cdfb153.jpg','Touring @Jati Luhur Citroën Club Indonesia',30,'2020-07-03 07:15:35'),(24,18,'bb67fff6b4713b9d4cb5fb24f9810152.jpg','Kopdar Gabungan Jateng-DIY CR-V G3 Brotherhood',30,'2020-07-04 02:59:52'),(25,18,'4811342b959717bcd307b8a8b3ad6a83.jpg','Rally look Bogor meet up',30,'2020-07-04 04:53:02'),(26,18,'eed092c3921e8e3b117f8118786cec05.jpg','Kopdar awal kami Stay Social Datsuncing',30,'2020-07-04 06:25:04'),(27,20,'07e7bbf96e30cf4400efdfcd5b0d776a.jpg','Pose sejenak menjelang Musda DKI',30,'2020-07-04 06:27:31'),(29,19,'843d8a9f6a204b1f4b80aaddcab3e603.jpg','Salam satu lumpur',30,'2020-07-04 15:33:31'),(30,19,'c82e4f96e24d7beae0ca24ed059e0fb0.jpg','Salam satu Fun Rally',30,'2020-07-04 15:58:37'),(31,20,'a41803c528130a83136e0833f83de2a7.jpg','Bali Peduli 2005',30,'2020-07-04 16:05:01'),(32,21,'5ab54374b4caad2e01ff8a876375a5a8.jpg','Salam dari Peugeot L\'autres Indonesie',30,'2020-07-05 00:15:53'),(33,19,'94c34eb35bef15dfa5ba3c16039cc934.jpg','Salam satu lumpur dari borneo (kalimantan-selatan) 🤝',30,'2020-07-05 00:37:06'),(34,18,'6799b9bd099fb7965ea8e74ab88e71cb.jpg','Rekan2 BCC kopdar di Kebon Vintage Cars 😍',30,'2020-07-05 03:30:02'),(35,19,'a5c6c2db45a65ea5342c50ef28e9d9b1.jpg','Salam kenal slur Tasikmalaya menyapa👋',30,'2020-07-05 03:39:34'),(36,19,'4205d37024acba87c36722e71655a692.jpg','Nikahan anak klx tuh kek gini🤣',30,'2020-07-05 03:40:53'),(37,19,'ec2f798dedf2f4f6e18768c92c51c24f.jpg','Sisa Kopdar Komunitas Honda CBR 150R Facelift Indonesia Semalam',30,'2020-07-05 08:25:59'),(38,20,'2a76516f1c2a7154aca976c23a5a3a0d.jpg','PPMKI dan Jokowi',30,'2020-07-05 22:22:31'),(39,18,'1ce6e7cfcddba0d07600f4e493add443.jpg','Kopdar reg JAKARTA #SATUCOROLLASEJUTASAUDARA',30,'2020-07-07 19:14:29'),(40,19,'219f8345ac3fcd9a452722b1828741bd.jpg','Corolla Twincam Owners Club',30,'2020-07-09 04:45:16'),(41,19,'2666313c5e91b8706c0f8616b4bd2c8d.jpg','Absen kopdar malam sabtunya lurd...\nSalam dari Plat P Situbondo\nYg deket mari merapat ngopi bareng.\n#CCI_Situbondo',30,'2020-07-10 07:30:16'),(42,19,'62a50436e1d2bf11def844b0914df02f.jpg','Akibat ulah beberapa rombongan/oknum, seng duwe trail maleh kenek kabeh',30,'2020-07-11 21:09:14'),(43,19,'5c01a4d33357c024e6f2aee3de7af977.jpg','no comment',30,'2020-07-12 00:39:43'),(44,18,'0b7f4ed360581c852328d13ebb9977a4.jpg','nah....',30,'2020-07-12 00:50:36'),(46,19,'b19038943b74dfd4bb169ed42a0c0e7a.jpg','Buat sedulur2 yg tadi sore ikut riding \nPokok nya kalian LUAR BIASA\n#sedulurbebektua\n#grendpringsewulampung\n#timgabut\n#ngopirangopingumpul\n\nHONDA ASTREA LAMPUNG',30,'2020-07-12 16:49:24'),(47,19,'1cb6008c028e647a9033d65c7fe9b08f.jpg','Thank buat para tim gass Jakarta tembus  Anyer.gass dulu baru curhat.',30,'2020-07-13 06:17:37'),(48,21,'1bf1f00bc9669d156a6b58e0b6ec6209.jpg','Halan2 kemarin',30,'2020-07-13 15:33:32'),(49,19,'c7832eb77141dddf09db389178b10863.jpg','serahkan pada ahlinya😄',30,'2020-07-16 20:20:12');
/*!40000 ALTER TABLE `community_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `community_gallery_like`
--

DROP TABLE IF EXISTS `community_gallery_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community_gallery_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `likeordis` char(1) NOT NULL COMMENT '1 = like, 2 = unlike',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community_gallery_like`
--

LOCK TABLES `community_gallery_like` WRITE;
/*!40000 ALTER TABLE `community_gallery_like` DISABLE KEYS */;
INSERT INTO `community_gallery_like` VALUES (8,20,15,'1'),(9,25,16,'1'),(10,25,15,'2'),(11,30,16,'1'),(12,33,19,'1'),(13,33,17,'1'),(14,23,18,'1'),(15,30,20,'1'),(16,20,21,'2'),(17,20,20,'1'),(18,20,16,'1'),(19,20,24,'1'),(20,20,22,'1'),(21,25,25,'1'),(22,25,24,'1'),(23,25,22,'1'),(24,25,21,'1'),(25,25,19,'1'),(26,25,18,'1'),(27,25,17,'2'),(28,38,19,'1'),(29,25,20,'2'),(30,20,25,'1'),(32,38,27,'1'),(33,20,37,'1'),(34,37,32,'1'),(35,23,39,'1'),(36,30,46,'1');
/*!40000 ALTER TABLE `community_gallery_like` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cp_app_acl_access_actions`
--

DROP TABLE IF EXISTS `cp_app_acl_access_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_app_acl_access_actions` (
  `aca_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `aca_access_id` int(11) NOT NULL,
  `aca_action_id` int(11) NOT NULL,
  `app_id` int(5) NOT NULL,
  PRIMARY KEY (`aca_id`)
) ENGINE=MyISAM AUTO_INCREMENT=581 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cp_app_acl_access_actions`
--

LOCK TABLES `cp_app_acl_access_actions` WRITE;
/*!40000 ALTER TABLE `cp_app_acl_access_actions` DISABLE KEYS */;
INSERT INTO `cp_app_acl_access_actions` VALUES (1,1,1,1),(2,2,1,1),(3,2,2,1),(4,2,3,1),(5,2,4,1),(6,2,6,1),(7,3,1,1),(8,3,2,1),(9,3,3,1),(10,3,4,1),(11,4,1,1),(249,5,1,1),(250,5,2,1),(251,5,3,1),(252,5,4,1),(253,6,1,1),(254,6,2,1),(255,6,3,1),(256,6,4,1),(257,7,1,1),(258,7,2,1),(259,7,3,1),(260,7,4,1),(261,8,1,1),(262,8,2,1),(263,8,3,1),(264,8,4,1),(265,9,1,1),(266,9,2,1),(267,9,3,1),(268,9,4,1),(269,10,1,1),(270,10,2,1),(271,10,3,1),(272,10,4,1),(273,11,1,1),(274,11,2,1),(275,11,3,1),(276,11,4,1),(277,12,1,1),(278,12,2,1),(279,12,3,1),(280,12,4,1),(281,58,1,1),(282,58,2,1),(283,58,3,1),(284,58,4,1),(285,9,5,1),(286,59,1,1),(287,59,2,1),(288,60,1,1),(289,60,2,1),(290,61,1,1),(291,61,2,1),(292,63,1,1),(293,63,2,1),(294,63,3,1),(295,63,4,1),(307,65,4,1),(306,65,3,1),(305,65,2,1),(304,65,1,1),(300,64,1,1),(301,64,2,1),(302,64,3,1),(303,64,4,1),(308,63,7,1),(309,66,1,1),(310,66,2,1),(311,66,3,1),(312,66,4,1),(313,6,5,1),(314,6,8,1),(315,59,3,1),(316,59,4,1),(317,67,1,1),(318,68,1,1),(319,68,2,1),(320,68,3,1),(321,68,4,1),(573,138,5,1),(572,138,4,1),(571,138,3,1),(570,138,2,1),(569,138,1,1),(568,136,1,1),(567,136,5,1),(566,136,4,1),(565,136,3,1),(564,136,2,1),(563,135,1,1),(562,135,5,1),(561,135,4,1),(560,135,3,1),(559,135,2,1),(558,134,5,1),(557,134,4,1),(556,134,3,1),(555,134,2,1),(554,134,1,1),(553,137,5,1),(552,137,4,1),(551,137,3,1),(550,137,2,1),(549,137,1,1),(548,133,5,1),(547,133,4,1),(546,133,3,1),(545,133,2,1),(544,133,1,1),(543,132,5,1),(542,132,4,1),(541,132,3,1),(540,132,2,1),(539,132,1,1),(538,131,5,1),(537,131,4,1),(536,131,3,1),(535,131,2,1),(534,131,1,1),(533,130,5,1),(532,130,4,1),(531,130,3,1),(530,130,2,1),(529,130,1,1),(528,129,5,1),(527,129,4,1),(526,129,3,1),(525,129,2,1),(524,129,1,1),(523,128,5,1),(522,128,4,1),(521,128,3,1),(520,128,2,1),(519,128,1,1),(518,127,5,1),(517,127,4,1),(516,127,3,1),(515,127,2,1),(514,127,1,1),(513,126,5,1),(512,126,4,1),(511,126,3,1),(510,126,2,1),(509,126,1,1),(508,125,5,1),(507,125,4,1),(506,125,3,1),(505,125,2,1),(504,125,1,1),(503,124,5,1),(574,139,1,1),(501,124,3,1),(500,124,2,1),(499,124,1,1),(575,139,2,1),(576,139,3,1),(577,139,4,1),(578,139,5,1),(580,124,4,1);
/*!40000 ALTER TABLE `cp_app_acl_access_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cp_app_acl_accesses`
--

DROP TABLE IF EXISTS `cp_app_acl_accesses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_app_acl_accesses` (
  `acc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `acc_group` varchar(255) DEFAULT NULL,
  `acc_menu` varchar(255) NOT NULL,
  `acc_group_controller` varchar(255) NOT NULL,
  `acc_controller_name` varchar(255) NOT NULL,
  `acc_access_name` varchar(255) DEFAULT NULL,
  `acc_description` varchar(255) NOT NULL,
  `acc_by_order` int(11) DEFAULT NULL,
  `app_id` int(5) NOT NULL,
  `acc_css_class` varchar(50) NOT NULL,
  `acc_isshow` char(1) NOT NULL DEFAULT '1',
  `acc_active` int(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`acc_id`)
) ENGINE=MyISAM AUTO_INCREMENT=140 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cp_app_acl_accesses`
--

LOCK TABLES `cp_app_acl_accesses` WRITE;
/*!40000 ALTER TABLE `cp_app_acl_accesses` DISABLE KEYS */;
INSERT INTO `cp_app_acl_accesses` VALUES (1,'home','Dashboard > Home','dashboard','','Home','Home',1,1,'fa fa-laptop','1',1),(2,'meme','Admin & Setting > Group User','meme','group','Admin & Setting','Group Access',103,1,'fa fa-cog','1',1),(3,'meme','Admin & Setting > User & Contact','meme','user','Users','User',104,1,'','0',0),(4,'meme','Admin & Setting > Setting','meme','config','Configuration','Configuration',102,1,'fa fa-cog','0',0),(59,'meme','Admin & Setting > News','meme','news','News','News',109,1,'fa fa-laptop','1',0),(63,'meme','Admin & Setting > Company','meme','company','Company','Company',105,1,'','1',0),(64,'home','Community > List Forum','community','forum','Forum','Forum',25,1,'','1',1),(65,'meme','Admin & Setting > Topic','meme','topic','Topic','Topic',107,1,'','0',0),(66,'meme','Admin & Setting > To do list','meme','todolist','Todolist','Todolist',108,1,'','0',0),(67,'home','Dashboard > News','home','news','News','News',2,1,'','0',0),(68,'merch','Merchandise> List Merchandise','merch','merchandise','Merchandise','Merchandise',30,1,'fa fa-shopping-basket','1',1),(138,'home','Master > Banner Slide','master','banner','Banner Image','Banner Image',4,1,'fa fa-th-large','1',1),(137,'home','Master > Type','master','type','Type','Type',6,1,'','1',1),(136,'report','Report > Sales','report','report_sales','Report Sales','Report Sales',72,1,'fa fa-book','0',0),(135,'report','Report > Buyer','report','report_buyer','Report Buyer','Report Buyer',71,1,'fa fa-book','0',0),(134,'report','Report > Vendor','report','report_vendor','Report Vendor','Report Vendor',50,1,'fa fa-book','0',0),(133,'home','Community > List Event','community','event','Event','Event',23,1,'','1',1),(132,'home','Community > List Community ','community','community','Community ','Community ',22,1,'fas fa-columns','1',1),(131,'mem','Member > List Member','mem','member','Member','Member',21,1,'fa fa-address-book','1',1),(130,'home','Master > Usia Kendaraan','master','usia_kendaraan','Usia Kendaraan','Usia Kendaraan',8,1,'','1',1),(128,'home','Master > Merek','master','merek','Merk','Merk',6,1,'','1',1),(129,'home','Community > List Polling','community','polling','polling','polling',24,1,'','1',1),(127,'polling','Polling Induk > Polling ','polling','polling_post','Polling Post','Polling Post',5,1,'fa fa-laptop','1',1),(126,'home','Master > Jenis Model','master','jenis_model','JenisM','JenisM',7,1,'','1',1),(125,'home','Master > Banner Iklan','master','banner_iklan','Banner Iklan','Banner Iklan',5,1,'','0',0),(124,'home','Master > On Boarding','master','on_boarding','On Boarding','On Boarding',10,1,'','1',1),(139,'home','Master > Warna','master','warna','Warna','Warna',9,1,'','1',1);
/*!40000 ALTER TABLE `cp_app_acl_accesses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cp_app_acl_actions`
--

DROP TABLE IF EXISTS `cp_app_acl_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_app_acl_actions` (
  `ac_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ac_action_name` varchar(255) NOT NULL,
  `ac_action` varchar(255) NOT NULL,
  `ac_action_image` varchar(255) DEFAULT NULL,
  `app_id` int(5) DEFAULT NULL,
  `ac_action_type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ac_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cp_app_acl_actions`
--

LOCK TABLES `cp_app_acl_actions` WRITE;
/*!40000 ALTER TABLE `cp_app_acl_actions` DISABLE KEYS */;
INSERT INTO `cp_app_acl_actions` VALUES (1,'Search','index','list.png',1,'normal'),(2,'Add','add','add.png',1,'normal'),(3,'Edit','edit','edit',1,'normal'),(4,'Delete','delete','trash',1,'confirm'),(5,'Detail','detail','file-archive',1,'normal'),(6,'Access','access','file-archive',1,'normal'),(7,'Department','department','detail.png',1,'normal'),(8,'Upload','upload','add.png',1,'normal');
/*!40000 ALTER TABLE `cp_app_acl_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cp_app_acl_group`
--

DROP TABLE IF EXISTS `cp_app_acl_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_app_acl_group` (
  `ag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ag_group_name` varchar(100) NOT NULL DEFAULT '0',
  `ag_group_desc` varchar(255) NOT NULL DEFAULT '',
  `ag_group_status` int(1) NOT NULL DEFAULT 0,
  `app_id` int(5) DEFAULT NULL,
  `ag_group_template` varchar(255) DEFAULT NULL,
  `is_trash` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ag_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cp_app_acl_group`
--

LOCK TABLES `cp_app_acl_group` WRITE;
/*!40000 ALTER TABLE `cp_app_acl_group` DISABLE KEYS */;
INSERT INTO `cp_app_acl_group` VALUES (1,'administrator','Administrator',1,1,'default',0),(9,'Atribute','Atribute',1,NULL,NULL,1),(15,'Admin Document','Admin Document',1,NULL,NULL,0),(16,'View','View Only',1,NULL,NULL,0),(17,'sales','Sales',1,NULL,NULL,1),(18,'sales admin','Sales Admin',1,NULL,NULL,1),(19,'Buyer','Buyer',1,NULL,NULL,1),(20,'Vendor','Vendor',1,NULL,NULL,1);
/*!40000 ALTER TABLE `cp_app_acl_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cp_app_acl_group_accesses`
--

DROP TABLE IF EXISTS `cp_app_acl_group_accesses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_app_acl_group_accesses` (
  `aga_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `aga_access_id` int(11) NOT NULL,
  `aga_group_id` int(11) NOT NULL,
  `app_id` int(5) DEFAULT NULL,
  `aga_action_id` int(11) NOT NULL,
  PRIMARY KEY (`aga_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8568 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cp_app_acl_group_accesses`
--

LOCK TABLES `cp_app_acl_group_accesses` WRITE;
/*!40000 ALTER TABLE `cp_app_acl_group_accesses` DISABLE KEYS */;
INSERT INTO `cp_app_acl_group_accesses` VALUES (3365,67,15,NULL,1),(3364,1,15,NULL,1),(3366,68,15,NULL,1),(3367,68,15,NULL,2),(3368,68,15,NULL,3),(3369,68,15,NULL,4),(3371,6,15,NULL,1),(3372,6,15,NULL,2),(3373,6,15,NULL,3),(3374,6,15,NULL,4),(3375,6,15,NULL,5),(3376,6,15,NULL,8),(3377,8,15,NULL,1),(3378,8,15,NULL,2),(3379,8,15,NULL,3),(3380,8,15,NULL,4),(3500,8,16,NULL,4),(3499,8,16,NULL,3),(3498,8,16,NULL,2),(3497,8,16,NULL,1),(3496,6,16,NULL,8),(3495,6,16,NULL,5),(3494,6,16,NULL,4),(3493,6,16,NULL,3),(3492,6,16,NULL,2),(3491,6,16,NULL,1),(3489,68,16,NULL,4),(3488,68,16,NULL,3),(3487,68,16,NULL,2),(3486,68,16,NULL,1),(3485,67,16,NULL,1),(3484,1,16,NULL,1),(3505,9,16,NULL,1),(3506,9,16,NULL,2),(3507,9,16,NULL,3),(3508,9,16,NULL,4),(3509,9,16,NULL,5),(7602,1,20,NULL,1),(7601,1,19,NULL,1),(8567,127,1,NULL,5),(8566,127,1,NULL,4),(8565,127,1,NULL,3),(8564,127,1,NULL,2),(8563,127,1,NULL,1),(8562,131,1,NULL,5),(8561,131,1,NULL,4),(8560,131,1,NULL,3),(8559,131,1,NULL,2),(8558,131,1,NULL,1),(8557,139,1,NULL,5),(8556,139,1,NULL,4),(6672,68,17,NULL,4),(6671,68,17,NULL,3),(6670,68,17,NULL,2),(6669,3,17,NULL,4),(6668,3,17,NULL,3),(6667,3,17,NULL,2),(6666,65,17,NULL,4),(6665,65,17,NULL,3),(6664,65,17,NULL,2),(6663,59,17,NULL,4),(6662,59,17,NULL,3),(6661,59,17,NULL,2),(6660,2,17,NULL,6),(6659,2,17,NULL,4),(6658,2,17,NULL,3),(6657,2,17,NULL,2),(6656,64,17,NULL,4),(6655,64,17,NULL,3),(6654,64,17,NULL,2),(6653,63,17,NULL,7),(6652,63,17,NULL,4),(6651,63,17,NULL,3),(6650,63,17,NULL,2),(8555,139,1,NULL,3),(8554,139,1,NULL,2),(8553,139,1,NULL,1),(8552,130,1,NULL,5),(8551,130,1,NULL,4),(8550,130,1,NULL,3),(8549,130,1,NULL,2),(8548,130,1,NULL,1),(8547,137,1,NULL,5),(8546,137,1,NULL,4),(8545,137,1,NULL,3),(8544,137,1,NULL,2),(8543,137,1,NULL,1),(8542,128,1,NULL,5),(8541,128,1,NULL,4),(8540,128,1,NULL,3),(8539,128,1,NULL,2),(8538,128,1,NULL,1),(8537,126,1,NULL,5),(8536,126,1,NULL,4),(8535,126,1,NULL,3),(8534,126,1,NULL,2),(8533,126,1,NULL,1),(8532,124,1,NULL,5),(8531,124,1,NULL,4),(8530,124,1,NULL,3),(8529,124,1,NULL,2),(8528,124,1,NULL,1),(8527,138,1,NULL,5),(8526,138,1,NULL,4),(8525,138,1,NULL,3),(8524,138,1,NULL,2),(8523,138,1,NULL,1),(8522,125,1,NULL,5),(8521,125,1,NULL,4),(8520,125,1,NULL,3),(8519,125,1,NULL,2),(8518,125,1,NULL,1),(8517,68,1,NULL,4),(8516,68,1,NULL,3),(8515,68,1,NULL,2),(8514,68,1,NULL,1),(8513,1,1,NULL,1),(8512,129,1,NULL,5),(8511,129,1,NULL,4),(8510,129,1,NULL,3),(8509,129,1,NULL,2),(8508,129,1,NULL,1),(8507,133,1,NULL,5),(8506,133,1,NULL,4),(8505,133,1,NULL,3),(8504,133,1,NULL,2),(8503,133,1,NULL,1),(8502,132,1,NULL,5),(8501,132,1,NULL,4),(8500,132,1,NULL,3),(8499,132,1,NULL,2),(8498,132,1,NULL,1),(8497,3,1,NULL,4),(8496,3,1,NULL,3),(8495,3,1,NULL,2),(8494,3,1,NULL,1),(8493,65,1,NULL,4),(8492,65,1,NULL,3),(8491,65,1,NULL,2),(8490,4,1,NULL,1),(7341,68,18,NULL,4),(7340,68,18,NULL,3),(7339,68,18,NULL,2),(8489,59,1,NULL,4),(8488,59,1,NULL,3),(8487,59,1,NULL,2),(8486,2,1,NULL,6),(7338,68,18,NULL,1),(7337,67,18,NULL,1),(7336,1,18,NULL,1),(7335,3,18,NULL,1),(7334,65,18,NULL,1),(7333,4,18,NULL,1),(7332,59,18,NULL,1),(7331,2,18,NULL,1),(7330,64,18,NULL,1),(7329,63,18,NULL,1),(8485,2,1,NULL,4),(8484,2,1,NULL,3),(8483,2,1,NULL,2),(8482,2,1,NULL,1),(8481,64,1,NULL,4),(8480,64,1,NULL,3),(8479,64,1,NULL,2),(8478,64,1,NULL,1);
/*!40000 ALTER TABLE `cp_app_acl_group_accesses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cp_app_config`
--

DROP TABLE IF EXISTS `cp_app_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_app_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `config_name` varchar(200) DEFAULT NULL,
  `config_value` varchar(200) DEFAULT NULL,
  `config_label` varchar(200) DEFAULT NULL,
  `config_obj` varchar(255) DEFAULT 'text',
  `config_obj_value` text DEFAULT NULL,
  `config_obj_attr` varchar(255) DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cp_app_config`
--

LOCK TABLES `cp_app_config` WRITE;
/*!40000 ALTER TABLE `cp_app_config` DISABLE KEYS */;
INSERT INTO `cp_app_config` VALUES (1,'app_name','Mithan.com','Aplication Name','text',NULL,NULL,1),(2,'app_email','admin@app.com','Email Admin','text',NULL,NULL,2),(3,'app_email_finance','finance@app.com','Email Finance','text',NULL,NULL,3);
/*!40000 ALTER TABLE `cp_app_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cp_app_log`
--

DROP TABLE IF EXISTS `cp_app_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_app_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log_date` datetime DEFAULT NULL,
  `log_class` varchar(100) DEFAULT NULL,
  `log_function` varchar(100) DEFAULT NULL,
  `log_user_name` varchar(100) DEFAULT NULL,
  `log_user_id` int(11) DEFAULT NULL,
  `log_role` varchar(70) DEFAULT NULL,
  `log_ip` varchar(100) DEFAULT NULL,
  `log_user_agent` varchar(255) DEFAULT NULL,
  `log_url` varchar(255) NOT NULL,
  `log_var_get` text DEFAULT NULL,
  `log_var_post` text DEFAULT NULL,
  `app_id` int(5) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cp_app_log`
--

LOCK TABLES `cp_app_log` WRITE;
/*!40000 ALTER TABLE `cp_app_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `cp_app_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cp_app_sessions`
--

DROP TABLE IF EXISTS `cp_app_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_app_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT 0,
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cp_app_sessions`
--

LOCK TABLES `cp_app_sessions` WRITE;
/*!40000 ALTER TABLE `cp_app_sessions` DISABLE KEYS */;
INSERT INTO `cp_app_sessions` VALUES ('8c021dfdcd9b9b74ee44c2d00f2dd270','8.37.232.70','Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) Ap',1548208081,'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),('ded4270270458b1fa2394ab8aede5c80','180.246.221.239','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0)',1548208597,'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),('b0cf88290ab6d28a26b29a83eb67cf66','103.83.173.13','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb',1548215389,'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),('50c5a40d56dffdda8e7ff189a29503fd','207.180.218.247','Mozilla/5.0 (compatible; MJ12bot/v1.4.8; http://mj',1548215580,'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),('b0360190b66e3d404ea5c83669217c61','103.83.173.13','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb',1548216325,'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),('fb18bb48896fba42940546f8b139f35b','207.46.13.13','Mozilla/5.0 (compatible; bingbot/2.0; +http://www.',1548220079,'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),('eeabbf6f82cfccf085c241f19a42e954','110.136.67.162','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb',1548222644,'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),('b5b9b788429d1436b7cb0a4ee0d8e94c','176.74.192.71','Mozilla/5.0 (X11; Linux x86_64; rv:63.0) Gecko/201',1548224419,'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),('4ecc212e2e7fd8fa7f80c38d422478fc','110.136.67.162','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb',1548227334,'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),('8dfd885a63744208d2a7d3d2ad52923b','110.136.67.162','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb',1548227413,'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}');
/*!40000 ALTER TABLE `cp_app_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cp_app_user`
--

DROP TABLE IF EXISTS `cp_app_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_app_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `user_status` int(1) DEFAULT NULL,
  `time_add` datetime DEFAULT NULL,
  `time_update` datetime DEFAULT NULL,
  `user_add` varchar(50) DEFAULT NULL,
  `user_update` varchar(50) DEFAULT NULL,
  `user_logindate` datetime DEFAULT NULL,
  `app_id` int(5) DEFAULT NULL,
  `is_trash` int(11) NOT NULL DEFAULT 0,
  `user_group` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cp_app_user`
--

LOCK TABLES `cp_app_user` WRITE;
/*!40000 ALTER TABLE `cp_app_user` DISABLE KEYS */;
INSERT INTO `cp_app_user` VALUES (1,'admin','$2y$10$3WFMemDQEdNF7OqbT63MJ.PBLaDBUEyqPw39PNUeGj7nnBPDmHw8i',1,NULL,'2015-10-01 07:23:22',NULL,'1','2020-02-06 16:27:49',1,0,1),(3,'buyer1','d41d8cd98f00b204e9800998ecf8427e',1,'2019-05-05 00:57:32','2019-05-07 14:37:32','1','1','0000-00-00 00:00:00',NULL,0,2),(2,'Vendor1','4297f44b13955235245b2497399d7a93',1,'2019-05-05 00:57:32','2019-05-05 00:57:32','1','1',NULL,NULL,0,3),(4,'buyer2','4297f44b13955235245b2497399d7a93',1,'2019-05-05 00:57:32','2019-05-05 00:57:32','1','1',NULL,NULL,0,3),(65,'admin','4297f44b13955235245b2497399d7a93',1,'2019-10-08 11:03:05','2019-10-08 11:03:05','1','1',NULL,NULL,0,2);
/*!40000 ALTER TABLE `cp_app_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cp_bayar`
--

DROP TABLE IF EXISTS `cp_bayar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_bayar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `invoice_code` varchar(255) DEFAULT NULL,
  `bank` varchar(100) DEFAULT NULL,
  `norek` varchar(50) DEFAULT NULL,
  `atas_nama` varchar(255) DEFAULT NULL,
  `img_bukti` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cp_bayar`
--

LOCK TABLES `cp_bayar` WRITE;
/*!40000 ALTER TABLE `cp_bayar` DISABLE KEYS */;
INSERT INTO `cp_bayar` VALUES (1,20,'201592242197','bca','123456','ed','f436626a682f052824c2c6ad27ce9837.jpg','2020-06-15 09:30:23'),(2,20,'201592242936','bca','123456','ed','4ce329e2f269346625a54f891490cf3d.jpg','2020-06-15 09:42:49'),(3,20,'201593134930','bca','123','ed','2ace44dfe8110d0218edbb66b86515dd.jpg','2020-06-25 17:29:26'),(4,25,'251593158918','bca','123','ed','1263a03e2ad60518d5b6116ccbcf36b0.jpg','2020-06-26 00:08:49'),(5,25,'251593159249','BCA','02835346542','btfu','a7167768dd5fc43a329808cebe1bca1a.jpg','2020-06-26 00:14:18'),(6,30,'301593431058','BCA','0950629398','Anda RukandaKomunita','3eae9d5edaa433d94ffb6309989611a1.jpg','2020-06-29 03:44:35');
/*!40000 ALTER TABLE `cp_bayar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cp_cart`
--

DROP TABLE IF EXISTS `cp_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'User Id untuk Buyer',
  `community_id` int(11) NOT NULL,
  `merchant_id` int(1) DEFAULT NULL,
  `qty` int(3) DEFAULT NULL,
  `invoice_code` varchar(255) DEFAULT NULL,
  `status_invoice` int(1) DEFAULT 0 COMMENT '0 = cart, 1 = checkout, 2= accepted, 3 = sedang di proes, 4, sedang dikirm, 5 = selesai, 6 = cancel',
  `tanggal_order` datetime DEFAULT NULL,
  `tanggal_checkout` datetime DEFAULT NULL,
  `tanggal_confirm` timestamp NULL DEFAULT NULL,
  `tanggal_delivery` datetime DEFAULT NULL,
  `tanggal_shipped` datetime DEFAULT NULL,
  `tanggal_cancel` timestamp NULL DEFAULT NULL,
  `no_resi` varchar(255) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `tax` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cp_cart`
--

LOCK TABLES `cp_cart` WRITE;
/*!40000 ALTER TABLE `cp_cart` DISABLE KEYS */;
INSERT INTO `cp_cart` VALUES (3,20,3,11,3,'201592242197',5,'2020-06-14 05:56:45','2020-06-15 17:29:57','2020-06-15 09:30:50','2020-06-15 17:31:27','2020-06-15 17:33:01',NULL,'12345',NULL,NULL),(4,12,1,12,1,NULL,0,'2020-06-14 06:00:14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,20,3,11,1,'201592242936',4,'2020-06-15 17:42:00','2020-06-15 17:42:16','2020-06-16 06:45:07','2020-06-16 14:45:16',NULL,NULL,'1234',NULL,NULL),(6,20,3,11,2,'201592474361',1,'2020-06-18 09:57:35','2020-06-18 09:59:21',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,20,14,14,1,'201593134930',4,'2020-06-26 01:28:17','2020-06-26 01:28:50','2020-06-25 17:30:47','2020-06-26 01:30:56',NULL,NULL,'1234',NULL,NULL),(8,25,14,14,1,'251593158918',1,'2020-06-26 08:08:03','2020-06-26 08:08:38',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,25,17,15,1,'251593159249',3,'2020-06-26 08:14:00','2020-06-26 08:14:09','2020-06-26 00:14:53',NULL,NULL,NULL,NULL,NULL,NULL),(10,25,20,17,1,NULL,0,'2020-06-27 10:57:54',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,30,23,18,1,'301593431058',1,'2020-06-29 06:30:07','2020-06-29 11:44:18',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,38,27,20,1,NULL,0,'2020-07-04 14:41:35',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(24,38,24,19,1,NULL,0,'2020-07-04 14:41:41',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(25,20,27,20,1,'201594103662',1,'2020-07-07 06:33:51','2020-07-07 06:34:22',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(26,20,18,16,1,'201594103662',1,'2020-07-07 06:33:57','2020-07-07 06:34:22',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(27,20,18,16,3,NULL,0,'2020-07-07 06:36:46',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(28,30,18,66,1,NULL,0,'2020-07-19 01:37:14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `cp_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cp_checkout`
--

DROP TABLE IF EXISTS `cp_checkout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_checkout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT 'Buyer Id''s',
  `invoice_code` varchar(255) DEFAULT NULL,
  `total_product_rate` int(11) DEFAULT NULL,
  `total_expedition_rate` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `disc` int(11) DEFAULT NULL,
  `grand_total` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `nama_penerima` varchar(255) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `metode_trx` varchar(20) DEFAULT NULL,
  `courier` varchar(255) DEFAULT NULL,
  `service` varchar(255) DEFAULT NULL,
  `url` mediumtext DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cp_checkout`
--

LOCK TABLES `cp_checkout` WRITE;
/*!40000 ALTER TABLE `cp_checkout` DISABLE KEYS */;
INSERT INTO `cp_checkout` VALUES (1,20,'201592242197',450000,15000,465000,NULL,NULL,1,'Ferdj','08381989588','transfer','sicepat','BEST',NULL,'PAID'),(2,20,'201592242936',150000,9000,159000,NULL,NULL,1,'tszt','22','transfer','jne','CTC',NULL,'PAID'),(3,20,'201592474361',300000,9000,309000,NULL,NULL,1,'Ferdi','083819899528','transfer','jne','CTC',NULL,'PENDING'),(4,20,'201593134930',1000,27000,28000,NULL,NULL,1,'test','083819899528','transfer','jnt','EZ',NULL,'PAID'),(5,25,'251593158918',1000,22000,23000,NULL,NULL,2,'riski','87870522640','transfer','jne','OKE',NULL,'PAID'),(6,25,'251593159249',10000,9000,19000,NULL,NULL,2,'ttt','606868','transfer','jne','CTC',NULL,'PAID'),(7,30,'301593431058',100000,9000,109000,NULL,NULL,3,'Abrianto','0811112227','transfer','jne','CTC',NULL,'PAID'),(8,20,'201594103662',110000,9000,119000,NULL,NULL,1,'ferdi','0815','transfer','jne','CTC',NULL,'PENDING');
/*!40000 ALTER TABLE `cp_checkout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cp_checkout_detail`
--

DROP TABLE IF EXISTS `cp_checkout_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_checkout_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header_id` int(11) DEFAULT NULL,
  `cart_id` int(11) DEFAULT NULL,
  `merchant_id` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `courier` varchar(255) DEFAULT NULL,
  `courier_service` varchar(255) DEFAULT NULL,
  `courier_cost` int(11) DEFAULT NULL,
  `fee` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cp_checkout_detail`
--

LOCK TABLES `cp_checkout_detail` WRITE;
/*!40000 ALTER TABLE `cp_checkout_detail` DISABLE KEYS */;
INSERT INTO `cp_checkout_detail` VALUES (1,1,3,11,450000,'sicepat','BEST',15000,NULL),(2,2,5,11,150000,'jne','CTC',9000,NULL),(3,3,6,11,300000,'jne','CTC',9000,NULL),(4,4,7,14,1000,'jnt','EZ',27000,NULL),(5,5,8,14,1000,'jne','OKE',22000,NULL),(6,6,9,15,10000,'jne','CTC',9000,NULL),(7,7,12,18,100000,'jne','CTC',9000,NULL),(8,8,25,20,100000,'jne','CTC',9000,NULL),(9,8,26,16,10000,'jne','CTC',9000,NULL);
/*!40000 ALTER TABLE `cp_checkout_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cp_pengiriman`
--

DROP TABLE IF EXISTS `cp_pengiriman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_pengiriman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `province_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cp_pengiriman`
--

LOCK TABLES `cp_pengiriman` WRITE;
/*!40000 ALTER TABLE `cp_pengiriman` DISABLE KEYS */;
INSERT INTO `cp_pengiriman` VALUES (1,20,9,78,'Jl. Ps. Cikereteg, Ciderum, Kec. Caringin, Bogor, Jawa Barat, Indonesia','-6.6961181','106.8510753'),(2,25,6,155,'Teluk Pinang, Ciawi, Bogor, West Java, Indonesia','-6.681725500000001','106.851763'),(3,30,9,79,'Jalan Janaka 2, Nomer 13, Bogor',NULL,NULL),(4,38,9,78,'Jl. Ps. Cikereteg, Ciderum, Kec. Caringin, Bogor, Jawa Barat, Indonesia','-6.6961181','106.8510753');
/*!40000 ALTER TABLE `cp_pengiriman` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cp_report`
--

DROP TABLE IF EXISTS `cp_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cp_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `id_type` int(11) NOT NULL,
  `alasan` varchar(255) DEFAULT NULL,
  `id_member` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `modified_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cp_report`
--

LOCK TABLES `cp_report` WRITE;
/*!40000 ALTER TABLE `cp_report` DISABLE KEYS */;
INSERT INTO `cp_report` VALUES (1,'community',1,'plagiat nich',20,'2020-06-07 18:47:29',NULL),(2,'community',1,'test',20,'2020-06-08 01:05:47',NULL),(3,'community',3,'tesst',20,'2020-06-08 01:06:12',NULL),(4,'community',2,'test',20,'2020-06-08 01:18:12',NULL),(5,'merchant',20,'test',20,'2020-07-04 06:14:31',NULL);
/*!40000 ALTER TABLE `cp_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `firebase` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sms_verfy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activate` enum('active','banned') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (3,'c5u_OTP4RlCANaHoPo9Bms:APA91bGJVdfDbOVxXER2QxniIrswK1inJMf2ochR4749FoG6XFvysUZA0-cOF5Gr1B_V-9eNweRnBF3_2q_KKakYruLDiz78wPoaMFm7Lke0J5F6RbPKOlZ0l9TsFbzqgEEARX4xNsMp','xnwGI3K5dsgXWxWkaXz5DqHV6DjSHqdtKiiicMSkvZ9z9Dr3vRNnLPpxwbv8weQe34OHvIRP751VPYgI','$2y$10$v/p14BeQdbg7R3ZNog5CW.GLW3RoEobuRHgJyn4UM0gnm5ZubUroC','Farhan banyu',NULL,'085884985396','farhanbanyufebryan@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-05-03 22:28:48','2020-05-03 22:28:48'),(5,'c7-cz1RQQZO7UPBPjS1lzI:APA91bFy08CD0fVwopSfkh64IQoAXhp0o_0s0f4gmecsdq0q1vwaqJmbtDMcvffboYPmlB5GF5A2w9BolVNHEoDo44tyraoZZHxNxwBqafLqEH5_3zRucAOduPnZbI2JwBDBLrl3IjE8','iXGQrClx2dveQSP3sSUuL9zqFGJXshopCA8J6ARpqBZ9NNjhKMUup8JBrlugYSwf4ldsRMg6tRJ41XoN','7706','AFKdude',NULL,'0895333030274','muhamadaderohayat122@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-05-03 22:33:46','2020-05-03 22:33:46'),(9,'coP8TLs5R_qLTZ2Zzj0g1B:APA91bENVshrntL6217GKyjm7h9Be3JEsM1eehYUm5y-ZN9z2ipz3jZco_U7rrNDOW5BvgnqdbBuaf7Jlp8WwkOpDb4OoQdkrzgOotCSSCuwHkEsdZrzDJ2vaVyiOCQE6F4WZj0KjLLY','fK6NeBrBvPP1YXHv6NIumoxiyyVpsKbHC5ix0Lh0UMjp8Y5yH1eJ18QM3DMB2qDGTjastJ6wxjedrcm9','$2y$10$4e38gdHV1JzJOHoODysVdOwS/UxLXiA3RLqEZReDUn3EGjADPhBPW','iwan',NULL,'085776125559','ionebogor@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-05-08 19:16:27','2020-05-08 19:16:27'),(12,'fh55TUJKeyE:APA91bGMPP0tGGE3QnjcFKA9JLUIfIxInI6yTGdv9I8Zw1ELJxXnE7XseFafXCHugTh3_b8wd9Jj3UkXMw6Ur68_bQFqgz56uIztjHyqkEm3LMpH9n2NSjm95VMGBmb1C-lpQeHmTvfD','Tfepcf9wSxAf8v4fjPXrLEo3av1G7riRBD1pmGeLaXN4QnaOiRbIBi5Gwvj9QbkUXRAs83xGWZl3ZMVg','$2y$10$1jLxkg4/D9yu9JsRMrQhUOFphaeQYngWmg1kx2dq9/O8d.bNaQEHy','riski',NULL,'87870522640','riski.gustriana.id@gmail.com',NULL,NULL,NULL,NULL,'dc5d9568e8bc29c17f7c9758899a84b1.jpg','active','2020-05-08 23:40:32','2020-05-13 03:00:05'),(13,'fwVYi3nVi_k:APA91bFmEZvYNeRukc3iWBrWgm0jo6ClsCIj1Cs0vI_g5Xg8yEHYdhnn0mZlftZCmfjNxizk6LOUkmccT3qJYRnJVBEuObeEBZPsmDW8R_6g2DCV_P6Y5Bj9Hog96TyNCvIATlTJO7Zr','inSiBH1lxnzJWqjQ3sDYRkKfoVdnXVW822aOE7k3Lb13VD8XdUB7HME6rV60f9I5X6TTkdzBuSzVQOba','$2y$10$ucsiO2C0dB7y0H6GPnIpnuv0ETtcbJJqtcXg8i1grhhgCNtm7hJm.','vania',NULL,'08111292272','agathadiandravania@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-05-09 21:01:40','2020-05-09 21:01:40'),(14,'dpHwVgu8r0s:APA91bFGg-_QILpNvyD4GUJNHvydO3XeFRZ-XQVUvnJQG_kr8JohOp6Lm6GU6tjUCyE_mkurQ11OLFA8AM8OFReFAF5vJO2oBZwE2iTnj-Gv43tnBBIvL9egOndbosbgiNHSnypL3BXp','iSOSZ2bUHW4mr7wSQtRjb3y1VpAwsO4QlQQM7RcvlohxRPgph2BoTdbhvxtqkh3yZAHFgON9webO08yd','$2y$10$7KRsrbZou8zfxBuOJmfSJuOPVTHr44NY0I/I15xQLpWkx3bm0zCb2','Marius Pratiknjo',NULL,'0816755557','admin@ppmki.org',NULL,NULL,NULL,NULL,'e236151c21f01802db212c4c37fc7e48.jpg','active','2020-05-10 21:35:38','2020-05-13 17:13:33'),(15,'e-pwTLeqr88:APA91bF0ovDDBtjK2eIlVrCsmrk395ChUtebXBPCXgbkoF-P_eS-CzBB1SFttEB5pOEOPR7XTu1fyuasSTK9ZKsiyUm878hkhaMAm6fxMBo0gR_inQ8ngLW0p2mT0CaNLXH_6gx8n9J0','wIExPdR3z66iJF8uinHZ2kCao28GoujOr0FmA5etYGFhv0NjiCvuNEB2Ls03UaUKMq1mZc3AF87BGp9i','$2y$10$MWM4sUhvDsigNCArqXXH6.rENKy6TgX1sypSHosTKozzMdf7ZU0uW','Agha',NULL,'082191533799','aghams30@gmail.com',NULL,NULL,NULL,NULL,'447c268159db3842c732a872192a423f.jpg','active','2020-05-10 22:29:44','2020-05-18 00:35:03'),(16,'eSMz3wZeXy8:APA91bHpWpDrRuwg7ea9-GusvJgdekKbApi2c9Zm0OF453Awy9lTzv-O_nd-_Gkx7d2XM723E0cn06gCVe7EPLPTjUL8RbFvSRtCvd8lo6IKkukKv1ZGgoIGNZozPghvGHf5owAgInf4','ggaj5Ua3DsKsam428p61rbiWK0BspK2AblXxlTDMenlR0iJlFpM9Yy16Cbk4grCPZtecj6qB8gs1kTRG','$2y$10$fCHojs4AtwQ5TM8KyWXnwOyHRkPf6ddhgu0PgZVTLkC9CyTJb0EY6','Ilham Mudzakirs',NULL,'087870338115','ilhamudzakisr@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-05-10 23:56:02','2020-05-10 23:56:02'),(17,'eSMz3wZeXy8:APA91bHpWpDrRuwg7ea9-GusvJgdekKbApi2c9Zm0OF453Awy9lTzv-O_nd-_Gkx7d2XM723E0cn06gCVe7EPLPTjUL8RbFvSRtCvd8lo6IKkukKv1ZGgoIGNZozPghvGHf5owAgInf4','YLQhCZe906WEfr63MkxnPc6FQJB7h8s9ykwZlUjC1N9Z6Nd4HqpBKsjiXv6FIdIl76pTwyytIUd4IjLL','$2y$10$fCHojs4AtwQ5TM8KyWXnwOyHRkPf6ddhgu0PgZVTLkC9CyTJb0EY6','Ilham Mudzakirs',NULL,'087870338115','ilshamudzakisr@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-05-10 23:59:47','2020-05-10 23:59:47'),(20,'edvm5YcRRE27jxnq4KzYRu:APA91bGgNQcqv2yRuIFPRJ1bSY4BaQqTuCMaOvaplt1di_FAgKxA64MoPvtpiWj4rMkX2kPwcQ6cofhKqwJFFlFqPVeGNeTDgVM7MZZpeAjSHMSTDUiNkP_eIGqZRyw_QlIpqfn0NQ7P','6IkHtU2EXbOMrfeoig9jKb3BdTzkRL07QiKsyuQks2pRfCb8QkhPBWYl6DfwK9AqvFUBXU97s49ZhdMf','$2y$10$xK4tGfZRGz4LCKbGsI3alOwEh5ArQNqEyC.TIkmPltxW.7f/k/QXK','Ferdii',NULL,'083819899528','ferdiansyah071@gmail.com',NULL,NULL,NULL,NULL,'1b5cf557c21d9e92eca9d08865c1fe21.jpg','active','2020-05-12 02:03:45','2020-06-08 01:26:50'),(23,'ce26yykySg-NE_sizrpMCs:APA91bFmg-L1KJgRIYIKS-FROxv0YlmvPLT3u4hGO-l8w57v_0Ki00D7sZUYvdNqGghpgMEK8KRzMEToRleMONrdp3FS7Wf00zbcamiL0q_a_QDRsfDsOGviGo_REDxMMsHtAJeFTMs4','yQq0HUNiU3sXP689nR523f6g19n65S0fK858sqzNNnYIiolBNxmWdTrTba9sSyOuf8f5OYekQm7SIE30','$2y$10$6s1DV42thdH.HqhUnxvUkOvVLMZpBuqePWh7fRrktcoLujSIHaFxm','Wahyu Wibisono',NULL,'08118822272','abriantonistudio@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-05-12 04:05:40','2020-06-26 16:48:48'),(24,'cgS7ceVz5JE:APA91bGvv2th7uW9TYRj30GjLfgMDE9Mj9bm-FPY-UKhAPcyGxpz95m-vcy3ERZS7PbWmwcPdvBl_x9auiYIwnYaVf1twO8H2b1z6Z7DI1Y8jY6cCPH3mHY69O55txSe-tpg9c8k84Lz','pWc6ccywkAR7VGkHSzXam5kZpAOeakT7WbIRumVHVkE8vwYcF3Dh1DPTH42lEJhH9SjxVT1OF9pUfVOb','$2y$10$URn6I/ZfA4LmMxlOqZEUm.H/rdoH31wubQew3KHlJHt9b2b4v06rK','Rafa Agung',NULL,'081298781001','agungtalentis@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-05-12 08:01:27','2020-05-12 08:01:27'),(25,'fh55TUJKeyE:APA91bGMPP0tGGE3QnjcFKA9JLUIfIxInI6yTGdv9I8Zw1ELJxXnE7XseFafXCHugTh3_b8wd9Jj3UkXMw6Ur68_bQFqgz56uIztjHyqkEm3LMpH9n2NSjm95VMGBmb1C-lpQeHmTvfD','ipqarVUHqnvvkCz2DCUyMW8k4EVVZqbjVXtKAL4v9Syo0C3dbMYZrnpttmoPgFV2NDmwpTRWlCdYw2sJ','$2y$10$qvKekJhUS9jiutokr2HYgODlYpMx8Z5b15/DcXhzzBvPQnv.Et7Qi','riski',NULL,'087870522640','riski@gmail.com',NULL,NULL,NULL,NULL,'bbd7968926e38d39cb9fbc8a6cb324a4.jpg','active','2020-05-13 02:45:19','2020-05-13 02:47:50'),(26,'cq1W3_Sdmz8:APA91bGMO5FMNvqQ3REEdrWpDrhWukg4wp6rCy4cIC5WMc-Fe144WGl3hGCJ9Hf6aYuusDIPWABMavT_4mOUxQcI5pm1w0GjalzAkrjDhjLeaHhtou4RmBC31SfDc9U0gVGn1vbWRtQm','EuQURdQM76oPV57hvrDpFnTicPvfCiqK1xzJurLkjfkTa0zdQpPIseNkvg5jRNLJXPMwNYcrU7WT4tU4','$2y$10$YQLwA68X/2jniScPjGzGB.3szxgTItKebWnZr8W32hc7LdtLEOeQ6','Ega',NULL,'081319003492','ht.prilega@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-05-13 22:46:15','2020-05-13 22:49:22'),(28,'erng1YnLTXyuS1IKnnRiNS:APA91bGUGhDsYKVHa8fjLwUQG3MmK1Uq_cIyErl3CzzRK_EbeODZKAjJDYvRH1A2rsQ3xHw6TRR_69RJV7Y1s0dNPKHrWTKxKB9MeoO0_Wpsfj3XsaFfpVb8WfvUJ6yptEZIUVp1yZcN','k8JSsvvXmnMgJwdxX5GVnalQ9K3glZY3xp3O22YrMHlsDqMCtb5TIq0Mi6AZb2xD6HD2VS1BJ3Xmfwj8','$2y$10$u883a6sCG0k6jbTxDcjvK.Qx0zW.rEMN3FHyq9nXDChThwAc9iNpG','Rini',NULL,'081321122227','ekasetyarini14@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-05-14 03:48:19','2020-05-14 03:48:19'),(30,'d7mk1pnSSgaVwNH9dAsz_0:APA91bGvVZ0hmay8uNp4gI2XDLY3QA0Mb9WsUWkS8bH4Xb3w_FA9zRcOYwUFX0VRigFQngc60cY_g-OtGM9LH0pmb2IKxKYOYn5zy7ygsUvycYt8Ds3pvoy02mEEUtB7iD3b_zRw_Y2i','g9MjxIj6xhCZDMq9kA3XwyY6CFykCdlUpg6W1HCNBwH2DLMQR8Hf8VHz27SXDlcMp5sa12RbCg3TLWgz','$2y$10$A2KPykguvffEvWvploG1xeWqFIderBXC32rXXIB6YUAe43SpUtzAq','Abrianto',NULL,'08121112227','abrianto.ww@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-06-23 21:14:16','2020-06-23 21:14:16'),(31,'eHzyPqFvTU-fAmPvkyihkg:APA91bHhY5mf2voBH_z3nKremqOmiUT4eCoVw8dBUPxUbh-tFNk_-OrPT0XzAvqkGREnHPmWEY1fXRqyb8S5lPTckFj_jCaYE9FHXAMFPMQGiHI1Xp1SrWhBLNQmaBBRO9LJ5znmIgs9','V8ZeXB0scFMMAQDmjm7P4dmbP7nsZtRmOlAy0jTszoVIz6NVnl7Y41VpaONwm38M31NQl92TRs8O5lZh','$2y$10$eXHss6cisQPwKW/tCwH1A.ciWKQCYXn4jq3bSOeKY2U5h9j6Zm8e2','febri',NULL,'085714444252','febri.ansyah.drums@gmail.com',NULL,NULL,NULL,NULL,'5a53786bb698bba08fb645ec8bc478f7.jpg','active','2020-06-25 19:37:16','2020-06-25 19:53:36'),(32,'eW44xUa7SU6rESHShpWnUJ:APA91bHMO7f3GcLkzAHWtUBByudJqB3LJQDXdfRrLLl26C1Q5G_HQIv6RynsWEWK2WY25pOk2bRYkbxyK_pn578HDBo3g24FC60NVFTkv1OLno1yXt8qqWd5RsoplqRTFr_v-yZ0s-8e','osY55cXXEJdOkKA7uXIaZGhHKYoHCCNV8fFE4ZONIPVOyeBjBxoj8GMczjNS8jo10JbJjRp7PjIRclSz','$2y$10$pGpPh3l47qn51Xq9fcUef.eh0ZfoUlOicup5HWB1rM38ag9F2B3fi','yustyar',NULL,'085891658512','yustyarnuradha@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-06-28 20:55:03','2020-06-28 20:55:03'),(33,'fTK78bL3QdWYVHffZFzB2e:APA91bEiljRVsZ6aUIdutOWJ0lyENbPmDedkwkqOKjH0X_s9sa5T-Zy_zzE_pnrfr9xe-Au5mVpnul-DTAi3oPXtJZvvbnp2W08k2GiC1ZzpB308a6BitWUIUFQ5LZ2ZdTEoahSvhRUI','bdqVqEVQtFOQQLj2C5cYWMLf3So7OTnCxQtdFkxLTr58MmdB8udiPXNqYZDfEgcoZ7XWZcPr4gvqXvS3','$2y$10$ewjVP61Ud7dDAyxTBtHnu.TYGsYB/hcH8E5aDaDIo542sJVagfWnO','Anda Rukanda',NULL,'081388809990','warungflorizt@gmail.com',NULL,NULL,NULL,NULL,'e523ed7625fa1ea5c4f44baa68704c1c.jpg','active','2020-06-28 21:47:33','2020-06-28 21:55:57'),(34,'dyBun91SThKWZM5yETOsvT:APA91bHV6QS23v2mQNYSmdhFz40EQ-_c41kprcs2Gkg4FlYHcKLOBYLO1QwgUBoapl3I8aw5wsKocsF4f3lDy_cndD5Ji6iaqvltlElhB8ZCD9jlPjgYN2qc48jZvEu2HRpr4FgacYaA','WIJn4e0NTCxO4WkCH8QKHPGTCgWAB3pb77HIHuRmmO8t3rswGReoCKEV6tf83JQulFTKeaZiNSRTtpkQ','$2y$10$hCgXKbu/erXOjdkQdvAz2uqKJ4MuLNsAExBkOmJZW5ZBaU6cvrbOu','ione',NULL,'08991123771','ioenebogor@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-06-29 09:36:39','2020-06-29 09:36:39'),(35,'cfogqqYJQf2exvw7rQE9gt:APA91bFX9sMc4jJY1IIgNrUw3_wrULjPxX8ewAvds85hmD0EyCl9K1z3Q8iSJZ4plCBTStq889Bw0IBSCohk8GvHgbblR4PJqrZ3x6tHqIX8770f-vcnADMrsqN8rAoWr2oYwG7W9yot','iNPKKdlxhCvRBjqa5efJXqlKPzXcz4wF60RPg9PIT0nZC8VcRMGfMTBWJtfxxPyKtfolNII9072BRhgf','$2y$10$XPEce2UnX7/x9yJ2oxQm0.ExN7CPl6l74Plq72HDW/.aH2UvB1.ui','Dwi pujiarto',NULL,'082351728799','dwipujiarto95@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-07-01 03:36:54','2020-07-01 03:42:20'),(36,'etY3HZDrSJaZWWXFAn2BMt:APA91bFq_rqmPIiCUM7MFfocoPrvk344N14Bi795WJK0EhApoVxDm6Bdw-d9Y380Yu9435VjAct4LVlJTibv2-S4_kYGaQO5mKcHD7x1fOpTa5qlXJqMKFcMf1CoQpyzKZ3QHXwUkkPJ','zuBn3NHMT5C4TuZX5LzgJvP4eGY3znL5ppeu2wF1KTTwBxXWLcWoneZNyBhs0mLmsLMTe140vte6qPuJ','$2y$10$Bg/56ur1Xv/Xipsd.mhudO3FBsU7u4An79D16Mrn1wCP8mFUir2YC','Hilman',NULL,'08568258169','hilmaniman429@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-07-01 19:18:48','2020-07-01 19:18:48'),(37,'fc4rEKX-SqybI-0Py6L7SX:APA91bHERZQgmFhUOboEncxUDjpcRpfnu9APfPVOu1Ow3e3uowPG7pcvGDfxKnPESqFpjvg8_QJH0II83W9FssTtc1V-yunKAzAWenjMTsNss5uQ0dFv54jtQCjnqeon8Fqk1NqoLsDl','8OnAWw4hFUOHYqjZW8JU6DKVykD694S1CTdorVZrEbSE8Ol7uLwmXtLJ8I0iQe3q226FczVxwmGGCmai','$2y$10$AIoopX7fi/25jDYRfzCTW.1gMUNr7PvfcctQNWEJzuJPsbtQ094Km','Ifan Ramadhana',NULL,'087876941369','ifanramadhana1996@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-07-02 02:02:52','2020-07-02 02:02:52'),(39,'fAz2a4A6SyaBPDT76T7kaV:APA91bGnV43NXatPQkQnd4roPC-Zwy_zFNNGywl0BtbbuF-3hDFAZDqIX0G8bODf0jQRD37aWt6wd3-KtRZQzm0r04C0I44f0qkxfKGFMEqzkSJSim37vFPfp2vx52JBRghilsbjujf2','MSQjFy7KSUOClU8nc1gbIuQd1rRkzY3EQb4PdrDZyQ6gDWUC6jhhjgVxE5VLs69SDJfsmWr0YxEUY6dJ','$2y$10$3q58KSdrbGKr.e/rwc6vXuffJa/Tx/OsUEi3pv.tQX5K4dbWRHmey','Tedi sukmawandi',NULL,'08562050020','sukmawanditedi@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-07-09 00:39:53','2020-07-09 00:39:53'),(40,'cYvz2AM7QlaDERSXb-Wwdl:APA91bEs8qOhIpNx9y9dLoW9C4RcI9us6V3Xb35ne_FVDI-6zu2z-aOIwhHQk83fUlc0obu_Xy17qbDraYdwRbe9oQX26JZuLejZGjTXnes5UAaJTPJUeBu8gxR1kR9PGYMAguqI6zwO','fsxYoCSRvmRbbBOib2CkXrJXPKmb9bUOjoPFwEXlvi6xSW9J8GLpiDlZsXrqShPfKgxLwS3E3UPcrQHx','$2y$10$NjRI3AIUbt74dOFpT.78Cev6LruPtB/RJ1Laj8z0NtiuIwUA/iYtq','satrio kusumo',NULL,'081287719828','satriokusumo204@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-07-11 21:21:28','2020-07-11 21:21:28'),(41,'cx_6FObqS-arBSAeAUd0P-:APA91bH-g3jCoEOrwroYQW76SeXoqQv6pgUgJethBR6GIwQpv0h8vDxDIvPVTZ6_hRuohCMp5BHscPI29fa_S4400PlWb5tod90izccx-pMoa55zBa16zwoG-rsX3uw_f-uHYGtSjeua','vPvD9lfCvu2RnrnbLrNkeXGb5ELFO2XAgjNWoj2pqvZLx3wzwSZ1wdrulbmn0lYQxMDMc4urooAdEW5g','$2y$10$asN2/e5ss2g3PRx.9hUdm.ywXjQ8Ji4wpP/BwZzW3efmU.6qR5ira','Zaries Maulana SH',NULL,'081212392014','harleywla45@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-07-11 22:20:44','2020-07-11 22:20:44'),(42,'eR1XYZ1yTSqa8d6BLYVRct:APA91bFDdJ7DRhccsZF_VLEt9fbwKYhAeqnbpiO8-yFqeUA17jlRhmIwFqSVH_AN8ifcm4XjDn3sMNYLoWHxuAb20Uodn2v5kXsWnwwEUgyYHjqYft2bH1OstS2OfwGiPhB_fhxoIozV','osHAO1BqgE8d3ADMwaQKr6EzcmrfhYKFBe8XfBfOJVJCcYYuuKHzDKSSV57CJ8lPahZ3QS1RFAAMbkYH','$2y$10$2//HiWEG46kBH1Xc5Yb8o.ac6G54AqPQfzeBhgvoo.TnsnOKG7Gh2','mochammad donna kreoando',NULL,'082336116629','kreoando07@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-07-13 07:00:25','2020-07-13 07:00:25'),(43,'fdoVwY1VRbuQvKI8BPNugr:APA91bEvHoudd0CpgMiFSSJETIZbCioin50hVwZx6fiLj3I8DWbWW083S29FCPgpim9aOqMG1gtCa4hbuN1o3wNjaPKVWIVqLVzq87SOSOD-JuB926-nqaZkCoFYAcigaUc6ETlzGT1e','EEfNVdPiPlLpugM5gkWokRbMMLuAZsv3xdL6GQoiEIkvl4OBk9qTYjcOGeJD0xZLulJ0FloFf0CiFtg0','$2y$10$8Ps8SrqluNbqFisuixme7OsRPPlkyaWQW6CspZRppdRTxFop/mZKS','Muhtad Wassil',NULL,'08111504405','muhtad.wassil@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-07-13 16:01:08','2020-07-13 16:01:08'),(45,'eKG6qA_aR5mXOVsPt-ja6E:APA91bFOG_BwNWfsdjdiUOSWNw7GrNA65fgLabYFqinlmUs-pKQHNuosv7RiPldZ92KsGlEXjqaoSX4WHkMLZqttRWYh6HaV3kljXtNc0kmrDntUhlGqWWoSZSL_CdoGYk0lMmgRY0Yd','VmLW05YOLkpDNJM6MLo0DmBqyaLeNes6BBeZI1WAZXWawcjLBuVgFAPZpd1jMUALPNk9XuAT0H59gfNw','$2y$10$Ses1hLJTJE2ykjxdeNmHceZbMKPmJh7RD0yHcyeKc0GoGWS8Ys3Si','ferdi',NULL,'081283171288','frdansyah28@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-07-16 17:52:54','2020-07-16 17:52:54'),(46,'dsdfsd','2bC6EjS9KKf6BNZgkWtdYK7qQgDqfxES9E0l8xcbvswDgJvQraIhHMs0UtQv8I9aSDCvgU1sUDFWZcfu','$2y$10$wFVJyImWXXd1P/rQJYMhUeiXCT1CRp8PjZ9COntTkeX/pIacz376e','Ilham Mudzakirsxa1',NULL,'087870338115','ilhamudzakisrx@gmail.com',NULL,NULL,NULL,NULL,NULL,'active','2020-07-16 18:37:58','2020-07-16 18:37:58');
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (80,'2014_10_12_000000_create_users_table',1),(81,'2014_10_12_100000_create_password_resets_table',1),(82,'2019_08_19_000000_create_failed_jobs_table',1),(83,'2020_02_14_064819_create_communities_table',1),(84,'2020_02_14_073923_create_communities_detail_table',1),(85,'2020_02_14_082003_create_com_type_table',1),(86,'2020_02_14_082152_create_com_jenismodel_table',1),(87,'2020_02_14_082247_create_com_usiakendaraan_table',1),(88,'2020_02_14_082326_create_com_warna_table',1),(89,'2020_02_14_093857_create_com_merk_table',1),(90,'2020_02_14_095530_create_communitues_anggota_table',1),(91,'2020_02_14_100100_create_member_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `id_notification` int(15) NOT NULL AUTO_INCREMENT,
  `id_member` int(15) NOT NULL,
  `type` int(15) NOT NULL DEFAULT 1,
  `msg` text NOT NULL,
  `activity` text NOT NULL,
  `param` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`param`)),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('false','true') DEFAULT NULL,
  PRIMARY KEY (`id_notification`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
INSERT INTO `notification` VALUES (1,20,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":20}','2020-06-25 23:38:38','true'),(2,3,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":3}','2020-06-25 23:38:38','false'),(3,31,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":31}','2020-06-25 23:38:39','false'),(4,18,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":18}','2020-06-25 23:39:03','false'),(5,25,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"25\"}','2020-06-25 23:49:16','false'),(6,2,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"2\"}','2020-06-25 23:49:16','false'),(7,2,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"2\"}','2020-06-25 23:49:16','false'),(8,18,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":18}','2020-06-25 23:56:44','false'),(9,25,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"25\"}','2020-06-26 00:02:35','false'),(10,5,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"5\"}','2020-06-26 00:02:36','false'),(11,14,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"14\"}','2020-06-26 00:02:36','false'),(12,25,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"25\"}','2020-06-26 00:06:31','false'),(13,16,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"16\"}','2020-06-26 00:06:32','false'),(14,20,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"20\"}','2020-06-26 00:06:32','true'),(15,2,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"2\"}','2020-06-26 00:07:26','false'),(16,20,2,'Selamat, anda telah mendapatkan order','Marchendaise.HistoryMerchendaise','{\"token\":2,\"position\":\"CHECKOUT\",\"id_komunitas\":14,\"id_merchant\":14}','2020-06-26 00:08:49','true'),(17,25,2,'Selamat, anda telah mendapatkan order','Marchendaise.HistoryMerchendaise','{\"token\":2,\"position\":\"CHECKOUT\",\"id_komunitas\":17,\"id_merchant\":15}','2020-06-26 00:14:18','false'),(18,25,1,'Selamat, Order anda telah CONFIRM','Marchendaise.HistoryMerchendaise','{\"token\":1,\"position\":\"CONFIRM\",\"id_member\":\"25\",\"id_merchant\":15}','2020-06-26 00:14:53','false'),(19,30,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"30\"}','2020-06-26 16:22:27','true'),(20,23,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"23\"}','2020-06-26 16:22:27','false'),(21,23,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"23\"}','2020-06-26 16:22:27','false'),(22,23,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"23\"}','2020-06-26 16:57:14','false'),(23,30,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"30\"}','2020-06-26 16:57:14','true'),(24,30,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"30\"}','2020-06-26 16:57:14','true'),(25,23,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":23}','2020-06-26 19:41:29','false'),(26,30,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":30}','2020-06-26 19:41:29','true'),(27,30,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":30}','2020-06-26 19:42:18','true'),(28,23,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":23}','2020-06-26 19:42:18','false'),(29,23,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"23\"}','2020-06-27 00:58:49','false'),(30,28,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"28\"}','2020-06-27 00:58:49','false'),(31,30,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"30\"}','2020-06-27 00:58:49','true'),(32,23,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"23\"}','2020-06-27 01:48:53','false'),(33,28,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"28\"}','2020-06-27 01:48:53','false'),(34,30,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"30\"}','2020-06-27 01:48:53','false'),(35,30,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"30\"}','2020-06-27 01:51:44','false'),(36,20,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"20\"}','2020-06-27 03:49:41','true'),(37,3,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"3\"}','2020-06-27 03:49:41','false'),(38,9,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"9\"}','2020-06-27 03:49:42','true'),(39,30,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":30}','2020-06-27 03:55:33','false'),(40,23,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":23}','2020-06-27 03:55:33','false'),(41,14,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":14}','2020-06-27 04:01:36','true'),(42,30,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":30}','2020-06-27 04:01:37','false'),(43,23,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":23}','2020-06-27 04:28:45','false'),(44,30,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":30}','2020-06-27 04:28:45','false'),(45,23,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":23}','2020-06-28 21:53:23','false'),(46,30,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":30}','2020-06-28 21:53:23','false'),(47,33,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"33\"}','2020-06-28 22:07:01','true'),(48,30,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"30\"}','2020-06-28 22:07:01','false'),(49,23,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"23\"}','2020-06-28 22:07:01','false'),(50,23,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"23\"}','2020-06-28 22:15:11','false'),(51,33,2,'Selamat, anda telah mendapatkan order','Marchendaise.HistoryMerchendaise','{\"token\":2,\"position\":\"CHECKOUT\",\"id_komunitas\":23,\"id_merchant\":18}','2020-06-29 03:44:35','true'),(52,30,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"30\"}','2020-06-29 06:47:32','false'),(53,30,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"30\"}','2020-06-29 06:58:51','true'),(54,25,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"25\"}','2020-06-29 09:34:54','false'),(55,9,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"9\"}','2020-06-29 09:34:55','true'),(56,14,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"14\"}','2020-06-29 09:34:55','false'),(57,9,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"9\"}','2020-06-29 09:51:23','true'),(58,34,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"34\"}','2020-06-29 09:51:23','true'),(59,32,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"32\"}','2020-06-29 09:51:23','false'),(60,32,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"32\"}','2020-06-29 09:56:58','false'),(61,23,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":23}','2020-06-30 22:54:55','false'),(62,30,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":30}','2020-06-30 22:54:55','false'),(63,30,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"30\"}','2020-07-02 01:12:02','true'),(64,14,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":14}','2020-07-02 04:42:34','false'),(65,30,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":30}','2020-07-02 04:42:34','false'),(66,14,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"14\"}','2020-07-02 18:56:18','false'),(67,25,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"25\"}','2020-07-02 18:58:14','false'),(68,25,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"25\"}','2020-07-02 19:01:11','false'),(69,16,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"16\"}','2020-07-02 19:01:11','false'),(70,2,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"2\"}','2020-07-02 19:01:12','false'),(71,2,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"2\"}','2020-07-02 19:01:38','false'),(72,32,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"32\"}','2020-07-02 19:22:49','false'),(73,20,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"20\"}','2020-07-02 19:22:49','true'),(74,20,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"20\"}','2020-07-02 19:22:49','true'),(75,23,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":23}','2020-07-02 21:34:19','false'),(76,30,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":30}','2020-07-02 21:34:19','false'),(77,14,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":14}','2020-07-02 21:36:30','false'),(78,30,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":30}','2020-07-02 21:36:30','false'),(79,23,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":23}','2020-07-02 22:35:34','false'),(80,30,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":30}','2020-07-02 22:35:34','false'),(81,23,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":23}','2020-07-04 01:56:53','false'),(82,30,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":30}','2020-07-04 01:56:53','false'),(83,20,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"20\"}','2020-07-04 05:27:33','true'),(84,38,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"38\"}','2020-07-04 05:27:33','false'),(85,32,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"32\"}','2020-07-04 05:36:59','false'),(86,38,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"38\"}','2020-07-04 05:36:59','false'),(87,20,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"20\"}','2020-07-06 22:49:53','true'),(88,38,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"38\"}','2020-07-06 22:49:53','false'),(89,30,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"30\"}','2020-07-10 22:11:08','false'),(90,23,1,'Success, Create Communty','Akun.HomeAkun','{\"id_member\":\"23\"}','2020-07-11 00:41:45','false'),(91,23,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":23}','2020-07-16 17:53:39','false'),(92,30,1,'New Member, New member has joined','Akun.HomeAkun','{\"id_member\":30}','2020-07-16 17:53:39','false');
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_city`
--

DROP TABLE IF EXISTS `tb_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_city` (
  `city_id` int(100) NOT NULL,
  `province_id` int(3) NOT NULL,
  `province` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `city_name` varchar(100) NOT NULL,
  `postal_code` int(10) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_city`
--

LOCK TABLES `tb_city` WRITE;
/*!40000 ALTER TABLE `tb_city` DISABLE KEYS */;
INSERT INTO `tb_city` VALUES (1,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Aceh Barat',23681),(2,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Aceh Barat Daya',23764),(3,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Aceh Besar',23951),(4,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Aceh Jaya',23654),(5,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Aceh Selatan',23719),(6,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Aceh Singkil',24785),(7,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Aceh Tamiang',24476),(8,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Aceh Tengah',24511),(9,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Aceh Tenggara',24611),(10,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Aceh Timur',24454),(11,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Aceh Utara',24382),(12,32,'Sumatera Barat','Kabupaten','Agam',26411),(13,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Alor',85811),(14,19,'Maluku','Kota','Ambon',97222),(15,34,'Sumatera Utara','Kabupaten','Asahan',21214),(16,24,'Papua','Kabupaten','Asmat',99777),(17,1,'Bali','Kabupaten','Badung',80351),(18,13,'Kalimantan Selatan','Kabupaten','Balangan',71611),(19,15,'Kalimantan Timur','Kota','Balikpapan',76111),(20,21,'Nanggroe Aceh Darussalam (NAD)','Kota','Banda Aceh',23238),(21,18,'Lampung','Kota','Bandar Lampung',35139),(22,9,'Jawa Barat','Kabupaten','Bandung',40311),(23,9,'Jawa Barat','Kota','Bandung',40111),(24,9,'Jawa Barat','Kabupaten','Bandung Barat',40721),(25,29,'Sulawesi Tengah','Kabupaten','Banggai',94711),(26,29,'Sulawesi Tengah','Kabupaten','Banggai Kepulauan',94881),(27,2,'Bangka Belitung','Kabupaten','Bangka',33212),(28,2,'Bangka Belitung','Kabupaten','Bangka Barat',33315),(29,2,'Bangka Belitung','Kabupaten','Bangka Selatan',33719),(30,2,'Bangka Belitung','Kabupaten','Bangka Tengah',33613),(31,11,'Jawa Timur','Kabupaten','Bangkalan',69118),(32,1,'Bali','Kabupaten','Bangli',80619),(33,13,'Kalimantan Selatan','Kabupaten','Banjar',70619),(34,9,'Jawa Barat','Kota','Banjar',46311),(35,13,'Kalimantan Selatan','Kota','Banjarbaru',70712),(36,13,'Kalimantan Selatan','Kota','Banjarmasin',70117),(37,10,'Jawa Tengah','Kabupaten','Banjarnegara',53419),(38,28,'Sulawesi Selatan','Kabupaten','Bantaeng',92411),(39,5,'DI Yogyakarta','Kabupaten','Bantul',55715),(40,33,'Sumatera Selatan','Kabupaten','Banyuasin',30911),(41,10,'Jawa Tengah','Kabupaten','Banyumas',53114),(42,11,'Jawa Timur','Kabupaten','Banyuwangi',68416),(43,13,'Kalimantan Selatan','Kabupaten','Barito Kuala',70511),(44,14,'Kalimantan Tengah','Kabupaten','Barito Selatan',73711),(45,14,'Kalimantan Tengah','Kabupaten','Barito Timur',73671),(46,14,'Kalimantan Tengah','Kabupaten','Barito Utara',73881),(47,28,'Sulawesi Selatan','Kabupaten','Barru',90719),(48,17,'Kepulauan Riau','Kota','Batam',29413),(49,10,'Jawa Tengah','Kabupaten','Batang',51211),(50,8,'Jambi','Kabupaten','Batang Hari',36613),(51,11,'Jawa Timur','Kota','Batu',65311),(52,34,'Sumatera Utara','Kabupaten','Batu Bara',21655),(53,30,'Sulawesi Tenggara','Kota','Bau-Bau',93719),(54,9,'Jawa Barat','Kabupaten','Bekasi',17837),(55,9,'Jawa Barat','Kota','Bekasi',17121),(56,2,'Bangka Belitung','Kabupaten','Belitung',33419),(57,2,'Bangka Belitung','Kabupaten','Belitung Timur',33519),(58,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Belu',85711),(59,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Bener Meriah',24581),(60,26,'Riau','Kabupaten','Bengkalis',28719),(61,12,'Kalimantan Barat','Kabupaten','Bengkayang',79213),(62,4,'Bengkulu','Kota','Bengkulu',38229),(63,4,'Bengkulu','Kabupaten','Bengkulu Selatan',38519),(64,4,'Bengkulu','Kabupaten','Bengkulu Tengah',38319),(65,4,'Bengkulu','Kabupaten','Bengkulu Utara',38619),(66,15,'Kalimantan Timur','Kabupaten','Berau',77311),(67,24,'Papua','Kabupaten','Biak Numfor',98119),(68,22,'Nusa Tenggara Barat (NTB)','Kabupaten','Bima',84171),(69,22,'Nusa Tenggara Barat (NTB)','Kota','Bima',84139),(70,34,'Sumatera Utara','Kota','Binjai',20712),(71,17,'Kepulauan Riau','Kabupaten','Bintan',29135),(72,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Bireuen',24219),(73,31,'Sulawesi Utara','Kota','Bitung',95512),(74,11,'Jawa Timur','Kabupaten','Blitar',66171),(75,11,'Jawa Timur','Kota','Blitar',66124),(76,10,'Jawa Tengah','Kabupaten','Blora',58219),(77,7,'Gorontalo','Kabupaten','Boalemo',96319),(78,9,'Jawa Barat','Kabupaten','Bogor',16911),(79,9,'Jawa Barat','Kota','Bogor',16119),(80,11,'Jawa Timur','Kabupaten','Bojonegoro',62119),(81,31,'Sulawesi Utara','Kabupaten','Bolaang Mongondow (Bolmong)',95755),(82,31,'Sulawesi Utara','Kabupaten','Bolaang Mongondow Selatan',95774),(83,31,'Sulawesi Utara','Kabupaten','Bolaang Mongondow Timur',95783),(84,31,'Sulawesi Utara','Kabupaten','Bolaang Mongondow Utara',95765),(85,30,'Sulawesi Tenggara','Kabupaten','Bombana',93771),(86,11,'Jawa Timur','Kabupaten','Bondowoso',68219),(87,28,'Sulawesi Selatan','Kabupaten','Bone',92713),(88,7,'Gorontalo','Kabupaten','Bone Bolango',96511),(89,15,'Kalimantan Timur','Kota','Bontang',75313),(90,24,'Papua','Kabupaten','Boven Digoel',99662),(91,10,'Jawa Tengah','Kabupaten','Boyolali',57312),(92,10,'Jawa Tengah','Kabupaten','Brebes',52212),(93,32,'Sumatera Barat','Kota','Bukittinggi',26115),(94,1,'Bali','Kabupaten','Buleleng',81111),(95,28,'Sulawesi Selatan','Kabupaten','Bulukumba',92511),(96,16,'Kalimantan Utara','Kabupaten','Bulungan (Bulongan)',77211),(97,8,'Jambi','Kabupaten','Bungo',37216),(98,29,'Sulawesi Tengah','Kabupaten','Buol',94564),(99,19,'Maluku','Kabupaten','Buru',97371),(100,19,'Maluku','Kabupaten','Buru Selatan',97351),(101,30,'Sulawesi Tenggara','Kabupaten','Buton',93754),(102,30,'Sulawesi Tenggara','Kabupaten','Buton Utara',93745),(103,9,'Jawa Barat','Kabupaten','Ciamis',46211),(104,9,'Jawa Barat','Kabupaten','Cianjur',43217),(105,10,'Jawa Tengah','Kabupaten','Cilacap',53211),(106,3,'Banten','Kota','Cilegon',42417),(107,9,'Jawa Barat','Kota','Cimahi',40512),(108,9,'Jawa Barat','Kabupaten','Cirebon',45611),(109,9,'Jawa Barat','Kota','Cirebon',45116),(110,34,'Sumatera Utara','Kabupaten','Dairi',22211),(111,24,'Papua','Kabupaten','Deiyai (Deliyai)',98784),(112,34,'Sumatera Utara','Kabupaten','Deli Serdang',20511),(113,10,'Jawa Tengah','Kabupaten','Demak',59519),(114,1,'Bali','Kota','Denpasar',80227),(115,9,'Jawa Barat','Kota','Depok',16416),(116,32,'Sumatera Barat','Kabupaten','Dharmasraya',27612),(117,24,'Papua','Kabupaten','Dogiyai',98866),(118,22,'Nusa Tenggara Barat (NTB)','Kabupaten','Dompu',84217),(119,29,'Sulawesi Tengah','Kabupaten','Donggala',94341),(120,26,'Riau','Kota','Dumai',28811),(121,33,'Sumatera Selatan','Kabupaten','Empat Lawang',31811),(122,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Ende',86351),(123,28,'Sulawesi Selatan','Kabupaten','Enrekang',91719),(124,25,'Papua Barat','Kabupaten','Fakfak',98651),(125,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Flores Timur',86213),(126,9,'Jawa Barat','Kabupaten','Garut',44126),(127,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Gayo Lues',24653),(128,1,'Bali','Kabupaten','Gianyar',80519),(129,7,'Gorontalo','Kabupaten','Gorontalo',96218),(130,7,'Gorontalo','Kota','Gorontalo',96115),(131,7,'Gorontalo','Kabupaten','Gorontalo Utara',96611),(132,28,'Sulawesi Selatan','Kabupaten','Gowa',92111),(133,11,'Jawa Timur','Kabupaten','Gresik',61115),(134,10,'Jawa Tengah','Kabupaten','Grobogan',58111),(135,5,'DI Yogyakarta','Kabupaten','Gunung Kidul',55812),(136,14,'Kalimantan Tengah','Kabupaten','Gunung Mas',74511),(137,34,'Sumatera Utara','Kota','Gunungsitoli',22813),(138,20,'Maluku Utara','Kabupaten','Halmahera Barat',97757),(139,20,'Maluku Utara','Kabupaten','Halmahera Selatan',97911),(140,20,'Maluku Utara','Kabupaten','Halmahera Tengah',97853),(141,20,'Maluku Utara','Kabupaten','Halmahera Timur',97862),(142,20,'Maluku Utara','Kabupaten','Halmahera Utara',97762),(143,13,'Kalimantan Selatan','Kabupaten','Hulu Sungai Selatan',71212),(144,13,'Kalimantan Selatan','Kabupaten','Hulu Sungai Tengah',71313),(145,13,'Kalimantan Selatan','Kabupaten','Hulu Sungai Utara',71419),(146,34,'Sumatera Utara','Kabupaten','Humbang Hasundutan',22457),(147,26,'Riau','Kabupaten','Indragiri Hilir',29212),(148,26,'Riau','Kabupaten','Indragiri Hulu',29319),(149,9,'Jawa Barat','Kabupaten','Indramayu',45214),(150,24,'Papua','Kabupaten','Intan Jaya',98771),(151,6,'DKI Jakarta','Kota','Jakarta Barat',11220),(152,6,'DKI Jakarta','Kota','Jakarta Pusat',10540),(153,6,'DKI Jakarta','Kota','Jakarta Selatan',12230),(154,6,'DKI Jakarta','Kota','Jakarta Timur',13330),(155,6,'DKI Jakarta','Kota','Jakarta Utara',14140),(156,8,'Jambi','Kota','Jambi',36111),(157,24,'Papua','Kabupaten','Jayapura',99352),(158,24,'Papua','Kota','Jayapura',99114),(159,24,'Papua','Kabupaten','Jayawijaya',99511),(160,11,'Jawa Timur','Kabupaten','Jember',68113),(161,1,'Bali','Kabupaten','Jembrana',82251),(162,28,'Sulawesi Selatan','Kabupaten','Jeneponto',92319),(163,10,'Jawa Tengah','Kabupaten','Jepara',59419),(164,11,'Jawa Timur','Kabupaten','Jombang',61415),(165,25,'Papua Barat','Kabupaten','Kaimana',98671),(166,26,'Riau','Kabupaten','Kampar',28411),(167,14,'Kalimantan Tengah','Kabupaten','Kapuas',73583),(168,12,'Kalimantan Barat','Kabupaten','Kapuas Hulu',78719),(169,10,'Jawa Tengah','Kabupaten','Karanganyar',57718),(170,1,'Bali','Kabupaten','Karangasem',80819),(171,9,'Jawa Barat','Kabupaten','Karawang',41311),(172,17,'Kepulauan Riau','Kabupaten','Karimun',29611),(173,34,'Sumatera Utara','Kabupaten','Karo',22119),(174,14,'Kalimantan Tengah','Kabupaten','Katingan',74411),(175,4,'Bengkulu','Kabupaten','Kaur',38911),(176,12,'Kalimantan Barat','Kabupaten','Kayong Utara',78852),(177,10,'Jawa Tengah','Kabupaten','Kebumen',54319),(178,11,'Jawa Timur','Kabupaten','Kediri',64184),(179,11,'Jawa Timur','Kota','Kediri',64125),(180,24,'Papua','Kabupaten','Keerom',99461),(181,10,'Jawa Tengah','Kabupaten','Kendal',51314),(182,30,'Sulawesi Tenggara','Kota','Kendari',93126),(183,4,'Bengkulu','Kabupaten','Kepahiang',39319),(184,17,'Kepulauan Riau','Kabupaten','Kepulauan Anambas',29991),(185,19,'Maluku','Kabupaten','Kepulauan Aru',97681),(186,32,'Sumatera Barat','Kabupaten','Kepulauan Mentawai',25771),(187,26,'Riau','Kabupaten','Kepulauan Meranti',28791),(188,31,'Sulawesi Utara','Kabupaten','Kepulauan Sangihe',95819),(189,6,'DKI Jakarta','Kabupaten','Kepulauan Seribu',14550),(190,31,'Sulawesi Utara','Kabupaten','Kepulauan Siau Tagulandang Biaro (Sitaro)',95862),(191,20,'Maluku Utara','Kabupaten','Kepulauan Sula',97995),(192,31,'Sulawesi Utara','Kabupaten','Kepulauan Talaud',95885),(193,24,'Papua','Kabupaten','Kepulauan Yapen (Yapen Waropen)',98211),(194,8,'Jambi','Kabupaten','Kerinci',37167),(195,12,'Kalimantan Barat','Kabupaten','Ketapang',78874),(196,10,'Jawa Tengah','Kabupaten','Klaten',57411),(197,1,'Bali','Kabupaten','Klungkung',80719),(198,30,'Sulawesi Tenggara','Kabupaten','Kolaka',93511),(199,30,'Sulawesi Tenggara','Kabupaten','Kolaka Utara',93911),(200,30,'Sulawesi Tenggara','Kabupaten','Konawe',93411),(201,30,'Sulawesi Tenggara','Kabupaten','Konawe Selatan',93811),(202,30,'Sulawesi Tenggara','Kabupaten','Konawe Utara',93311),(203,13,'Kalimantan Selatan','Kabupaten','Kotabaru',72119),(204,31,'Sulawesi Utara','Kota','Kotamobagu',95711),(205,14,'Kalimantan Tengah','Kabupaten','Kotawaringin Barat',74119),(206,14,'Kalimantan Tengah','Kabupaten','Kotawaringin Timur',74364),(207,26,'Riau','Kabupaten','Kuantan Singingi',29519),(208,12,'Kalimantan Barat','Kabupaten','Kubu Raya',78311),(209,10,'Jawa Tengah','Kabupaten','Kudus',59311),(210,5,'DI Yogyakarta','Kabupaten','Kulon Progo',55611),(211,9,'Jawa Barat','Kabupaten','Kuningan',45511),(212,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Kupang',85362),(213,23,'Nusa Tenggara Timur (NTT)','Kota','Kupang',85119),(214,15,'Kalimantan Timur','Kabupaten','Kutai Barat',75711),(215,15,'Kalimantan Timur','Kabupaten','Kutai Kartanegara',75511),(216,15,'Kalimantan Timur','Kabupaten','Kutai Timur',75611),(217,34,'Sumatera Utara','Kabupaten','Labuhan Batu',21412),(218,34,'Sumatera Utara','Kabupaten','Labuhan Batu Selatan',21511),(219,34,'Sumatera Utara','Kabupaten','Labuhan Batu Utara',21711),(220,33,'Sumatera Selatan','Kabupaten','Lahat',31419),(221,14,'Kalimantan Tengah','Kabupaten','Lamandau',74611),(222,11,'Jawa Timur','Kabupaten','Lamongan',64125),(223,18,'Lampung','Kabupaten','Lampung Barat',34814),(224,18,'Lampung','Kabupaten','Lampung Selatan',35511),(225,18,'Lampung','Kabupaten','Lampung Tengah',34212),(226,18,'Lampung','Kabupaten','Lampung Timur',34319),(227,18,'Lampung','Kabupaten','Lampung Utara',34516),(228,12,'Kalimantan Barat','Kabupaten','Landak',78319),(229,34,'Sumatera Utara','Kabupaten','Langkat',20811),(230,21,'Nanggroe Aceh Darussalam (NAD)','Kota','Langsa',24412),(231,24,'Papua','Kabupaten','Lanny Jaya',99531),(232,3,'Banten','Kabupaten','Lebak',42319),(233,4,'Bengkulu','Kabupaten','Lebong',39264),(234,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Lembata',86611),(235,21,'Nanggroe Aceh Darussalam (NAD)','Kota','Lhokseumawe',24352),(236,32,'Sumatera Barat','Kabupaten','Lima Puluh Koto/Kota',26671),(237,17,'Kepulauan Riau','Kabupaten','Lingga',29811),(238,22,'Nusa Tenggara Barat (NTB)','Kabupaten','Lombok Barat',83311),(239,22,'Nusa Tenggara Barat (NTB)','Kabupaten','Lombok Tengah',83511),(240,22,'Nusa Tenggara Barat (NTB)','Kabupaten','Lombok Timur',83612),(241,22,'Nusa Tenggara Barat (NTB)','Kabupaten','Lombok Utara',83711),(242,33,'Sumatera Selatan','Kota','Lubuk Linggau',31614),(243,11,'Jawa Timur','Kabupaten','Lumajang',67319),(244,28,'Sulawesi Selatan','Kabupaten','Luwu',91994),(245,28,'Sulawesi Selatan','Kabupaten','Luwu Timur',92981),(246,28,'Sulawesi Selatan','Kabupaten','Luwu Utara',92911),(247,11,'Jawa Timur','Kabupaten','Madiun',63153),(248,11,'Jawa Timur','Kota','Madiun',63122),(249,10,'Jawa Tengah','Kabupaten','Magelang',56519),(250,10,'Jawa Tengah','Kota','Magelang',56133),(251,11,'Jawa Timur','Kabupaten','Magetan',63314),(252,9,'Jawa Barat','Kabupaten','Majalengka',45412),(253,27,'Sulawesi Barat','Kabupaten','Majene',91411),(254,28,'Sulawesi Selatan','Kota','Makassar',90111),(255,11,'Jawa Timur','Kabupaten','Malang',65163),(256,11,'Jawa Timur','Kota','Malang',65112),(257,16,'Kalimantan Utara','Kabupaten','Malinau',77511),(258,19,'Maluku','Kabupaten','Maluku Barat Daya',97451),(259,19,'Maluku','Kabupaten','Maluku Tengah',97513),(260,19,'Maluku','Kabupaten','Maluku Tenggara',97651),(261,19,'Maluku','Kabupaten','Maluku Tenggara Barat',97465),(262,27,'Sulawesi Barat','Kabupaten','Mamasa',91362),(263,24,'Papua','Kabupaten','Mamberamo Raya',99381),(264,24,'Papua','Kabupaten','Mamberamo Tengah',99553),(265,27,'Sulawesi Barat','Kabupaten','Mamuju',91519),(266,27,'Sulawesi Barat','Kabupaten','Mamuju Utara',91571),(267,31,'Sulawesi Utara','Kota','Manado',95247),(268,34,'Sumatera Utara','Kabupaten','Mandailing Natal',22916),(269,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Manggarai',86551),(270,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Manggarai Barat',86711),(271,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Manggarai Timur',86811),(272,25,'Papua Barat','Kabupaten','Manokwari',98311),(273,25,'Papua Barat','Kabupaten','Manokwari Selatan',98355),(274,24,'Papua','Kabupaten','Mappi',99853),(275,28,'Sulawesi Selatan','Kabupaten','Maros',90511),(276,22,'Nusa Tenggara Barat (NTB)','Kota','Mataram',83131),(277,25,'Papua Barat','Kabupaten','Maybrat',98051),(278,34,'Sumatera Utara','Kota','Medan',20228),(279,12,'Kalimantan Barat','Kabupaten','Melawi',78619),(280,8,'Jambi','Kabupaten','Merangin',37319),(281,24,'Papua','Kabupaten','Merauke',99613),(282,18,'Lampung','Kabupaten','Mesuji',34911),(283,18,'Lampung','Kota','Metro',34111),(284,24,'Papua','Kabupaten','Mimika',99962),(285,31,'Sulawesi Utara','Kabupaten','Minahasa',95614),(286,31,'Sulawesi Utara','Kabupaten','Minahasa Selatan',95914),(287,31,'Sulawesi Utara','Kabupaten','Minahasa Tenggara',95995),(288,31,'Sulawesi Utara','Kabupaten','Minahasa Utara',95316),(289,11,'Jawa Timur','Kabupaten','Mojokerto',61382),(290,11,'Jawa Timur','Kota','Mojokerto',61316),(291,29,'Sulawesi Tengah','Kabupaten','Morowali',94911),(292,33,'Sumatera Selatan','Kabupaten','Muara Enim',31315),(293,8,'Jambi','Kabupaten','Muaro Jambi',36311),(294,4,'Bengkulu','Kabupaten','Muko Muko',38715),(295,30,'Sulawesi Tenggara','Kabupaten','Muna',93611),(296,14,'Kalimantan Tengah','Kabupaten','Murung Raya',73911),(297,33,'Sumatera Selatan','Kabupaten','Musi Banyuasin',30719),(298,33,'Sumatera Selatan','Kabupaten','Musi Rawas',31661),(299,24,'Papua','Kabupaten','Nabire',98816),(300,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Nagan Raya',23674),(301,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Nagekeo',86911),(302,17,'Kepulauan Riau','Kabupaten','Natuna',29711),(303,24,'Papua','Kabupaten','Nduga',99541),(304,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Ngada',86413),(305,11,'Jawa Timur','Kabupaten','Nganjuk',64414),(306,11,'Jawa Timur','Kabupaten','Ngawi',63219),(307,34,'Sumatera Utara','Kabupaten','Nias',22876),(308,34,'Sumatera Utara','Kabupaten','Nias Barat',22895),(309,34,'Sumatera Utara','Kabupaten','Nias Selatan',22865),(310,34,'Sumatera Utara','Kabupaten','Nias Utara',22856),(311,16,'Kalimantan Utara','Kabupaten','Nunukan',77421),(312,33,'Sumatera Selatan','Kabupaten','Ogan Ilir',30811),(313,33,'Sumatera Selatan','Kabupaten','Ogan Komering Ilir',30618),(314,33,'Sumatera Selatan','Kabupaten','Ogan Komering Ulu',32112),(315,33,'Sumatera Selatan','Kabupaten','Ogan Komering Ulu Selatan',32211),(316,33,'Sumatera Selatan','Kabupaten','Ogan Komering Ulu Timur',32312),(317,11,'Jawa Timur','Kabupaten','Pacitan',63512),(318,32,'Sumatera Barat','Kota','Padang',25112),(319,34,'Sumatera Utara','Kabupaten','Padang Lawas',22763),(320,34,'Sumatera Utara','Kabupaten','Padang Lawas Utara',22753),(321,32,'Sumatera Barat','Kota','Padang Panjang',27122),(322,32,'Sumatera Barat','Kabupaten','Padang Pariaman',25583),(323,34,'Sumatera Utara','Kota','Padang Sidempuan',22727),(324,33,'Sumatera Selatan','Kota','Pagar Alam',31512),(325,34,'Sumatera Utara','Kabupaten','Pakpak Bharat',22272),(326,14,'Kalimantan Tengah','Kota','Palangka Raya',73112),(327,33,'Sumatera Selatan','Kota','Palembang',31512),(328,28,'Sulawesi Selatan','Kota','Palopo',91911),(329,29,'Sulawesi Tengah','Kota','Palu',94111),(330,11,'Jawa Timur','Kabupaten','Pamekasan',69319),(331,3,'Banten','Kabupaten','Pandeglang',42212),(332,9,'Jawa Barat','Kabupaten','Pangandaran',46511),(333,28,'Sulawesi Selatan','Kabupaten','Pangkajene Kepulauan',90611),(334,2,'Bangka Belitung','Kota','Pangkal Pinang',33115),(335,24,'Papua','Kabupaten','Paniai',98765),(336,28,'Sulawesi Selatan','Kota','Parepare',91123),(337,32,'Sumatera Barat','Kota','Pariaman',25511),(338,29,'Sulawesi Tengah','Kabupaten','Parigi Moutong',94411),(339,32,'Sumatera Barat','Kabupaten','Pasaman',26318),(340,32,'Sumatera Barat','Kabupaten','Pasaman Barat',26511),(341,15,'Kalimantan Timur','Kabupaten','Paser',76211),(342,11,'Jawa Timur','Kabupaten','Pasuruan',67153),(343,11,'Jawa Timur','Kota','Pasuruan',67118),(344,10,'Jawa Tengah','Kabupaten','Pati',59114),(345,32,'Sumatera Barat','Kota','Payakumbuh',26213),(346,25,'Papua Barat','Kabupaten','Pegunungan Arfak',98354),(347,24,'Papua','Kabupaten','Pegunungan Bintang',99573),(348,10,'Jawa Tengah','Kabupaten','Pekalongan',51161),(349,10,'Jawa Tengah','Kota','Pekalongan',51122),(350,26,'Riau','Kota','Pekanbaru',28112),(351,26,'Riau','Kabupaten','Pelalawan',28311),(352,10,'Jawa Tengah','Kabupaten','Pemalang',52319),(353,34,'Sumatera Utara','Kota','Pematang Siantar',21126),(354,15,'Kalimantan Timur','Kabupaten','Penajam Paser Utara',76311),(355,18,'Lampung','Kabupaten','Pesawaran',35312),(356,18,'Lampung','Kabupaten','Pesisir Barat',35974),(357,32,'Sumatera Barat','Kabupaten','Pesisir Selatan',25611),(358,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Pidie',24116),(359,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Pidie Jaya',24186),(360,28,'Sulawesi Selatan','Kabupaten','Pinrang',91251),(361,7,'Gorontalo','Kabupaten','Pohuwato',96419),(362,27,'Sulawesi Barat','Kabupaten','Polewali Mandar',91311),(363,11,'Jawa Timur','Kabupaten','Ponorogo',63411),(364,12,'Kalimantan Barat','Kabupaten','Pontianak',78971),(365,12,'Kalimantan Barat','Kota','Pontianak',78112),(366,29,'Sulawesi Tengah','Kabupaten','Poso',94615),(367,33,'Sumatera Selatan','Kota','Prabumulih',31121),(368,18,'Lampung','Kabupaten','Pringsewu',35719),(369,11,'Jawa Timur','Kabupaten','Probolinggo',67282),(370,11,'Jawa Timur','Kota','Probolinggo',67215),(371,14,'Kalimantan Tengah','Kabupaten','Pulang Pisau',74811),(372,20,'Maluku Utara','Kabupaten','Pulau Morotai',97771),(373,24,'Papua','Kabupaten','Puncak',98981),(374,24,'Papua','Kabupaten','Puncak Jaya',98979),(375,10,'Jawa Tengah','Kabupaten','Purbalingga',53312),(376,9,'Jawa Barat','Kabupaten','Purwakarta',41119),(377,10,'Jawa Tengah','Kabupaten','Purworejo',54111),(378,25,'Papua Barat','Kabupaten','Raja Ampat',98489),(379,4,'Bengkulu','Kabupaten','Rejang Lebong',39112),(380,10,'Jawa Tengah','Kabupaten','Rembang',59219),(381,26,'Riau','Kabupaten','Rokan Hilir',28992),(382,26,'Riau','Kabupaten','Rokan Hulu',28511),(383,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Rote Ndao',85982),(384,21,'Nanggroe Aceh Darussalam (NAD)','Kota','Sabang',23512),(385,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Sabu Raijua',85391),(386,10,'Jawa Tengah','Kota','Salatiga',50711),(387,15,'Kalimantan Timur','Kota','Samarinda',75133),(388,12,'Kalimantan Barat','Kabupaten','Sambas',79453),(389,34,'Sumatera Utara','Kabupaten','Samosir',22392),(390,11,'Jawa Timur','Kabupaten','Sampang',69219),(391,12,'Kalimantan Barat','Kabupaten','Sanggau',78557),(392,24,'Papua','Kabupaten','Sarmi',99373),(393,8,'Jambi','Kabupaten','Sarolangun',37419),(394,32,'Sumatera Barat','Kota','Sawah Lunto',27416),(395,12,'Kalimantan Barat','Kabupaten','Sekadau',79583),(396,28,'Sulawesi Selatan','Kabupaten','Selayar (Kepulauan Selayar)',92812),(397,4,'Bengkulu','Kabupaten','Seluma',38811),(398,10,'Jawa Tengah','Kabupaten','Semarang',50511),(399,10,'Jawa Tengah','Kota','Semarang',50135),(400,19,'Maluku','Kabupaten','Seram Bagian Barat',97561),(401,19,'Maluku','Kabupaten','Seram Bagian Timur',97581),(402,3,'Banten','Kabupaten','Serang',42182),(403,3,'Banten','Kota','Serang',42111),(404,34,'Sumatera Utara','Kabupaten','Serdang Bedagai',20915),(405,14,'Kalimantan Tengah','Kabupaten','Seruyan',74211),(406,26,'Riau','Kabupaten','Siak',28623),(407,34,'Sumatera Utara','Kota','Sibolga',22522),(408,28,'Sulawesi Selatan','Kabupaten','Sidenreng Rappang/Rapang',91613),(409,11,'Jawa Timur','Kabupaten','Sidoarjo',61219),(410,29,'Sulawesi Tengah','Kabupaten','Sigi',94364),(411,32,'Sumatera Barat','Kabupaten','Sijunjung (Sawah Lunto Sijunjung)',27511),(412,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Sikka',86121),(413,34,'Sumatera Utara','Kabupaten','Simalungun',21162),(414,21,'Nanggroe Aceh Darussalam (NAD)','Kabupaten','Simeulue',23891),(415,12,'Kalimantan Barat','Kota','Singkawang',79117),(416,28,'Sulawesi Selatan','Kabupaten','Sinjai',92615),(417,12,'Kalimantan Barat','Kabupaten','Sintang',78619),(418,11,'Jawa Timur','Kabupaten','Situbondo',68316),(419,5,'DI Yogyakarta','Kabupaten','Sleman',55513),(420,32,'Sumatera Barat','Kabupaten','Solok',27365),(421,32,'Sumatera Barat','Kota','Solok',27315),(422,32,'Sumatera Barat','Kabupaten','Solok Selatan',27779),(423,28,'Sulawesi Selatan','Kabupaten','Soppeng',90812),(424,25,'Papua Barat','Kabupaten','Sorong',98431),(425,25,'Papua Barat','Kota','Sorong',98411),(426,25,'Papua Barat','Kabupaten','Sorong Selatan',98454),(427,10,'Jawa Tengah','Kabupaten','Sragen',57211),(428,9,'Jawa Barat','Kabupaten','Subang',41215),(429,21,'Nanggroe Aceh Darussalam (NAD)','Kota','Subulussalam',24882),(430,9,'Jawa Barat','Kabupaten','Sukabumi',43311),(431,9,'Jawa Barat','Kota','Sukabumi',43114),(432,14,'Kalimantan Tengah','Kabupaten','Sukamara',74712),(433,10,'Jawa Tengah','Kabupaten','Sukoharjo',57514),(434,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Sumba Barat',87219),(435,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Sumba Barat Daya',87453),(436,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Sumba Tengah',87358),(437,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Sumba Timur',87112),(438,22,'Nusa Tenggara Barat (NTB)','Kabupaten','Sumbawa',84315),(439,22,'Nusa Tenggara Barat (NTB)','Kabupaten','Sumbawa Barat',84419),(440,9,'Jawa Barat','Kabupaten','Sumedang',45326),(441,11,'Jawa Timur','Kabupaten','Sumenep',69413),(442,8,'Jambi','Kota','Sungaipenuh',37113),(443,24,'Papua','Kabupaten','Supiori',98164),(444,11,'Jawa Timur','Kota','Surabaya',60119),(445,10,'Jawa Tengah','Kota','Surakarta (Solo)',57113),(446,13,'Kalimantan Selatan','Kabupaten','Tabalong',71513),(447,1,'Bali','Kabupaten','Tabanan',82119),(448,28,'Sulawesi Selatan','Kabupaten','Takalar',92212),(449,25,'Papua Barat','Kabupaten','Tambrauw',98475),(450,16,'Kalimantan Utara','Kabupaten','Tana Tidung',77611),(451,28,'Sulawesi Selatan','Kabupaten','Tana Toraja',91819),(452,13,'Kalimantan Selatan','Kabupaten','Tanah Bumbu',72211),(453,32,'Sumatera Barat','Kabupaten','Tanah Datar',27211),(454,13,'Kalimantan Selatan','Kabupaten','Tanah Laut',70811),(455,3,'Banten','Kabupaten','Tangerang',15914),(456,3,'Banten','Kota','Tangerang',15111),(457,3,'Banten','Kota','Tangerang Selatan',15332),(458,18,'Lampung','Kabupaten','Tanggamus',35619),(459,34,'Sumatera Utara','Kota','Tanjung Balai',21321),(460,8,'Jambi','Kabupaten','Tanjung Jabung Barat',36513),(461,8,'Jambi','Kabupaten','Tanjung Jabung Timur',36719),(462,17,'Kepulauan Riau','Kota','Tanjung Pinang',29111),(463,34,'Sumatera Utara','Kabupaten','Tapanuli Selatan',22742),(464,34,'Sumatera Utara','Kabupaten','Tapanuli Tengah',22611),(465,34,'Sumatera Utara','Kabupaten','Tapanuli Utara',22414),(466,13,'Kalimantan Selatan','Kabupaten','Tapin',71119),(467,16,'Kalimantan Utara','Kota','Tarakan',77114),(468,9,'Jawa Barat','Kabupaten','Tasikmalaya',46411),(469,9,'Jawa Barat','Kota','Tasikmalaya',46116),(470,34,'Sumatera Utara','Kota','Tebing Tinggi',20632),(471,8,'Jambi','Kabupaten','Tebo',37519),(472,10,'Jawa Tengah','Kabupaten','Tegal',52419),(473,10,'Jawa Tengah','Kota','Tegal',52114),(474,25,'Papua Barat','Kabupaten','Teluk Bintuni',98551),(475,25,'Papua Barat','Kabupaten','Teluk Wondama',98591),(476,10,'Jawa Tengah','Kabupaten','Temanggung',56212),(477,20,'Maluku Utara','Kota','Ternate',97714),(478,20,'Maluku Utara','Kota','Tidore Kepulauan',97815),(479,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Timor Tengah Selatan',85562),(480,23,'Nusa Tenggara Timur (NTT)','Kabupaten','Timor Tengah Utara',85612),(481,34,'Sumatera Utara','Kabupaten','Toba Samosir',22316),(482,29,'Sulawesi Tengah','Kabupaten','Tojo Una-Una',94683),(483,29,'Sulawesi Tengah','Kabupaten','Toli-Toli',94542),(484,24,'Papua','Kabupaten','Tolikara',99411),(485,31,'Sulawesi Utara','Kota','Tomohon',95416),(486,28,'Sulawesi Selatan','Kabupaten','Toraja Utara',91831),(487,11,'Jawa Timur','Kabupaten','Trenggalek',66312),(488,19,'Maluku','Kota','Tual',97612),(489,11,'Jawa Timur','Kabupaten','Tuban',62319),(490,18,'Lampung','Kabupaten','Tulang Bawang',34613),(491,18,'Lampung','Kabupaten','Tulang Bawang Barat',34419),(492,11,'Jawa Timur','Kabupaten','Tulungagung',66212),(493,28,'Sulawesi Selatan','Kabupaten','Wajo',90911),(494,30,'Sulawesi Tenggara','Kabupaten','Wakatobi',93791),(495,24,'Papua','Kabupaten','Waropen',98269),(496,18,'Lampung','Kabupaten','Way Kanan',34711),(497,10,'Jawa Tengah','Kabupaten','Wonogiri',57619),(498,10,'Jawa Tengah','Kabupaten','Wonosobo',56311),(499,24,'Papua','Kabupaten','Yahukimo',99041),(500,24,'Papua','Kabupaten','Yalimo',99481),(501,5,'DI Yogyakarta','Kota','Yogyakarta',55222);
/*!40000 ALTER TABLE `tb_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_province`
--

DROP TABLE IF EXISTS `tb_province`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_province` (
  `province_id` int(10) NOT NULL,
  `province` varchar(100) NOT NULL,
  PRIMARY KEY (`province_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_province`
--

LOCK TABLES `tb_province` WRITE;
/*!40000 ALTER TABLE `tb_province` DISABLE KEYS */;
INSERT INTO `tb_province` VALUES (1,'Bali'),(2,'Bangka Belitung'),(3,'Banten'),(4,'Bengkulu'),(5,'DI Yogyakarta'),(6,'DKI Jakarta'),(7,'Gorontalo'),(8,'Jambi'),(9,'Jawa Barat'),(10,'Jawa Tengah'),(11,'Jawa Timur'),(12,'Kalimantan Barat'),(13,'Kalimantan Selatan'),(14,'Kalimantan Tengah'),(15,'Kalimantan Timur'),(16,'Kalimantan Utara'),(17,'Kepulauan Riau'),(18,'Lampung'),(19,'Maluku'),(20,'Maluku Utara'),(21,'Nanggroe Aceh Darussalam (NAD)'),(22,'Nusa Tenggara Barat (NTB)'),(23,'Nusa Tenggara Timur (NTT)'),(24,'Papua'),(25,'Papua Barat'),(26,'Riau'),(27,'Sulawesi Barat'),(28,'Sulawesi Selatan'),(29,'Sulawesi Tengah'),(30,'Sulawesi Tenggara'),(31,'Sulawesi Utara'),(32,'Sumatera Barat'),(33,'Sumatera Selatan'),(34,'Sumatera Utara');
/*!40000 ALTER TABLE `tb_province` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'syauri','syauri04@gmail.com',NULL,'$2y$10$iwsFlfRvSbKYgoeO38xWGeW5o/7V2X1vha7j0IJOyyDbXhj8glvlW',NULL,'2020-02-18 09:42:19','2020-02-18 09:42:19'),(2,'admin','admin@gmail.com',NULL,'$2y$10$3WFMemDQEdNF7OqbT63MJ.PBLaDBUEyqPw39PNUeGj7nnBPDmHw8i',NULL,'2020-02-20 20:50:30','2020-02-20 20:50:30');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-20 19:04:20
