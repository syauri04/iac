<?php
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;


if (!function_exists('ngepush_notif')) {
    function ngepush_notif($token,$data){
        $url = "https://fcm.googleapis.com/fcm/send";
        $serverKey = 'AAAAfBPwKl4:APA91bF-UXBuQ1VVihofdcezT1GR5nsCZAKhB0LzplyWicdJYcdQD29HUb1ZUkuwMwFD0SoC-EqoV8gvieYVdmUIv0pC5rLp9mOSODDpR-daRFShg6u_vYWQBib0402yOB5WiMKik8HU';
        $arrayToSend = array('to' => $token,'data'=>$data,'priority'=>'high');
        $json = json_encode($arrayToSend);
        //print_r($json);
        // $fields = array(
        //     'registration_ids' => rand(10,10),
        //     'data' => 'dsadsa',
        // );
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key='. $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        //Send the request
        return curl_exec($ch);
        //Close request
        //return $response;
        curl_close($ch);
        }
}
if (!function_exists('makeDirectory')) {
    function makeDirectory($path, $mode = 0777, $recursive = false, $force = false)
    {
        // dd($path);
        if ($force)
        {
            return @mkdir($path, $mode, $recursive);
        }
        else
        {
            return mkdir($path, $mode, $recursive);
        }
    }
}
if (!function_exists('base64_get')) {
    function base64_get($image)
    {   
        $data = explode(',', $image);
        $pos  = strpos($image, ';');
        $type = explode('/', substr($image, 0, $pos));
        return [
            'extension' =>$type[1],
            'base64'    =>$data[1]
        ];
    }
}

if (!function_exists('get_ongkir')) {
    function get_ongkir($param)
    {   
        // return($param);
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "origin=".$param['origin']."&originType=city&destination=".$param['destination']."&destinationType=city&weight=".$param['weight']."&courier=".$param['courier']."",
          CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: d4436683f438b4dde5108aedc2539939"
          ),
        ));
       
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        // $foo = new StdClass();
        // $foo = $response;
        return json_decode($response, true);
    }
}

if (!function_exists('LinkActions')) {
    function LinkActions($links=array(),$id=""){
      
        if(count($links)>0){
           
            foreach($links as $m){
                $property = "";
                
                if($m['type']=='simple'){
                    $property = " class=\"btn btn-primary btn-icon btn-circle\" ";
                }elseif($m['type']=='confirm'){
                    $property = " class=\"btn btn-primary btn-icon btn-circle\" ";
                }else{
                    $property = " class=\"btn btn-primary btn-icon btn-circle\" ";	
                }	
            ?>
            <a href="<?php echo $m['link']."/".$id;?>" <?php echo $property;?> title="<?php echo ucwords($m['title']);?> Data "><i class="fa fa-<?=$m['image']?> "></i></a>
            <?php  	
            } 
        }
    }
}

if (!function_exists('default_img')) {
    function default_img($img,$default_path,$img_path)
    {
        if($img==''){
            return asset($default_path);
        }else{
            return asset($img_path.'/'.$img);
        }
    }
}

if (!function_exists('change_null')) {
    function change_null($data)
    {
        if($data==''){
            return '';
        }else{
            return $data;
        }
    }
}

if (!function_exists('get_count')) {
    function get_count($param="")
    {
        // dd($param['tabel']);
        $count = DB::table($param['table'])
                ->where($param['field1'],$param['where1'])
                ->where($param['field2'],$param['where2'])
                ->count();
        // dd($count);
        return $count;
        
    }
}

if (!function_exists('get_count_polling')) {
    function get_count_polling($poliingID="",$answer="")
    {
        // dd($param['tabel']);
        $count = DB::table('com_polling_count')
                ->where('polling_id',$poliingID)
                ->where('count',$answer)
                ->count();
        // dd($count);
        return $count;
        
    }
}

function get_date_id($date=""){
	
	$date = trim($date)==""?date("Y-m-d"):$date;

	$tgl = myDate($date,"d",false);
	$thn = myDate($date,"Y",false);

	$month = array(
			'01' 	=> "Januari",
			'02' 	=> "Februari",
			'03' 	=> "Maret",
			'04' 	=> "April",
			'05' 	=> "Mei",
			'06' 	=> "Juni",
			'07'	=> "Juli",
			'08' 	=> "Agustus",
			'09' 	=> "September",
			'10'  	=> "Oktober",
			'11'  	=> "November",
			'12' 	=> "Desember"
		);
	$bulan = $month[myDate($date,"m",false)];

	return $tgl." ".$bulan." ".$thn;
}

function myDate($dt,$f="",$s=true){
	$day = array(
		1 => "Senin",
		2 => "Selasa",
		3 => "Rabu",
		4 => "Kamis",
		5 => "Jumat",
		6 => "Sabtu",
		7 => "Minggu"
	);
	if(trim($dt)!="0000-00-00" && trim($dt)!=""){
		$ts = strtotime($dt);
		$dtm = date($f,$ts);
		if( trim($dtm) == "01/01/1970" ){
			return "-";
		}else{
			return ($s)?$day[date("N",$ts)].", ".get_date_id($dt)." ".$dtm:$dtm;
		}
	}else{
		return "-";
	}
}

if (!function_exists('set_action')) {
    function set_action($v=array("bug","add","index"),$type='MAIN'){
		if(count($v)>0){
            $acc = set_sess_action();
           
			foreach($v as $r){
				if(isset($acc[$r])){
					$arr_action = array(
						'title'	=> $r,
						'action'=> $r,
						'type'	=> $acc[$r]->ac_action_type,
						'link'	=> url($acc[$r]->acc_group_controller."/".$acc[$r]->acc_controller_name."/".$r),
						'image'	=> $acc[$r]->ac_action_image
					);
					if($type=='MAIN'){
						 $links[] = $arr_action;
					}elseif($type=='TABLE'){
						 $links[] = $arr_action;
					}elseif($type=='ITEM'){
						 $links[] = $arr_action;
					}else{
						 $links[] = $arr_action;
					}
				}
            }
            
            return $links;
		}
    }
}


if (!function_exists('set_sess_action')) {
    function set_sess_action(){
        
        $route = Route::current();
        $class = explode('/',$route->uri);
        if(count($class) == 2){
            $nameRoute = $class[1];
        }else{
            $nameRoute = $route->uri;
        }

        
        $action =   DB::table('cp_app_user AS mu')
                    ->select('maa.ac_action', 'maa.ac_action_image', 'aac.acc_access_name', 'maa.ac_action_type',
                    'aac.acc_group_controller', 'aac.acc_controller_name', 'maa.ac_action_name')
                    ->leftJoin('cp_app_acl_group AS ag', 'ag.ag_id', '=' ,'mu.user_group')
                    ->leftJoin('cp_app_acl_group_accesses AS agc', 'agc.aga_group_id', '=', 'ag.ag_id')
                    ->leftJoin('cp_app_acl_actions AS maa', 'agc.aga_action_id', '=', 'maa.ac_id')
                    ->leftJoin('cp_app_acl_accesses AS aac', 'agc.aga_access_id', '=', 'aac.acc_id')
                    ->where('ag.ag_group_status','1')
                    ->where('mu.id',Auth::id())
                    ->where('aac.acc_controller_name',strtolower(str_ireplace("/", "", $nameRoute)))
                    ->get();
        
        
    
    //    dd($action);
        $acc=array();
        if(count($action) > 0){
            foreach($action as $r){
                $acc[$r->ac_action] = $r;
            }
        }
        
        return $acc;
        // $this->setAccess($acc);
    }
}


if (!function_exists('set_sess_menu')) {
    function set_sess_menu(){
        
        $menu = DB::table("cp_app_user AS mu")
                ->select("aac.*")
                ->leftJoin('cp_app_acl_group AS ag', 'ag.ag_id', '=' ,'mu.user_group')
                ->leftJoin('cp_app_acl_group_accesses AS agc', 'agc.aga_group_id', '=', 'ag.ag_id')
                ->leftJoin('cp_app_acl_actions AS maa', 'agc.aga_action_id', '=', 'maa.ac_id')
                ->leftJoin('cp_app_acl_accesses AS aac', 'agc.aga_access_id', '=', 'aac.acc_id')
                ->where('ag.ag_group_status','1')
                ->where('mu.id',Auth::id())
                ->where('maa.ac_action','index')
                ->where('aac.acc_isshow','1')
                ->orderBy('aac.acc_by_order','ASC')
                ->orderBy('aac.acc_access_name','ASC')
                ->get();
        
        return $menu;

	}
}

if (!function_exists('dnmcMenu')) {
    function dnmcMenu($m,$top=true,$sub=false)
    {
        $uris = Request::segment(1);
        echo ($top)?"<ul class='nav'>":"<ul class='sub-menu'>";
        echo ($top)?'<li class="nav-header">Navigation</li>':'';
            $url1 = Request::segment(1);
            $url2 = Request::segment(2);
            $url3 = Request::segment(2)."/".Request::segment(3);
            
            if(!empty($m)){
                foreach($m as $k=>$v){
                    
                    $active = ($k=='Admin & Setting ')?'':'';
                    if(is_array($v) && !isset($v['menu']) && !isset($v['id']) && !isset($v['name']) ){
                        $css_class = isset($v[0]['css_class']) && trim($v[0]['css_class'])!=""?$v[0]['css_class']:'';
                        if($sub=='true'){
                            $css_class = '';
                        }else{
                            if($k == "Info Management "){
                                $css_class = isset(current($v)[0]['css_class']) && trim(current($v)[0]['css_class'])!=""?current($v)[0]['css_class']:'';
                            }
                        }

                        $e = ( strtolower(trim($k))==trim($url1))?'active':'';
                        echo "<li class='has-sub ".$e." ".$active." menus' >";
                        echo'
                                <a href="javascript:;">
                                    <b class="caret pull-right"></b>
                                    <i class="'.$css_class.'"></i>
                                    <span>'.$k.'</span>
                                </a>
                            ';
                        dnmcMenu($v,false,true);
                        echo "</li>";
                    }else{

                        if($v['menu'] == "Booking " OR $v['menu'] == "Process Qurban"){

                            $css_class = isset($v['css_class']) && trim($v['css_class'])!=""?$v['css_class']:'';
                            $a=( $v['group']==$url1 && $v['name']==$url2)?'active':'';
                            echo "<li class='".$a." mensfsfus'> ";
                            echo "<a href='".base_url($v['group']."/".$v['name'])."' > <i class='".$css_class."'></i> ".$v['menu']."</a>";
                            echo "</li>";
                        }else{
                            $a=( $v['group']==$url1 && $v['name']==$url2)?'active':'';
                            echo "<li class='".$a." menus'> ";
                            echo "<a href='".url("/{$v['group']}/{$v['name']}")."' >".$v['menu']."</a>";
                            echo "</li>";
                        }
                    }		
                
                }
            }
        echo "</ul>";
    }
}

function _get_menu($menu=array()){
	
	if(count($menu)<0) return array();
	$mnn=[];
	foreach($menu as $mn){
       
		$mnx 	= preg_split("/>/",$mn->acc_menu);
		$count	= count($mnx);
		$t		= "\$mnn";
		for($i=0;$i<$count;$i++){
			if(($count-1)==$i){				
				$t .= "[]=array('menu'=>'".$mnx[$i]."','id'=>'".$mn->acc_id."','class_group'=>'".$mn->acc_group."','group'=>'".$mn->acc_group_controller."','name'=>'".$mn->acc_controller_name."','css_class'=>'".$mn->acc_css_class."');";
			} else {
				$t .= "['".$mnx[$i]."']";
			}
		}
		eval($t);
	}
	return $mnn;
	
}

function link_action($links=array(),$id=""){
	if(count($links)>0){
		foreach($links as $m){
			$property = "";
			if($m['type']=='simple'){
				$property = " class=\"btn btn-primary btn-icon btn-circle\" ";
			}elseif($m['type']=='confirm'){
				$property = " class=\"btn btn-primary btn-icon btn-circle\" ";
			}else{
				$property = " class=\"btn btn-primary btn-icon btn-circle\" ";	
			}	
		?>
		<a href="<?php echo $m['link']."/".$id;?>" <?php echo $property;?> title="<?php echo ucwords($m['title']);?> Data "><i class="fa fa-<?=$m['image']?> "></i><!-- <img src="<?php echo themeUrl();?>images/<?php echo $m['image'];?>" /> --></a>
		<?php  	
		} 
	}
}

function send_sms($phone,$token){
    $data = array(
        "destination" => $phone,
        "country" => "ID",
        "productname" => "WL INFO",
        "codeLength" => "6",
        "codeValidity" => 180,
        "clientMessageId"=> "WL INFO",
        "source" => "WL INFO",
        "text" => "Your verification code is ".$token.". It will remain valid for 3 minutes. Thank you",
        //"encoding" =>"AUTO",
        "createNew" => true,
        "resendingInterval" => 15,
    );
    $data_string = json_encode($data);
    $ch = curl_init('https://api.wavecell.com/sms/v1/IAC_OTP/single');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Authorization: Bearer 1qQ0CED6039EC6144E8929C4630994'
        )
    );
    $response = curl_exec($ch);
    return $response;
}

?>