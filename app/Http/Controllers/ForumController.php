<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Image;

class ForumController extends Controller
{
    public function create(Request $request){
        $validator= Validator::make($request->all(), [
            
            'title'     => 'required',
            'content'     => 'required',
            
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $idCom = $request->input('id_community');
        // dd($idCom);
        $idUser = Auth::id();

        $insert=DB::table('com_forum')
                    ->insert(array(
                        'community_id'=>$idCom,
                        'title' =>$request->input('title'),
                        'content' =>$request->input('content'),
                        'user_create'  => $idUser,
                        'created_at'  => date('Y-m-d H:i:s'),
                    ));
        
        if($insert){
            $message=array(
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'Success inserted the data',
            );
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'Failed inserted the data'
            );
        }
        
        return response()->json($message);
    }

    public function edit(Request $request){
        $validator= Validator::make($request->all(), [
            
            'title'     => 'required',
            'content'     => 'required',
            'id_forum'     => 'required',
            
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '500',
                    'message' => $validator->messages(),
                ], 302);
        }

        // dd($request->input());
        $idForum = $request->input('id_forum');
        // dd($idCom);
        $idUser = Auth::id();
        $check = DB::table('com_forum')->select('*')
                ->where('id',$idForum)
                ->where('user_create',$idUser)
                ->count();
        if ($check > 0) {
            $insert=DB::table('com_forum')
                            ->where('id', $idForum)
                            ->update([
                                    'title' => $request->input('title'),
                                    'content' => $request->input('content'),
                                    'updated_at' => date('Y-m-d H:i:s'),
                            ]);
            
            if($insert){
                $message=array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Success upadated the data',
                );
            }else{
                $message=array(
                    'status'      =>'error',
                    'code'       =>'500',
                    'message'   =>'Failed upadated the data'
                );
            }
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'You dont have permission'
            );
        }
        
        return response()->json($message);
    }

    public function destroy(Request $request){
        $validator= Validator::make($request->all(), [  
            'id_forum'     => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 200);
        }
        $idForum = $request->input("id_forum");
        $idUser = Auth::id();
        $check = DB::table('com_forum')->select('*')
                ->where('id',$idForum)
                ->where('user_create',$idUser)
                ->count();
        if ($check > 0) {
            DB::table('com_forum')->where('id', $idForum)->delete();
            DB::table('com_forum_comment')->where('forum_id', $idForum)->delete();
            return response()->json(
                [   'status'  =>'Success',
                    'code'   => '200',
                    'message' => "Delete data success",
                ], 200);
        }else{
            return response()->json(
                [   'status'  =>'Failed',
                    'code'   => '500',
                    'message' => "You dont have permission",
                ], 200);
        }
        

    }

    public function comment(Request $request){
        $validator= Validator::make($request->all(), [
            
            'comment'     => 'required',
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $id = $request->input('id_forum');
        $forum = DB::table('com_forum')->where('id',$id)->first();

        $idUser = Auth::id();
        $check= DB::table('communites_anggota')->where('community_id',isset($forum->community_id)?$forum->community_id:"")->where('member_id',$idUser)->first();
        // dd($check);
        if (NULL!==$check) {
            $insert=DB::table('com_forum_comment')
                    ->insert(array(
                        'forum_id'=>$id,
                        'comment' =>$request->input('comment'),
                        'user_create'  => $idUser,
                        'created_at'  => date('Y-m-d H:i:s'),
                    ));
        
            if($insert){
                $message=array(
                    'status'    => 'success',
                    'code'      => '200',
                    'message'   => 'Success inserted the data',
                );
            }else{
                $message=array(
                    'status'      =>'error',
                    'code'       =>'200',
                    'message'   =>'Failed inserted the data'
                );
            }
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'200',
                'message'   =>'You dont have permission'
            );
        }
        
        
        return response()->json($message);
    }

    public function get_forum(Request $request){
        // dd($request->input('comunity_id'));
        $data = DB::table('com_forum')->select('*');
        if (null!==$request->input('limit')) {
            $data = $data->limit($request->input('limit'));
        
        }
        
        if (null!==$request->input('id_user') && $request->input('community_id') == null ) {
            $getcom = DB::table('communites_anggota')->where('member_id',$request->input('id_user'))->groupBy('community_id')->get();

            $tmp = [];
            foreach ($getcom as $key) {
               array_push($tmp,$key->community_id);
            }
           
            // $data = $data->whereIn('community_id', $tmp);
            
           
        }else  if (null!==$request->input('id_user') && $request->input('community_id') !== null ) {
            $data = $data->where('community_id', $request->input('community_id'));
        }else if ($request->input('community_id') !== null ) {
            $data = $data->where('community_id', $request->input('community_id'));
        }
       
      
        $data = $data->orderBy('id', 'desc')->get();
        // dd($data);
        $dr = json_decode( json_encode($data), true);
        return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'response' => $data
            ]); 
       
    }


    public function get_forum_detail(Request $request){
        if (null!==$request->input('id_forum')) {
            $id = $request->input('id_forum');
        }else{
            $id="";
        }
       
        $d = DB::table('com_forum')->select('*')
                ->where('id',$id)
                ->first();
        // $d->logo=asset('storage/img/community/'.$d->logo);
        $dr = json_decode( json_encode($d), true);
        if (NULL!==$d) {
            # code...
        
            $comment = DB::table('com_forum_comment')->where('forum_id',$d->id)->orderBy('created_at', 'desc')->get();
            $cekcoment = DB::table('com_forum_comment')->select('*')->where('forum_id',$d->id)->count();

            if($cekcoment > 0){
                
                foreach ($comment as $v) {
                    $user = DB::table('member')->where('id',$v->user_create)->first();
                    // dd($user);
                    $item['nama_lengkap']=$user->nama_lengkap;
                    $item['profile_pic']=default_img($user->profile_pic,'img/user_default.png','member/'.$user->profile_pic);
                    $item['komentar']=$v->comment;
                    $item['created_at']=$v->created_at;
                    $items[] = $item;
                
                }
                $dr['comment'] =$items;
            }else{
                
                $items[]="";
            }
            
            
            return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'response' => $dr
            ]); 
        }else{
            return response()->json([
                'status'  =>'eorr',
                'code'   => '202',
                'response' => 'Forum not found'
            ]); 
        }
        
        
    }
}
