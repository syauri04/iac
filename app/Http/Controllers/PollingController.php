<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Image;

class PollingController extends Controller
{
    public function create(Request $request){
        $validator= Validator::make($request->all(), [
            
            'topik'     => 'required',
            'jawaban_a'     => 'required',
            'jawaban_b'     => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $idCom = $request->input('id_community');
        // dd($idCom);
        $idUser = Auth::id();

        $insert=DB::table('com_polling')
                    ->insert(array(
                        'community_id'=>$idCom,
                        'topik' =>$request->input('topik'),
                        'jawaban_a' =>$request->input('jawaban_a'),
                        'jawaban_b' =>$request->input('jawaban_b'),
                        'jawaban_c' =>(NULL!==$request->input('jawaban_c'))?$request->input('jawaban_c'):'NULL',
                        'jawaban_d' =>(NULL!==$request->input('jawaban_d'))?$request->input('jawaban_d'):'NULL',
                        'jawaban_e' =>(NULL!==$request->input('jawaban_e'))?$request->input('jawaban_e'):'NULL',
                        'type' =>$request->input('type'),
                        'user_create'  => $idUser,
                        'created_at'  => date('Y-m-d H:i:s'),
                    ));
        
        if($insert){
            $message=array(
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'Success inserted the data',
            );
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'Failed inserted the data'
            );
        }
        
        return response()->json($message);
    }

    public function edit(Request $request){
        $validator= Validator::make($request->all(), [
            'polling_id'=>'required',
            'topik'     => 'required',
            'jawaban_a'     => 'required',
            'jawaban_b'     => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        // dd($idCom);
        $idUser = Auth::id();

        $insert=DB::table('com_polling')
                    ->where('id',$request->input('polling_id'))
                    ->update(array(
                        'topik' =>$request->input('topik'),
                        'jawaban_a' =>$request->input('jawaban_a'),
                        'jawaban_b' =>$request->input('jawaban_b'),
                        'jawaban_c' =>(NULL!==$request->input('jawaban_c'))?$request->input('jawaban_c'):'NULL',
                        'jawaban_d' =>(NULL!==$request->input('jawaban_d'))?$request->input('jawaban_d'):'NULL',
                        'jawaban_e' =>(NULL!==$request->input('jawaban_e'))?$request->input('jawaban_e'):'NULL',
                        'type' =>$request->input('type'),
                    ));
        
        if($insert){
            $message=array(
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'Success inserted the data',
            );
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'Failed inserted the data'
            );
        }
        
        return response()->json($message);
    }


    public function delete(Request $request){
        $validator= Validator::make($request->all(), [
            'polling_id'=>'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        // dd($idCom);
        $idUser = Auth::id();

        $insert=DB::table('com_polling')
                    ->where('id',$request->input('polling_id'))
                    ->delete();
        
        if($insert){
            $message=array(
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'Success inserted the data',
            );
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'Failed inserted the data'
            );
        }
        
        return response()->json($message);
    }

    public function get_polling(Request $request){

        // dd($request->input());
        if ($request->input('community_id')!==NULL) {
           

            $data = DB::table('com_polling')->select('*')
            ->where('community_id', $request->input('community_id'));
            // dd($request->input('flag'));
            
            if($request->input('flag') == "Home"){
                $data = $data->first();
                $dr = json_decode( json_encode($data), true);
                $a = get_count_polling($data->id,'a');
                $b = get_count_polling($data->id,'b');
                $c = get_count_polling($data->id,'c');
                $d = get_count_polling($data->id,'d');
                $e = get_count_polling($data->id,'e');

                $dr['countA']=$a;
                $dr['countB']=$b;
                $dr['countC']=$c;
                $dr['countD']=$d;
                $dr['countE']=$e;

                
            }else{
                $data = $data->get();
                $dr = json_decode( json_encode($data), true);
                foreach ($data as $key => $value) {
                    $a = get_count_polling($value->id,'a');
                    $b = get_count_polling($value->id,'b');
                    $c = get_count_polling($value->id,'c');
                    $d = get_count_polling($value->id,'d');
                    $e = get_count_polling($value->id,'e');
        
                    $dr[$key]['countA']=$a;
                    $dr[$key]['countB']=$b;
                    $dr[$key]['countC']=$c;
                    $dr[$key]['countD']=$d;
                    $dr[$key]['countE']=$e;
                }
            }
           
            
            
            $cek =0;
            // dd($dr);
        }else{
            $data = DB::table('com_polling')->select('*')
                ->where('type', $request->input('type'))
                ->orderBy('id', 'desc')->first();

            $dr = json_decode( json_encode($data), true);
            $a = get_count_polling($data->id,'a');
            $b = get_count_polling($data->id,'b');
            $c = get_count_polling($data->id,'c');
            $d = get_count_polling($data->id,'d');
            $e = get_count_polling($data->id,'e');

            $dr['countA']=$a;
            $dr['countB']=$b;
            $dr['countC']=$c;
            $dr['countD']=$d;
            $dr['countE']=$e;

            $cek = DB::table('com_polling_count')
            ->where('polling_id', $data->id)
            ->where('user_id', $request->input('id_user'))
            ->count();
        }
        
       
        

        

        

        if ($cek != 0) {
            return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'response' => ''
            ]); 
        }else{
            return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'response' => $dr
            ]);
        }
         
       
    }

    public function polling_post(Request $request){
        $validator= Validator::make($request->all(), [
            'polling_id'   => 'required',
            'set'     => 'required',
           
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }

        $pollingID = $request->input('polling_id');
        $set = $request->input('set');
        $idUser = Auth::id();


        $push = DB::table('com_polling_count')
        ->updateOrInsert(
            ['user_id' => $idUser, 'polling_id' => $pollingID],
            ['count' => $set]
        );

        if($push){
            $message=array(
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'Success inserted the data',
            );
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'Failed inserted the data'
            );
        }
        // dump($request->file('logo')->getClientOriginalName());
     
        return response()->json($message);
    }


   
}
