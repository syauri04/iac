<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class PollingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['links_table_item'] = set_action(array("edit","delete"),"ITEM");
        // dd($data);
        $d =  DB::table('com_polling')->select('*')->OrderBy('id','desc')->get();
      
        $dr = json_decode( json_encode($d), true);
        foreach($d as $key => $val){
            if(NULL!==$val->community_id){
                $com = DB::table('communities')->where('id',$val->community_id)->first();
                // $dr[$key]['community_name'] = $com->name;
            }
          

            
        }
        $data['data'] = $dr;
        // dd($data['data']);
        // $data['file'] = Storage::disk('public')->get($data['data']['image']);
       
        return view('Master/Polling/index',$data);
    }

    public function add()
    {
        $data = ['title','sdfsf'];
        return view('Master/Polling/form');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $idUser = Auth::id();
        // dd($idUser);
        $validator      =   Validator::make($request->all(),
        ['topik'      =>   'required']);

        // if validation fails
        if($validator->fails()) {
            return back()->withErrors($validator->errors());
        }

        $save=DB::table('com_polling')->insert(
            [
                'topik' =>$request->input('topik'),
                'jawaban_a' =>$request->input('jawaban_a'),
                'jawaban_b' =>$request->input('jawaban_b'),
                'jawaban_c' =>(NULL!==$request->input('jawaban_c'))?$request->input('jawaban_c'):'NULL',
                'jawaban_d' =>(NULL!==$request->input('jawaban_d'))?$request->input('jawaban_d'):'NULL',
                'jawaban_e' =>(NULL!==$request->input('jawaban_e'))?$request->input('jawaban_e'):'NULL',
                'type' =>'INDUK',
                'user_create'  => $idUser,
                'created_at'  => date('Y-m-d H:i:s')
            ]);
        if($save){
            return redirect('polling/polling_post')->with('status', 'Insert for Data Success');
        }else{
            return redirect('polling/polling_post')->with('status', 'Failed ! Insert for Data ');
        }

        // $name_file=Str::random(30).''.date('ymd').'.png';
        // $request->image->storeAs('public/img/banner', $name_file);
        
    }


    public function edit($id)
    {
        $data = DB::table('com_polling')
                ->where('id',$id)
                ->first();
        // dd($data);
        return view('Master/Polling/form')->with('val',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($id);
        $idUser = Auth::id();
        $update = DB::table('com_polling')
                ->where('id', $id)
                ->update([ 
                    'topik' =>$request->input('topik'),
                    'jawaban_a' =>$request->input('jawaban_a'),
                    'jawaban_b' =>$request->input('jawaban_b'),
                    'jawaban_c' =>(NULL!==$request->input('jawaban_c'))?$request->input('jawaban_c'):'NULL',
                    'jawaban_d' =>(NULL!==$request->input('jawaban_d'))?$request->input('jawaban_d'):'NULL',
                    'jawaban_e' =>(NULL!==$request->input('jawaban_e'))?$request->input('jawaban_e'):'NULL',
                    'type' =>'INDUK',
                    'user_create'  => $idUser,
                    'updated_at'=> date('Y-m-d H:i:s')
                ]);
        
        if($update){

            return redirect('polling/polling_post')->with('status', 'Update for Data Image Banner Success');
        }else{
            return redirect('polling/polling_post')->with('status', 'Failed ! Update for Data Image Banner');
        }
        // dd($request->file('image'));
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('com_polling')->where('id', $id)->delete();
        return redirect('polling/polling_post')->with('status', 'Delete for This Data Success');
    }
}
