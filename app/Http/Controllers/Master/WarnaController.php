<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class WarnaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['links_table_item'] = set_action(array("edit","delete"),"ITEM");
        $data['data'] = DB::table('com_warna')
                        ->orderBy('created_at','asc')
                        ->get();
        // dd($data);
        return view('Master/Warna/index',$data);
    }

    public function add()
    {
       
        return view('Master/Warna/form');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {


        $save=DB::table('com_warna')->insert(
            [
                'warna' => $request->input('warna'),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        if($save){
            return redirect('master/warna')->with('status', 'Insert for This Data Success');
        }else{
            return redirect('master/warna')->with('status', 'Failed ! Insert for This Data');
        }
        
    }


    public function edit($id)
    {
        $data = DB::table('com_warna')
                ->where('id',$id)
                ->first();
        // dd($data);
        return view('Master/Warna/form')->with('val',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = DB::table('com_warna')
                ->where('id', $id)
                ->update([ 
                    'warna' => $request->input('warna'),
                    'updated_at'=> date('Y-m-d H:i:s')
                ]);
        
        if($update){
            
            return redirect('master/warna')->with('status', 'Update for This Data Success');
        }else{
            return redirect('master/warna')->with('status', 'Failed ! Update for This Data ');
        }
        // dd($request->file('image'));
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('com_warna')->where('id', $id)->delete();
        return redirect('master/warna')->with('status', 'Delete for This Data Success');
    }
}
