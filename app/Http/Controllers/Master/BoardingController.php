<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class BoardingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['links_table_item'] = set_action(array("edit","delete"),"ITEM");
        $data['data'] = DB::table('com_boarding')
                        ->orderBy('created_at','DESC')
                        ->get();
        // $data['file'] = Storage::disk('public')->get($data['data']['image']);
       
        return view('Master/Boarding/index',$data);
    }

    public function add()
    {
        $data = ['title','sdfsf'];
        return view('Master/Boarding/form');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        // dd($request->input());
        $validator      =   Validator::make($request->all(),
        ['image'      =>   'required']);

        // if validation fails
        if($validator->fails()) {
            return back()->withErrors($validator->errors());
        }

        if($file   =   $request->file('image')) {

            $name      =   time().time().'.'.$file->getClientOriginalExtension();
            
            $target_path    =   storage_path('app/public/img/boarding/');
            
                if($file->move($target_path, $name)) {
                   
                    // save file name in the database
                    $save=DB::table('com_boarding')->insert(
                        [
                            'image' => $name, 
                            'summary'=> $request->input('summary'),
                            'order_by'=> $request->input('order_by'),
                            'created_at' => date('Y-m-d H:i:s')
                        ]);
                    if($save){
                        return redirect('master/on_boarding')->with('status', 'Insert for Data Success');
                    }else{
                        return redirect('master/on_boarding')->with('status', 'Failed ! Insert for Data ');
                    }
                }
        }

        // $name_file=Str::random(30).''.date('ymd').'.png';
        // $request->image->storeAs('public/img/banner', $name_file);
        
    }


    public function edit($id)
    {
        $data = DB::table('com_boarding')
                ->where('id',$id)
                ->first();
        // dd($data);
        return view('Master/Boarding/form')->with('val',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = DB::table('com_boarding')
                ->where('id', $id)
                ->update([ 
                    'summary'=> $request->input('summary'),
                    'order_by'=> $request->input('order_by'),
                    'updated_at'=> date('Y-m-d H:i:s')
                ]);
        
        if($update){
            if($request->hasFile('image')) {
                $file   =   $request->file('image');
                $name      =   time().time().'.'.$file->getClientOriginalExtension();
                
                $target_path    =   storage_path('app/public/img/boarding/');
                
                if($file->move($target_path, $name)) {
                    
                    DB::table('com_boarding')
                    ->where('id', $id)
                    ->update([ 
                        'image' => $name
                    ]);
                    
                }
            }
            return redirect('master/on_boarding')->with('status', 'Update for Data Image Banner Success');
        }else{
            return redirect('master/on_boarding')->with('status', 'Failed ! Update for Data Image Banner');
        }
        // dd($request->file('image'));
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('com_boarding')->where('id', $id)->delete();
        return redirect('master/on_boarding')->with('status', 'Delete for This Data Success');
    }
}
