<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class BannerIklanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['links_table_item'] = set_action(array("edit","delete"),"ITEM");
        $data['data'] = DB::table('banner')
                        ->where('type','Iklan')
                        ->orderBy('created_at','asc')
                        ->get();
        // $data['file'] = Storage::disk('public')->get($data['data']['image']);
       
        return view('Master/BannerIklan/banner',$data);
    }

    public function add()
    {
        $data = ['title','sdfsf'];
        return view('Master/BannerIklan/form');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        // dd($request->input());
        $validator      =   Validator::make($request->all(),
        ['image'      =>   'required']);

        // if validation fails
        if($validator->fails()) {
            return back()->withErrors($validator->errors());
        }

        if($file   =   $request->file('image')) {

            $name      =   time().time().'.'.$file->getClientOriginalExtension();
            
            $target_path    =   storage_path('app/public/img/banner/');
            
                if($file->move($target_path, $name)) {
                   
                    // save file name in the database
                    $save=DB::table('banner')->insert(
                        [
                            'type' => 'Iklan',
                            'title' => $request->input('title'),
                            'image' => $name, 
                            'url_mobile'=> $request->input('url_mobile'),
                            'url_web'=> $request->input('url_web'),
                            'created_by'=> Auth::id()
                        ]);
                    if($save){
                        return redirect('master/banner_iklan')->with('status', 'Insert for Data Image Banner Success');
                    }else{
                        return redirect('master/banner_iklan')->with('status', 'Failed ! Insert for Data Image Banner');
                    }
                }
        }

        // $name_file=Str::random(30).''.date('ymd').'.png';
        // $request->image->storeAs('public/img/banner', $name_file);
        
    }


    public function edit($id)
    {
        $data = DB::table('banner')
                ->where('id_banner',$id)
                ->first();
        // dd($data);
        return view('Master/BannerIklan/form')->with('val',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = DB::table('banner')
                ->where('id_banner', $id)
                ->update([ 
                    'type' => 'Iklan',
                    'title' => $request->input('title'),
                    'url_mobile'=> $request->input('url_mobile'),
                    'url_web'=> $request->input('url_web'),
                    'updated_by'=> Auth::id(),
                    'updated_at'=> date('Y-m-d H:i:s')
                ]);
        
        if($update){
            if($request->hasFile('image')) {
                $file   =   $request->file('image');
                $name      =   time().time().'.'.$file->getClientOriginalExtension();
                
                $target_path    =   storage_path('app/public/img/banner/');
                
                if($file->move($target_path, $name)) {
                    
                    DB::table('banner')
                    ->where('id_banner', $id)
                    ->update([ 
                        'image' => $name
                    ]);
                    
                }
            }
            return redirect('master/banner_iklan')->with('status', 'Update for Data Image Banner Success');
        }else{
            return redirect('master/banner_iklan')->with('status', 'Failed ! Update for Data Image Banner');
        }
        // dd($request->file('image'));
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('banner')->where('id_banner', $id)->delete();
        return redirect('master/banner_iklan')->with('status', 'Delete for This Data Success');
    }
}
