<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['links_table_item'] = set_action(array("edit","delete"),"ITEM");
        $data['data'] = DB::table('com_type')
                        ->orderBy('created_at','asc')
                        ->get();
        // dd($data);
        return view('Master/Type/index',$data);
    }

    public function add()
    {
       
        return view('Master/Type/form');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {


        $save=DB::table('com_type')->insert(
            [
                'type' => $request->input('type'),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        if($save){
            return redirect('master/type')->with('status', 'Insert for This Data Success');
        }else{
            return redirect('master/type')->with('status', 'Failed ! Insert for This Data');
        }
        
    }


    public function edit($id)
    {
        $data = DB::table('com_type')
                ->where('id',$id)
                ->first();
        // dd($data);
        return view('Master/Type/form')->with('val',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = DB::table('com_type')
                ->where('id', $id)
                ->update([ 
                    'type' => $request->input('type'),
                    'updated_at'=> date('Y-m-d H:i:s')
                ]);
        
        if($update){
            
            return redirect('master/type')->with('status', 'Update for This Data Success');
        }else{
            return redirect('master/type')->with('status', 'Failed ! Update for This Data ');
        }
        // dd($request->file('image'));
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('com_type')->where('id', $id)->delete();
        return redirect('master/type')->with('status', 'Delete for This Data Success');
    }
}
