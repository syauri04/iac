<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class MerekController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['links_table_item'] = set_action(array("edit","delete"),"ITEM");
        $data['data'] = DB::table('com_merk')
                        ->orderBy('created_at','asc')
                        ->get();
        // dd($data);
        return view('Master/Merek/merek',$data);
    }

    public function add()
    {
       
        return view('Master/Merek/form');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {


        $save=DB::table('com_merk')->insert(
            [
                'merk' => $request->input('merk'),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        if($save){
            return redirect('master/merek')->with('status', 'Insert for Data Merk Success');
        }else{
            return redirect('master/merek')->with('status', 'Failed ! Insert for Data Merk');
        }
        
    }


    public function edit($id)
    {
        $data = DB::table('com_merk')
                ->where('id',$id)
                ->first();
        // dd($data);
        return view('Master/Merek/form')->with('val',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = DB::table('com_merk')
                ->where('id', $id)
                ->update([ 
                    'merk' => $request->input('merk'),
                    'updated_at'=> date('Y-m-d H:i:s')
                ]);
        
        if($update){
            
            return redirect('master/merek')->with('status', 'Update for This Data Success');
        }else{
            return redirect('master/merek')->with('status', 'Failed ! Update for This Data ');
        }
        // dd($request->file('image'));
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('com_merk')->where('id', $id)->delete();
        return redirect('master/merek')->with('status', 'Delete for This Data Success');
    }
}
