<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Image;

class CommunityController extends Controller
{
    public function accept_join(Request $request){
        $validator= Validator::make($request->all(), [
            'community_id'   => 'required',
            'member_id'     => 'required',
            'status'     => 'required'
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }

        $idUser = Auth::id();
        $cek = DB::table('communites_anggota')
                ->where('community_id',$request->input('community_id'))
                ->where('member_id',$request->input('member_id'))
                ->where('status','Pending')
                ->first();
        if(NULL!== $cek){
            $update= DB::table('communites_anggota')
                    ->where('id', $cek->id)
                    ->update(['status' => $request->input('status'), 'user_approve' => $idUser, 'updated_at' => date('Y-m-d H:i:s')]);
            if($update){
                $message=array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Success inserted the data',
                );
            }else{
                $message=array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Failed inserted the data',
                );
            }
                
        }else{
            $message=array(
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'Failed inserted the data',
            );
        }
        
        return response()->json($message);
    }
    
    public function create(Request $request){
        $validator= Validator::make($request->all(), [
            'logo'   => 'required',
            'name'     => 'required',
            'city_id'     => 'required',
            'alamat_detail'     => 'required',
            'category'     => 'required',
            'description'     => 'required',
            'bank_name'     => 'required',
            'noreq'     => 'required',
            'atas_nama'     => 'required',
            'ketua'     => 'required',
            'wakil'     => 'required'
            
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 200);
        }

        $cekcom = DB::table('communities')->where('name',$request->input('name'))->count();
        if($cekcom > 0){
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '500',
                    'message' => 'Community Name has already registered',
                ], 200);
        }else{
            $insert=DB::table('communities')
                ->insertGetId(array(
                    'name'=>$request->input('name'),
                    'alamat_detail'=>$request->input('alamat_detail'),
                    'city_id'=>$request->input('city_id'),
                    'bank_name'=>$request->input('bank_name'),
                    'noreq'=>$request->input('noreq'),
                    'atas_nama'=>$request->input('atas_nama'),
                    'category' =>$request->input('category'),
                    'ketua' =>Auth::id(),
                    'wakil' =>$request->input('wakil'),
                    'activate'  => "active",
                    'user_create'  => Auth::id(),
                    'created_at'  => date('Y-m-d H:i:s'),

                ));
        
            $idCOM = $insert;
        }
        // dump(Auth::id());
        
        // dd($request->input('id'));
        $insertDetail = DB::table('communities_detail')
        ->insert(array(
            'communities_id'=>$idCOM,
            'description'=>$request->input('description')
        ));
           
        $insertAnggota = DB::table('communites_anggota')
        ->insert(array(
            'community_id'=> $idCOM,
            'member_id'=> $request->input('ketua'),
            'level'=> 'Ketua',
            'status'=> "Verified",
            'created_at'=> date('Y-m-d H:i:s')

        ));

        $members=DB::table('member')
        ->select('*')
        ->where('id',$request->input('ketua'))
        ->first();
        $param=array(
            
            'id_member'=>$request->input('ketua')
        );
        $data_push_notif = array(
            'activity'=>'Akun.HomeAkun',
            'body'=>'Berhasil Buat Komunitas '.$request->input('name'),
            'data'=> $param,
            'icon'=>url('assets/uploads/settings/Untitled-1.png'),
            'title'=>'Berhasil Buat Komunitas',
        );
        $insertnotif = DB::table('notification')
                        ->insert(array(
                            'id_member'=>$request->input('ketua'),
                            'type'=>1,
                            'msg'=>'Berhasil membuat komunitas '.$request->input('name'),
                            'activity'=>'Akun.HomeAkun',
                            'param'  => json_encode($param),
                            'created_at'  => date('Y-m-d H:i:s'),
                            'status'  =>'false',
    
                        ));
    
        $x=ngepush_notif($members->firebase,$data_push_notif);

        $insertAnggota = DB::table('communites_anggota')
        ->insert(array(
            'community_id'=> $idCOM,
            'member_id'=> $request->input('wakil'),
            'level'=> 'Wakil',
            'status'=> "Verified",
            'created_at'=> date('Y-m-d H:i:s')

        ));

        $members=DB::table('member')
        ->select('*')
        ->where('id',$request->input('wakil'))
        ->first();
        $param=array(
            
            'id_member'=>$request->input('wakil')
        );
        $data_push_notif = array(
            'activity'=>'Akun.HomeAkun',
            'body'=>'Berhasil Buat Komunitas '.$request->input('name'),
            'data'=> $param,
            'icon'=>url('assets/uploads/settings/Untitled-1.png'),
            'title'=>'Berhasil Buat Komunitas',
        );
        $insertnotif = DB::table('notification')
                        ->insert(array(
                            'id_member'=>$request->input('wakil'),
                            'type'=>1,
                            'msg'=>'Berhasil Membuat Komunitas '.$request->input('name'),
                            'activity'=>'Akun.HomeAkun',
                            'param'  => json_encode($param),
                            'created_at'  => date('Y-m-d H:i:s'),
                            'status'  =>'false',
    
                        ));
    
        $x=ngepush_notif($members->firebase,$data_push_notif);

        for($i=0; $i <= 4; $i++){
            $strm = 'member'.$i;
                $idmem = $request->input($strm);
                $level = "Pengurus";
            // dd($idmem);
            if(NULL!==$idmem){
                $insertAnggota = DB::table('communites_anggota')
                ->insert(array(
                    'community_id'=> $idCOM,
                    'member_id'=> $idmem,
                    'level'=> $level,
                    'status'=> "Verified",
                    'created_at'=> date('Y-m-d H:i:s')

                ));

                $members=DB::table('member')
                ->select('*')
                ->where('id',$idmem)
                ->first();
                $param=array(
                    
                    'id_member'=>$idmem
                );
                $data_push_notif = array(
                    'activity'=>'Akun.HomeAkun',
                    'body'=>'Berhasil Buat Komunitas '.$request->input('name'),
                    'data'=> $param,
                    'icon'=>url('assets/uploads/settings/Untitled-1.png'),
                    'title'=>'Berhasil Buat Komunitas',
                );
                $insertnotif = DB::table('notification')
                                ->insert(array(
                                    'id_member'=>$idmem,
                                    'type'=>1,
                                    'msg'=>'Berhasil Membuat Komunitas '.$request->input('name'),
                                    'activity'=>'Akun.HomeAkun',
                                    'param'  => json_encode($param),
                                    'created_at'  => date('Y-m-d H:i:s'),
                                    'status'  =>'false',
            
                                ));
            
                $x=ngepush_notif($members->firebase,$data_push_notif);
                error_log($x);
            }
            
        }
        
        // dump($request->file('logo')->getClientOriginalName());
        if(null!==$request->file('logo')){

            $file   =   $request->file('logo');
            $imageName = md5($idCOM.''.date('Y-m-d H:i:s')).'.'.$file->getClientOriginalName();
            $ImageUpload = Image::make($file);
            $originalPath = storage_path('app/public/img/community/'.$imageName);
            $ImageUpload->save($originalPath);
             
            // for save thumnail image
            $thumbnailPath = storage_path('app/public/img/community/thumb/'.$imageName);
            $ImageUpload->resize(300,null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $ImageUpload = $ImageUpload->save($thumbnailPath);
         

            $update=DB::table('communities')
                    ->where('id', $idCOM)
                    ->update(['logo' => $imageName]);
        }
        if($insert){
            $message=array(
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'Success inserted the data',
            );
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'Failed inserted the data'
            );
        }
        return response()->json($message);
    }

    public function edit(Request $request){
        $validator= Validator::make($request->all(), [
            'name'     => 'required',
            'city_id'     => 'required',
            'alamat_detail'     => 'required',
            'category'     => 'required',
            'description'     => 'required',
            'ketua'     => 'required',
            'wakil'     => 'required',
            'community_id'     => 'required'
            
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 200);
        }
        $communityx=DB::table('communities')
        ->where('id',$request->input('community_id'))
        ->first();
        $idCOM = $request->input('community_id');
        $cekcom = DB::table('communities')->where('name',$request->input('name'))->count();
        if($cekcom > 0 and $request->input('name')!=$communityx->name){
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '500',
                    'message' => 'Community Name has already registered',
                ], 200);
        }else{
            $insert=DB::table('communities')
                ->where('id',$request->input('community_id'))
                ->update(array(
                    'name'=>$request->input('name'),
                    'alamat_detail'=>$request->input('alamat_detail'),
                    'city_id'=>$request->input('city_id'),
                    'bank_name'=>$request->input('bank_name'),
                    'noreq'=>$request->input('noreq'),
                    'atas_nama'=>$request->input('atas_nama'),
                    'category' =>$request->input('category'),
                    'ketua' =>$request->input('ketua'),
                    'wakil' =>$request->input('wakil'),
                    'activate'  => "active",
                    'user_create'  => $request->input('ketua'),
                    'updated_at'  => date('Y-m-d H:i:s'),

                ));
        
            
        }
        // dump(Auth::id());
        
        // dd($request->input('id'));

        $insertDetail = DB::table('communities_detail')
                        ->where('id',$request->input('community_id'))
                        ->update(array(
                            'communities_id'=>$idCOM,
                            'description'=>$request->input('description')
                        ));
        DB::table('communites_anggota')
        ->where('community_id',$idCOM)
        ->delete();
        $insertAnggota = DB::table('communites_anggota')
        ->insert(array(
            'community_id'=> $idCOM,
            'member_id'=> $request->input('ketua'),
            'level'=> 'Ketua',
            'status'=> "Verified",
            'created_at'=> date('Y-m-d H:i:s')

        ));
        $insertAnggota = DB::table('communites_anggota')
                ->insert(array(
                    'community_id'=> $idCOM,
                    'member_id'=> $request->input('wakil'),
                    'level'=> 'Wakil',
                    'status'=> "Verified",
                    'created_at'=> date('Y-m-d H:i:s')

                ));
        for($i=0; $i <= 8; $i++){
            $strm = 'member'.$i;
                $idmem = $request->input($strm);
                $level = "Pengurus";
            // dd($idmem);
            if(NULL!==$idmem){
                $insertAnggota = DB::table('communites_anggota')
                ->insert(array(
                    'community_id'=> $idCOM,
                    'member_id'=> $idmem,
                    'level'=> $level,
                    'status'=> "Verified",
                    'created_at'=> date('Y-m-d H:i:s')

                ));

                $members=DB::table('member')
                ->select('*')
                ->where('id',$idmem)
                ->first();
                $param=array(
                    
                    'id_member'=>$idmem
                );
                $data_push_notif = array(
                    'activity'=>'Akun.HomeAkun',
                    'body'=>'Berhasil Buat Komunitas '.$request->input('name'),
                    'data'=> $param,
                    'icon'=>url('assets/uploads/settings/Untitled-1.png'),
                    'title'=>'Berhasil Buat Komunitas',
                );
                $insertnotif = DB::table('notification')
                                ->insert(array(
                                    'id_member'=>$idmem,
                                    'type'=>1,
                                    'msg'=>'Berhasil Membuat Komunitas '.$request->input('name'),
                                    'activity'=>'Akun.HomeAkun',
                                    'param'  => json_encode($param),
                                    'created_at'  => date('Y-m-d H:i:s'),
                                    'status'  =>'false',
            
                                ));
            
                //$x=ngepush_notif($members->firebase,$data_push_notif);
                // error_log($x);
            }
            
        }
        
        // dump($request->file('logo')->getClientOriginalName());
        if(null!==$request->file('logo')){

            $file   =   $request->file('logo');
            $imageName = md5($idCOM.''.date('Y-m-d H:i:s')).'.'.$file->getClientOriginalName();
            $ImageUpload = Image::make($file);
            $originalPath = storage_path('app/public/img/community/'.$imageName);
            $ImageUpload->save($originalPath);
             
            // for save thumnail image
            $thumbnailPath = storage_path('app/public/img/community/thumb/'.$imageName);
            $ImageUpload->resize(300,null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $ImageUpload = $ImageUpload->save($thumbnailPath);
         

            $update=DB::table('communities')
                    ->where('id', $idCOM)
                    ->update(['logo' => $imageName]);
        }
        if($insert){
            $message=array(
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'Success inserted the data',
            );
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'Failed inserted the data'
            );
        }
        return response()->json($message);
    }

    public function galer_likeordis(Request $request){
       
        $validator= Validator::make($request->all(), [
            'gallery_id'   => 'required',
            'set'     => 'required',
           
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }

        $idGaler = $request->input('gallery_id');
        $set = $request->input('set');
        $idUser = Auth::id();
       
        // $valid = DB::table('community_gallery_like AS com')->select('com.*')
        // ->where('user_id',$idUser)
        // ->where('gallery_id',$idGaler)
        // ->first();

        $check = DB::table('community_gallery AS com')->select('com.*')
        ->where('id',$idGaler)
        ->first();

        $valid = DB::table('communites_anggota')->select('*')
        ->where('community_id',$check->communities_id)
        ->where('member_id',$idUser)
        ->count();
        // dd($valid);
        
        if ($valid > 0) {
            
        
            if($set == 'like'){
                $push = DB::table('community_gallery_like')
                        ->updateOrInsert(
                            ['user_id' => $idUser, 'gallery_id' => $idGaler],
                            ['likeordis' => '1']
                        );
            }else{
                $push = DB::table('community_gallery_like')
                ->updateOrInsert(
                    ['user_id' => $idUser, 'gallery_id' => $idGaler],
                    ['likeordis' => '2']
                );
            }

            if($push){
                $message=array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Success inserted the data',
                );
            }else{
                $message=array(
                    'status'      =>'error',
                    'code'       =>'500',
                    'message'   =>'Failed inserted the data'
                );
            }
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'Dosent join in community'
            );
        }
        // dump($request->file('logo')->getClientOriginalName());
     
        return response()->json($message);
    }

    public function upload_gallery(Request $request){

        // dd($request->input());
        $validator= Validator::make($request->all(), [
            'image'   => 'required',
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $id= $request->input('id_community');
        $cek = DB::table('communities')->select('*')->where('id',$id)->count();

        if($cek > 0){
            // $comm = DB::table('users')->where('name', 'John')->first();
            $file   =   $request->file('image');
            $imageName = md5($id.''.date('Y-m-d H:i:s')).'.'.$file->getClientOriginalExtension();
            $ImageUpload = Image::make($file);
            
            $path = storage_path().'/app/public/img/community/' . $id;
            $dir =  makeDirectory($path, $mode = 0777, true, true);

            $originalPath = storage_path('app/public/img/community/'.$id.'/'.$imageName);
            $ImageUpload->save($originalPath);


            $insert= DB::table('community_gallery')
                        ->insert(array(
                            'communities_id'=> $id,
                            'image'=> $imageName,
                            'title'=> $request->input('title'),
                            'user_create'=> Auth::id(),
                            'created_at'=> date('Y-m-d H:i:s')

                        ));
            if($insert){
                $message=array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Success inserted the data',
                );
            }else{
                $message=array(
                    'status'      =>'error',
                    'code'       =>'500',
                    'message'   =>'Failed inserted the data'
                );
            }
        }else{
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => 'ID community not found',
                ], 302);
        }
        return response()->json($message);
    }

  

    public function join_community(Request $request){
        $validator= Validator::make($request->all(), [
            'photo_ktp'   => 'required',
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $id = $request->input('id');
        if(null!==$request->file('photo_ktp')){
            // $comm = DB::table('users')->where('name', 'John')->first();
            // dd($id);
            $userid = Auth::id();
            
            $file   =   $request->file('photo_ktp');
            $imageName = md5($userid.''.date('Y-m-d H:i:s')).'.'.$file->getClientOriginalExtension();
            // error_log($imageName);
            $ImageUpload = Image::make($file);
            $ImageUpload->save(storage_path('app/public/img/community/ktp/'.$imageName));
            //$originalPath = 'storage/img/community/ktp/'.$imageName;
            //$ImageUpload->save($originalPath);
            //return response()->json(storage_path('img/community/ktp/'.$imageName));
            $communities=DB::table('communities')->select('name')->where('id',$id)->first();
            $member=DB::table('member')->select('nama_lengkap')->where('id',$userid)->first();
            $insert= DB::table('communites_anggota')
                        ->insert(array(
                            'community_id'=> $id,
                            'member_id'=> $userid,
                            'photo_ktp'=> $imageName,
                            'level'=> 'Member',
                            'status'=> 'Verified',
                            'created_at'=> date('Y-m-d H:i:s')

                        ));
            if($insert){

                $cekpengurus = DB::table('communites_anggota')->where('community_id',$id)->whereIn('level', ['Ketua', 'Pengurus'])->get();
                foreach ($cekpengurus as $key => $val) {
                    $members=DB::table('member')
                    ->select('*')
                    ->where('id',$val->member_id)
                    ->first();
                    $param=array(
                        
                        'id_member'=>$val->member_id
                    );
                    $data_push_notif = array(
                        'activity'=>'Akun.HomeAkun',
                        'body'=>'Member baru telah bergabung dengan komunitas '.$communities->name.', '.$member->nama_lengkap.' sebagai Member' ,
                        'data'=> $param,
                        'icon'=>url('assets/uploads/settings/Untitled-1.png'),
                        'title'=>'Member Baru Telah Gabung',
                    );
                    $insertnotif = DB::table('notification')
                                    ->insert(array(
                                        'id_member'=>$val->member_id,
                                        'type'=>1,
                                        'msg'=>'Member Baru Telah bergabung dengan komunitas '.$communities->name.', '.$member->nama_lengkap.' sebagai Member' ,
                                        'activity'=>'Akun.HomeAkun',
                                        'param'  => json_encode($param),
                                        'created_at'  => date('Y-m-d H:i:s'),
                                        'status'  =>'false',
                
                                    ));
                
                    $x=ngepush_notif($members->firebase,$data_push_notif);
                    error_log($x);
                }
                

                $message=array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Success Request',
                );
            }else{
                $message=array(
                    'status'      =>'error',
                    'code'       =>'500',
                    'message'   =>'Failed Request'
                );
            }
        }else{
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => 'KTP not found',
                ], 302);
        }
        return response()->json($message);
        
    }

    public function get_community(Request $request){
        
        
        $data = DB::table('communities AS com')->select('com.*');
        if (null!==$request->input('category')) {
            $data = $data->where('category',$request->input('category'));
        }

        if (null!==$request->input('limit')) {
            $data = $data->limit($request->input('limit'));
        
        }
        $data = $data->orderBy('id', 'desc')->where('activate','!=', 'banned' )->get();
        // ($cat != 'all')?$sss = $ss->where('category',$cat):$sss = $ss;

        // $data = $sss->limit($limit)->orderBy('com.id', 'desc')->get();
        $dr = json_decode( json_encode($data), true);
        foreach($data as $key => $val){
            // dd($key);
            $dr[$key]['logo']=asset('storage/img/community/'.$val->logo);
            $datash = DB::table('communities_detail')->where('communities_id',$val->id)->first();
            $dr[$key]['detail'] =$datash;
        }
        return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'response' => $dr
            ]); 
       
    }

    public function detail_community(Request $request){
      
        $id = $request->input('id');
        $d = DB::table('communities AS com')->select('com.*')
                ->where('id',$id);
        if($d->count()>0){
            $d=$d->first();
        $d->logo=asset('storage/img/community/'.$d->logo);
        $dr = json_decode( json_encode($d), true);
        if(NULL!==$request->input('id_member')){
            $statjoin = DB::table('communites_anggota')->where('community_id',$d->id)->where('member_id',$request->input('id_member'))->count();
            if($statjoin > 0){
                $dr['join'] = true;
            }else{
                $dr['join'] = false;
            }
        }else{
            $dr['join'] = false;
        }
        $datash = DB::table('communities_detail')->where('communities_id',$d->id)->first();
        $dr['detail'] =$datash;
        $pengurus=DB::table('communites_anggota')
                    ->select('member_id','level')
                    ->where('level','!=','Member')
                    ->where('community_id',$d->id)
                    ->where('level','!=','Pengurus')
                    ->get();
        $com_anggota=[];
        foreach ($pengurus as $key) {
            $members=DB::table('member')
                    ->select('nama_lengkap')
                    ->where('id',$key->member_id)
                    ->first();
            $key->name=$members->nama_lengkap;
        }
        $com_anggota['ketua_wakil']=$pengurus;
        $penguruss=DB::table('communites_anggota')
                    ->select('communites_anggota.*','member.nama_lengkap')
                    ->join('member','member.id','=','communites_anggota.member_id')
                    ->where('communites_anggota.community_id',$d->id)
                    ->where('communites_anggota.level','Pengurus')
                    ->get();
        $no=0;
        $p=[];
        foreach ($penguruss as $key) {
            $no++;
           
            $array= array(
                'pengurus_id' => $key->member_id,
                'pengurus_level' => 'Pengurus', 
                'pengurus_name' => $key->nama_lengkap, 
            );
            array_push($p,$array);
        }
        for ($i=$no; $i <4 ; $i++) { 
            $array= array(
                'pengurus_id' => "",
                'pengurus_level' => "", 
                'pengurus_name' => "", 
            );
            array_push($p,$array);
        }
        $com_anggota['pengurus']=$p;
        $dr['pengurus']=$com_anggota;
        $city=DB::table('tb_city')->where('city_id',$d->city_id)->first();
        $dr['city_name']=$city->type.' '.$city->city_name;
        $dr['province_id']=$city->province_id;
        $dr['province_name']=$city->province;
        $gallery = DB::table('community_gallery')->select('image')->where('communities_id',$d->id)->get();
        $cekgal = DB::table('community_gallery')->select('image')->where('communities_id',$d->id)->count();

        if($cekgal > 0){
            
            foreach($gallery as $key){
                
                $item[]=array('image' => asset('storage/img/community/'.$d->id.'/'.$key->image));
            }
        }else{
            
            $item=[
                array('image'=>'https://increasify.com.au/wp-content/uploads/2016/08/default-image.png')
            ];
        }
        
        
        $dr['gallery'] =$item;
       
        $getMember = DB::table('communites_anggota')->where('community_id',$d->id)->where('level','Member')->where('status','Verified')->count();
        $dr['CountMember'] = $getMember;
        $getPengurus = DB::table('communites_anggota')->where('community_id',$d->id)->where('level','Pengurus')->count();
        $dr['CountPengurus'] = $getPengurus;
        $getMerchant = DB::table('com_merchandise')->where('community_id',$d->id)->count();
        $dr['CountMerchendaise'] = $getMerchant;
        return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'response' => $dr
            ]); 
        }else{
            return response()->json([
                'status'  =>'error',
                'code'   => '404',
                'response' => 'Ccommunity not found'
            ]); 
        }
                
        
    }

    public function delete_gallery(Request $request){
        $validator= Validator::make($request->all(), [
            'id_gallery'   => 'required',
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $id_gallery=$request->get('id_gallery');
        $gallery=DB::table('community_gallery')
            ->where('id',$id_gallery);
        if($gallery->count()>0){
            $gallery->delete();
            DB::table('community_gallery_like')
            ->where('gallery_id',$id_gallery)
            ->delete();
            return response()->json([   
                'status'  =>'success',
                'code'   => '200',
                'message'=>'successfully deleted this image gallery'
            ]);
        }else{
            return response()->json([   
                'status'  =>'error',
                'code'   => '404',
                'message'=>'id_gallery not found'
            ]);
        }
         
    }
    public function get_gallery(Request $request){
      
        $id = $request->input('id_community');
        $d = DB::table('community_gallery')->select('*');

        // dd($request->input('id_user'));
        if (null!==$request->input('id_user') && $request->input('id_community') == null ) {
            $getcom = DB::table('communites_anggota')->where('member_id',$request->input('id_user'))->groupBy('community_id')->get();
            $tmp = [];
            
            foreach ($getcom as $key) {
               array_push($tmp,$key->community_id);
            }
            //dd($tmp);
            // $d = $d->whereIn('communities_id', $tmp);
            
           
        }else  if (null!==$request->input('id_user') && $request->input('id_community') !== null ) {
            $d = $d->where('communities_id', $request->input('id_community'));
        }else if ($request->input('id_community') !== null ) {
            $d = $d->where('communities_id', $request->input('id_community'));
        }
       
      
        $d = $d->orderBy('id','desc')->get();
        // $d = DB::table('community_gallery AS com')->select('com.*')
        //         ->where('communities_id',$id)
        //         ->orderBy('id','desc')
        //         ->get();
        // dd($d);
        $dr = json_decode( json_encode($d), true);
        if(NULL!== $d){
            
            foreach ($d as $key => $val) {
                $like = get_count( array(
                    'table' => 'community_gallery_like',
                    'field1' => 'gallery_id',
                    'where1' => $val->id,
                    'field2' => 'likeordis',
                    'where2' => '1',
                ));

                $dislike = get_count( array(
                    'table' => 'community_gallery_like',
                    'field1' => 'gallery_id',
                    'where1' => $val->id,
                    'field2' => 'likeordis',
                    'where2' => '2',
                ));

                $dr[$key]['like']=$like;
                $dr[$key]['dislike']=$dislike;
                $dr[$key]['image']=asset('storage/img/community/'.$val->communities_id.'/'.$val->image);
            }
           
            return response()->json([   
                    'status'  =>'success',
                    'code'   => '200',
                    'response' => $dr
                ]); 
        }else{
            return response()->json([   
                'status'  =>'success',
                'code'   => '200',
               
            ]); 
        }
        
        
    }

    public function search_community(Request $request){

        // dd($request->input());
        $validator= Validator::make($request->all(), [
            'search'   => 'required',
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $search= $request->input('search');

        $cek = DB::table('communities')
                ->where('name', 'like', '%'.$search.'%')
                ->count();
        if($cek > 0){
            $data = DB::table('communities')->select('*')
                        ->where('name', 'like', '%'.$search.'%')
                        ->get();
           
            $dr = json_decode( json_encode($data), true);
            foreach($data as $key => $val){
                // dd($key);
                $dr[$key]['logo']=asset('storage/img/community/'.$val->logo);
                $datash = DB::table('communities_detail')->where('communities_id',$val->id)->first();
                $dr[$key]['detail'] =$datash;
            }
            return response()->json([
                    'status'  =>'success',
                    'code'   => '200',
                    'response' => $dr
                ]); 
        }else{
            return response()->json([
                'status'  =>'eror',
                'code'   => '200',
                'response' => 'Data Not Found'
            ]); 
        }

       
    }
}
