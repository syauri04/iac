<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Image;

class EventController extends Controller
{
    public function event_tertarik(Request $request){
        $validator= Validator::make($request->all(), [
            'event_id'   => 'required',
            'set'     => 'required',
           
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }

        $idEvent = $request->input('event_id');
        $set = $request->input('set');
        $idUser = Auth::id();
       
        $check = DB::table('com_event')->select('*')
        ->where('id',$idEvent)
        ->first();
        // dd($check);
        $valid = DB::table('communites_anggota')->select('*')
        ->where('community_id',isset($check->community_id)?$check->community_id:"")
        ->where('member_id',$idUser)
        ->count();
        if ($valid > 0) {
            $cek = DB::table('com_event_tertarik')->where('user_id',$idUser)->where('event_id',$idEvent)->first();
            if($set == 'tertarik'){
                if (NULL!==$cek) {
                    if($cek->tertarikorhadir == 1){
                        $va = 0;
                    }else{
                        $va = 1;
                    }
                }else{
                    $va = 1;
                }
                $push = DB::table('com_event_tertarik')
                        ->updateOrInsert(
                            ['user_id' => $idUser, 'event_id' => $idEvent],
                            ['tertarikorhadir' => $va]
                        );
            }else{
                if (NULL!==$cek) {
                    if($cek->tertarikorhadir == 2){
                        $va = 0;
                    }else{
                        $va = 2;
                    }
                }else{
                    $va = 2;
                }
                
                $push = DB::table('com_event_tertarik')
                ->updateOrInsert(
                    ['user_id' => $idUser, 'event_id' => $idEvent],
                    ['tertarikorhadir' => $va]
                );
            }

            if($push){
                $message=array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Success inserted the data',
                );
            }else{
                $message=array(
                    'status'      =>'error',
                    'code'       =>'500',
                    'message'   =>'Failed inserted the data'
                );
            }
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'Dosent join in community'
            );
        }
        // dump($request->file('logo')->getClientOriginalName());
     
        return response()->json($message);
    }
    public function event_likeordis(Request $request){
        $validator= Validator::make($request->all(), [
            'event_id'   => 'required',
            'set'     => 'required',
           
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }

        $idEvent = $request->input('event_id');
        $set = $request->input('set');
        $idUser = Auth::id();
       
        $check = DB::table('com_event')->select('*')
        ->where('id',$idEvent)
        ->first();
        
        $valid = DB::table('communites_anggota')->select('*')
        ->where('community_id',$check->community_id)
        ->where('member_id',$idUser)
        ->count();
        if ($valid > 0) {
            if($set == 'like'){
                $push = DB::table('com_event_like')
                        ->updateOrInsert(
                            ['user_id' => $idUser, 'event_id' => $idEvent],
                            ['likeordis' => '1']
                        );
            }else{
                $push = DB::table('com_event_like')
                ->updateOrInsert(
                    ['user_id' => $idUser, 'event_id' => $idEvent],
                    ['likeordis' => '2']
                );
            }

            if($push){
                $message=array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Success inserted the data',
                );
            }else{
                $message=array(
                    'status'      =>'error',
                    'code'       =>'500',
                    'message'   =>'Failed inserted the data'
                );
            }
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'Dosent join in community'
            );
        }
        // dump($request->file('logo')->getClientOriginalName());
     
        return response()->json($message);
    }

    public function delete(Request $request){
        $validator= Validator::make($request->all(), [
            'id_event'  =>'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
            $insert=DB::table('com_event')
                ->where('id',$request->input('id_event'))
                ->delete();
            if($insert){
                $message=array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Success inserted the data',
                );
            }else{
                $message=array(
                    'status'      =>'error',
                    'code'       =>'500',
                    'message'   =>'Failed inserted the data'
                );
            }
        return response()->json($message);
    }

    public function edit(Request $request){
        $validator= Validator::make($request->all(), [
            'id_event'  =>'required',
            'title'     => 'required',
            'start_date'     => 'required',
            'end_date'     => 'required',
            'start_time'     => 'required',
            'end_time'     => 'required',
            'location'     => 'required',
            'description'     => 'required'
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
            $insert=DB::table('com_event')
                ->where('id',$request->input('id_event'))
                ->update(array(
                    'title'=>$request->input('title'),
                    'start_date'=>$request->input('start_date'),
                    'end_date' =>$request->input('end_date'),
                    'start_time' =>$request->input('start_time'),
                    'end_time' =>$request->input('end_time'),
                    'location' =>$request->input('location'),
                    'description' =>$request->input('description'),
                ));
            if(null!==$request->file('image')){

                $file   =   $request->file('image');
                $imageName = md5($request->input('id_event').''.date('Y-m-d H:i:s')).'.'.$file->getClientOriginalExtension();
                $ImageUpload = Image::make($file);
                $originalPath = storage_path('app/public/img/event/'.$imageName);
                $yes = $ImageUpload->save($originalPath);
                $insert=DB::table('com_event')
                    ->where('id',$request->input('id_event'))
                    ->update(array(
                        'image' =>$imageName,
                    ));
            }
            if($insert){
                $message=array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Success inserted the data',
                );
            }else{
                $message=array(
                    'status'      =>'error',
                    'code'       =>'500',
                    'message'   =>'Failed inserted the data'
                );
            }
        return response()->json($message);
    }

    public function create(Request $request){
        $validator= Validator::make($request->all(), [
            'image'   => 'required',
            'title'     => 'required',
            'start_date'     => 'required',
            'end_date'     => 'required',
            'start_time'     => 'required',
            'end_time'     => 'required',
            'location'     => 'required',
            'description'     => 'required'
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $idCom = $request->input('id_community');
        // dd($idCom);
        $idUser = Auth::id();
        // dump(Auth::id());
        
        
        // dump($request->file('logo')->getClientOriginalName());
        if(null!==$request->file('image')){

            $file   =   $request->file('image');
            $imageName = md5($idCom.$idUser.''.date('Y-m-d H:i:s')).'.'.$file->getClientOriginalExtension();
            $ImageUpload = Image::make($file);
            $originalPath = storage_path('app/public/img/event/'.$imageName);
            $yes = $ImageUpload->save($originalPath);
             
            if($yes){
                $insert=DB::table('com_event')
                    ->insert(array(
                        'community_id'=>$idCom,
                        'image'=>$imageName,
                        'title'=>$request->input('title'),
                        'start_date'=>$request->input('start_date'),
                        'end_date' =>$request->input('end_date'),
                        'start_time' =>$request->input('start_time'),
                        'end_time' =>$request->input('end_time'),
                        'location' =>$request->input('location'),
                        'description' =>$request->input('description'),
                        'status'  => "Ongoing",
                        'user_create'  => $idUser,
                        'created_at'  => date('Y-m-d H:i:s'),

                    ));
                
                if($insert){
                    $message=array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'Success inserted the data',
                    );
                }else{
                    $message=array(
                        'status'      =>'error',
                        'code'       =>'500',
                        'message'   =>'Failed inserted the data'
                    );
                }
            }else{
                $message=array(
                    'status'      =>'error',
                    'code'       =>'500',
                    'message'   =>'Upload Image Error'
                );
            }
            
        }
        
        return response()->json($message);
    }

    public function get_event(Request $request){
        DB::enableQueryLog(); 
        $data = DB::table('com_event')->select('*');
        if (null!==$request->input('status')) {
            $data = $data->where('status','Ongoing');
        }

        if (null!==$request->input('limit')) {
            $data = $data->limit($request->input('limit'));
        
        }
        // dd($request->input('community_ids'));
        if (null!==$request->input('id_user') && $request->input('community_id') == null ) {
            $getcom = DB::table('communites_anggota')->where('member_id',$request->input('id_user'))->groupBy('community_id')->get();
           
            $tmp = [];
            foreach ($getcom as $key) {
               array_push($tmp,$key->community_id);
            }
            //dd($tmp);
            // $data = $data->whereIn('community_id', $tmp);
           
        }else  if (null!==$request->input('id_user') && $request->input('community_id') !== null ) {
            $data = $data->where('community_id', $request->input('community_id'));
        }else if ($request->input('community_id') !== null ) {
            $data = $data->where('community_id', $request->input('community_id'));
        }
        if($request->input('status')=='Ongoing'){
            $data=$data->where('start_date','>',date('Y-m-d'))->where('end_date','>',date('Y-m-d'));
        }elseif($request->input('status')=='Done'){
            $data=$data->where('end_date','<',date('Y-m-d'));
        }
        $data = $data->orderBy('start_date', 'asc')->get();
        // dd(DB::getQueryLog()); // Show results of log
        $dr = json_decode( json_encode($data), true);
        
        foreach($data as $key => $val){
          
            $like = get_count( array(
                                'table' => 'com_event_like',
                                'field1' => 'event_id',
                                'where1' => $val->id,
                                'field2' => 'likeordis',
                                'where2' => '1',
                            ));

            $dislike = get_count( array(
                'table' => 'com_event_like',
                'field1' => 'event_id',
                'where1' => $val->id,
                'field2' => 'likeordis',
                'where2' => '2',
            ));

            $tertarik = get_count( array(
                'table' => 'com_event_tertarik',
                'field1' => 'event_id',
                'where1' => $val->id,
                'field2' => 'tertarikorhadir',
                'where2' => '1',
            ));

            $hadir = get_count( array(
                'table' => 'com_event_tertarik',
                'field1' => 'event_id',
                'where1' => $val->id,
                'field2' => 'tertarikorhadir',
                'where2' => '2',
            ));
            
            $dr[$key]['like']=$like;
            $dr[$key]['dislike']=$dislike;

            $dr[$key]['tertarik']=$tertarik;
            $dr[$key]['siap_hadir']=$hadir;

            $dr[$key]['image']=asset('storage/img/event/'.$val->image);
            // $datash = DB::table('communities_detail')->where('communities_id',$val->id)->first();
            // $dr[$key]['detail'] =$datash;
        }
        return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'response' => $dr
            ]); 
       
    }

    public function detail_event(Request $request){
        $id = $request->input('id_event');
       
        // dd($id);
        $d = DB::table('com_event AS com')->select('com.*')
                ->where('id',$id)
                ->first();
        
        if (NULL!== $d) {
            $d->image=asset('storage/img/event/'.$d->image);
            $dr = json_decode( json_encode($d), true);

            $like = get_count( array(
                'table' => 'com_event_like',
                'field1' => 'event_id',
                'where1' => $d->id,
                'field2' => 'likeordis',
                'where2' => '1',
            ));

            $dislike = get_count( array(
            'table' => 'com_event_like',
            'field1' => 'event_id',
            'where1' => $d->id,
            'field2' => 'likeordis',
            'where2' => '2',
            ));

            $tertarik = get_count( array(
            'table' => 'com_event_tertarik',
            'field1' => 'event_id',
            'where1' => $d->id,
            'field2' => 'tertarikorhadir',
            'where2' => '1',
            ));

            $hadir = get_count( array(
            'table' => 'com_event_tertarik',
            'field1' => 'event_id',
            'where1' => $d->id,
            'field2' => 'tertarikorhadir',
            'where2' => '2',
            ));

            $dr['like']=$like;
            $dr['dislike']=$dislike;

            $dr['tertarik']=$tertarik;
            $dr['siap_hadir']=$hadir;

            if (null!==$request->input('id_user')) {
                $idUser = $request->input('id_user');
                $cklike = DB::table('com_event_like')
                        ->where('user_id', $idUser)
                        ->where('event_id', $id)
                        ->first();
                if(NULL!=$cklike){
                    $dr['flag_like'] = true;
                }else{
                    $dr['flag_like'] = false;
                }

                $cktertarik = DB::table('com_event_tertarik')
                        ->where('user_id', $idUser)
                        ->where('event_id', $id)
                        ->first();

                if(NULL!=$cktertarik){
                    $dr['flag_tertarik'] = true;
                }else{
                    $dr['flag_tertarik'] = false;
                }
            }else{
                $dr['flag_like'] = false;
                $dr['flag_tertarik'] = false;
            }
            
                
            

            return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'response' => $dr
            ]);
        }else{
            return response()->json([
                'status'  =>'eror',
                'code'   => '200',
                'response' => 'Event not FOund'
            ]);
        }
       

         
        
    }
}
