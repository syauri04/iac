<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Image;

class AlamatController extends Controller
{
    public function get_province(Request $request){
      
        
        $data = DB::table('tb_province AS com')->select('com.*')
        ->get();
        
        $message=array(
            'status'      =>'success',
            'code'       =>'200',
            'response'   => $data
        );
        // dump($request->file('logo')->getClientOriginalName());
     
        return response()->json($message);
    }
    public function get_city(Request $request){
      
        $validator= Validator::make($request->all(), [
            'province_id'   => 'required',
           
        ]);
        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '200',
                    'message' => $validator->messages(),
                ], 302);
        }

        $cek = DB::table('tb_city AS com')->select('com.*')
        ->where('province_id', $request->input('province_id'))
        ->count();
        
        if($cek > 0){
            $data = DB::table('tb_city AS com')->select('com.*')
            ->where('province_id', $request->input('province_id'))
            ->get();

            $message=array(
                'status'      =>'success',
                'code'       =>'200',
                'response'   => $data
            );
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
            );
        }
        
        // dump($request->file('logo')->getClientOriginalName());
     
        return response()->json($message);
    }
   
}
