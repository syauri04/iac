<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class MerchController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $data['links_table_item'] = set_action(array("delete"),"ITEM");
        $d = DB::table('com_merchandise')->select('*')->orderBY('id','DESC')->get();
       foreach ($d as $key => $value) {
            $com = DB::table('communities')->where('id',$value->community_id)->first();
            if (NULL!=$com) {
                $d[$key]->community_name = $com->name;
            }else{
                $d[$key]->community_name = "";
            }
            $disclaim = DB::table('cp_report')->where('type','merchant ')->where('id_type',$value->id)->count();
            $d[$key]->count = $disclaim;
       } 
    //    dd($d);
       $data['data'] = $d;
       
        return view('Backend/merch',$data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function event()
    {
        // $data['links_table_item'] = set_action(array("edit"),"ITEM");
        $d = DB::table('com_event')
            ->select('com_event.*','communities.name')
            ->leftJoin('communities', 'com_event.community_id', '=', 'communities.id')
            ->OrderBy('com_event.id','desc')
            ->get();

        $data['data'] = $d;
       
        return view('Backend/com_event',$data);
    }


    public function disclaimer($id)
    {
        // $data['links_table_item'] = set_action(array("edit"),"ITEM");
        $d = DB::table('cp_report')
            ->where('id_type',$id)
            ->get();

        foreach ($d as $key => $value) {
            $mem = DB::table('member')->where('id',$value->id_member)->first();
            $d[$key]->id_member = $mem->email;
        }

        $data['data'] = $d;
        $data['idcom'] = $id;
        // dd($data);
        return view('Backend/merch_disclaimer',$data);
    }

    public function polling()
    {
        // $data['links_table_item'] = set_action(array("edit"),"ITEM");
        $d = DB::table('com_polling')->select('*')->where('type','CABANG')->OrderBy('id','desc')->get();
      
        // $dr = json_decode( json_encode($d), true);
        foreach($d as $key => $val){
            
            $com = DB::table('communities')->where('id',$val->community_id)->first();
            if (NULL!=$com) {
                $d[$key]->community_name = $com->name;
            }else{
                $d[$key]->community_name = "";
            }
            

            
        }
        $data['data'] = $d;
       
        return view('Backend/com_polling',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        // $data['links_table_item'] = set_action(array("edit"),"ITEM");
        $d = DB::table('communites_anggota')->select('*')->where('community_id',$id)->get();
        $dr = json_decode( json_encode($d), true);
        foreach($d as $key => $val){
            // dd($key);
           
            $datash = DB::table('member')->where('id',$val->member_id)->first();
            if(NULL!== $datash){
                $dr[$key]['member_name'] =$datash->nama_lengkap;
            }else{
                $dr[$key]['member_name'] = "Member Test";
            }

            $com = DB::table('communities')->where('id',$val->community_id)->first();
            $dr[$key]['community_name'] = $com->name;
        }
        $data['data'] = $dr;
        // dd($data);
        return view('Backend/community_detail',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('communities')
                ->where('id',$id)
                ->first();
        // dd($data);
        return view('Backend/community_form')->with('val',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->input());
        $update = DB::table('communities')
                ->where('id', $id)
                ->update([ 
                    'name' =>$request->input('name'),
                    'category' =>$request->input('category'),
                    'alamat_detail' =>$request->input('alamat_detail'),
                    'bank_name' =>$request->input('bank_name'),
                    'atas_nama' =>$request->input('atas_nama'),
                    'activate' =>$request->input('status'),
                   
                ]);
        
        if($update){

            return redirect('community/community')->with('status', 'Update for Data  Success');
        }else{
            return redirect('community/community')->with('status', 'Update for Data  Failed');
        }
    }
    
    public function update_status(Request $request, $id)
    {
        // dd($request->input('status'));
        if (NULL!==$request->input('stat_banned')) {
            $update = DB::table('com_merchandise')
                ->where('id', $id)
                ->update([ 
                    'stat_banned' => $request->input('stat_banned')
                ]);
                
            return redirect('merch/merchandise')->with('status', 'Update for Data  Success');
        }else{
            return redirect('merch/disclaimer/$id');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('com_merchandise')->where('id', $id)->delete();
        return redirect('merch/merchandise')->with('status', 'Delete for This Data Success');
    }
}
