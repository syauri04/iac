<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['links_table_item'] = set_action(array("detail","delete"),"ITEM");
        $data['data'] = DB::select('select * from member');
        
        // dd($data);
        return view('Backend/member',$data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $data['data'] = DB::table('member')
                ->where('id',$id)
                ->first();

        $data['community'] = DB::table('communities')
                            ->leftJoin('communites_anggota', 'communities.id', '=', 'communites_anggota.community_id')
                            ->where('communites_anggota.member_id',$id)
                            ->groupBy('communites_anggota.community_id')
                            ->get();
        
        // dd($data['community']);
        return view('Backend/detailMember')->with('val',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('member')
                ->where('id',$id)
                ->first();
        // dd($data);
        return view('Backend/memberForm')->with('val',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('member')->where('id', $id)->delete();
        return redirect('mem/member')->with('status', 'Delete for This Data Success');
    }
}
