<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class CommunityController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['links_table_item'] = set_action(array("detail","edit","delete"),"ITEM");
        $data['data'] = DB::table('communities')->select('*')->get();
       foreach ($data['data'] as $key => $value) {
           $disclaim = DB::table('cp_report')->where('type','community')->where('id_type',$value->id)->count();
           $data['data'][$key]->count = $disclaim;
       } 
        // dd($data['data']);
        return view('Backend/community',$data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function event()
    {
        $data['links_table_item'] = set_action(array("delete"),"ITEM");
        $d = DB::table('com_event')
            ->select('com_event.*','communities.name')
            ->leftJoin('communities', 'com_event.community_id', '=', 'communities.id')
            ->OrderBy('com_event.id','desc')
            ->get();

        $data['data'] = $d;
       
        return view('Backend/com_event',$data);
    }

    public function forum()
    {
        $data['links_table_item'] = set_action(array("delete"),"ITEM");
        $d = DB::table('com_forum')
            ->select('com_forum.*','communities.name')
            ->leftJoin('communities', 'com_forum.community_id', '=', 'communities.id')
            ->OrderBy('com_forum.id','desc')
            ->get();

        $data['data'] = $d;
       
        return view('Backend/com_forum',$data);
    }


    public function disclaimer($id)
    {
        // $data['links_table_item'] = set_action(array("edit"),"ITEM");
        $d = DB::table('cp_report')
            ->where('id_type',$id)
            ->get();

        foreach ($d as $key => $value) {
            $mem = DB::table('member')->where('id',$value->id_member)->first();
            $d[$key]->id_member = $mem->email;
        }

        $data['data'] = $d;
        $data['idcom'] = $id;
        return view('Backend/community_disclaimer',$data);
    }

    public function polling()
    {
        // $data['links_table_item'] = set_action(array("edit"),"ITEM");
        $d = DB::table('com_polling')->select('*')->where('type','CABANG')->OrderBy('id','desc')->get();
      
        // $dr = json_decode( json_encode($d), true);
        foreach($d as $key => $val){
            
            $com = DB::table('communities')->where('id',$val->community_id)->first();
            if (NULL!=$com) {
                $d[$key]->community_name = $com->name;
            }else{
                $d[$key]->community_name = "";
            }
            

            
        }
        $data['data'] = $d;
       
        return view('Backend/com_polling',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        // $data['links_table_item'] = set_action(array("edit"),"ITEM");
        $d = DB::table('communites_anggota')->select('*')->where('community_id',$id)->get();
        $dr = json_decode( json_encode($d), true);
        foreach($d as $key => $val){
            // dd($key);
           
            $datash = DB::table('member')->where('id',$val->member_id)->first();
            if(NULL!== $datash){
                $dr[$key]['member_name'] =$datash->nama_lengkap;
            }else{
                $dr[$key]['member_name'] = "Member Test";
            }

            $com = DB::table('communities')->where('id',$val->community_id)->first();
            $dr[$key]['community_name'] = $com->name;
        }
        $data['data'] = $dr;
        // dd($data);
        $gallery=DB::table('community_gallery')
                ->where('communities_id',$id)
                ->get();
                print_r($gallery);
        return view('Backend/community_detail',$data,compact(['gallery']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('communities')
                ->where('id',$id)
                ->first();
        // dd($data);
        return view('Backend/community_form')->with('val',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->input());
        $update = DB::table('communities')
                ->where('id', $id)
                ->update([ 
                    'name' =>$request->input('name'),
                    'category' =>$request->input('category'),
                    'alamat_detail' =>$request->input('alamat_detail'),
                    'bank_name' =>$request->input('bank_name'),
                    'atas_nama' =>$request->input('atas_nama'),
                    'activate' =>$request->input('status'),
                   
                ]);
        
        if($update){

            return redirect('community/community')->with('status', 'Update for Data  Success');
        }else{
            return redirect('community/community')->with('status', 'Update for Data  Failed');
        }
    }

    public function delete_gallery($id,$communities_id)
    {
        // dd($request->input());
        $update = DB::table('community_gallery')
                ->where('id', $id)
                ->delete();
        if($update){
            return redirect('community/community/detail/'.$communities_id)->with('status', 'Update for Data  Success');
        }else{
            return redirect('community/communitydetail/'.$communities_id)->with('status', 'Update for Data  Failed');
        }
    }
    
    public function update_status(Request $request, $id)
    {
        // dd($request->input('status'));
        if (NULL!==$request->input('status')) {
            $update = DB::table('communities')
                ->where('id', $id)
                ->update([ 
                    'activate' => $request->input('status')
                ]);
                
            return redirect('community/community')->with('status', 'Update for Data  Success');
        }else{
            return redirect('community/disclaimer/$id');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        DB::table('communities')->where('id', $id)->delete();
        return redirect('community/community')->with('status', 'Delete for This Data Success');
    }

    public function destroy_event($id)
    {
        // dd($id);
        DB::table('com_event')->where('id', $id)->delete();
        DB::table('com_event_like')->where('event_id', $id)->delete();
        DB::table('com_event_tertarik')->where('event_id', $id)->delete();
        return redirect('community/event')->with('status', 'Delete for This Data Success');
    }

    public function destroy_forum($id)
    {
        // dd($id);
        DB::table('com_forum')->where('id', $id)->delete();
        DB::table('com_forum_comment')->where('forum_id', $id)->delete();
        return redirect('community/forum')->with('status', 'Delete for This Data Success');
    }
}
