<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['links_table_item'] = set_action(array("access","edit","delete"),"ITEM");
        $data['data'] = DB::select('select * from cp_app_acl_group where is_trash != ?', [1]);
        
        // dd($data);
        return view('Meme/group',$data);
    }

    public function access($id)
    {
        if(isset($_POST['simpan'])){
            	
            DB::table('cp_app_acl_group_accesses')->where('aga_group_id', '=', $id)->delete();
            
            if(isset($_POST['acc_name']) && count($_POST['acc_name']) > 0){
                
                foreach($_POST['acc_name'] as $id_access=>$v){
                    // $this->DATA->table = "cp_app_acl_group_accesses";
                   
                    if(count($v)>0){
                        foreach($v as $id_action){
                            $data_actions = array(
                                'aga_access_id' => $id_access,
                                'aga_group_id'	=> $id,
                                'aga_action_id'	=> $id_action
                            );
                            // dd($data_actions);
                            DB::table('cp_app_acl_group_accesses')->insert([
                                $data_actions
                            ]);
                           
                            // $this->DATA->_add($data_actions);
                        }
                    }
                    
                }
            }

            return redirect('meme/group')->with('status', 'Update for Access Control List Success');

        }else{
            $actions = DB::select('select * from cp_app_acl_actions');
            $m_tbl=array();
            $access_mod = DB::select("select * from cp_app_acl_accesses where acc_active = 1 order by acc_menu asc");
            foreach($access_mod as $m){
                $a= explode(' > ',$m->acc_menu);
                $action_module = array();
                foreach($actions as $o){
                    $val = DB::table('cp_app_acl_group_accesses')
                            ->select('*')
                            ->where('aga_access_id',$m->acc_id)
                            ->where('aga_group_id',$id)
                            ->where('aga_action_id',$o->ac_id)
                            ->get();

                    $obj = DB::table('cp_app_acl_access_actions')
                            ->select('*')
                            ->where('aca_access_id',$m->acc_id)
                            ->where('aca_action_id',$o->ac_id)
                            ->get();

                    $action_module[]=array(
                        'id'	=> $o->ac_id,
                        'name'	=> $o->ac_action,
                        'show'	=> (count($obj) > 0)?"1":"",
                        'value'	=> (count($val) > 0)?"1":""
                    );
                
                }
                $m_tbl[$m->acc_id] = array(
                    'id_module'		=> $m->acc_id,
                    'group_menu'	=> $a['0'],
                    'module_name'	=> $m->acc_access_name,
                    'action'		=> $action_module
                );
            }
        }
        
        $data = array(
            "actions"	=> $actions,
            "access"	=> $m_tbl );
        // dd($m_tbl);
        return view('Meme/group_access',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
