<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use DataTables;

class BannerController extends Controller
{
    public function __construct()
    {
        
    }

    public function list(){
        return view('banner.list_banner');
    }

    public function data(Request $request){
        $type = $request->input('type');
        $data=DB::table('banner')
                ->where('type',$type)
                ->orderBy('created_at','desc')
                ->get();
        if (NULL!==$data) {
            foreach ($data as $key) {
                $key->content=substr(strip_tags($key->content),0,30)."...";
                $key->image=asset('storage/img/banner/'.$key->image);
            }
    
            return response()->json(
                [
                    'status' => true,
                    'code'   => 200,
                    'message'=> 'success fetch data',
                    'response'=>$data
                ], 200);
        }else{
            return response()->json(
                [
                    'status' => false,
                    'code'   => 200,
                    'message'=> 'data not found'
                ], 200);
        }
       
    }

    public function boarding(Request $request){
       
        $data=DB::table('com_boarding')
                ->orderBy('order_by','asc')
                ->get();

        if (NULL!==$data) {
            foreach ($data as $key) {
                
                $key->image=asset('storage/img/boarding/'.$key->image);
            }
    
            return response()->json(
                [
                    'status' => true,
                    'code'   => 200,
                    'message'=> 'success fetch data',
                    'response'=>$data
                ], 200);
        }else{
            return response()->json(
                [
                    'status' => false,
                    'code'   => 200,
                    'message'=> 'data not found'
                ], 200);
        }
       
    }
}
