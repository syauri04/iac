<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
// use App\Mail\VerifyMail;
use Illuminate\Support\Facades\Mail;
use Image;
use App\Member;

class MemberController extends Controller
{
    public function report(Request $request){
        $validator= Validator::make($request->all(), [
            'type'   => 'required',
            'id'     => 'required',
            'alasan'     => 'required',
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
       
        $idUser = Auth::id();

         $insert=DB::table('cp_report')
                    ->insert(array(
                        'type'=>$request->input('type'),
                        'id_type'=>$request->input('id'),
                        'id_member'=>$idUser,
                        'alasan'=>$request->input('alasan'),
                        'created_at'  => date('Y-m-d H:i:s'),

                    ));
                
        if($insert){
            $message=array(
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'Success inserted the data',
            );
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'Failed inserted the data'
            );
        }
        
        return response()->json($message);
    }


    public function read_notif(Request $request){
        $validator= Validator::make($request->all(), [
            'id_notif'   => 'required',
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }

        $affected = DB::table('notification')
                    ->where('id_notification', $request->input('id_notif'))
                    ->update(['id_notification' => $request->input('id_notif'),'status' => 'true']);

                
        if($affected){
            $message=array(
                "status"=> "success",
                "message"=> "Success fetch data"
            );
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'Failed inserted the data'
            );
        }
        
        return response()->json($message);
    }

    public function count_notif(Request $request){
      
        $affected = DB::table('notification')
                    ->where('id_member', $request->input('id_member'))
                    ->where('status','false')
                    ->count();
                    
                
        return response()->json([
            'status'  =>'success',
            'code'   => '200',
            'response' => ["count"=>$affected]
        ]); 
    }

    public function list_notification(Request $request){
        
        // $d = DB::table('cp_cart AS CT')
        //             ->select('CT.*','MC.user_create','MC.image','CO.status')
        //             ->leftJoin('cp_checkout AS CO', 'CT.invoice_code', '=', 'CO.invoice_code')
        //             ->leftJoin('com_merchandise AS MC', 'CT.merchant_id', '=', 'MC.id')
        //             ->where('CT.status_invoice','1')
        //             ->where('CO.status','PAID')
        //             ->where('MC.user_create',$request->input('id_member'))
        //             ->get();

        $d = DB::table('notification')->where('id_member',$request->input('id_member'))->get();
    //    dd($d);
        if(NULL!==$d){
            foreach ($d as $key => $value) {
               
                if(isset(json_decode($value->param)->id_merchant)){
                    $img = DB::table('com_merchandise')->where('id',json_decode($value->param)->id_merchant);
                    if($img->count()>0){
                        $img= $img->first();
                        $ast = asset('storage/img/merchandise/'.$img->community_id.'/'.$img->image);
                    }else{
                        $ast ="https://increasify.com.au/wp-content/uploads/2016/08/default-image.png";
                    }
                    
                }else{
                    $ast ="https://increasify.com.au/wp-content/uploads/2016/08/default-image.png";
                }
                
                $item[] = [
                        "id" => $value->id_notification,
                        "type_notif" => $value->type,
                        "text" => $value->msg,
                        "image" => $ast ,
                        "status_read" => $value->status,
                        "date_created" => $value->created_at,
                        "activity" => $value->activity,
                        "param" => json_decode($value->param)
    
                ];
            }
            $data = $item;
            // $dr = json_decode( json_encode($d), true);
            // foreach ($d as $key => $value) {
            //     $dr[$key]['image'] = asset('storage/img/merchandise/'.$value->community_id.'/'.$value->image);
            // }
            // dd("sfsdf");
    
            return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'response' => $data
            ]); 
        }else{
            return response()->json([
                'status'  =>'eror',
                'code'   => '200',
                'message' => "id member not found"
            ]); 
        }
        
        
       

 
       

       
    }

    public function search_member(Request $request){

        // dd($request->input());
        $validator= Validator::make($request->all(), [
            'telp'   => 'required',
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $telp= $request->input('telp');
        // dd($telp);
        $cek = DB::table('member')
                ->where('telp', 'like', '%'.$telp.'%')
                ->count();
        if($cek > 0){
            $data = DB::table('member')->select('*')
                        ->where('telp', 'like', '%'.$telp.'%')
                        ->get();
           
            $dr = json_decode( json_encode($data), true);
            foreach($data as $key => $val){
                // dd($key);
                $dr[$key]['profile_pic']=asset('storage/img/member/'.$val->profile_pic);
                // $datash = DB::table('communities_detail')->where('communities_id',$val->id)->first();
                // $dr[$key]['detail'] =$datash;
            }
            return response()->json([
                    'status'  =>'success',
                    'code'   => '200',
                    'response' => $dr
                ]); 
        }else{
            return response()->json([
                'status'  =>'eror',
                'code'   => '200',
                'response' => 'Data Not Found'
            ]); 
        }

       
    }

    public function edit(Request $request) 
    { 
        $validator= Validator::make($request->all(), [
            'nama_lengkap'   => 'required',
            'telp'     => 'required',
            'email'     => 'required|email',
           
        ]);
        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $idUser = Auth::id();
        // dd($request->input());
        $token = Str::random(80);
        $password=Hash::make($request->input('password'));
        $update=Member::where('id',$idUser)->update([
                    'nama_lengkap'=>$request->input('nama_lengkap'),
                    'email'=>$request->input('email'),
                    'telp' =>$request->input('telp'),
                    // 'api_token' => hash('sha256', $token),
                    'updated_at'=>date('Y-m-d H:i:s')
                    ]
                );

        if(null!==$request->file('profile_pic')){
            $get_user=Member::select('id','profile_pic')->where('id',$idUser)->first();
           
            $file   =   $request->file('profile_pic');
            $imageName = md5($idUser.''.date('Y-m-d H:i:s')).'.'.$file->getClientOriginalExtension();
            $ImageUpload = Image::make($file);
            $originalPath = storage_path('app/public/img/member/'.$imageName);
            $ImageUpload->save($originalPath);

            if(NULL!==$get_user->profile_pic){
                unlink(storage_path('app/public/img/member/'.$get_user->profile_pic));
            }
           
            $update=Member::where('id',$idUser)
            ->update(['profile_pic' => $imageName]);
            // for save thumnail image
            // $thumbnailPath = storage_path('app/public/img/community/thumb/'.$imageName);
            // $ImageUpload->resize(300,null, function ($constraint) {
            //     $constraint->aspectRatio();
            // });
            // $ImageUpload = $ImageUpload->save($thumbnailPath);
        }
        

        if($update){
            $message=array(
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'Success updated the data',
            );
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'Failed updated the data'
            );
        }
        return response()->json($message);
        
    }

    public function kirim_ulang(Request $request){
        $checkUser =DB::table('member')->where('telp',$request->input('telp'))->first();
        if(NULL!==$checkUser){
            $token_sms=rand(1000,9999);
            $sms=send_sms($request->input('telp'),$token_sms);
            $affected = DB::table('member')
              ->where('telp', $request->input('telp'))
              ->update(['sms_verfy' => $token_sms]);
            return response()->json(
                [
                    'status' => 'success',
                    'code'   => '200',
                    'message'=> 'Send Code success',
                ], 200);
        }else{
            return response()->json(
                [
                    'status' => 'error',
                    'code'   => '200',
                    'message'=> 'Phone Not Registered',
                ], 200);
        }
        dd($checkUser);
    }
    
    public function register(Request $request){
        $validator= Validator::make($request->all(), [
            'nama_lengkap'   => 'required',
            'telp'     => 'required',
            'email'     => 'required|email',
            'token'     => 'required',
           
        ]);
        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '200',
                    'message' => $validator->messages(),
                ], 200);
        }
        $checkUser = Member::where('email', '=', $request->input('email'))->count();
        if($checkUser>0){
            return response()->json(
                [
                    'status' => 'duplicate',
                    'code'   => '200',
                    'message'=> 'Email has already register',
                ], 200);
        }else{
            $token = Str::random(80);
            $member = new Member;
            $member->nama_lengkap = $request->input('nama_lengkap');
            $member->firebase = $request->input('token');
            $member->api_token = $token;
            $token_sms=rand(1000,9999);
            $member->sms_verfy = Hash::make($token_sms);
            $member->telp = $request->input('telp');
            $member->email = $request->input('email');
            $member->activate = "active";
            $insert = $member->save();

            if($insert){
                $sms=send_sms($request->input('telp'),$token_sms);
                // Mail::to($request->input('email'))->send(new VerifyMail($request->input('nama_lengkap'),$token));
                return response()->json(
                        [
                            'status' => 'success',
                            'code'   => '200',
                            'message'=> 'success insert data',
                            'response'=>[
                                'token'=>$token,
                                'token_sms'=>$token_sms,
                                'sms'=>json_decode($sms)
                            ]
                        ], 200);
                }else{
                    return response()->json(
                        [
                            'status' => 'error',
                            'code'   => '200',
                            'message'=> 'error insert data',
                        ], 500);
                }
            
        }

        // return response()->json(['data' => $model]);
    }
    public function login(Request $request){
        $validator= Validator::make($request->all(), [
            'telp'     => 'required',
            'token'     => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '200',
                    'message' => $validator->messages(),
                ], 302);
        }
        $member=DB::table('member')
                ->where('telp',$request->input('telp'));
        if($member->count()>0){
                $token_sms=rand(1000,9999);
                $token = Str::random(80);
                $sms_verfy = Hash::make($token_sms);
                DB::table('member')
                    ->where('telp',$request->input('telp'))
                    ->update([
                        'firebase'=>$request->input('token'),
                        'sms_verfy'=>$sms_verfy,
                        // 'api_token'=>$token
                    ]);
                $sms=send_sms($request->input('telp'),$token_sms);
                return response()->json(
                    [
                        'status' => 'success',
                        'code'   => 200,
                        'message'=> 'Success fetch data',
                        'response'=>[
                            'token'=>$token,
                            'token_sms'=>$token_sms,
                            'sms'=>json_decode($sms)
                        ]
                    ], 200);
        }else{
            return response()->json(
                [
                    'status' => 'not_found',
                    'code'   => '200',
                    'message'=> 'Account is not found',
                ], 404);
        }
    }

    public function verify_sms(Request $request){
        $validator= Validator::make($request->all(), [
            'token_sms'     => 'required',
            'telp'     => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $member=DB::table('member')
                ->where('telp',$request->input('telp'));
        if($member->count()>0){
            $member=$member->first();
            if (Hash::check($request->input('token_sms'), $member->sms_verfy)) {
                $member->profile_pic=default_img($member->profile_pic,'img/user_default.png','member/'.$member->profile_pic);
                $member->alamat=change_null($member->alamat);
                return response()->json(
                    [
                        'status' => 'success',
                        'code'   => 200,
                        'message'=> 'Success fetch data',
                        'response'=>$member
                    ], 200);
            }else{
                return response()->json(
                    [
                        'status' => 'error',
                        'code'   => '200',
                        'message'=> 'Token is not match',
                    ], 200);
            }
        }else{
            return response()->json(
                [
                    'status' => 'not_found',
                    'code'   => '200',
                    'message'=> 'Account is not found',
                ], 200);
        }
    }
    

    public function detail(Request $request) 
    { 
        $id = $request->input('id_member');
        $d = DB::table('member')->select('*')
                ->where('id',$id)
                ->first();
        if (NULL !== $d) {
            # code...
        
        $d->profile_pic=default_img($d->profile_pic,'img/user_default.png','storage/img/member');
        $dr = json_decode( json_encode($d), true);
        
        $cekanggota = DB::table('communites_anggota')->where('member_id',$d->id)->count();
        if($cekanggota > 0){
            $cm = DB::table('communites_anggota as ca')
                ->select('ca.*')
                ->join('communities','ca.community_id','=','communities.id')
                ->where('ca.member_id',$d->id)
                ->groupBy('ca.community_id')
                ->get();
            $items=[];
             foreach($cm as $key){
                $comnya = DB::table('communities')->where('id',$key->community_id);
                // $item[]= $key;
                if($comnya->count()>0){
                    $comnya=$comnya->first();
                    $item['communty_id']=$key->community_id;
                    $item['communty_name']=$comnya->name;
                    $item['communty_level']=$key->level;
                    $item['communty_logo']=default_img($comnya->logo,'img/user_default.png','storage/img/community');
                    array_push($items,$item);
                }else{
                    $item['communty_id']=$key->community_id;
                    $item['communty_name']='';
                    $item['communty_level']=$key->level;
                    $item['communty_logo']=url('storage/img/user_default.png');
                    array_push($items,$item);
                }
                
            }
            $dr['list_community'] =$items;
        }

        return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'response' => $dr
            ]); 
        }else{
            return response()->json([
                'status'  =>'eror',
                'code'   => '202',
                'response' => 'Member Not FOundss'
            ]); 
        }
        
    }
    
    function add_advertisement(Request $request){
        $validator= Validator::make($request->all(), [
            'id_member'     => 'required',
            'name'     => 'required',
            'description'     => 'required',
            'phone'     => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }

        $adverdtising=DB::table('tb_adverdtising')
                        ->where('id_member',$request->input('id_member'))
                        ->where('expire_date','>=',date('Y-m-d H:i:s'))
                        ->count();
        if($adverdtising >=3){
            return response()->json([
                'status'  =>'eror',
                'code'   => '192',
                'response' => 'Advertisment cant more than 3'
            ]); 
        }else{
            if(null!==$request->file('image')){
                $file   =   $request->file('image');
                $imageName = md5('advertisment-'.date('Y-m-d H:i:s')).'.'.$file->getClientOriginalExtension();
                $ImageUpload = Image::make($file);
                $originalPath = storage_path('app/public/img/member/'.$imageName);
                $ImageUpload->save($originalPath);
            }
            $adverd=DB::table('tb_adverdtising')
                    ->insert(
                        [
                            'id_member'=>$request->input('id_member'),
                            'image'=>$imageName,
                            'name'=>$request->input('name'),
                            'description'=>$request->input('description'),
                            'whatsapp'=>$request->input('whatsapp'),
                            'website'=>$request->input('website'),
                            'phone'=>$request->input('phone'),
                            'instagram'=>$request->input('instagram'),
                            'facebook'=>$request->input('facebook'),
                            'shoppee'=>$request->input('shoppee'),
                            'tokopedia'=>$request->input('tokopedia'),
                            'bukalapak'=>$request->input('bukalapak'),
                            'expire_date'=>date('Y-m-d H:i:s', strtotime("+30 day", strtotime(date('Y-m-d H:i:s')))),
                        ]
                    );
            return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'message' => 'success inserted the data'
            ]); 
        }
    }

    function edit_advertisement(Request $request){
        $validator= Validator::make($request->all(), [
            'name'     => 'required',
            'description'     => 'required',
            'phone'     => 'required',
            
        ]);
        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
            if(null!==$request->file('image')){
                $file   =   $request->file('image');
                $imageName = md5('advertisment-'.date('Y-m-d H:i:s')).'.'.$file->getClientOriginalExtension();
                $ImageUpload = Image::make($file);
                $originalPath = storage_path('app/public/img/member/'.$imageName);
                $ImageUpload->save($originalPath);
                $adverd=DB::table('tb_adverdtising')
                    ->where('id_tb_adverdtising',$request->input('id_advertisement'))
                    ->update(
                        [
                            'image'=>$imageName,
                        ]
                    );
            }
            $adverd=DB::table('tb_adverdtising')
                    ->where('id_adverdtising',$request->input('id_advertisement'))
                    ->update(
                        [
                            'name'=>$request->input('name'),
                            'description'=>$request->input('description'),
                            'whatsapp'=>$request->input('whatsapp'),
                            'phone'=>$request->input('phone'),
                            'instagram'=>$request->input('instagram'),
                            'website'=>$request->input('website'),
                            'facebook'=>$request->input('facebook'),
                            'shoppee'=>$request->input('shoppee'),
                            'tokopedia'=>$request->input('tokopedia'),
                            'bukalapak'=>$request->input('bukalapak'),
                        ]
                    );
            return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'message' => 'success edited the data'
            ]); 
    }

    function delete_advertisement(Request $request){
        $validator= Validator::make($request->all(), [
            'id_advertisement'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
            $adverd=DB::table('tb_adverdtising')
                    ->where('id_adverdtising',$request->get('id_advertisement'))
                    ->delete();
            return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'message' => 'success deleted the data'
            ]);
    }

    function detail_advertisment(Request $request){
        $validator= Validator::make($request->all(), [
            'id_advertisement'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $adverd=DB::table('tb_adverdtising')
                    ->where('id_adverdtising',$request->input('id_advertisement'));
        if($adverd->count()>0){
            $adverd=$adverd->first();
            $adverd->image=default_img($adverd->image,'img/user_default.png','storage/img/member');
            $adverd->id_advertisement=$adverd->id_adverdtising;
            return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'message' => 'success deleted the data',
                'response'=>$adverd
            ]);

        }else{
            return response()->json([
                'status'  =>'error',
                'code'   => '404',
                'message' => 'id_advertisment is not found',
            ]);
        }
    }

    function advertsiemnt_list_member(Request $request){
        $validator= Validator::make($request->all(), [
            'id_member'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $adverd=DB::table('tb_adverdtising')->where('id_member',$request->get('id_member'))->where('expire_date','>=',date('Y-m-d H:i:s'))->get();
            foreach ($adverd as $key) {
                $key->id_advertisement=$key->id_adverdtising;
                $key->image=default_img($key->image,'img/user_default.png','storage/img/member');
            }
            return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'message' => 'success deleted the data',
                'response'=>$adverd
            ]);
    }

    function advertsiemnt_list_home(Request $request){
        $validator= Validator::make($request->all(), [
            'page_start'=>'required',
            'limit'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $page_start=$request->get('page_start');
        $limit=$request->get('limit');
        if($page_start==1){
            $skip=0;
        }else{
            $skip=$page_start*$limit-1;
        }
        $adverd=DB::table('tb_adverdtising')
                    ->where('expire_date','>=',date('Y-m-d H:i:s'))
                    ->skip($skip)
                    ->take($limit)
                    ->get();
            foreach ($adverd as $key) {
                $key->id_advertisement=$key->id_adverdtising;
                $key->image=default_img($key->image,'img/user_default.png','storage/img/member');
            }
            return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'message' => 'success deleted the data',
                'response'=>$adverd
            ]);
    }
    
}
