<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Mail\VerifyMail;
use Illuminate\Support\Facades\Mail;
use Image;
class AddressController extends Controller
{
    public function __construct()
    {
        
    }

    public function get_province(Request $request){
        $province=DB::table('province')->get();
        return response()->json([
            'status' => 'success',
            'code'   => 200,
            'message'=> 'Success fetch data',
            'response'=>$province
        ], 200);
    }

    public function get_city(Request $request){
        $city=DB::table('city');
        if(null!==$request->get('province_id')){
            $city=$city->where('province_id',$request->get('province_id'));
        }
        $city=$city->get();
        $province=DB::table('province')->get();
        return response()->json([
            'status' => 'success',
            'code'   => 200,
            'message'=> 'Success fetch data',
            'response'=>$city
        ], 200);
    }

    public function get_city_by_id(Request $request){
        $validator= Validator::make($request->all(), [
            'city_id'     => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $city=DB::table('city')->where('city_id',$request->get('city_id'))->first();
        $province=DB::table('province')->get();
        return response()->json([
            'status' => 'success',
            'code'   => 200,
            'message'=> 'Success fetch data',
            'response'=>$city
        ], 200);
    }

}
