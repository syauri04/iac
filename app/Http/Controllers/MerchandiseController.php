<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Image;

class MerchandiseController extends Controller
{
    public function list_pengiriman(Request $request){
        if (null!==$request->input('user_id')) {
            $id = $request->input('user_id');
            $d = DB::table('cp_pengiriman')->select('*')
            ->where('user_id',$id)
            ->get();
        }else{
            $d = DB::table('cp_pengiriman')->select('*')
            ->get();
        }

        $dr = json_decode( json_encode($d), true);
       foreach ($d as $key => $v) {
            $prov = DB::table('tb_province')
                    ->where('province_id', $v->province_id)
                    ->first();
            
            $dr[$key]['province_name']=$prov->province;

            $cit = DB::table('tb_city')
                    ->where('city_id', $v->city_id)
                    ->first();
            $dr[$key]['city_name']=$cit->city_name;
       }
        return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'response' => $dr
            ]); 
        
    }
    public function delete_pengiriman(Request $request){
 

        if (null!==$request->input('user_id')) {
            $idUser = $request->input('user_id');
        }else{
            $idUser="";
        }
        // dd($idCom);
        
        // dd($idUser);
        $cek = DB::table('cp_pengiriman')
                        ->where('user_id',$idUser)
                        ->first();
        if(NULL!==$cek){
            $push = DB::table('cp_pengiriman')->where('user_id', $idUser)->delete();

                if($push){
                    $message=array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'Success deleted the data',
                    );
                }else{
                    $message=array(
                        'status'      =>'error',
                        'code'       =>'500',
                        'message'   =>'Failed deleted the data'
                    );
                }
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'Data not found'
            );
        }
        

       
        
        return response()->json($message);
    }
    public function add_pengiriman(Request $request){
        $validator= Validator::make($request->all(), [
            'province_id'   => 'required',
            'city_id'     => 'required',
            // 'latitude'     => 'required',
            // 'longitude'     => 'required',
           
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }

      
        // dd($idCom);
        $idUser = Auth::id();

        $matchcity = DB::table('tb_city')
                        ->where('province_id',$request->input('province_id'))
                        ->first();
        if(NULL!==$matchcity){
            $push = DB::table('cp_pengiriman')
                ->updateOrInsert(
                    ['user_id' => $idUser],
                    [
                        'user_id' => $idUser,
                        'province_id' => $request->input('province_id'),
                        'city_id' => $request->input('city_id'),
                        'alamat' => $request->input('alamat'),
                        'latitude' => $request->input('latitude'),
                        'longitude' => $request->input('longitude')
                    ]
                );

                if($push){
                    $message=array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'Success inserted the data',
                    );
                }else{
                    $message=array(
                        'status'      =>'error',
                        'code'       =>'500',
                        'message'   =>'Failed inserted the data'
                    );
                }
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'City Dosent match on province id'
            );
        }
        return response()->json($message);
    }

    public function update_status_trx(Request $request){
        $validator= Validator::make($request->all(), [
            'invoice_code'   => 'required',
            'status_invoice'     => 'required',
            'user_id'     => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }

      
        // dd($idCom);
        $idUser = Auth::id();

        $match = DB::table('cp_cart')
                        ->where('user_id',$request->input('user_id'))
                        ->where('invoice_code',$request->input('invoice_code'))
                        ->first();
        if(NULL!==$match){
            $datapost = [
                'status_invoice' 	=> $request->input('status_invoice')
            ];
            switch ($request->input('status_invoice')) {
                case 3:
                    $stat = 'CONFIRM';
                    $datapost['tanggal_confirm'] = date("Y-m-d H:i:s");
                    break;
                case 4:
                    $stat = 'DELIVERY';
                    $datapost['tanggal_delivery'] = date("Y-m-d H:i:s");
                    break;
                case 5:
                    $stat = 'SHIPPED';
                    $datapost['tanggal_shipped'] = date("Y-m-d H:i:s");
                    break;
                case 6:
                    $stat = 'CANCEL';
                    $datapost['tanggal_cancel'] = date("Y-m-d H:i:s");
                    break;
        
                default:
                  
            }
            
            if(NULL!==$request->input('no_resi')){
                $datapost['no_resi'] = $request->input('no_resi');
            }

            $push = DB::table('cp_cart')
                    ->where('id', $match->id)
                    ->update($datapost);

          
            $members=DB::table('member')
                    ->select('*')
                    ->where('id',$request->input('user_id'))
                    ->first();
            $param=array(
                'token'=>1,
                'position'=>$stat,
                'id_member'=>$request->input('user_id'),
                'id_merchant'=>$match->merchant_id
            );
            $data_push_notif = array(
                'activity'=>'Marchendaise.HistoryMerchendaise',
                'body'=>'Selamat, Order anda telah '.$stat,
                'data'=> $param,
                'icon'=>url('assets/uploads/settings/Untitled-1.png'),
                'title'=>'Selamat',
            );
            $insertnotif = DB::table('notification')
                            ->insert(array(
                                'id_member'=>$request->input('user_id'),
                                'type'=>1,
                                'msg'=>'Selamat, Order anda telah '.$stat,
                                'activity'=>'Marchendaise.HistoryMerchendaise',
                                'param'  => json_encode($param),
                                'created_at'  => date('Y-m-d H:i:s'),
                                'status'  =>'false',
        
                            ));
         
            $x=ngepush_notif($members->firebase,$data_push_notif);
            error_log($x);
            
            if($push){
                $message=array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Success Update the data',
                );
            }else{
                $message=array(
                    'status'      =>'error',
                    'code'       =>'500',
                    'message'   =>'Failed Update the data'
                );
            }
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'Data Dosent match on cart'
            );
        }
        return response()->json($message);
    }

    public function detail_trx(Request $request){
        if (null!==$request->input('user_id')) {
            $id = $request->input('user_id');
        }else{
            $id="";
        }
        $d = DB::table('cp_checkout')
                    ->where('cp_checkout.user_id',$id)
                    ->where('cp_checkout.invoice_code',$request->input('invoice_code'))
                    ->first();
       
        if (NULL!==$d) {
            $dr = json_decode( json_encode($d), true);
            
            // dd($id);
            $bayar = DB::table('cp_bayar')->where('invoice_code',$d->invoice_code)->first();
            // dd($bayar);
            $dr['bukti_transfer']=(NULL!==$bayar)? asset('storage/img/bukti_bayar/'.$bayar->user_id.'/'.$bayar->img_bukti):"";
            $alamat = DB::table('cp_pengiriman')->where('id',$d->address_id)->first();
            $dr['address_id'] = $alamat->alamat;
            $cart = DB::table('cp_cart')->where('invoice_code',$d->invoice_code)->get();
            $cmtyid = DB::table('cp_cart')->where('invoice_code',$d->invoice_code)->first();
            $cmty = DB::table('communities')->where('id',$cmtyid->community_id)->first();
            
            $dr['noreq']=$cmty->noreq;
            $dr['bank_name']=$cmty->bank_name;
            $dr['atas_nama']=$cmty->atas_nama;
            foreach ($cart as $v) {
                $merc = DB::table('com_merchandise')->where('id',$v->merchant_id)->first();
                
                // dd($user);
              
                
                $item['title']=$merc->title;
                $item['qty']=$v->qty;
                $item['harga']=$merc->harga;
                $item['image']=asset('storage/img/merchandise/'.$merc->community_id.'/'.$merc->image);
            
                $items[] = $item;
            
            }
            $dr['cart'] =$items;

            $cr = DB::table('cp_checkout')
                    ->select('cp_checkout.*','cp_cart.status_invoice','cp_cart.tanggal_order','cp_cart.tanggal_checkout','cp_cart.tanggal_confirm','cp_cart.tanggal_delivery','cp_cart.tanggal_shipped','cp_cart.no_resi')
                    ->leftJoin('cp_cart', 'cp_checkout.invoice_code', '=', 'cp_cart.invoice_code')
                    ->where('cp_cart.invoice_code',$d->invoice_code)->first();
            // dd($cr->status);
            // $cr = DB::table('cp_cart')->where('invoice_code',$d->invoice_code)->first();
            switch ($cr->status_invoice) {
                case 1:
                    if($cr->status == 'PENDING'){
                        $dr['status_trx'] = 'ORDER';
                        $dr['tanggal_trx'] = (NULL!==($cr->tanggal_order))?myDate(date("Y-m-d",strtotime($cr->tanggal_order))):'';
                    }else{
                        $dr['status_trx'] = 'CHECKOUT';
                        $dr['tanggal_trx'] = (NULL!==($cr->tanggal_checkout))?myDate(date("Y-m-d",strtotime($cr->tanggal_checkout))):'';
                    }
                  
                    break;
                case 3:
                    $dr['status_trx'] = 'CONFIRM';
                    $dr['tanggal_trx'] = (NULL!==($cr->tanggal_confirm))?myDate(date("Y-m-d",strtotime($cr->tanggal_confirm))):'';
                    break;
                case 4:
                    $dr['status_trx'] = 'DELIVERY';
                    $dr['tanggal_trx'] = (NULL!==($cr->tanggal_delivery))?myDate(date("Y-m-d",strtotime($cr->tanggal_delivery))):'';
                    break;
                case 5:
                    $dr['status_trx'] = 'SHIPPED';
                    $dr['tanggal_trx'] = (NULL!==($cr->tanggal_shipped))?myDate(date("Y-m-d",strtotime($cr->tanggal_shipped))):'';
                    break;
                case 6:
                    $dr['status_trx'] = 'CANCEL';
                    $dr['tanggal_trx'] = (NULL!==($cr->tanggal_cancel))?myDate(date("Y-m-d",strtotime($cr->tanggal_cancel))):'';
                    break;
              
                default:
                   
            }
            $dr['no_resi'] =$cr->no_resi;
            $dr['cart'] =$items;
            return response()->json([
                    'status'  =>'success',
                    'code'   => '200',
                    'response' => $dr
                ]); 
        }else{
            return response()->json([
                'status'  =>'eror',
                'code'   => '200',
                'response' => 'Not found'
            ]); 
        }
        
        
    }

    public function list_trx(Request $request){
        
        $d = DB::table('cp_checkout')
                    ->leftJoin('cp_cart', 'cp_checkout.invoice_code', '=', 'cp_cart.invoice_code');


        if (null!==$request->input('pengurus_id')) {
            $d = $d->leftJoin('com_merchandise', 'cp_cart.merchant_id', '=', 'com_merchandise.id');
            $d = $d->where('com_merchandise.user_create',$request->input('pengurus_id'));
            
        }  
        
        if (null!==$request->input('community_id')) {
            $d = $d->where('com_merchandise.community_id',$request->input('community_id'));
        }



        $d = $d->where('cp_cart.status_invoice',$request->input('status_invoice'));
        
        if (null!==$request->input('user_id')) {
             $d = $d->where('cp_checkout.user_id',$request->input('user_id'));
        }
        if (null!==$request->input('status_paid')) {
            $d = $d->where('cp_checkout.status',$request->input('status_paid'));
        }
        
        
        $data = $d->select('cp_checkout.*','cp_cart.status_invoice','cp_cart.tanggal_order','cp_cart.tanggal_checkout','cp_cart.tanggal_confirm','cp_cart.tanggal_delivery','cp_cart.tanggal_shipped')->groupBy('cp_cart.invoice_code')->get();
  
        $dr = change_null($data);
       
        if($dr !== ''){
            $dr = json_decode( json_encode($data), true);
            foreach($data as $key => $val){
                switch ($val->status_invoice) {
                    case 1:
                        if($val->status == 'PENDING'){
                            $dr[$key]['status_invoice'] = 'ORDER';
                            $dr[$key]['tanngal_invoice'] = (NULL!==($val->tanggal_order))?myDate(date("Y-m-d",strtotime($val->tanggal_order))):'';
                        }else{
                            $dr[$key]['status_invoice'] = 'CHECKOUT';
                            $dr[$key]['tanngal_invoice'] = (NULL!==($val->tanggal_checkout))?myDate(date("Y-m-d",strtotime($val->tanggal_checkout))):'';
                        }
                      
                        break;
                    case 3:
                        $dr[$key]['status_invoice'] = 'CONFIRM';
                        $dr[$key]['tanngal_invoice'] = (NULL!==($val->tanggal_confirm))?myDate(date("Y-m-d",strtotime($val->tanggal_confirm))):'';
                        break;
                    case 4:
                        $dr[$key]['status_invoice'] = 'DELIVERY';
                        $dr[$key]['tanngal_invoice'] = (NULL!==($val->tanggal_delivery))?myDate(date("Y-m-d",strtotime($val->tanggal_delivery))):'';
                        break;
                    case 5:
                        $dr[$key]['status_invoice'] = 'SHIPPED';
                        $dr[$key]['tanngal_invoice'] = (NULL!==($val->tanggal_shipped))?myDate(date("Y-m-d",strtotime($val->tanggal_shipped))):'';
                        break;
                    case 6:
                        $dr[$key]['status_invoice'] = 'CANCEL';
                        $dr[$key]['tanngal_invoice'] = (NULL!==($val->tanggal_cancel))?myDate(date("Y-m-d",strtotime($val->tanggal_cancel))):'';
                        break;
                  
                    default:
                       
                }
                // $dr[$key]['tanggal_order']=(NULL!==($val->tanggal_order))?myDate(date("Y-m-d",strtotime($val->tanggal_order))):'';
                // $dr[$key]['tanggal_checkout']=(NULL!==($val->tanggal_checkout))?myDate(date("Y-m-d",strtotime($val->tanggal_checkout))):'';
                // $dr[$key]['tanggal_confirm']=(NULL!==($val->tanggal_confirm))?myDate(date("Y-m-d",strtotime($val->tanggal_confirm))):'';
                // $dr[$key]['tanggal_delivery']=(NULL!==($val->tanggal_delivery))?myDate(date("Y-m-d",strtotime($val->tanggal_delivery))):'';
                // $dr[$key]['tanggal_shipped']=(NULL!==($val->tanggal_shipped))?myDate(date("Y-m-d",strtotime($val->tanggal_shipped))):'';
                // $datash = DB::table('communities_detail')->where('communities_id',$val->id)->first();
                // $dr[$key]['detail'] =$datash;
            }
            return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'response' => $dr
            ]); 
        }else{
            return response()->json([
                'status'  =>'eror',
                'code'   => '500',
                'response' => 'not found'
            ]); 
        }
        // $dr = json_decode( json_encode($d), true);


        
        
    }

    public function bayar(Request $request){
        // dd($request->input());
        $validator= Validator::make($request->all(), [
            'invoice_code'   => 'required',
            'bank'     => 'required',
            'norek'     => 'required',
            'atas_nama'     => 'required',
            'img_bukti'     => 'required',
           
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        
        // dd($ongkir['rajaongkir']['results'][0]['costs']['1']['cost'][0]['value']);
        $userid = Auth::id();
        $cek = DB::table('cp_checkout')
                ->where('user_id', $userid)  
                ->where('invoice_code', $request->input('invoice_code'))
                ->where('status', 'PENDING')
                ->count();  
        if($cek > 0){
            if(null!==$request->file('img_bukti')){

                $file   =   $request->file('img_bukti');
                $imageName = md5($userid.''.date('Y-m-d H:i:s')).'.'.$file->getClientOriginalExtension();
                $ImageUpload = Image::make($file);
                $path = storage_path().'/app/public/img/bukti_bayar/' . $userid;
                $dir =  makeDirectory($path, $mode = 0777, true, true);
    
                $originalPath = storage_path('app/public/img/bukti_bayar/'.$userid.'/'.$imageName);
              
                $yes = $ImageUpload->save($originalPath);
                 
                if($yes){
                    $insert=DB::table('cp_bayar')
                        ->insert(array(
                            'user_id'=>$userid,
                            'invoice_code'=>$request->input('invoice_code'),
                            'bank'=>$request->input('bank'),
                            'norek'=>$request->input('norek'),
                            'atas_nama'=>$request->input('atas_nama'),
                            'img_bukti' =>$imageName,
                            'created_at' => date('Y-m-d H:i:s')
                        ));
                    
                    if($insert){
                        $affected = DB::table('cp_checkout')
                                        ->where('user_id', $userid)  
                                        ->where('invoice_code', $request->input('invoice_code'))
                                        ->update(['status' => 'PAID']);
                                        
                        $pengurus=DB::table('cp_cart AS CT')
                        ->select('CT.community_id','CT.merchant_id','MC.user_create')
                        ->leftJoin('com_merchandise AS MC', 'CT.merchant_id', '=', 'MC.id')
                        ->where('CT.invoice_code',$request->input('invoice_code'))
                        ->groupBy('invoice_code')
                        ->get();
                        foreach ($pengurus as $keys) {

                            $members=DB::table('member')
                                    ->select('firebase')
                                    ->where('id',$keys->user_create)
                                    ->first();
                            $param=array(
                                'token'=>2,
                                'position'=>'CHECKOUT',
                                'id_komunitas'=>$keys->community_id,
                                'id_merchant'=>$keys->merchant_id
                            );
                            $data_push_notif = array(
                                'activity'=>'Marchendaise.HistoryMerchendaise',
                                'body'=>'Selamat, anda telah mendapatkan order',
                                'data'=> $param,
                                'icon'=>url('assets/uploads/settings/Untitled-1.png'),
                                'title'=>'Selamat',
                            );
                            $insertnotif = DB::table('notification')
                                            ->insert(array(
                                                'id_member'=>$keys->user_create,
                                                'type'=>2,
                                                'msg'=>'Selamat, anda telah mendapatkan order',
                                                'activity'=>'Marchendaise.HistoryMerchendaise',
                                                'param'  => json_encode($param),
                                                'created_at'  => date('Y-m-d H:i:s'),
                                                'status'  =>'false',
                        
                                            ));
                            
                            $x=ngepush_notif($members->firebase,$data_push_notif);
                            error_log($x);
                        }
                        

                        $message=array(
                            'status'    => 'success',
                            'code'      => 200,
                            'message'   => 'Success Paid',
                        );
                    }else{
                        $message=array(
                            'status'      =>'error',
                            'code'       =>'500',
                            'message'   =>'Failed Paid'
                        );
                    }
                }else{
                    $message=array(
                        'status'      =>'error',
                        'code'       =>'500',
                        'message'   =>'Upload Image Error'
                    );
                }
                
            }
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'Error'
            );
        }        
        
        return response()->json($message);
        
       
    }

    public function checkout(Request $request){
        // dd($request->input());
        
                   
        // dd($ongkir['rajaongkir']['results'][0]['costs']['1']['cost'][0]['value']);
        $userid = Auth::id();
        $cart = DB::table('cp_cart')
                ->where('user_id', $userid)
                ->where('status_invoice',0)
                ->count();
        if ($cart > 0) {
            $code = $userid.strtotime(date('Y-m-d H:i:s'));
            $chekout = DB::table('cp_checkout')
                        ->insertGetId(
                            [
                                'user_id' => $userid, 
                                'invoice_code' => $code, 
                                'total_product_rate' => $request->input('total_barang'), 
                                'total_expedition_rate' => $request->input('ongkos_kirim'), 
                                'sub_total' => $request->input('sub_total'), 
                                'address_id' => $request->input('address_id'), 
                                'nama_penerima' => $request->input('nama_penerima'), 
                                'no_hp' => $request->input('no_hp'), 
                                'metode_trx' => $request->input('metode_trx'), 
                                'courier' => $request->input('courier'), 
                                'service' => $request->input('service'), 
                                'status' => "PENDING", 
                            
                            ]
                        );
            // dd($chekout->invoice_code);
            $header_id = $chekout;
            if($chekout){
                $get_cart = DB::table('cp_cart')
                            ->where('user_id', $userid)
                            ->where('status_invoice',0)
                            ->get();
               
                $inv = DB::table('cp_checkout')
                            ->where('id', $header_id)
                            ->first();
                $dr = [];
               
                foreach ($get_cart as $key => $value) {

                    $origin = DB::table('communities')
                                ->where('id', $value->community_id)
                                ->first();

                    $origin = DB::table('communities')
                    ->where('id', $value->community_id)
                    ->first();

                    $cmty = DB::table('communities')->where('id',$value->community_id)->first();
            
                    $dr['noreq']=$cmty->noreq;
                    $dr['bank_name']=$cmty->bank_name;
                    $dr['atas_nama']=$cmty->atas_nama;
                                
                    $desti = DB::table('cp_pengiriman')
                                ->where('id', $request->input('address_id'))
                                ->first();

                    if(NULL==$desti){
                        return response()->json([
                            'status'  =>'eror',
                            'code'   => '200',
                            'message' => 'city id not found'
                        ]); 
                    }
                    $weight = DB::table('com_merchandise')
                                ->where('id', $value->merchant_id)
                                ->first();
                    $courier = $request->input('courier');
                    $insert_cd = DB::table('cp_checkout_detail')
                    ->insert(array(
                        'header_id'=>$header_id,
                        'cart_id'=>$value->id,
                        'merchant_id'=>$value->merchant_id,
                        'total'=>$weight->harga*$value->qty,
                        'courier'=>$request->input('courier'),
                        'courier_service'=>$request->input('service'),
                        'courier_cost' =>$request->input('ongkos_kirim')
                        // 'courier_service'=>$ongkir['rajaongkir']['results'][0]['costs']['1']['service'],
                        // 'courier_cost' =>$ongkir['rajaongkir']['results'][0]['costs']['1']['cost'][0]['value']
                    ));

                    $affected = DB::table('cp_cart')
                                ->where('id', $value->id)
                                ->update(['invoice_code' => $inv->invoice_code,'status_invoice' => 1, 'tanggal_checkout' => date('Y-m-d H:i:s')]);
                    
            }
                return response()->json([
                    'status'  =>'success',
                    'code'   => '200',
                    'message' => 'success',
                    'invoice_code' => $inv->invoice_code,
                    'noreq' => $dr['noreq'],
                    'bank_name' => $dr['bank_name'],
                    'atas_nama' => $dr['atas_nama']
                ]); 

            }
        }else{
            return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'message' => 'erorr'
            ]); 
        }
       
    }

    public function list_cart(Request $request){
        if (null!==$request->input('user_id')) {
            $id = $request->input('user_id');
        }else{
            $id="";
        }
       
        $d = DB::table('cp_cart')->select('*')
                ->where('user_id',$id)
                ->where('status_invoice',0)
                ->get();

        $dr = json_decode( json_encode($d), true);

        // dd($id);
        $total = 0;
        foreach ($d as $key => $v) {
            $merc = DB::table('com_merchandise')->where('id',$v->merchant_id)->first();
            $dr[$key]['merchant']['title'] = $merc->title;
            $dr[$key]['merchant']['harga'] = $merc->harga;
            $dr[$key]['merchant']['image'] = asset('storage/img/merchandise/'.$merc->community_id.'/'.$merc->image);

            $kominitas = DB::table('communities')->where('id',$v->community_id)->first();
            $st = $merc->harga*$v->qty;
            $total = $total + $st;

            
        }
       
        // $dr['TotalHarga'] = $total;

        return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'response' => $dr,
                'Total_harga' => $total,
                'id_city_community' => $kominitas->city_id
            ]); 
        
    }
    public function update_cart(Request $request){
        $validator= Validator::make($request->all(), [
            'community_id'   => 'required',
            'merchant_id'     => 'required',
            // 'qty'     => 'required',
           
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 200);
        }
        $idCom = $request->input('community_id');
        $idMerchant = $request->input('merchant_id');
        // dd($idCom);
        $idUser = Auth::id();
        $cek = DB::table('cp_cart')
                ->where('user_id',$idUser)
                ->where('community_id',$idCom)
                ->where('merchant_id',$idMerchant)
                ->where('status_invoice',0)
                ->first();
        
        if(null!==$cek){
            if($cek->community_id == $idCom){
                $cekmerch = DB::table('com_merchandise')
                            ->where('id',$idMerchant)
                            ->where('community_id',$idCom)
                            ->first();
                if(null!==$cekmerch){
                    $update = DB::table('cp_cart')
                        ->where('id', $cek->id)
                        ->update(['qty' => $cek->qty+1]);
                    

                    $message=array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'Success inserted the data',
                    );
                    
                }else{
                    $message=array(
                        'status'      =>'error',
                        'code'       =>'500',
                        'message'   =>'Merchandise not found'
                    );
                }
            }else{
                $message=array(
                    'status'    => 'error',
                    'code'      => 200,
                    'message'   => 'Your cart must be the same as the previous Community id',
                );
            } 
        }else{
            $cekmerch = DB::table('com_merchandise')
                        ->where('id',$idMerchant)
                        ->where('community_id',$idCom)
                        ->first();
            if(null!==$cekmerch){
                $insert=DB::table('cp_cart')
                    ->insert(array(
                        'user_id'=>$idUser,
                        'community_id'=>$idCom,
                        'merchant_id'=>$idMerchant,
                        'qty'=>1,
                        'status_invoice'=>0,
                        'tanggal_order' =>date('Y-m-d H:i:s')
                    ));
                
                   if($insert){
                    $message=array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'Success inserted the data',
                    );
                    }else{
                        $message=array(
                            'status'      =>'error',
                            'code'       =>'500',
                            'message'   =>'Failed inserted the data'
                        );
                    } 
            }else{
                $message=array(
                    'status'      =>'error',
                    'code'       =>'500',
                    'message'   =>'Merchandise not found'
                );
            }
            
        }
        
        return response()->json($message);
    }

    public function cart(Request $request){
        $validator= Validator::make($request->all(), [
            'community_id'   => 'required',
            'merchant_id'     => 'required',
            'qty'     => 'required',
           
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $idCom = $request->input('community_id');
        $idMerchant = $request->input('merchant_id');
        // dd($idCom);
        $idUser = Auth::id();
        $cek = DB::table('cp_cart')
                ->where('user_id',$idUser)
                ->where('status_invoice',0)
                ->first();
        
        if(null!==$cek){
            if($cek->community_id == $idCom){
                $cekmerch = DB::table('com_merchandise')
                            ->where('id',$idMerchant)
                            ->where('community_id',$idCom)
                            ->first();
                if(null!==$cekmerch){
                    $cekf = DB::table('cp_cart')
                        ->where('user_id',$idUser)
                        ->where('community_id',$idCom)
                        ->where('merchant_id',$idMerchant)
                        ->where('status_invoice',0)
                        ->first();
                    
                        if(null==$cekf){
                            
                            if($request->input('qty') != 0){
                               
                                $insert=DB::table('cp_cart')
                                ->insert(array(
                                    'user_id'=>$idUser,
                                    'community_id'=>$idCom,
                                    'merchant_id'=>$idMerchant,
                                    'qty'=>$request->input('qty'),
                                    'status_invoice'=>0,
                                    'tanggal_order' =>date('Y-m-d H:i:s')
                                ));
                                if($insert){
                                    $message=array(
                                        'status'    => 'success',
                                        'code'      => 200,
                                        'message'   => 'Success inserted the data',
                                    );
                                }else{
                                    $message=array(
                                        'status'      =>'error',
                                        'code'       =>'500',
                                        'message'   =>'Failed inserted the data'
                                    );
                                } 
                            }else{
                                
                                $message=array(
                                    'status'      =>'error',
                                    'code'       =>'500',
                                    'message'   =>'Qty 0'
                                );
                            }
                            
                        
                            
                        }else{
                            
                            if($request->input('qty') != 0){
                                // dd("update");
                                $update = DB::table('cp_cart')
                                ->where('id', $cekf->id)
                                ->update(['qty' => $request->input('qty')]);
                            }else{
                                // dd("deletesss");
                                $delete = DB::table('cp_cart')->where('id', $cekf->id)->delete();
                            }
                            

                            $message=array(
                                'status'    => 'success',
                                'code'      => 200,
                                'message'   => 'Success inserted the data',
                            );
                        }
                    
                }else{
                    $message=array(
                        'status'      =>'error',
                        'code'       =>'500',
                        'message'   =>'Merchandise not found'
                    );
                }
            }else{
                $message=array(
                    'status'    => 'error',
                    'code'      => 200,
                    'message'   => 'Your cart must be the same as the previous Community id',
                );
            } 
        }else{
            $cekmerch = DB::table('com_merchandise')
                        ->where('id',$idMerchant)
                        ->where('community_id',$idCom)
                        ->first();
            if(null!==$cekmerch){
                if($request->input('qty') != 0){
                    
                
                $insert=DB::table('cp_cart')
                    ->insert(array(
                        'user_id'=>$idUser,
                        'community_id'=>$idCom,
                        'merchant_id'=>$idMerchant,
                        'qty'=>$request->input('qty'),
                        'status_invoice'=>0,
                        'tanggal_order' =>date('Y-m-d H:i:s')
                    ));
                
                   if($insert){
                    $message=array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'Success inserted the data',
                    );
                }else{
                    $message=array(
                        'status'      =>'error',
                        'code'       =>'500',
                        'message'   =>'Failed inserted the data'
                    );
                } 
                }else{
                    $message=array(
                        'status'      =>'error',
                        'code'       =>'500',
                        'message'   =>'Qty 0'
                    );
                }
            }else{
                $message=array(
                    'status'      =>'error',
                    'code'       =>'500',
                    'message'   =>'Merchandise not found'
                );
            }
            
        }
        
        return response()->json($message);
    }

    public function create(Request $request){
        $validator= Validator::make($request->all(), [
            'image'   => 'required',
            'title'     => 'required',
            'harga'     => 'required',
            'weight'     => 'required',
            'status'     => 'required',
            'description'     => 'required'
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $idCom = $request->input('id_community');
        // dd($idCom);
        $idUser = Auth::id();

        if(null!==$request->file('image')){

            $file   =   $request->file('image');
            $imageName = md5($idCom.$idUser.''.date('Y-m-d H:i:s')).'.'.$file->getClientOriginalExtension();
            $ImageUpload = Image::make($file);
            $path = storage_path().'/app/public/img/merchandise/' . $idCom;
            $dir =  makeDirectory($path, $mode = 0777, true, true);

            $originalPath = storage_path('app/public/img/merchandise/'.$idCom.'/'.$imageName);
          
            $yes = $ImageUpload->save($originalPath);
             
            if($yes){
                $insert=DB::table('com_merchandise')
                    ->insert(array(
                        'community_id'=>$idCom,
                        'image'=>$imageName,
                        'title'=>$request->input('title'),
                        'harga'=>$request->input('harga'),
                        'weight'=>$request->input('weight'),
                        'description' =>$request->input('description'),
                        'status'  => $request->input('status'),
                        'user_create'  => $idUser,
                        'created_at'  => date('Y-m-d H:i:s'),

                    ));
                
                if($insert){
                    $message=array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'Success inserted the data',
                    );
                }else{
                    $message=array(
                        'status'      =>'error',
                        'code'       =>'500',
                        'message'   =>'Failed inserted the data'
                    );
                }
            }else{
                $message=array(
                    'status'      =>'error',
                    'code'       =>'500',
                    'message'   =>'Upload Image Error'
                );
            }
            
        }
        
        return response()->json($message);
    }

    public function edit(Request $request){
        $validator= Validator::make($request->all(), [
            'id_merch'   => 'required',
            
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $idMerch = $request->input('id_merch');
      
        // dd($idCom);
        $idUser = Auth::id();
       
        $check = DB::table('com_merchandise')->select('*')
                ->where('id',$idMerch)
                ->where('user_create',$idUser)
                ->first();
        if (NULL!== $check) {
            $idCom = $check->community_id;
            $insert=DB::table('com_merchandise')
                    ->where('id', $idMerch)
                    ->update(array(
                        'title'=>$request->input('title'),
                        'harga'=>$request->input('harga'),
                        'weight'=>$request->input('weight'),
                        'description' =>$request->input('description'),
                        'status'  => $request->input('status'),
                        'updated_at'  => date('Y-m-d H:i:s'),

                    ));
                
                if($insert){
                    if(null!==$request->file('image')){

                        $file   =   $request->file('image');
                        $imageName = md5($idCom.$idUser.''.date('Y-m-d H:i:s')).'.'.$file->getClientOriginalExtension();
                        $ImageUpload = Image::make($file);
                        $path = storage_path().'/app/public/img/merchandise/' . $idCom;
                        $dir =  makeDirectory($path, $mode = 0777, true, true);
            
                        $originalPath = storage_path('app/public/img/merchandise/'.$idCom.'/'.$imageName);
                      
                        $yes = $ImageUpload->save($originalPath);
                         
                        if($yes){
                            $up=DB::table('com_merchandise')
                                ->where('id', $idMerch)
                                ->update(array(
                                    'image'=>$imageName,
            
                                ));
                        }else{
                            $message=array(
                                'status'      =>'error',
                                'code'       =>'500',
                                'message'   =>'Upload Image Error'
                            );
                        }
                        
                    }
                    $message=array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'Success updated the data',
                    );
                }else{
                    $message=array(
                        'status'      =>'error',
                        'code'       =>'500',
                        'message'   =>'Failed updated the data'
                    );
                }
        }else{
            $message=array(
                'status'      =>'error',
                'code'       =>'500',
                'message'   =>'you dont have permission'
            );
        }
        
        
        return response()->json($message);
    }

    public function destroy(Request $request){
        $validator= Validator::make($request->all(), [  
            'id_merch'     => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 200);
        }
        $idMerch = $request->input("id_merch");
        $idUser = Auth::id();
        $check = DB::table('com_merchandise')->select('*')
                ->where('id',$idMerch)
                ->where('user_create',$idUser)
                ->count();
        if ($check > 0) {
            DB::table('com_merchandise')->where('id', $idMerch)->delete();
          
            return response()->json(
                [   'status'  =>'Success',
                    'code'   => '200',
                    'message' => "Delete data success",
                ], 200);
        }else{
            return response()->json(
                [   'status'  =>'Failed',
                    'code'   => '500',
                    'message' => "You dont have permission",
                ], 200);
        }
        

    }

    public function get_merchandise(Request $request){
        
        $data = DB::table('com_merchandise')->select('*');
        if (null!==$request->input('limit')) {
            $data = $data->limit($request->input('limit'));
        
        }

       
        if (null!==$request->input('id_user') && $request->input('community_id') == null ) {
            $getcom = DB::table('communites_anggota')->where('member_id',$request->input('id_user'))->groupBy('community_id')->get();
        //    dd($getcom);
            $tmp = [];
            foreach ($getcom as $key) {
            array_push($tmp,$key->community_id);
            }
            //dd($tmp);
            $data = $data->whereIn('community_id', $tmp);
           
        }else  if (null!==$request->input('id_user') && $request->input('community_id') !== null ) {
            $data = $data->where('community_id', $request->input('community_id'));
        }else if ($request->input('community_id') !== null ) {
            $data = $data->where('community_id', $request->input('community_id'));
        }
    
        $data = $data->where('stat_banned','!=', 'banned' )->orderBy('created_at','desc')->get();
        // dd($data);
        $dr = json_decode( json_encode($data), true);
     
        foreach($data as $key => $val){
            
            $dr[$key]['image']=asset('storage/img/merchandise/'.$val->community_id.'/'.$val->image);
           
        }
      
        return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'response' => $dr
            ]); 
       
    }

    
    public function get_merchandise_detail(Request $request){
        if (null!==$request->input('id_merch')) {
            $id = $request->input('id_merch');
        }else{
            $id="";
        }
       
        $d = DB::table('com_merchandise')->select('*')
                ->where('id',$id);
        if($d->count()>0){
            $d=$d->first();
        $d->image=asset('storage/img/merchandise/'.$d->community_id.'/'.$d->image);
        // $d->logo=asset('storage/img/community/'.$d->logo);
        $dr = json_decode( json_encode($d), true);
        

        return response()->json([
                'status'  =>'success',
                'code'   => '200',
                'response' => $dr
            ]); 
        }else{
            return response()->json([
                'status'  =>'error',
                'code'   => '404',
            ]); 
        }
                
        
    }

    public function search_merchandise(Request $request){

        // dd($request->input());
        $validator= Validator::make($request->all(), [
            'search'   => 'required',
            
        ]);

        if ($validator->fails()) {
            return response()->json(
                [   'status'  =>'error',
                    'code'   => '302',
                    'message' => $validator->messages(),
                ], 302);
        }
        $search= $request->input('search');

        $data = DB::table('com_merchandise')
                ->where('title', 'like', '%'.$search.'%')
                ->get();
                // dd(count($data));
        if(count($data) > 0){
            
        //    dd('masuk');
            $dr = json_decode( json_encode($data), true);
            
            foreach($data as $key => $val){
                
                $dr[$key]['image']=asset('storage/img/merchandise/'.$val->community_id.'/'.$val->image);
                
            }
            return response()->json([
                    'status'  =>'success',
                    'code'   => '200',
                    'response' => $dr
                ]); 
        }else{
            return response()->json([
                'status'  =>'eror',
                'code'   => '200',
                'response' => 'Data Not Found'
            ]); 
        }

       
    }
}
