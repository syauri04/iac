<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/dashboard', ['middleware' => 'auth', 'uses' => 'Backend\BackendController@dashboard']);
Route::get('/backend', 'Backend\BackendController@index')->name('backend');
Route::post('/authenticate','Backend\BackendController@authenticate');
Route::get('/auth-logout', function () {
    Auth::logout();
    return redirect('/backend');
});

Route::group(
    ['prefix' => 'master'],
    function () {
       

        Route::get('/on_boarding', ['middleware' => 'auth', 'uses' => 'Master\BoardingController@index',]);
        Route::get('/on_boarding/add', ['middleware' => 'auth', 'uses' => 'Master\BoardingController@add',])->name('add-boarding');
        Route::post('/on_boarding/save', ['middleware' => 'auth', 'uses' => 'Master\BoardingController@save',])->name('save-boarding');
        Route::get('/on_boarding/edit/{id?}', ['middleware' => 'auth', 'uses' => 'Master\BoardingController@edit']);
        Route::get('/on_boarding/delete/{id?}', ['middleware' => 'auth', 'uses' => 'Master\BoardingController@destroy']);
        Route::put('/on_boarding/update/{id?}', ['middleware' => 'auth', 'uses' => 'Master\BoardingController@update',]);

        Route::get('/banner', ['middleware' => 'auth', 'uses' => 'Master\BannerController@index',]);
        Route::get('/banner/add-banner', ['middleware' => 'auth', 'uses' => 'Master\BannerController@add',])->name('add-banner');
        Route::post('/banner/save-banner', ['middleware' => 'auth', 'uses' => 'Master\BannerController@save',])->name('save-banner');
        Route::get('/banner/edit/{id?}', ['middleware' => 'auth', 'uses' => 'Master\BannerController@edit']);
        Route::get('/banner/delete/{id?}', ['middleware' => 'auth', 'uses' => 'Master\BannerController@destroy']);
        Route::put('/banner/update-banner/{id?}', ['middleware' => 'auth', 'uses' => 'Master\BannerController@update',]);

        // Route::get('/banner_iklan', ['middleware' => 'auth', 'uses' => 'Master\BannerIklanController@index',]);
        // Route::get('/banner_iklan/add-banner', ['middleware' => 'auth', 'uses' => 'Master\BannerIklanController@add',])->name('add-banner');
        // Route::post('/banner_iklan/save-banner', ['middleware' => 'auth', 'uses' => 'Master\BannerIklanController@save',])->name('save-banner');
        // Route::get('/banner_iklan/edit/{id?}', ['middleware' => 'auth', 'uses' => 'Master\BannerIklanController@edit']);
        // Route::get('/banner_iklan/delete/{id?}', ['middleware' => 'auth', 'uses' => 'Master\BannerIklanController@destroy']);
        // Route::put('/banner_iklan/update-banner/{id?}', ['middleware' => 'auth', 'uses' => 'Master\BannerIklanController@update',]);

        Route::get('/merek', ['middleware' => 'auth', 'uses' => 'Master\MerekController@index']);
        Route::get('/merek/add-merek', ['middleware' => 'auth', 'uses' => 'Master\MerekController@add',])->name('add-merek');
        Route::post('/merek/save-merek', ['middleware' => 'auth', 'uses' => 'Master\MerekController@save',])->name('save-merek');
        Route::get('/merek/edit/{id?}', ['middleware' => 'auth', 'uses' => 'Master\MerekController@edit']);
        Route::get('/merek/delete/{id?}', ['middleware' => 'auth', 'uses' => 'Master\MerekController@destroy']);
        Route::put('/merek/update-merek/{id?}', ['middleware' => 'auth', 'uses' => 'Master\MerekController@update',]);

        Route::get('/type', ['middleware' => 'auth', 'uses' => 'Master\TypeController@index']);
        Route::get('/type/add-type', ['middleware' => 'auth', 'uses' => 'Master\TypeController@add',])->name('add-type');
        Route::post('/type/save-type', ['middleware' => 'auth', 'uses' => 'Master\TypeController@save',])->name('save-type');
        Route::get('/type/edit/{id?}', ['middleware' => 'auth', 'uses' => 'Master\TypeController@edit']);
        Route::get('/type/delete/{id?}', ['middleware' => 'auth', 'uses' => 'Master\TypeController@destroy']);
        Route::put('/type/update-type/{id?}', ['middleware' => 'auth', 'uses' => 'Master\TypeController@update',]);

        Route::get('/usia_kendaraan', ['middleware' => 'auth', 'uses' => 'Master\UKController@index']);
        Route::get('/usia_kendaraan/add', ['middleware' => 'auth', 'uses' => 'Master\UKController@add',])->name('add-uk');
        Route::post('/usia_kendaraan/save', ['middleware' => 'auth', 'uses' => 'Master\UKController@save',])->name('save-uk');
        Route::get('/usia_kendaraan/edit/{id?}', ['middleware' => 'auth', 'uses' => 'Master\UKController@edit']);
        Route::get('/usia_kendaraan/delete/{id?}', ['middleware' => 'auth', 'uses' => 'Master\UKController@destroy']);
        Route::put('/usia_kendaraan/update/{id?}', ['middleware' => 'auth', 'uses' => 'Master\UKController@update',]);

        Route::get('/jenis_model', ['middleware' => 'auth', 'uses' => 'Master\JMController@index']);
        Route::get('/jenis_model/add', ['middleware' => 'auth', 'uses' => 'Master\JMController@add',])->name('add-jm');
        Route::post('/jenis_model/save', ['middleware' => 'auth', 'uses' => 'Master\JMController@save',])->name('save-jm');
        Route::get('/jenis_model/edit/{id?}', ['middleware' => 'auth', 'uses' => 'Master\JMController@edit']);
        Route::get('/jenis_model/delete/{id?}', ['middleware' => 'auth', 'uses' => 'Master\JMController@destroy']);
        Route::put('/jenis_model/update/{id?}', ['middleware' => 'auth', 'uses' => 'Master\JMController@update',]);

        Route::get('/warna', ['middleware' => 'auth', 'uses' => 'Master\WarnaController@index']);
        Route::get('/warna/add-warna', ['middleware' => 'auth', 'uses' => 'Master\WarnaController@add',])->name('add-warna');
        Route::post('/warna/save-warna', ['middleware' => 'auth', 'uses' => 'Master\WarnaController@save',])->name('save-warna');
        Route::get('/warna/edit/{id?}', ['middleware' => 'auth', 'uses' => 'Master\WarnaController@edit']);
        Route::get('/warna/delete/{id?}', ['middleware' => 'auth', 'uses' => 'Master\WarnaController@destroy']);
        Route::put('/warna/update-warna/{id?}', ['middleware' => 'auth', 'uses' => 'Master\WarnaController@update',]);
    }
);
Route::group(
    ['prefix' => 'polling'],
    function () {
        Route::get('/polling_post', ['middleware' => 'auth', 'uses' => 'Master\PollingController@index',]);
        Route::get('/polling_post/add', ['middleware' => 'auth', 'uses' => 'Master\PollingController@add',])->name('add-polling');
        Route::post('/polling_post/save', ['middleware' => 'auth', 'uses' => 'Master\PollingController@save',])->name('save-polling');
        Route::get('/polling_post/edit/{id?}', ['middleware' => 'auth', 'uses' => 'Master\PollingController@edit']);
        Route::get('/polling_post/delete/{id?}', ['middleware' => 'auth', 'uses' => 'Master\PollingController@destroy']);
        Route::put('/polling_post/update/{id?}', ['middleware' => 'auth', 'uses' => 'Master\PollingController@update']);

    }
);

Route::group(
    ['prefix' => 'mem'],
    function () {
        Route::get('/member', ['middleware' => 'auth', 'uses' => 'Backend\MemberController@index']);
        Route::get('/member/detail/{id?}', ['middleware' => 'auth', 'uses' => 'Backend\MemberController@detail']);
        // Route::get('/member/edit/{id?}', ['middleware' => 'auth', 'uses' => 'Backend\MemberController@edit']);
        Route::get('/member/delete/{id?}', ['middleware' => 'auth', 'uses' => 'Backend\MemberController@destroy']);
        Route::put('/member/update/{id?}', ['middleware' => 'auth', 'uses' => 'Backend\MemberController@update']);

    }
);

Route::group(
    ['prefix' => 'community'],
    function () {
        Route::get('/community', ['middleware' => 'auth', 'uses' => 'Backend\CommunityController@index']);
        Route::get('/community/detail/{id?}', ['middleware' => 'auth', 'uses' => 'Backend\CommunityController@detail']);
        Route::get('/disclaimer/{id?}', ['middleware' => 'auth', 'uses' => 'Backend\CommunityController@disclaimer',])->name('disclaimer');
        Route::put('/update/{id?}', ['middleware' => 'auth', 'uses' => 'Backend\CommunityController@update']);
        Route::put('/update-status/{id?}', ['middleware' => 'auth', 'uses' => 'Backend\CommunityController@update_status']);
        Route::get('/community/edit/{id?}', ['middleware' => 'auth', 'uses' => 'Backend\CommunityController@edit']);
        Route::get('/community/delete/{id?}', ['middleware' => 'auth', 'uses' => 'Backend\CommunityController@destroy']);

        Route::get('/event', ['middleware' => 'auth', 'uses' => 'Backend\CommunityController@event']);
        Route::get('/event/delete/{id?}', ['middleware' => 'auth', 'uses' => 'Backend\CommunityController@destroy_event']);

        Route::get('/forum', ['middleware' => 'auth', 'uses' => 'Backend\CommunityController@forum']);
        Route::get('/forum/delete/{id?}', ['middleware' => 'auth', 'uses' => 'Backend\CommunityController@destroy_forum']);
        Route::get('/delete-gallery/{id?}/{communities_id?}', ['middleware' => 'auth', 'uses' => 'Backend\CommunityController@delete_gallery']);
        
        Route::get('/polling', ['middleware' => 'auth', 'uses' => 'Backend\CommunityController@polling']);
        
       
    }
);

Route::group(
    ['prefix' => 'meme'],
    function () {
        Route::get('/group', ['middleware' => 'auth', 'uses' => 'Backend\GroupController@index']);
        Route::match(['get', 'post'],'/group/access/{id}', ['middleware' => 'auth', 'uses' => 'Backend\GroupController@access',]);

    }
);

Route::group(
    ['prefix' => 'merch'],
    function () {
        Route::get('/merchandise', ['middleware' => 'auth', 'uses' => 'Backend\MerchController@index']);
        Route::get('/merchandise/delete/{id?}', ['middleware' => 'auth', 'uses' => 'Backend\MerchController@destroy']);
        Route::get('/disclaimer/{id?}', ['middleware' => 'auth', 'uses' => 'Backend\MerchController@disclaimer',])->name('disclaim_merch');
        Route::put('/update-status/{id?}', ['middleware' => 'auth', 'uses' => 'Backend\MerchController@update_status']);

    }
);


Auth::routes(['register' => false]);

// Route::get('/home', 'HomeController@index')->name('home');

