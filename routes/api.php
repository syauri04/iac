<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(
    ['prefix' => 'member'],
    function () {
        Route::post('/register','MemberController@register');
        Route::post('/login','MemberController@login');
        Route::post('/verify-sms','MemberController@verify_sms');
        Route::post('/send-code','MemberController@kirim_ulang');
        Route::get('/detail','MemberController@detail');
        Route::middleware('auth:api')->post('/edit','MemberController@edit');
        Route::post('/search-member','MemberController@search_member');

        Route::post('/add-advertisement','MemberController@add_advertisement');
        Route::post('/edit-advertisement','MemberController@edit_advertisement');
        Route::get('/delete-advertisement','MemberController@delete_advertisement');
        Route::get('/detail-advertisment','MemberController@detail_advertisment');
        Route::get('/list-advertisement-member','MemberController@advertsiemnt_list_member');
        Route::get('/list-advertisement-home','MemberController@advertsiemnt_list_home');
    }
);

Route::group(
    ['prefix' => 'user'],
    function () {
        Route::post('/read_notif','MemberController@read_notif');
        Route::get('/notification','MemberController@list_notification');
        Route::get('/count-notif','MemberController@count_notif');
        Route::middleware('auth:api')->post('/report','MemberController@report');
    }
);

Route::group(
    ['prefix' => 'alamat'],
    function () {
        Route::get('/province','AlamatController@get_province');
        Route::get('/city','AlamatController@get_city');
      
    }
);

Route::group(
    ['prefix' => 'community'],
    function () {
        Route::middleware('auth:api')->post('/add-community','CommunityController@create');
        Route::post('/edit-community','CommunityController@edit');
        Route::middleware('auth:api')->post('/add-gallery','CommunityController@upload_gallery');
        Route::middleware('auth:api')->get('/delete-gallery','CommunityController@delete_gallery');
        Route::get('/get-community','CommunityController@get_community');
        Route::get('/detail-community','CommunityController@detail_community');
        Route::middleware('auth:api')->post('/join-community','CommunityController@join_community');
        Route::post('/search-community','CommunityController@search_community');
        Route::get('/get-gallery','CommunityController@get_gallery');
        Route::middleware('auth:api')->post('/accept-join','CommunityController@accept_join');
        Route::middleware('auth:api')->post('/galer-likeordis','CommunityController@galer_likeordis');

    }
);

Route::group(
    ['prefix' => 'event'],
    function () {
        Route::middleware('auth:api')->post('/add-event','EventController@create');
        Route::middleware('auth:api')->post('/edit-event','EventController@edit');
        Route::middleware('auth:api')->post('/delete-event','EventController@delete');
        Route::get('/get-event/{stat?}/{limit?}','EventController@get_event');
        Route::get('/get-detail-event','EventController@detail_event');
        Route::middleware('auth:api')->post('/event-likeordis','EventController@event_likeordis');
        Route::middleware('auth:api')->post('/event-tertarik','EventController@event_tertarik');
    }
);

Route::group(
    ['prefix' => 'forum'],
    function () {
        Route::middleware('auth:api')->post('/add-forum','ForumController@create');
        Route::middleware('auth:api')->post('/edit-forum','ForumController@edit');
        Route::middleware('auth:api')->post('/delete-forum','ForumController@destroy');
        Route::middleware('auth:api')->post('/add-comment','ForumController@comment');
        Route::get('/get-forum','ForumController@get_forum');
        Route::get('/get-forum-detail','ForumController@get_forum_detail');
    }
);

Route::group(
    ['prefix' => 'merchandise'],
    function () {
        Route::middleware('auth:api')->post('/add-merchandise','MerchandiseController@create');
        Route::middleware('auth:api')->post('/edit-merchandise','MerchandiseController@edit');
        Route::middleware('auth:api')->post('/delete-merchandise','MerchandiseController@destroy');
        Route::get('/get-merchandise','MerchandiseController@get_merchandise');
        Route::get('/get-merchandise-detail','MerchandiseController@get_merchandise_detail');
        Route::post('/search-merchendaise','MerchandiseController@search_merchandise');
        
        Route::middleware('auth:api')->post('/add-cart','MerchandiseController@cart');
        Route::middleware('auth:api')->post('/update-cart','MerchandiseController@update_cart');
        
        Route::get('/list-cart','MerchandiseController@list_cart');
        Route::middleware('auth:api')->post('/checkout','MerchandiseController@checkout');
        Route::middleware('auth:api')->post('/bayar','MerchandiseController@bayar');
        Route::get('/list-trx','MerchandiseController@list_trx');
        Route::get('/detail-trx','MerchandiseController@detail_trx');
        
        Route::middleware('auth:api')->post('/update-status-trx','MerchandiseController@update_status_trx');


        Route::middleware('auth:api')->post('/add-pengiriman','MerchandiseController@add_pengiriman');
        Route::post('/delete-pengiriman','MerchandiseController@delete_pengiriman');
        Route::get('/list-pengiriman','MerchandiseController@list_pengiriman');
    }
);

Route::group(
    ['prefix' => 'polling'],
    function () {
        Route::middleware('auth:api')->post('/add-polling','PollingController@create');
        Route::middleware('auth:api')->post('/edit-polling','PollingController@edit');
        Route::middleware('auth:api')->post('/delete-polling','PollingController@delete');
        Route::get('/get-polling','PollingController@get_polling');
        Route::middleware('auth:api')->post('/polling-post','PollingController@polling_post');
    }
);



Route::group(
    ['prefix' => 'banner'],
    function () {
        Route::get('/get-banner','BannerController@data');
        // Route::get('/get-banner-iklan','BannerController@data_iklan');
        // Route::middleware('auth:api')->get('/','BannerController@data');
    }
);

Route::group(
    ['prefix' => 'boarding'],
    function () {
        Route::get('/get-boarding','BannerController@boarding');
    }
);





