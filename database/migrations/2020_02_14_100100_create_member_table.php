<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firebase')->nullable();
            $table->string('token');
            $table->string('nama_lengkap');
            $table->string('alamat')->nullable();
            $table->string('telp')->nullable();
            $table->string('email');
            $table->string('password')->nullable();
            $table->string('province_id')->nullable();
            $table->string('city_id')->nullable();
            $table->string('postcode')->nullable();
            $table->string('profile_pic')->nullable();
            $table->enum('activate',['active','banned']);
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member');
    }
}
