<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommunitiesDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communities_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('communities_id')->unique();
            $table->longText('description');
            $table->string('merk',255);
            $table->string('type',255);
            $table->string('jenis_model',255);
            $table->string('usia_kendaraan',255);
            $table->enum('jml_silinder',['2 Silinder', '3 Silinder', '4 Silinder', '6 Silinder']);
            $table->string('warna',50);
            $table->enum('jenis_mesin',['Bensin', 'Diesel']);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communities_detail');
    }
}
