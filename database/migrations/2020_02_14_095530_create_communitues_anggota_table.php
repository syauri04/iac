<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommunituesAnggotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communites_anggota', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('comm_id');
            $table->integer('member_id');
            $table->enum('level',['Ketua', 'Pengurus', 'Member']);
            $table->string('photo_ktp');
            $table->enum('status',['Pending', 'Verified', 'Not Verified']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communites_anggota');
    }
}
