(function($) {
	var oLink = $('#ownLink').text();
	

	$('#wysihtml5').wysihtml5();
	$(".chosen").chosen({
		// disable_search_threshold: 10,
		no_results_text: "Oops, nothing found!",
		width: "100%",
		height: "110%",
	});
	// console.log($('#idHewan').val());
	$.get( oLink+'getPhoto/'+$('#id').val(), function( data ) {
		// console.log(data);
		var d = JSON.parse(data);
	
			var l = d.length;
			// var gA = "";
			for(var i = 0; i < l; i++){
				var t = '<div class="containerP">';
					t += '<img src="'+$('.ownlink').text()+'assets/img/gallery/small/'+d[i].image+'" alt="Avatar" class="image" >';
					t += '<div class="middle">';
					t += '<div class="text"><a href="" class="deletePhoto" data-id="'+d[i].id+'"><i class="fa fa-trash fa-2x"></i></a></div>';
					t += '<div class="text"><a href="" class="starsPhoto" id="sp" data-id="'+d[i].id+'"><i class="fa fa-star fa-2x"></i></a></div>';
					t += '</div>';
					t += '</div>';
				$('.imageRemove').append(t);
			}
	
	});

	$.get( oLink+'getPhotoF/'+$('#id').val(), function( data ) {
		// console.log(data);
		var d = JSON.parse(data);
		console.log(d.featuredimg);
		var t = '<div class="containerP">';
			t += '<img src="'+$('.ownlink').text()+'assets/img/gallery/small/'+d.featuredimg+'" alt="Avatar" class="image" >';
			t += '<div class="middle">';
			t += '<div class="text"><a href="" class="deletePhotoF" data-id="'+d.id+'"><i class="fa fa-trash fa-2x"></i></a></div>';
			// t += '<div class="text"><a href="" class="starsPhoto" id="sp" data-id="'+d[i].id+'"><i class="fa fa-star fa-2x"></i></a></div>';
			t += '</div>';
			t += '</div>';
		$('.imageRemoveF').append(t);

	
	});

	setTimeout(function(){
		$('.deletePhoto').each(function(){
			$(this).click(function(e){
				e.preventDefault(); 
				// alert("asdd");
				var p = $(this).parent().parent().parent()[0];
				$.post(oLink+'deletePhoto',{'id' : $(this).data('id')},function(e){
					var m = JSON.parse(e);
					console.log(m);
					// alert(m);
					if(m == 'd'){
						p.remove();
						
					} else {
						alert('Delete Error');
					}
				});
			});
		});
	}, 300);
	setTimeout(function(){
		$('.starsPhoto').each(function(index){
			
			$(this).on("click", function(e){
				e.preventDefault(); 
				// alert("masuk");
				var p = $(this).parent().parent().parent()[0];
				$.post(oLink+'starsPhoto',{'id' : $(this).data('id')},function(e){
					var m = JSON.parse(e);
					console.log(m);
					// alert(m);
					if(m != 'd'){
						alert('Change Error');
					}else{
						alert('Featued Image Success');
					}
				});
			});
		});
	}, 300);
})(jQuery);



