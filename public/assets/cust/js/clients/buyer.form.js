var ownLink = $('#ownLink').text();

$(".chosen").chosen({
    // disable_search_threshold: 10,
    no_results_text: "Oops, nothing found!",
    width: "100%",
    height: "110%",
});

var handleActionForm = function(){
    "use strict";

    $('#provinsiID').on('change',function(){
        $.post(ownLink+'getCity',{ id: $(this).val() },function(x){
            $('#kotaID').html(x);
            $('#kotaID').trigger("chosen:updated");
        });
    })

}

var TableManageDefault = function () {
	"use strict";
    return {
        //main function
        init: function () {
            handleActionForm();
        }
    };
}();