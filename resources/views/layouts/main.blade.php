<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
  
<head>
	<meta charset="utf-8" />
	<title>IAC | Dashboard </title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="{{ URL::asset('assets/def/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/def/plugins/bootstrap/4.0.0/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/def/plugins/font-awesome/5.0/css/fontawesome-all.min.css') }}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/def/plugins/animate/animate.min.css') }}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/def/css/default/style.min.css') }}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/def/css/default/style-responsive.min.css') }}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/def/css/default/theme/default.css') }}" rel="stylesheet" id="theme" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->
    <link href="{{ URL::asset('assets/def/plugins/jquery-jvectormap/jquery-jvectormap.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/def/plugins/bootstrap-calendar/css/bootstrap_calendar.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/def/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/def/plugins/nvd3/build/nv.d3.css') }}" rel="stylesheet" />
	<!-- ================== END PAGE LEVEL CSS STYLE ================== -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{ URL::asset('assets/def/plugins/pace/pace.min.js') }}"></script>
	<script src="{{ URL::asset('assets/def/plugins/jquery/jquery-3.2.1.min.js') }}"></script>
	<script src="{{ URL::asset('assets/def/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
	<script src="{{ URL::asset('assets/def/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js') }}"></script>
	<!-- ================== END BASE JS ================== -->
</head>
<body>
	
	<div id="ownLink" style="display: none;"></div>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		<!-- begin #header -->
		<div id="header" class="header navbar-default">
			<!-- begin navbar-header -->
			<div class="navbar-header">
				<a href="" class="navbar-brand"><img width="50" src="{{ asset('assets/def/img/logo/logo-admin.png') }}"></a>
				<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- end navbar-header -->
			
			<!-- begin header-nav -->
			<ul class="navbar-nav navbar-right">
				<li>
					<form class="navbar-form">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Enter keyword" />
							<button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
						</div>
					</form>
				</li>
				<li class="dropdown navbar-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<img src="{{ asset('assets/def/img/user/user-13.jpg') }}" alt="" /> 
						<span class="d-none d-md-inline">Admin</span> <b class="caret"></b>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a href="javascript:;" class="dropdown-item">Edit Profile</a>
						<!-- <a href="javascript:;" class="dropdown-item"><span class="badge badge-danger pull-right">2</span> Inbox</a> -->
						<!-- <a href="javascript:;" class="dropdown-item">Calendar</a> -->
						<!-- <a href="javascript:;" class="dropdown-item">Setting</a> -->
						<div class="dropdown-divider"></div>
						<a href="{{ url('auth-logout') }}" class="dropdown-item">Log Out</a>
					</div>
				</li>
			</ul>
			<!-- end header navigation right -->
		</div>
		<!-- end #header -->

        <!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<a href="javascript:;" data-toggle="nav-profile">
							<div class="cover with-shadow"></div>
							<div class="image">
								<img src="{{asset('assets/def/img/user/user-13.jpg')}}" alt="" />
							</div>
							<div class="info">
								<b class="caret pull-right"></b>
								IAC
								<small>CMS</small>
							</div>
						</a>
					</li>
					<li>
						<ul class="nav nav-profile">
                            <li><a href="javascript:;"><i class="fa fa-cog"></i> Settings</a></li>
                            <li><a href="javascript:;"><i class="fa fa-pencil-alt"></i> Send Feedback</a></li>
                            <li><a href="javascript:;"><i class="fa fa-question-circle"></i> Helps</a></li>
                        </ul>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				{{dnmcMenu(_get_menu(set_sess_menu()) ?? '',true)}}
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>

		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->
		
		

		<!-- begin #content -->
		<div id="content" class="content">
            @yield('content')
		</div>
		<!-- end #content -->
	
	<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{ URL::asset('assets/def/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js') }}"></script>
	<script src="{{ URL::asset('assets/def/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
	<script src="{{ URL::asset('assets/def/plugins/js-cookie/js.cookie.js') }}"></script>
	<script src="{{ URL::asset('assets/def/js/theme/default.min.js') }}"></script>
	<script src="{{ URL::asset('assets/def/js/apps.js') }}"></script>
	<!-- ================== END BASE JS ================== -->
	
	<script>
		
		$(document).ready(function() {
			App.init();
		});
	</script>
	@if (session('status'))
	<div id="gritter-notice-wrapper"><div id="gritter-item-1" class="gritter-item-wrapper my-sticky-class" style="" role="alert"><div class="gritter-top"></div><div class="gritter-item"><a class="gritter-close" href="#" tabindex="1" style="display: none;">Close Notification</a><div class="gritter-with-image"><span class="gritter-title">Status</span><p>{{ session('status') }}.</p></div><div style="clear:both"></div></div><div class="gritter-bottom"></div></div></div>
	@endif
</body>
</html>
		
		
		
		
