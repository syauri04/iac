@extends('layouts/main')
@section('content')
<!-- begin row -->
<div class="row">
		<!-- begin col-6 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Form</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                @if(isset($val))
                    <form enctype="multipart/form-data" id="myForm" action="/master/banner/update-banner/{{$val->id_banner ?? ''}}" method="POST" data-toggle="validator">
                    @method('PUT')
                @else
                    <form enctype="multipart/form-data" id="myForm" action="{{route('save-banner')}}" method="POST" data-toggle="validator">    
                @endif
                     {{ csrf_field() }}
                       
                	    
                		<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Title</label>
							<div class="col-md-9">
								<input type="text" name="title" required class="form-control m-b-5" autocomplete="off" value="{{ $val->title ?? '' }}" />
							</div>
						</div>
                        <div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">URL Mobile</label>
							<div class="col-md-9">
								<input type="text" name="url_mobile" required class="form-control m-b-5" autocomplete="off" value="{{$val->url_mobile ?? ''}}" />
							</div>
						</div>
                        <div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">URL Web</label>
							<div class="col-md-9">
								<input type="text" name="url_web" required class="form-control m-b-5" autocomplete="off" value="{{$val->url_web ?? ''}}" />
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Image Banner <br/></label>
							<div class="col-md-9">
                              <span class="btn btn-success fileinput-button">
                                <i class="fa fa-plus"></i>
                                <span>Choose files...</span>
                                <input type="file" name="image"  id="image" value="{{$val->image ?? ''}}" onchange="loadFile(event)">
                              </span>
                              <span class="text-danger"> {{ $errors->first('image') }}</span>
                              <br>
                              
                              <img id="output" class="ouput_image_input" src="@if(isset($val)){{ asset('storage/img/banner/'.$val->image) }} @endif" />
                            </div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2"></label>
							<div class="col-md-9">
								<a href="{{url()->previous()}}" class="btn btn-white cancel">Cancel</a>
								<button type="submit" class="btn btn-primary simpan">Simpan</button>
							</div>
						</div>
					</form>
                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- Edn col-6 -->
	</div>
<!-- End Row -->


<link href="{{ URL::asset('assets/def/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/def/plugins/chosen/css/chosen.min.css')}}" rel="stylesheet" />

<script src="{{ URL::asset('assets/def/plugins/chosen/js/chosen.jquery.min.js')}}"></script>
<script src="{{ URL::asset('assets/def/plugins/ckeditor/ckeditor.js')}}"></script>
<script src="{{ URL::asset('assets/def/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script src="{{ URL::asset('assets/cust/js/posts/posts-form.js')}}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<!-- <script type="text/javascript">
    
	$('.simpan').on('click',function(){
       
		$('form').submit();
	});
</script> -->


@endsection