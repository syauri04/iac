@extends('layouts/main')
@section('content')
  <!-- begin row -->
  <div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
      <!-- begin panel -->
      <div class="panel panel-inverse">
        <div class="panel-heading">
          <div class="panel-heading-btn">
            <?php isset($links)?getLink2($links):'';?> 
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
          </div>
          <h4 class="panel-title">User Group Access</h4>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table id="data-table" class="table table-striped table-bordered">
              <thead>
                <tr>
                    <th width="30">No</th>
                    <th>Nama Grup Pengguna</th>
                    <th>Deskripsi</th>
                    <th>Status</th>
                    <th width="150">Action</th>
                </tr>
              </thead>
              <tbody>
                
                @if (count($data) > 0)
                  
                  @foreach ($data as $d)
                    <tr valign="top">
                      <td>{{ $loop->iteration}}</td>
                      <td>{{ $d->ag_group_name }}</td>
                      <td>{{ $d->ag_group_desc }}</td>
                      <td>@php echo ($d->ag_group_status=='1')?'Active':'Non Active'; @endphp</td>
                      <td align="center">
                            {{ LinkActions($links_table_item,$d->ag_id) }}
                      </td>
                    </tr>
                    @endforeach
                @endif 
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- end panel -->
    </div>
    <!-- end col-12 -->
  </div>
  <!-- end row -->
  @endsection
  