<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>Login Page</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="{{ URL::asset('assets/def/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/def/plugins/bootstrap/4.0.0/css/bootstrap.min.css')}}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/def/plugins/font-awesome/5.0/css/fontawesome-all.min.css')}}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/def/plugins/animate/animate.min.css')}}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/def/css/default/style.min.css')}}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/def/css/default/style-responsive.min.css')}}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/def/css/default/theme/default.css')}}" rel="stylesheet" id="theme" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{ URL::asset('assets/def/plugins/pace/pace.min.js')}}"></script>
	<!-- ================== END BASE JS ================== -->
</head>
<body class="pace-top">
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<div class="login-cover">
	    <div class="login-cover-image" style="background-image: url(assets/def/img/login-bg/login-bg-12.jpg)" data-id="login-cover-image"></div>
	    <div class="login-cover-bg"></div>
	</div>
	<!-- begin #page-container -->
	<div id="page-container" class="fade">
	    <!-- begin login -->
        <div class="login login-v2" data-pageload-addclass="animated fadeIn">
            <!-- begin brand -->
            <div class="login-header">
                <div class="brand">

				<img width="80" style="margin-right:30px" src="{{ asset('assets/def/img/logo/logo-admin.png') }}"><b>IAC</b> ADMIN
                    <!-- <small>Responsive , Fast & Reliable</small> -->
                </div>
                <div class="icon">
                    <i class="fa fa-lock"></i>
                </div>
            </div>
            <!-- end brand -->
            <!-- begin login-content -->
            <div class="login-content">
				@if (session('status'))
					<div class="alert alert-danger fade show">
						<span class="close" data-dismiss="alert">×</span>
						<strong>{{ session('status') }}</strong>
					</div>
				@endif
                <form action="{{url('authenticate')}}" method="post" class="margin-bottom-0">
                    {{ csrf_field() }}
                    <div class="form-group m-b-20">
                        <input type="text" class="form-control form-control-lg" name="username" placeholder="Username" required />
                    </div>
                    <div class="form-group m-b-20">
                        <input type="password" class="form-control form-control-lg" name="password" placeholder="Password" required />
                    </div>
                    <div class="checkbox checkbox-css m-b-20">
                        <input type="checkbox" id="remember_checkbox" /> 
                        <label for="remember_checkbox">
                        	Remember Me
                        </label>
                    </div>
                    <div class="login-buttons">
                        <button type="submit" class="btn btn-success btn-block btn-lg">SIGN IN</button>
                    </div>
                    <div class="m-t-20">
                        Not a user yet? Click <a href="javascript:;">here</a> to register.
                    </div>
                </form>
            </div>
            <!-- end login-content -->
        </div>
        <!-- end login -->        
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{ URL::asset('assets/def/plugins/jquery/jquery-3.2.1.min.js')}}"></script>
	<script src="{{ URL::asset('assets/def/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
	<script src="{{ URL::asset('assets/def/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js')}}"></script>
	<!--[if lt IE 9]>
		<script src="../assets/crossbrowserjs/html5shiv.js"></script>
		<script src="../assets/crossbrowserjs/respond.min.js"></script>
		<script src="../assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="{{ URL::asset('assets/def/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
	<script src="{{ URL::asset('assets/def/plugins/js-cookie/js.cookie.js')}}"></script>
	<script src="{{ URL::asset('assets/def/js/theme/default.min.js')}}"></script>
	<script src="{{ URL::asset('assets/def/js/apps.min.js')}}"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="{{ URL::asset('assets/def//js/demo/login-v2.demo.min.js')}}"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->

	<script>
		$(document).ready(function() {
			App.init();
			LoginV2.init();
		});
	</script>
</body>
</html>
