@extends('layouts/main')
@section('content')
<div class="panel panel-inverse" data-sortable-id="table-basic-1">
	<!-- begin panel-heading -->
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">List Claim</h4>
    </div>
    <!-- end panel-heading -->
    <!-- begin panel-body -->
    <div class="panel-body">
    	<!-- begin table-responsive -->
		<table id="example" class="table table-striped ">
			<thead>
				<tr>
					<th>#</th>
					<th>User</th>
					<th>Alasan</th>
					<th>Create At</th>
					
				</tr>
			</thead>
			<tbody>
			
            @if (count($data) > 0)
                @foreach ($data as $d)
				
                <tr>
					<td>{{$loop->iteration}}</td>
					<td>{{$d->id_member}}</td>
					<td>{{$d->alasan}}</td>
					<td>{{$d->created_at}}</td>
                    
				</tr>
                @endforeach
            @endif 
			
			</tbody>
		</table>
		<form enctype="multipart/form-data" id="myForm" action="/community/update-status/{{$idcom}}" method="POST" data-toggle="validator">
		@method('PUT')
		{{ csrf_field() }}
			<div class="form-group row m-b-15">
				<label class="col-form-label col-md-2">Banned Commnity?</label>
				<div class="col-md-3">
				<select id="cars" class="form-control m-b-5" name="status">
					<option value="">-- Change Status --</option>
					<option value="banned">Banned</option>
					<option value="active">Active</option>
				
				</select>
					
				</div>
			</div>
		
			<div class="form-group row m-b-15">
				<label class="col-form-label col-md-2"></label>
				<div class="col-md-9">
					<a href="{{url()->previous()}}" class="btn btn-white cancel">Cancel</a>
					<button type="submit" class="btn btn-primary simpan">Simpan</button>
				</div>
			</div>
		</form>
		<!-- end table-responsive -->
    </div>
    <!-- end panel-body -->
</div>

<link href="{{ URL::asset('assets/def/plugins/DataTables/media/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/def/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css')}}" rel="stylesheet" />

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{ URL::asset('assets/def/plugins/DataTables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{ URL::asset('assets/def/plugins/DataTables/media/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('assets/def/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ URL::asset('assets/def/js/demo/table-manage-default.demo.min.js')}}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script type="text/javascript">
	TableManageDefault.init();
</script>
@endsection