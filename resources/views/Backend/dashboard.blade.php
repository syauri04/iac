@extends('layouts/main')
<!-- begin row -->
@section('content')
<div class="row">
    <!-- begin col-3 -->
    <div class="col-lg-3 col-md-6">
        <div class="widget widget-stats bg-gradient-green">
            <div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div>
            <div class="stats-content">
                <div class="stats-title">TOTAL COMMUNITY</div>
                <div class="stats-number">{{$tot_kom}}</div>
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 70.1%;"></div>
                </div>
                <!-- <div class="stats-desc">Better than last week (70.1%)</div> -->
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-lg-3 col-md-6">
        <div class="widget widget-stats bg-gradient-blue">
            <div class="stats-icon stats-icon-lg"><i class="fa fa-dollar-sign fa-fw"></i></div>
            <div class="stats-content">
                <div class="stats-title">TOTAL MEMBER</div>
                <div class="stats-number">{{$tot_mem}}</div>
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 40.5%;"></div>
                </div>
                <!-- <div class="stats-desc">Better than last week (40.5%)</div> -->
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-lg-3 col-md-6">
        <div class="widget widget-stats bg-gradient-purple">
            <div class="stats-icon stats-icon-lg"><i class="fa fa-archive fa-fw"></i></div>
            <div class="stats-content">
                <div class="stats-title">TOTAL ACARA</div>
                <div class="stats-number">{{$tot_event}}</div>
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 76.3%;"></div>
                </div>
                <!-- <div class="stats-desc">Better than last week (76.3%)</div> -->
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-lg-3 col-md-6">
        <div class="widget widget-stats bg-gradient-black">
            <div class="stats-icon stats-icon-lg"><i class="fa fa-comment-alt fa-fw"></i></div>
            <div class="stats-content">
                <div class="stats-title">TOTAL FORUM</div>
                <div class="stats-number">{{$tot_forum}}</div>
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 54.9%;"></div>
                </div>
                <!-- <div class="stats-desc">Better than last week (54.9%)</div> -->
            </div>
        </div>
    </div>
    <!-- end col-3 -->
</div>
<!-- end row -->
            
<!-- begin row -->

<!-- end row -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js"></script>
<script src="{{ URL::asset('assets/def/plugins/nvd3/build/nv.d3.js') }}"></script>
<script src="{{ URL::asset('assets/def/plugins/jquery-jvectormap/jquery-jvectormap.min.js') }}"></script>
<script src="{{ URL::asset('assets/def/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js') }}"></script>
<script src="{{ URL::asset('assets/def/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js') }}"></script>
<script src="{{ URL::asset('assets/def/plugins/gritter/js/jquery.gritter.js') }}"></script>
<script src="{{ URL::asset('assets/def/js/demo/dashboard-v2.min.js') }}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function() {

        DashboardV2.init();
    });
</script>
@endsection

