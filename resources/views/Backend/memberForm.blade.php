@extends('layouts/main')
@section('content')
<!-- begin row -->
<div class="row">
		<!-- begin col-6 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Form</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                @if(isset($val))
                    <form enctype="multipart/form-data" id="myForm" action="/mem/member/update/{{$val->id ?? ''}}" method="POST" data-toggle="validator">
                    @method('PUT')
                @else
                    <form enctype="multipart/form-data" id="myForm" action="{{route('save-polling')}}" method="POST" data-toggle="validator">    
                @endif
                     {{ csrf_field() }}
					 	
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Nama Lengkap</label>
							<div class="col-md-9">
							    <!-- <textarea name="summary" required class="form-control m-b-5">{{ $val->summary ?? '' }}</textarea> -->
								<input type="text" name="topik" required class="form-control m-b-5" autocomplete="off" value="{{ $val->topik ?? '' }}" />
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Jawaban A</label>
							<div class="col-md-9">
							    <!-- <textarea name="summary" required class="form-control m-b-5">{{ $val->summary ?? '' }}</textarea> -->
								<input type="text" name="jawaban_a" required class="form-control m-b-5" autocomplete="off" value="{{ $val->jawaban_a ?? '' }}" />
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Jawaban B</label>
							<div class="col-md-9">
							    <!-- <textarea name="summary" required class="form-control m-b-5">{{ $val->summary ?? '' }}</textarea> -->
								<input type="text" name="jawaban_b" required class="form-control m-b-5" autocomplete="off" value="{{ $val->jawaban_b ?? '' }}" />
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Jawaban C</label>
							<div class="col-md-9">
							    <!-- <textarea name="summary" required class="form-control m-b-5">{{ $val->summary ?? '' }}</textarea> -->
								<input type="text" name="jawaban_c"  class="form-control m-b-5" autocomplete="off" value="{{ $val->jawaban_c ?? '' }}" />
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Jawaban D</label>
							<div class="col-md-9">
							    <!-- <textarea name="summary" required class="form-control m-b-5">{{ $val->summary ?? '' }}</textarea> -->
								<input type="text" name="jawaban_d"  class="form-control m-b-5" autocomplete="off" value="{{ $val->jawaban_d ?? '' }}" />
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Jawaban E</label>
							<div class="col-md-9">
							    <!-- <textarea name="summary" required class="form-control m-b-5">{{ $val->summary ?? '' }}</textarea> -->
								<input type="text" name="jawaban_e"  class="form-control m-b-5" autocomplete="off" value="{{ $val->jawaban_e ?? '' }}" />
							</div>
						</div>
					
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2"></label>
							<div class="col-md-9">
								<a href="{{url()->previous()}}" class="btn btn-white cancel">Cancel</a>
								<button type="submit" class="btn btn-primary simpan">Simpan</button>
							</div>
						</div>
					</form>
                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- Edn col-6 -->
	</div>
<!-- End Row -->


<link href="{{ URL::asset('assets/def/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/def/plugins/chosen/css/chosen.min.css')}}" rel="stylesheet" />

<script src="{{ URL::asset('assets/def/plugins/chosen/js/chosen.jquery.min.js')}}"></script>
<script src="{{ URL::asset('assets/def/plugins/ckeditor/ckeditor.js')}}"></script>
<script src="{{ URL::asset('assets/def/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script src="{{ URL::asset('assets/cust/js/posts/posts-form.js')}}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<!-- <script type="text/javascript">
    
	$('.simpan').on('click',function(){
       
		$('form').submit();
	});
</script> -->


@endsection