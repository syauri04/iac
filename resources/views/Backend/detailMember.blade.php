@extends('layouts/main')
@section('content')
<!-- begin row -->
<div class="row">
		<!-- begin col-6 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Detail Member</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
				<form enctype="multipart/form-data" id="myForm" action="" method="POST" data-toggle="validator">
                     {{ csrf_field() }}
                       
                	   
                		
					 	<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Picture</label>
							<div class="col-md-9">
								<label class="col-form-label col-md-2"><img src="{{ default_img($val['data']->profile_pic,'img/user_default.png','storage/img/member') }}" width="50"></label>
								<!-- <input type="text" name="title" required class="form-control m-b-5" autocomplete="off" value="{{ $val->title ?? '' }}" /> -->
						</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Nama Lengkap</label>
							<div class="col-md-9">
								<label class="col-form-label col-md-2">{{ $val['data']->nama_lengkap ?? '' }}</label>
								</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Email</label>
							<div class="col-md-9">
								<label class="col-form-label col-md-2">{{ $val['data']->email ?? '' }}</label>
								</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Phone</label>
							<div class="col-md-9">
								<label class="col-form-label col-md-2">{{ $val['data']->telp ?? '' }}</label>
								</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Community List</label>
							<!-- <div class="col-md-9">
								<label class="col-form-label col-md-2">{{ $val['data']->telp ?? '' }}</label>
								</div> -->
						</div>
						<table id="data-table-default" class="table table-striped ">
							<thead>
								<tr>
									<th>#</th>
									<th>Community</th>
									<th>Alamat</th>
									<th>Level</th>
									<th>Status </th>
									<th>Created at</th>
								</tr>
							</thead>
							<tbody>
							@if (count($val['community']) > 0)
								@foreach ($val['community'] as $d)
							
								<tr>
									<td>{{$loop->iteration}}</td>
									<td>{{$d->name}}</td>
									<td>{{$d->alamat_detail}}</td>
									<td>{{$d->level}}</td>
									<td>{{$d->status}}</td>
									<td>{{$d->created_at}}</td>
									
								</tr>
								@endforeach
							@endif 
							
							</tbody>
						</table>

						<div class="form-group row m-b-15">
							<!-- <label class="col-form-label col-md-2"></label> -->
							<div class="col-md-9">
								<a href="{{url()->previous()}}" class="btn btn-white cancel">Back</a>
								<!-- <button type="submit" class="btn btn-primary simpan">Simpan</button> -->
							</div>
						</div>
					</form>
                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- Edn col-6 -->
	</div>
<!-- End Row -->


<link href="{{ URL::asset('assets/def/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/def/plugins/chosen/css/chosen.min.css')}}" rel="stylesheet" />

<script src="{{ URL::asset('assets/def/plugins/chosen/js/chosen.jquery.min.js')}}"></script>
<script src="{{ URL::asset('assets/def/plugins/ckeditor/ckeditor.js')}}"></script>
<script src="{{ URL::asset('assets/def/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script src="{{ URL::asset('assets/cust/js/posts/posts-form.js')}}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<!-- <script type="text/javascript">
    
	$('.simpan').on('click',function(){
       
		$('form').submit();
	});
</script> -->


@endsection