@extends('layouts/main')
@section('content')
<div class="panel panel-inverse" data-sortable-id="table-basic-1">
	<!-- begin panel-heading -->
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">List Event</h4>
    </div>
    <!-- end panel-heading -->
    <!-- begin panel-body -->
    <div class="panel-body">
    	<!-- begin table-responsive -->
		<table id="data-table-default" class="table table-striped ">
			<thead>
				<tr>
					<th>No</th>
					<th>Topik</th>
					<th>Jawaban A</th>
					<th>Jawaban B</th>
					<th>Jawaban C</th>
					<th>Jawaban D</th>
					<th>Jawaban E</th>
					<th>Count A</th>
					<th>Count B</th>
					<th>Count C</th>
					<th>Count D</th>
					<th>Count E</th>
					<th>Community</th>
					
					<th>Create At</th>
					
				</tr>
			</thead>
			<tbody>
			
            @if (count($data) > 0)
                @foreach ($data as $d)
				
                <tr>
					<td>{{$loop->iteration}}</td>
					<td>{{$d->topik}}</td>
					<td>{{ $d->jawaban_a }}</td>
					<td>{{ $d->jawaban_b }}</td>
					<td>{{ $d->jawaban_c }}</td>
					<td>{{ $d->jawaban_d }}</td>
					<td>{{ $d->jawaban_e }}</td>
					<td>{{ get_count_polling($d->id,'a') }}</td>
					<td>{{ get_count_polling($d->id,'b') }}</td>
					<td>{{ get_count_polling($d->id,'c') }}</td>
					<td>{{ get_count_polling($d->id,'d') }}</td>
					<td>{{ get_count_polling($d->id,'e') }}</td>
					<td>{{$d->community_name}}</td>
					<td>{{$d->created_at}}</td>
                    
				</tr>
                @endforeach
            @endif 
			
			</tbody>
		</table>
		<!-- end table-responsive -->
    </div>
    <!-- end panel-body -->
</div>

<link href="{{ URL::asset('assets/def/plugins/DataTables/media/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/def/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css')}}" rel="stylesheet" />

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{ URL::asset('assets/def/plugins/DataTables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{ URL::asset('assets/def/plugins/DataTables/media/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('assets/def/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ URL::asset('assets/def/js/demo/table-manage-default.demo.min.js')}}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script type="text/javascript">
	TableManageDefault.init();
</script>
@endsection