@extends('layouts/main')
@section('content')
<!-- begin row -->
<div class="row">
		<!-- begin col-6 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Form</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
              
                    <form enctype="multipart/form-data" id="myForm" action="/community/update/{{$val->id ?? ''}}" method="POST" data-toggle="validator">
                    @method('PUT')
               
                     {{ csrf_field() }}
					 	
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Community</label>
							<div class="col-md-9">
							    <!-- <textarea name="summary" required class="form-control m-b-5">{{ $val->summary ?? '' }}</textarea> -->
								<input type="text" name="name" required class="form-control m-b-5" autocomplete="off" value="{{ $val->name ?? '' }}" />
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Category</label>
							<div class="col-md-9">
							<select id="cars" class="form-control m-b-5" name="category">
								<option value=""></option>
								<option value="mobil" <?=($val->category == "mobil")?"selected":"";?>>mobil</option>
								<option value="motor" <?=($val->category == "motor")?"selected":"";?>>motor</option>
							
							</select>
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Alamat</label>
							<div class="col-md-9">
							    <!-- <textarea name="summary" required class="form-control m-b-5">{{ $val->summary ?? '' }}</textarea> -->
								<input type="text" name="alamat_detail" required class="form-control m-b-5" autocomplete="off" value="{{ $val->alamat_detail ?? '' }}" />
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Bank Name</label>
							<div class="col-md-9">
							    <!-- <textarea name="summary" required class="form-control m-b-5">{{ $val->summary ?? '' }}</textarea> -->
								<input type="text" name="bank_name" required class="form-control m-b-5" autocomplete="off" value="{{ $val->bank_name ?? '' }}" />
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Norek</label>
							<div class="col-md-9">
							    <!-- <textarea name="summary" required class="form-control m-b-5">{{ $val->summary ?? '' }}</textarea> -->
								<input type="text" name="noreq"  class="form-control m-b-5" autocomplete="off" value="{{ $val->noreq ?? '' }}" />
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Atas Nama</label>
							<div class="col-md-9">
							    <!-- <textarea name="summary" required class="form-control m-b-5">{{ $val->summary ?? '' }}</textarea> -->
								<input type="text" name="atas_nama"  class="form-control m-b-5" autocomplete="off" value="{{ $val->atas_nama ?? '' }}" />
							</div>
						</div>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Status</label>
							<div class="col-md-3">
							<select id="cars" class="form-control m-b-5" name="status">
								<option value="">-- Change Status --</option>
								<option value="banned" <?=($val->activate == "banned")?"selected":"";?>>Banned</option>
								<option value="active" <?=($val->activate == "active")?"selected":"";?>>Active</option>
							
							</select>
								
							</div>
						</div>
					
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2"></label>
							<div class="col-md-9">
								<a href="{{url()->previous()}}" class="btn btn-white cancel">Cancel</a>
								<button type="submit" class="btn btn-primary simpan">Simpan</button>
							</div>
						</div>
					</form>
                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- Edn col-6 -->
	</div>
<!-- End Row -->


<link href="{{ URL::asset('assets/def/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/def/plugins/chosen/css/chosen.min.css')}}" rel="stylesheet" />

<script src="{{ URL::asset('assets/def/plugins/chosen/js/chosen.jquery.min.js')}}"></script>
<script src="{{ URL::asset('assets/def/plugins/ckeditor/ckeditor.js')}}"></script>
<script src="{{ URL::asset('assets/def/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script src="{{ URL::asset('assets/cust/js/posts/posts-form.js')}}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<!-- <script type="text/javascript">
    
	$('.simpan').on('click',function(){
       
		$('form').submit();
	});
</script> -->


@endsection