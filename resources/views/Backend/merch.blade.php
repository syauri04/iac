@extends('layouts/main')
@section('content')
<div class="panel panel-inverse" data-sortable-id="table-basic-1">
	<!-- begin panel-heading -->
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">List Merchandise</h4>
    </div>
    <!-- end panel-heading -->
    <!-- begin panel-body -->
    <div class="panel-body">
    	<!-- begin table-responsive -->
		<table id="data-table-default" class="table table-striped ">
			<thead>
				<tr>
					<th>#</th>
					<th>Community</th>
					<th>Title</th>
					<th>Image</th>
					<th>Harga</th>
					<th>Weight</th>
					<th>Description</th>
					<th>Dilaporkan</th>
					<th>Status</th>
					<th>Create At</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			@if (count($data) > 0)

                @foreach ($data as $d)

                <tr>
					<td>{{$loop->iteration}}</td>
					<td>{{$d->community_name}}</td>
					<td>{{$d->title}}</td>
					<td><img src="{{ asset('storage/img/merchandise/'.$d->community_id.'/'.$d->image) }}" width="50"></td>
					<td>{{$d->harga}}</td>
					<td>{{$d->weight}}</td>
					<td>{{$d->description}}</td>
					<td>
						@if ($d->count == 0)
							<span >No Disclaimer</span>
						@else
						<a type="button" href="{{route('disclaim_merch',['id' => $d->id])}}"  data-id="{{$d->id}}" style="padding: 5px 8px;font-size: 12px;" class="btn btn-danger btn-lg" >
							Disclaimer
						</a>
						
						@endif
					</td>
					<td>{{$d->stat_banned}}</td>
					<td>{{$d->created_at}}</td>
                    <td align="center">
                    
                            {{ LinkActions($links_table_item,$d->id) }}
                      </td>
				</tr>
                @endforeach
			@endif
           
			
			</tbody>
		</table>

		
		<!-- end table-responsive -->
    </div>
    <!-- end panel-body -->
</div>

<link href="{{ URL::asset('assets/def/plugins/DataTables/media/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/def/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css')}}" rel="stylesheet" />

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{ URL::asset('assets/def/plugins/DataTables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{ URL::asset('assets/def/plugins/DataTables/media/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('assets/def/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ URL::asset('assets/def/js/demo/table-manage-default.demo.min.js')}}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<script type="text/javascript">
	TableManageDefault.init();
</script>
@endsection